<?php
return [

    /*site*/
    '/' => 'site/index',
    '/login' => 'site/login',
    '/logout' => 'site/logout',
    '/settings' => 'site/settings',

	/*in progress pages*/
	'/progress' => 'site/progress',
	'/customers' =>  'customer/index',
	'/customer/view/<id>' =>  'customer/view',
	'/customer/update/<id>' => 'customer/update',

    /*category*/
    '/category' => 'category/index',
    '/category/create' => 'category/create',
    '/category/filter' => 'category/filter',
    '/category/update/<id>' => 'category/update',
    '/category/delete/<id>' => 'category/delete',

     /*product*/
    '/product' => 'product/index',
    '/product/create' => 'product/create',
    '/product/update/<id>' => 'product/update',
    '/product/delete/<id>' => 'product/delete',

     /*brand*/
    '/brand' => 'brand/index',
    '/brand/create' => 'brand/create',
    '/brand/update/<id>' => 'brand/update',
    '/brand/delete/<id>' => 'brand/delete',

    /*content pages*/
    '/content-pages' => 'content-pages/index',
    '/content-pages/create' => 'content-pages/create',
    '/content-pages/update/<id>' => 'content-pages/update',
    '/content-pages/delete/<id>' => 'content-pages/delete',

    '/content-pages/upload-imperavi' => 'content-pages/upload-imperavi',

	/*orders*/
	'/orders' => 'order/index',
	'/order' => 'order/index',
	'/order/delete/<id>' => 'order/delete',
	'/order/update/<id>' => 'order/update',
	'/order/status/<id>' => 'order/status',

	/* stats */
	'/statistic/orders' => 'statistic/orders',
	'/statistic/watched' => 'statistic/watched',
	'/statistic/lists' => 'statistic/lists',

	'/discounts' => 'discount/index',
	'/discount/create' => 'discount/create',
	'/discount/update/<id>' => 'discount/update',
	'/discount/delete/<id>' => 'discount/delete',

	/*cabinet*/
	'/cabinet' => 'cabinet/index',


	/*blog*/
	'/blog' => 'blog/index',
	'/blog/update/<id>' => 'blog/update',

    /*import*/
    '/import' => 'import/index',
    '/import-insert' => 'import/import-insert',
    '/import-execute' => 'import/import-execute',


    /*properties*/
    ['class' => 'yii\rest\UrlRule', 'pluralize' => false, 'controller' => 'property'],
    ['class' => 'yii\rest\UrlRule', 'pluralize' => false, 'controller' => 'value'],
	['class' => 'yii\rest\UrlRule', 'pluralize' => false, 'controller' => 'order-item'],
	['class' => 'yii\rest\UrlRule', 'pluralize' => false, 'controller' => 'api/product-property'],
	['class' => 'yii\rest\UrlRule', 'pluralize' => false, 'controller' => 'api/settings'],
	['class' => 'yii\rest\UrlRule', 'pluralize' => false, 'controller' => 'api/preorder-item'],
	['class' => 'yii\rest\UrlRule', 'pluralize' => false, 'controller' => 'api/brand'],
	['class' => 'yii\rest\UrlRule', 'pluralize' => false, 'controller' => 'api/order'],
	['class' => 'yii\rest\UrlRule', 'pluralize' => false, 'controller' => 'api/discount'],
	['class' => 'yii\rest\UrlRule', 'pluralize' => false, 'controller' => 'api/order-item-discount'],
	['class' => 'yii\rest\UrlRule', 'pluralize' => false, 'controller' => 'api/category'],
	['class' => 'yii\rest\UrlRule', 'pluralize' => false, 'controller' => 'api/product'],
	['class' => 'yii\rest\UrlRule', 'pluralize' => false, 'controller' => 'api/blog'],

];