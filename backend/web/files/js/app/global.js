/**
 * Small, but site-wide pieces of functionality
 */
(function($, app) {

	/**
	 * jQuery extension that allows to get HTML *including* the element's tag (as opposed to $.html())
	 *
	 * @returns {string}  Element's HTML including the element's tag
	 */
	$.fn.outerHtml = function() {
		return $('<div/>').append(this).html();
	};

	/**
	 * Triggered after all scripts are loaded and the document is rendered.
	 */
	$(document).ready(function() {
		/**
		 * Global on-click event that allows to attach the destination URL to any HTML element by setting data-href
		 * attribute.
		 */
		$('body').on('click', '[data-href]', function() {
			document.location = $(this).data('href');
		});

		toastr.options = {
			"closeButton": true,
			"debug": false,
			"newestOnTop": true,
			"progressBar": false,
			"positionClass": "toast-top-right",
			"preventDuplicates": false,
			"onclick": null,
			"showDuration": "300",
			"hideDuration": "1000",
			"timeOut": "5000",
			"extendedTimeOut": "1000",
			"showEasing": "swing",
			"hideEasing": "linear",
			"showMethod": "fadeIn",
			"hideMethod": "fadeOut"
		};

        Dropzone.autoDiscover = false;
        app.config.dzConfig = {


        };
        /*dropzone*/



		/**
		 * Once the application is loaded we trigger post-render event to process page HTML same way we process HTML
		 * after rendering a template
		 */
		$(app).trigger('post-render');
		this.notifications = app.data.notifications;
		if (this.notifications) {
			toastr[this.notifications.status](this.notifications.message);
		}

	});

	/**
	 * AJAX call returns 401 when the session is expired and the user is logged out.
	 */
	$(document).ajaxError(function(e, xhr) {
		if (xhr.status == 401) {
			document.location = app.config.baseUrl + 'login?expired=1';
		}
	});

	/**
	 * This code processes all HTML (the whole body after the page is loaded or just the new HTML after the template
	 * has been rendered).
	 */
	$(app).on('post-render', function(ev, $target) {
		$target = $target || $('body');

		/* Automatically create views based on element's data-view-type attribute */
		$target.find('[data-view-type]').each(function(idx, element) {
			var viewClassName = $(element).data('view-type');
			var viewClass = app.views;
			$.each(viewClassName.split('.'), function(idx, name) {
				viewClass = viewClass[name];
			});
			if (!viewClass) {
				throw 'Unknown class ' + viewClassName;
			}
			var view = new viewClass({el: element});
			$(element).data('view', view);
		});

		/* Displays or hides elements based on the current user's permissions */
		$target.find('[data-permission]').each(function(idx, el) {
			if (!app.data.permissions[$(el).data('permission')]) {
				$(el).addClass('hide');
			}
			else {
				$(el).removeClass('hide');
			}
		});

	});
	
})(jQuery, app);
