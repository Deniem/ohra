(function($, app) {

	$.extend(app.views, {
		product: {
			List: Backbone.View.extend({

				events: {
					'change #product-status-filter': 'filterByStatus'
				},

				initialize: function(){

				},

				filterByStatus: function(ev) {
					$(ev.target).closest('form').submit();
				}

			}),

			Update: Backbone.View.extend({

				events: {

				},

				initialize: function(){
					console.log('update prod page');
				}

			}),

			Form: Backbone.View.extend({

				events: {
					'change .status-checkbox':'checkStatus',
					'change .top-checkbox' : 'topCheckStatus',
					'change .product-price' : 'updatePrice'
				},

				checkStatus: function(ev){
					var status = $(ev.target).prop('checked') ? '1' : '0';
					$(ev.target).parent('label').find('.status-input').val(status);
				},

				topCheckStatus: function(ev){
					var status = $(ev.target).prop('checked') ? '1' : '0';
					console.log(status);
					$(ev.target).parent('label').find('.top-input').val(status);
					console.log($(ev.target).parent('label').find('.top-input').val());
				},

				updatePrice: function(){
					app.event_bus.trigger('updateProdPrice', $('.product-price').val());
				},

				initialize: function(){

				}

			}),
			
			Discount: Backbone.View.extend({
				events: {
					'click .btn-add-discount' : 'addDiscount',
					'click .btn-discount-submit' : 'applyDiscount',
					'click .btn-discount-delete' : 'deleteDiscount',
					'change .discount-status-checkbox':'checkStatus',
					'change #discount-value' : 'updateDiscount',
					'keyup #discount-value' : 'updateDiscount',
					'change #discount-type' : 'updateDiscount',
					'blur .discount-date' : 'updateDate'
				},
				
				initialize: function() {
					this.discount =
						app.data.discount !== null ?
							new app.models.Discount(app.data.discount, {parse:true}) :
							app.data.discount;
					this.product = new app.models.Product(app.data.product, {parse: true});
					if(this.discount !== null){
						this.discount.set('origin-price', this.product.get('price'));
					}
					this.template = this.createTemplate('#product-discount-template');
					this.listenTo(app.event_bus, 'updateProdPrice', this.updateProdPrice);
					this.render();
				},
				
				render: function () {
					this.template('.product-discount-placeholder', {
						discount: this.discount,
						product: this.product
					});

					//init checkbox status
					var status = $('.discount-status-checkbox').prop('checked') ? 'enabled' : 'disabled';
					$('.discount-status-input').val(status);

					//init DatePickers for discount
					this.disTimeStartId = $('#disTimeStart').val();
					this.disTimeEndId = $('#disTimeEnd').val();

					// init datetimepickers status
					if(this.disTimeEndId !== undefined
					&& this.disTimeStartId !== undefined){
						$('#' + this.disTimeStartId).datetimepicker(app.data.discountDateTime);
						$('#' + this.disTimeEndId).datetimepicker(app.data.discountDateTime);

						this.discount.get('start_date')
						? $('#' + this.disTimeStartId)
								.data("DateTimePicker")
								.date(this.discount.get('start_date'))
						: '' ;

						this.discount.get('end_date')
						? $('#' + this.disTimeEndId)
							.data("DateTimePicker")
							.date(this.discount.get('end_date'))
						: '' ;
					}
				},

				addDiscount: function(){
					this.discount = new app.models.Discount({
						'prod_id': this.product.get('id')
					});
					this.product.get('price')
						? this.discount.set('origin-price', this.product.get('price'))
						: app.event_bus.trigger('updateProdPrice', $('.product-price').val());
					this.render();
				},

				updateProdPrice: function(priceValue){
					$('#discount-origin-price').val(priceValue);
					this.product.set('price', priceValue);
					this.updateDiscount();
				},

				checkStatus: function(){
					var status = $('.discount-status-checkbox').prop('checked') ? 'enabled' : 'disabled';
					$('.discount-status-input').val(status);
					this.grabData();
					this.render();
				},

				applyDiscount: function(){
					this.grabData();
					this.discount.save({},
					{
						success: function () {
							app.helpers.notifications.show('success', '<b>Акция успешно применена</b>');
						}
					});
				},

				deleteDiscount: function(){
					this.discount.destroy();
					this.discount = null;
					this.render();
				},

				updateDiscount: function(){
					var self = this;
					setTimeout(function(){
						self.grabData();
						if(self.discount.get('type') == 'percentage'){
							var percent = self.product.get('price') / 100 * self.discount.get('value');
							self.discount.set('price',
								self.discount.get('origin-price')
								- percent);
						}else if(self.discount.get('type') == 'sum'){
							self.discount.set('price',
								self.product.get('price')
								- self.discount.get('value'))
						}

						self.render();
					}, 1500);
				},

				updateDate: function(){
					this.grabData();
					this.render();
				},

				grabData: function(){
					var data = app.helpers.collectForm('#discount-form');
					data['status'] = $('#stat-val').val();
					this.discount.set(data);
				}
			})
		}
	})



})(jQuery, app);

