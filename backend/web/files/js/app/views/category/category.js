(function($, app) {

	$.extend(app.views, {
		category: {
			List: Backbone.View.extend({

				events: {

				},

				initialize: function(){
					console.log('list cat page');
				}

			}),

			Update: Backbone.View.extend({

				events: {

				},

				initialize: function(){
					console.log('update cat page');
				}

			}),

			FilterList: Backbone.View.extend({

				events: {
					'click .btn-switch-filter': 'switchFilter',
					'click .collapse-category': 'switchCollapse'
				},

				initialize: function(){
					this.template = this.createTemplate('#property-list-template');
					this.categories = app.data.categories;
					$.each(this.categories, function(idx, cat){
						cat.collapsed = 1;
					});
					console.log(this.categories);
					this.render();
				},

				render: function(){
					this.template('.property-list-placeholder', {
						categories: this.categories
					})
				},

				switchFilter: function(ev) {
					var data = $(ev.target).closest('tr').data();
					var propData = this.categories[data.catId].properties[data.propId];
					propData.is_filter = propData.is_filter ? 0 : 1;
					var model = new app.models.Property(propData, {parse: true});
					model.save(null,{
						success: $.proxy(function(model){
							this.categories[data.catId].properties[data.propId] = model.toJSON();
							var is_enabled = model.get('is_filter') ? 'включен' : 'выключен';
							app.helpers.notifications.show('success', 'Фильтр успешно ' + is_enabled);
							this.render();
						}, this),
						error: function(){
							app.helpers.notifications.show('error', 'Произошла ошибка, попробуйте позже!');
						}
					});
				},

				switchCollapse: function(ev){
					var data = $(ev.target).closest('.panel').data();
					this.categories[data.catId].collapsed = data.collapsed ? 0 : 1;
				}

			})
		}
	})



})(jQuery, app);

