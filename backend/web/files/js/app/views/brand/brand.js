(function($, app) {

    $.extend(app.views, {
        brand: {
            MainList: Backbone.View.extend({

                events: {
                    'click .brand-delete-action': 'deleteAction'
                },
                
                deleteAction: function(ev){
                    var id = $(ev.target).closest('td').attr('data-id');
                    app.modal('brand-delete', new app.models.Brand({id: id}));
                }
            }),

            BrandItem: Backbone.View.extend({

                events: {
                    'change .status-checkbox':'checkStatus'
                },

                checkStatus: function(ev){
                    var status = $(ev.target).prop('checked') ? '1' : '0';
                    $(ev.target).parent('label').find('.status-input').val(status);
                },

                initialize: function(){
                },

                render: function(){

                }
            })
        }
    })
    
})(jQuery, app);

