(function($, app) {

	$.extend(true,
		app.views, {
			settings: {
				List: Backbone.View.extend({

					events: {
						'click .btn-save': 'updateSettings',
						'keyup .settings-input':function(ev){
							var target = $(ev.target);
							var targetValue = $.trim(target.val());
							var model = this.settingsCollection.find({id:target.data('id')}).toJSON();
							if (model && targetValue != '' && model.value != targetValue) {
								$(ev.target).closest('div.form-group').find('.btn-save').removeAttr('disabled');
							}
						},
						'click a[data-rel="reload"]': function(ev){
							this.settingsCollection.fetch();
						}
					},

					initialize: function () {
						this.settingsCollection = new app.models.SettingsCollection();
						this.options = {
							data: {}
						};
						this.settingsCollection.fetch(this.options);
						this.template = this.createTemplate('#settings-list-template');
						this.listenTo(this.settingsCollection, 'sync', this.render);
					},

					render: function () {
						this.sortedCollection = _.groupBy(this.settingsCollection.toJSON(), 'type');
						this.template('.settings-list-placeholder', {
							settings: this.sortedCollection
						});
					},

					updateSettings: function(ev){
						$(ev.target).button('loading');
						var input = $(ev.target).closest('div.form-group').find('.settings-input');
						var data = input.data();
						var model = this.settingsCollection.find({id:data.id});
						model.set('value', $.trim(_.escape(input.val())));
						model.save(null, {
							success: $.proxy(function(model){
								$(ev.target).button('reset');
								console.log(model);
								this.settingsCollection.add(model,{merge: true});
								app.helpers.notifications.show('success','Поле ' + model.get('label') + ' успешно сохранено.');
							}, this),
							error: function(response){
								console.log(response);
								$(ev.target).button('reset');
								app.helpers.notifications.show('error','Не удалось сохранить. Попробуйте позже!');
							}
						})

					}

			})
		}
		})

})(jQuery, app);

