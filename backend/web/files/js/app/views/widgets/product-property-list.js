(function($, app) {

	$.extend(true,
		app.views, {
			product:{
				propertyList: Backbone.View.extend({

					events: {
						'change .prop-input': 'updateValue',
						'click .btn-add': 'addValue'
					},

					initialize: function(){
						this.template = this.createTemplate('#property-list-template');
						this.product = new app.models.Product(app.data.product, {parse:true});
						this.properties = this.product.get('category') ?
							this.product.get('category').properties : '';
						this.productPropertiesCollection = new app.models.ProductPropertiesCollection();
						this.options = {data:{
							idProduct: this.product.get('id')
						}};
						this.productPropertiesCollection.fetch(this.options);
						this.listenTo(this.productPropertiesCollection, 'sync' ,this.render);
						this.listenTo(this.product, 'sync' ,this.render);
					},

					render: function(){
						this.template('.property-list-placeholder', {
							properties: this.properties,
							productProperty: this.productPropertiesCollection
						});
					},

					addValue: function(ev){
						var data = $(ev.target).closest('tr').data();
						var prop = this.properties[data.key];
						this.oldValue = $(ev.target).closest('tr').find('select').length ? $(ev.target).closest('tr').find('select').val() : 0;
						var callback = $.proxy(this.newProductValue, this);
						app.modal('add-property-value', prop, callback);
					},

					newProductValue: function(value){
						var value = value;

						var ProductProperty = new app.models.ProductProperty({
							id_product: this.product.get('id'),
							id_value: value.get('id')
						}, {parse: true});

						if (this.oldValue != 0) {
							ProductProperty.set('id', this.productPropertiesCollection.findWhere({id_value:parseInt(this.oldValue)}).get('id'));
						}

						ProductProperty.save(null, {
							success: $.proxy(function(model){
								window.location.reload();
							}, this),
							error: function(response){
								console.log('error' + console.log(response));
							}
						})

					},

					updateValue: function(ev){
						var data = {};
						$.map($(ev.target).closest('tr').find('select').serializeArray(), function(item, idx){
							data[item.name] = item.value;
						});
						data['id_product'] = this.product.get('id');
						data['id_property'] = $(ev.target).closest('tr').data('id');
						var productPropertyModel = new app.models.ProductProperty(data, {parse:true});
						productPropertyModel .save(null, {
							success: $.proxy(function(model){
								this.productPropertiesCollection.fetch(this.options);
							}, this),
							error: function(response){
								/*errors*/
								console.log(response);
							}
						});

					}

				})
			}
		})

})(jQuery, app);

