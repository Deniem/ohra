(function($, app) {

	$.extend(true,
		app.views, {
			category:{
				propertyList: Backbone.View.extend({

					events: {
						'click .btn-delete': 'deleteProperty',
						'click .btn-update': 'updateProperty',
						'click .btn-save': 'addProperty',
						'change .prop-input': function(ev){
							$(ev.target).closest('tr').find('.btn-update').prop('disabled', false);
						}
					},

					initialize: function(){
						this.propCollection = new app.models.PropertiesCollection();
						this.options = {data:{id_category:app.data.category.id}};
						this.template = this.createTemplate('#property-list-template');
						this.propCollection.on('remove', this.render, this);
						this.propCollection.on('add', this.render, this);
						this.propCollection.fetch(this.options);
						this.listenTo(this.propCollection,'sync', this.render);
					},

					render: function(){
						this.template('.property-list-placeholder', {
							properties: this.propCollection.toJSON()
						});
					},


					addProperty: function(ev){
						ev.preventDefault();
						$(ev.target).button('loading');
						var data = app.helpers.collectForm('#add-property-form');
						var model = new app.models.Property(data,{parse:true});
						model.save(null, {
							success: $.proxy(function(model){
								this.propCollection.add(model);
								app.helpers.clearForm('#add-property-form');
								app.helpers.notifications.show('success', 'Характеристика добавлена успешно!');
								$(ev.target).button('reset');
							}, this),
							error: function(response){
								/*show error here*/
								console.log(response);
								app.helpers.notifications.show('error', 'Возникла ошибка. <br> Попробуйте позже!');
								console.log(response);
								$(ev.target).button('reset');
							}
						});

					},

					deleteProperty: function(ev){
						var data = $(ev.target).closest('tr').data();
						var prop  = this.propCollection.get(data.id);
						$(ev.target).button('loading');
						prop.destroy({
							success: $.proxy(function(model){
								this.propCollection.remove(model);
								app.helpers.notifications.show('success', 'Характеристика удалена успешно!');
								$(ev.target).button('reset');
							}, this),
							error: function(response){
								/*show error here*/
								app.helpers.notifications.show('error', 'Возникла ошибка. <br> Попробуйте позже!');
								console.log(response);
								$(ev.target).button('reset');
							}
						});
					},

					updateProperty: function(ev){
						$(ev.target).button('loading');
						var model = this.propCollection.get($(ev.target).closest('tr').data('id'));
						var row = $(ev.target).closest('tr');
						$.each(row.find('input, select').serializeArray(), function(idx, item){
							model.set(item.name, item.value);
						});

						//get checkboxes values
						if (row.find('input[type="checkbox"]').length > 0) {
							$.each(row.find('input[type="checkbox"]'), function(idx, item){
								model.set($(item).attr('name'), $(item).prop('checked') ? 1 : 0);
							});
						}

						model.save(null, {
							success: $.proxy(function(model){
								$(ev.target).prop('disabled', true);
								app.helpers.notifications.show('success', 'Характеристика обновлена успешно!');
								$(ev.target).button('reset');
							}, this),
							error: function(response){
								/*errors*/
								console.log(response);
								app.helpers.notifications.show('error', 'Возникла ошибка. <br> Попробуйте позже!');
								$(ev.target).button('reset');
							}
						});


					}

				})
			}
		})

})(jQuery, app);

