(function ($, app) {
    $.extend(true,
        app.views, {
            customer: {
                customers_list: Backbone.View.extend({
                    events:{
                        'change .customer-status-filter' : 'changeStatus'
                    },

                    initialize: function (){
                        this.urlParams = app.helpers.parseURLParams();
                        this.url = app.config.baseUrl + 'customers';
                        this.template = this.createTemplate('#customers-status-filter-template');
                        this.render();
                    },

                    render: function () {
                        this.template('.customers-status-filter-placeholder');
                        this.setSelected();
                    },

                    changeStatus: function(ev){
                        var sel = $(ev.target).find(':selected').val();
                        app.helpers.gotoURL(this.url, {'customer-status-filter': sel});
                    },

                    setSelected: function () {
                        var self = this;
                        $('.customer-status-filter > option').each(function(){
                            if(self.urlParams.hasOwnProperty('customer-status-filter')){
                                if($(this).val() == self.urlParams['customer-status-filter']){
                                    $(this).attr('selected', 'selected');
                                }
                            }
                        });
                    }
                }),
                
                customer:  Backbone.View.extend({
                    
                    events: {
                        'change .customer-account-filter' : 'updateCustomerStatus',
                    },
                    
                    initialize: function (){
                        this.url = app.config.baseUrl + 'customers';
                        this.model = new app.models.Customer(app.data.customer);
                        this.customerStatus = app.data.customerStatus;
                        this.template = this.createTemplate('#customer-params-template');
                        this.render();
                    },

                    render: function () {
                        this.template('.customer-params-placeholder', {
                            customer: this.model.toJSON(),
                            customerStatus: this.customerStatus
                        });
                    },

                    updateCustomerStatus: function () {
                        var val = $('.customer-account-filter').find(':selected').val();
                        this.model.set('status', val);
                        this.model.save();
                    }
                }),

                customerNavTabs: Backbone.View.extend({
                    initialize: function(){
                        this.compareCount = app.data.customerCompare.length;
                        this.favoriteCount = app.data.customerFavorite.length;
                        this.template = this.createTemplate('#customer-nav-tab-template');
                        this.render();
                    },

                    render: function(){
                        this.template('.customer-nav-tab-placeholder', {
                            compareCount: this.compareCount,
                            favoriteCount: this.favoriteCount
                        });
                    }
                }),

                customerFavorite:  Backbone.View.extend({

                    initialize: function(){
                        this.categoryCollection = new app.models.CategoryCollection(app.data.categories);
                        this.favoriteCollection = new app.models.ProductCollection(app.data.customerFavorite);
                        this.template = this.createTemplate('#customer-favorite-template');
                        this.render();
                    },

                    render: function(){
                        this.template('.customer-favorite-placeholder', {
                            categoryCollection: this.categoryCollection,
                            itemsCollection: this.favoriteCollection
                        });
                    }
                }),

                customerCompare:  Backbone.View.extend({
                    initialize: function(){
                        this.compareCollection = new app.models.ProductCollection(app.data.customerCompare);
                        this.categoryCollection = new app.models.CategoryCollection(app.data.categories);

                        this.template = this.createTemplate('#customer-compare-template');
                        this.render();
                    },

                    render: function(){
                        this.template('.customer-compare-placeholder', {
                            itemsCollection: this.compareCollection,
                            categoryCollection: this.categoryCollection
                        });
                    }
                }),

                customerPreorder: Backbone.View.extend({

                    events: {
                        'click .show-preopred-items' : 'showPreorderItems',
                        'click .page-item' : 'loadPage'
                    },

                    initialize: function(){
                        this.template = this.createTemplate('#customer-preorder-template');
                        this.pageSize = 5;
                        this.pageCount = 1;
                        this.currPage = 1;
                        this.preorderCollection = new app.models.PreorderCollection(app.data.preorders);
                        this.pageCollection =  [];

                        if(this.preorderCollection.length > this.pageSize){
                            this.pageCollection = new app.models.PreorderCollection(this.preorderCollection.slice(0, this.pageSize));
                            this.pageCount = Math.floor(this.preorderCollection.length / this.pageSize ) +1 ;
                        }else{
                            this.pageCollection = this.preorderCollection.clone();
                        }
                        this.render();
                    },

                    loadPage: function(ev){
                        var nextPage = $(ev.target).attr('data-page');

                        if(nextPage == 1){
                            this.pageCollection.reset();
                            this.pageCollection = new app.models.PreorderCollection(
                                this.preorderCollection.slice(0, this.pageSize));
                        }else{
                            this.pageCollection.reset();
                            this.pageCollection = new app.models.PreorderCollection(
                                this.preorderCollection.slice(this.pageSize * (parseInt(nextPage) -1 ),
                                    this.pageSize * (parseInt(nextPage) )) );
                        }

                        this.currPage = nextPage;
                        this.render();
                    },

                    render: function(){
                        this.template('.customer-preorder-placeholder', {
                            preorderCollection: this.pageCollection,
                            currentPage: this.currPage,
                            pageCount: this.pageCount,
                            pageSize: this.pageSize
                        });
                    },

                    showPreorderItems: function (ev) {
                        var id = $(ev.target).closest('tr').attr('data-id');
                        app.event_bus.trigger('draw-preorder-items',
                            this.preorderCollection.get(id)
                        );
                    }
                }),

                PreorderItems: Backbone.View.extend({
                    
                    events: {
                        'click .page-item' : 'loadPage'    
                    },
                    
                    initialize: function(){
                        this.listenTo(app.event_bus, 'draw-preorder-items', this.doRender);
                        this.template = this.createTemplate('#customer-preorder-items-template');
                        this.pagination = [];
                        this.model = {};
                        this.itemsCollection = [];
                        this.currentPage = 1;
                    },
                    
                    loadPage: function(ev){
                        var page = $(ev.target).attr('data-page');
                        var perPage = $(ev.target).attr('data-per-page');
                        var params = '&' + this.pagination['pageParam'] + '=' + page
                            + '&' + this.pagination['pageSizeParam'] + '=' + perPage;

                        this.currentPage = page;
                        this.doRender(this.model, params);
                    },

                    doRender: function(model, params){
                        var self = this;
                        $.when(model.getPreorderItems(params))
                            .done(function (resp) {
                                self.itemsCollection = new app.models.PreorderItemCollection(resp['items']);
                                self.model = model;
                                self.pagination  = resp['pagination'];

                                var pageCount = Math.floor(self.pagination['totalCount']/self.pagination['defaultPageSize'] + 1);

                                self.template('.customer-preorder-items-placeholder', {
                                    itemsCollection: self.itemsCollection,
                                    pagination: self.pagination,
                                    pageCount: pageCount,
                                    currentPage: self.currentPage
                                });
                            });
                    }
                }),

                customer_order_filter: Backbone.View.extend({

                    events:{
                        'click .customer-order' :'orderHandler',
                        'change .order-status-filter': 'applyOrderStatusFilter'
                    },

                    initialize: function (){
                        this.collection = new app.models.OrdersCollection(app.data.orders);
                        this.defColl = this.collection.clone();
                        this.orderOptions = app.data.orderOptions;
                        this.selected = 'all';
                        this.template = this.createTemplate('#customer-order-filter-template');
                        this.render();
                    },

                    render: function () {
                        this.template('.customer-order-filter-placeholder',{
                            orders: this.collection,
                            orderOptions: this.orderOptions
                        });
                        this.setSelected();
                    },

                    orderHandler: function(ev){
                        var id = $(ev.target).closest('tr').attr('data-view-id');
                        app.event_bus.trigger('draw-order-item', this.collection.get(id));
                    },

                    applyOrderStatusFilter: function(ev){
                        this.selected = $(ev.target).find(':selected').val();
                        console.log(this.defColl);

                        this.collection = this.defColl.clone();
                        if(this.selected != 'all') {
                            this.collection = new app.models.OrdersCollection(
                                this.collection.where({status: this.selected}));
                        }

                        this.render();
                    },

                    setSelected: function(){
                        var self = this;
                        $('.order-status-filter > option').each(function(){
                            if($(this).val() == self.selected){
                                $(this).attr('selected', 'selected');
                            }
                        });
                    }
                }),

                order_items:  Backbone.View.extend({
                    initialize: function (){
                        this.listenTo(app.event_bus, 'draw-order-item', this.doRender);
                        this.template = this.createTemplate('#order-items-template');
                        this.orderOptions = app.data.orderOptions;
                        this.render();
                    },

                    render: function () {
                    },

                    doRender: function(model){
                        this.template('.order-items-placeholder', {
                            model: model,
                            orderOptions: this.orderOptions
                        });
                    }
                })
            }

        });

})(jQuery, app);
                