(function ($, app) {

    $.extend(app.views, {
        import: {
            index: Backbone.View.extend({

                events: {
                    'click .btn-import-decline' : 'declineImport',
                    'click .btn-import-accept' : 'acceptImport'
                },

                initialize: function () {
                    this.importFile = null;
                    this.dzConfig = _.extend(app.config.dzConfig, {
                        createImageThumbnails: false,
                        maxFiles: 1,
                        uploadMultiple: false,
                        addRemoveLinks: false,
                        renameFilename: function (name) {
                            return 'import.csv';
                        },
                        acceptedFiles: '.csv',
                        dictFallbackText: null,
                        dictDefaultMessage: 'Загрузить файл импорта<i class="entypo-upload"></i>',
                        url: app.config.baseUrl + 'import-insert',
                        success: $.proxy(function (file, response) {
                            $('.dz-progress').hide();
                            $('.dz-size').hide();
                            $('.dz-error-mark').hide();
                            this.renderFileInfo(JSON.parse(response));
                            app.thankYou();
                        }, this),
                        addedfile: function (file) {
                            app.pleaseWait();
                            if (this.files.length > 1) {
                                this.removeFile(this.files[0]);
                            }
                        }
                    });
                    this.$el.find('.dropzone').dropzone(this.dzConfig);

                    this.templates = {
                        'preupload' : this.createTemplate('#pre-upload-template'),
                        'fileInfo' : this.createTemplate('#file-info-template'),
                        'postupload' : this.createTemplate('#post-upload-template')
                    };
                    this.render();
                },

                render: function () {
                    this.templates.preupload('.pre-upload-placeholder', {});
                },

                renderFileInfo: function (info) {
                    this.$el.find('.pre-upload-placeholder').hide();
                    this.importFile = info.fileInfo.path;
                    this.templates.fileInfo('.file-info-placeholder', {
                        info: info
                    });
                },

                declineImport: function () {
                    this.templates.fileInfo('.file-info-placeholder', {
                        info: null
                    });
                },

                acceptImport: function (ev) {
                    $(ev.target).button('loading');

                    $.ajax({
                        url: app.config.baseUrl + 'import-execute',
                        data: {
                            file: this.importFile
                        },
                        type: "POST",
                        beforeSend: function () {
                          app.pleaseWait();
                        },
                        success: $.proxy(function(response){
                            app.thankYou();
                            $(ev.target).button('reset');
                            console.log(JSON.parse(response));
                            app.helpers.notifications.show('success', 'Импорт прошел успешно.');
                            this.templates.fileInfo('.file-info-placeholder', {
                                info: null
                            });
                            this.templates.postupload('.post-upload-placeholder', {
                               data: JSON.parse(response)
                            });
                        }, this),
                        error: $.proxy(function(response){
                            $(ev.target).button('reset');
                            app.thankYou();
                            app.helpers.notifications.show('error', 'Ошибка импорта. Проверьте файл импорта.');
                        }, this)
                    })

                }

            })
        }
    })


})(jQuery, app);

