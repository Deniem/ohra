(function($, app) {
	$.extend(
		true,
		app.views,
		{
			modals: {
				addPropertyValue: app.views.modals.Modal.extend({

					events: {
						'click .btn-save': 'doSave'
					},

					display: function(model, callback) {

						this.model = model;
						this.callback = callback || $.noop;

						this.template(
							'.modal-content',
							{
								prop: this.model
							}
						);
						this.$el.modal();
					},


					doSave: function(ev) {
						$(ev.target).button('loading');
						var data = this.collectForm('.values');
						if ($.trim(data.name) != '') {
							var valueModel = new app.models.Value(data,{parse:true});
							valueModel.save(null,{
								success: $.proxy(function(model){
									this.$el.modal('hide');
									this.callback(model);
								}, this),
								error: function(response){
									console.log('error' + console.log(response));
								}
							});
						}
						$(ev.target).button('reset');
					}

				})
			}
		});

})(jQuery, app);

