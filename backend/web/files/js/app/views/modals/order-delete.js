(function($, app) {
    $.extend(
        true,
        app.views,
        {
            modals: {
                DeleteOrder: app.views.modals.Modal.extend({

                    events: {
                        'click .deleteBtn': 'doDelete'
                    },

                    display: function(model, callback) {

                        this.model = model;
                        this.callback = callback || $.noop;

                        this.template(
                            '.modal-content',
                            {
                                prop: this.model
                            }
                        );
                        this.$el.modal();
                    },


                    doDelete: function(ev) {
                        var self = this;
                        this.model.destroy({
                            success: function(){
                                self.$el.modal('hide');
                                window.location.reload();
                                app.helpers.notifications.show('success', 'Заказ бы успешно удален');
                            },
                            error: function(){
                                self.$el.modal('hide');
                                app.helpers.notifications.show('error', 'Ошибка в процессе удаления заказа');
                            }
                        });
                    }

                })
            }
        });

})(jQuery, app);

