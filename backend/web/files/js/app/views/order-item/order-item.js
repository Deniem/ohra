(function ($, app) {

    $.extend(true,
        app.views, {
            orders: {
                ordersList: Backbone.View.extend({

                    events: {
                        'click .deleteItem': 'deleteItem',
                        'change .quantityItem' : 'changeQuantity',
                        'click .btn-new-order-item' : 'newItemButtonTrigger'
                    },

                    initialize: function () {
                        this.itemsCollection = new app.models.OrderItemsCollection();
                        this.orderItemsDiscountCollection = new app.models.OrderItemDiscountsCollection();
                        //add product button availability
                        this.newOrderProductButtonAvailability = true;

                        this.template = this.createTemplate('#items-list-template');
                        this.listenTo(app.event_bus, 'addNewOrderProduct', this.addNewItem);
                        this.listenTo(app.event_bus, 'cancelAddingNewOrderProduct', this.cancelAddingNewItem);

                        var self = this;
                        var data = {
                            orderId: $('#items-list-template').attr('data-id-order')
                        };

                        //this.itemsCollection.on('remove', this.render, this);
                        //this.itemsCollection.on('add', this.render, this);

                        this.itemsCollection.fetch({
                            data: data,
                            success: function(){
                                self.itemsCollection.each(function(model) {
                                    var m = new app.models.OrderItemDiscount({'order_item_id': model.get('id')});
                                    self.orderItemsDiscountCollection.push(m);
                                });
                                var ids = self.orderItemsDiscountCollection.pluck('order_item_id');
                                self.orderItemsDiscountCollection.fetch({
                                    data: {
                                        orderItemsIds : ids.join(';')
                                    },
                                    success: function(){
                                        self.totalCost();
                                        self.render();
                                    }
                                });

                            }
                        });
                    },

                    render: function () {
                        this.template('.items-list-placeholder',{
                            items: this.itemsCollection.toJSON(),
                            total : this.totalPrice,
                            discounts: this.orderItemsDiscountCollection,
                            newOrderProductButtonAvailability: this.newOrderProductButtonAvailability
                        });
                    },

                    deleteItem: function (ev) {
                        var el = $(ev.target);
                        var id = el.closest('tr').attr('data-id');
                        var disId = el.closest('tr').attr('data-discount-id');
                        var self = this;
                        if(this.orderItemsDiscountCollection.get(disId)){
                            this.orderItemsDiscountCollection
                                .get(disId).destroy( {
                                    success: function(){
                                        self.itemsCollection.get(id).destroy( {
                                            success: function(){
                                                app.helpers.notifications
                                                    .show('warning', '<b>Товар удален из заказа</b>');
                                                self.totalCost();
                                                self.render();
                                            }
                                        });
                                    }
                                });
                        }else{
                            this.itemsCollection.get(id).destroy( {
                                success: function(){
                                    app.helpers.notifications.show('warning', '<b>Товар удален из заказа</b>');
                                    self.totalCost();
                                    self.render();
                                }
                            });
                        }
                    },


                    changeQuantity: function (ev) {
                        var el = $(ev.target);
                        var id = el.closest('tr').attr('data-id');
                        this.itemsCollection.get(id).set({'quantity': el.val()});
                        var self = this;
                        this.itemsCollection.get(id).save(null,{
                            success: function(model, resp, opts){
                                self.totalCost();
                                self.render();
                            }
                        });
                    },

                    totalCost: function () {
                        var total = 0;
                        var self = this;
                        _.each(this.itemsCollection.models, function (el) {
                           var dis = self.orderItemsDiscountCollection
                               .findWhere({'order_item_id': el.get('id')});

                            if(dis !== undefined){
                                total += dis.get('price') * el.get('quantity');
                            }else{
                                total += el.get('price') * el.get('quantity');
                            }
                        });
                        this.totalPrice = parseFloat(total).toFixed(2) + ' грн';
                    },

                    newItemButtonTrigger: function(ev){
                        this.newOrderProductButtonAvailability = false;
                        app.event_bus.trigger('orderNewItemTrigger', this.grabAlreadyAddedModels());
                        this.render();
                    },

                    grabAlreadyAddedModels: function(){
                        var alreadyInItems = [];
                      $.each($('#order-items-list > tr'), function(k, v){
                            alreadyInItems.push( $(v).attr('data-product-id'));
                      });
                        console.log(alreadyInItems);
                        return alreadyInItems;
                    },

                    addNewItem: function(product){
                        console.log(product);
                        var newOrderItem = new app.models.OrderItem({
                            'order_id': $('#items-list-template').attr('data-id-order'),
                            'name': product.get('name'),
                            'price': product.get('price'),
                            'quantity': product.get('quantity'),
                            'product_id': product.get('id')
                        });

                        var newOrderItemDiscount = {};
                        var self = this;

                        newOrderItem.save(null, {
                            success: function(){
                                app.helpers.notifications.show('success', '<b>Товар добавлен в заказ</b>');

                                if(product.get('discount')){
                                    newOrderItemDiscount = new app.models.OrderItemDiscount({
                                        'order_item_id': newOrderItem.get('id'),
                                        'price': product.get('discount')['price'],
                                        'value': product.get('discount')['value'],
                                        'type': product.get('discount')['type'],
                                        'status': product.get('discount')['status'],
                                        'start_date': product.get('discount')['start_date'],
                                        'end_date': product.get('discount')['end_date']
                                    });

                                    console.log(newOrderItemDiscount);

                                    newOrderItemDiscount.save(null, {
                                        success:function(){
                                            self.orderItemsDiscountCollection.add(newOrderItemDiscount);
                                            self.itemsCollection.add(newOrderItem);
                                            self.newOrderProductButtonAvailability = true;
                                            self.totalCost();
                                            self.render();
                                        }
                                    })
                                }else{
                                    self.newOrderProductButtonAvailability = true;
                                    self.itemsCollection.add(newOrderItem);
                                    self.totalCost();
                                    self.render();
                                }
                            }
                        });
                    },

                    cancelAddingNewItem: function () {
                        this.newOrderProductButtonAvailability = true;
                        this.render();
                    }
                }),

                newOrderItem: Backbone.View.extend({

                    events: {
                        'change #new-item-category' : 'selectCategory',
                        'change #new-item-product' : 'selectProduct',
                        'click .btn-cancel-add' : 'cancelAddProduct',
                        'click .btn-addNewOrderItem' : 'addProduct',
                        'click #new-item-productCount' : 'updateCost'
                    },

                    initialize: function () {
                        var self = this;

                        this.template = this.createTemplate('#new-order-item-template');

                        //will draw product form on listener event
                        this.newItemButtonClicked = false;
                        this.listenTo(app.event_bus, 'orderNewItemTrigger', this.newItemClicked);

                        //if those items are active, layout will draw them
                        this.selectedCategory = this.selectedProduct = null;

                        this.products = new app.models.ProductCollection();
                        this.categories = new app.models.CategoryCollection();
                        this.categories.fetch({
                            success: function(){
                                self.render();
                            }
                        });
                    },

                    newItemClicked: function(alreadyOrderedProducts){
                        //init new product layout drawing from event bus action
                        this.newItemButtonClicked = true;
                        this.alreadyInProducts = alreadyOrderedProducts;
                        this.render();
                    },

                    render: function () {
                        this.template('.new-order-item-placeholder', {
                            newItemButtonClicked: this.newItemButtonClicked,
                            categories: this.categories.toJSON(),
                            products: this.products.toJSON(),
                            selectedCategory: this.selectedCategory,
                            selectedProduct: this.selectedProduct
                        });
                    },

                    selectCategory: function(ev){
                        //select category action from dropdown menu
                        var self = this;
                        var cat_id = $(ev.target).find('option:selected').val();
                        this.selectedCategory = cat_id;

                        this.products = new app.models.ProductCollection();
                        this.products.fetch({
                            url: '/api/category/' + cat_id,
                            data: {
                                catId: cat_id
                            },
                            success: function(){
                                self.render();
                            }
                        });
                    },

                    selectProduct: function(ev){
                        //load product from dropdown select action
                        var self = this;
                        var prod = $(ev.target).find('option:selected').val();

                        this.selectedProduct = new app.models.Product({'id': prod, 'loading': true});
                        this.render();

                        this.selectedProduct.fetch({
                            success: function(){
                                self.selectedProduct.set({
                                    'loading': false,
                                    'quantity': 1
                                });
                                self.checkIsAlreadyInOrder(prod);
                                self.isDiscountActive();
                                self.updateCost();
                                self.render();
                            }
                        });
                    },

                    checkIsAlreadyInOrder: function (prodId) {
                        if($.inArray(prodId, this.alreadyInProducts) !== -1 ){
                            this.selectedProduct.set({
                                'alreadyFound': true
                            });
                        }else{
                            this.selectedProduct.set({
                                'alreadyFound': false
                            });
                        }
                    },

                    isDiscountActive: function(){
                        var isActive = false;
                        if(this.selectedProduct.get('discount')){
                            if(this.selectedProduct.get('discount')['status'] == 'enabled') {
                                var startDate = this.selectedProduct.get('discount')['start_date'];
                                var endDate = this.selectedProduct.get('discount')['end_date'];
                                var now = moment();
                                var sdate = moment(startDate);
                                var edate = moment(endDate);

                                if(endDate !== null && startDate !== null){
                                    if(now.diff(sdate) > 0 && now.diff(edate) < 0){
                                        isActive = true;
                                    }
                                } else if (endDate == null && startDate == null){
                                    isActive = true;
                                } else if(startDate !== null && endDate == null){
                                    if(now.diff(sdate) > 0){
                                        isActive = true;
                                    }
                                } else if(startDate == null && endDate !== null) {
                                    if(now.diff(edate) < 0){
                                        isActive = true;
                                    }
                                }
                            }
                        }
                        this.selectedProduct.set({'discountActivityState': isActive});
                    },

                    updateCost: function(){
                        var quantity = $('#new-item-productCount').val() !== undefined
                            ? $('#new-item-productCount').val()
                            : 1;
                        var sum = 0;
                        if(this.selectedProduct.get('discountActivityState')){
                            sum = this.selectedProduct.get('discount')['price'] * quantity
                        }else{
                            sum = this.selectedProduct.get('price') * quantity
                        }

                        this.selectedProduct.set({
                            'finalCost' : parseFloat(sum).toFixed(2),
                            'quantity': quantity
                        });
                        this.render();
                    },

                    addProduct: function(){
                        // add product to order items list, finish work
                        app.event_bus.trigger('addNewOrderProduct', this.selectedProduct);
                        this.newItemButtonClicked = false;
                        this.selectedCategory = this.selectedProduct = null;
                        this.render();
                    },

                    cancelAddProduct: function () {
                        // cancel product select operation, finish work
                        app.event_bus.trigger('cancelAddingNewOrderProduct');
                        this.newItemButtonClicked = false;
                        this.selectedCategory = this.selectedProduct = null;
                        this.render();
                    }
                })
            }
        });

})(jQuery, app);
