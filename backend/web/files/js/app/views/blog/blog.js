(function($, app) {

    $.extend(app.views, {
        blog: {
            MainList: Backbone.View.extend({

                events: {
                    'click .blog-delete-action': 'deleteAction'
                },

                deleteAction: function(ev){
                    var id = $(ev.target).closest('td').attr('data-id');
                    app.modal('blog-delete', new app.models.Blog({id: id}));
                }
            })

        }
    })

})(jQuery, app);

