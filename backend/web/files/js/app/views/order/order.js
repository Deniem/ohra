


(function ($, app) {

    $.extend(true,
        app.views, {
            order: {
                order_filter: Backbone.View.extend({
                    events: {
                        'change .order-status-filter' : 'changeStatus',
                        'click .order-delete-btn' : 'deleteOrder'
                    },

                    initialize: function (){
                        this.urlParams = app.helpers.parseURLParams();
                        this.url = app.config.baseUrl + 'orders';
                        this.template = this.createTemplate('#orders-filter-list-template');
                        this.render();
                    },

                    render: function () {
                        this.template('.orders-filter-list-placeholder');
                    },

                    changeStatus: function(ev){
                        var sel = $(ev.target).find(':selected').val();
                        app.helpers.gotoURL(this.url, {'order-status-filter': sel});
                    },
                    
                    deleteOrder: function(ev){
                        var id = $(ev.target).closest('td').attr('data-id');
                        app.modal('order-delete', new app.models.Order({id: id}));
                    }

                }),

				Update: Backbone.View.extend({
                    events: {
						'click .btn-status': 'changeStatus'
					},

                    initialize: function (){
						this.order = app.data.order;
                    },

                    render: function () {

                    },

                    changeStatus: function(ev){
						$(ev.target).button('loading');
						this.order.status = $(ev.target).data('status');


						$.ajax({
							type:'POST',
							url:app.config.baseUrl + 'order/status/' + app.data.order.id,
							data: {status:this.order.status},
							success: $.proxy(function(response){
								$(ev.target).button('reset');

								$('#order-status').find('option[value="'+$(ev.target).data('status')+'"]').prop('selected','selected');
								app.helpers.notifications.show('success','Статус успешно изменен на <b>' + this.order.statusLabels[response.status] + '</b>');
							}, this),
							error: function(response){
								$(ev.target).button('reset');
								app.helpers.notifications.show('error','Статус не изменен!');
							}
						});


                    }

                })
            }

        });

})(jQuery, app);