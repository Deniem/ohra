(function($, app) {
	$.extend(
		true,
		app.views,
		{
			modals: {
				Modal: Backbone.View.extend({

					events: {},

					initialize: function() {
						_.extend(this.events, {
							'click .btn-save': 'save',
							'keyup': 'filterOnEnter'
						});
						this.template = this.createTemplate('.modal-template');
					},

					filterOnEnter: function(e) {
						if (e.keyCode != 13) return;
						this.save();
					},

					collectForm: function(selector) {
						selector = selector || '';
						var data = {};
						_.each(this.$('form' + selector).serializeArray(), function(o) {
							if (typeof data[o.name] == 'undefined') {
								data[o.name] = o.value;
							}
							else if (typeof data[o.name] == 'string') {
								data[o.name] = [data[o.name], o.value];
							}
							else {
								data[o.name].push(o.value);
							}
						});

						return data;
					},

					handleErrors: function(errors, selector) {
						selector = selector || '';
						if (typeof errors == 'undefined' || Object.keys(errors).length == 0) {
							return true;
						}
						for (var key in errors) {
							var $input = this.$('form' + selector + ' [name="' + key + '"]');
							$input.addClass('invalid');
							$input.parent().append(
								$('<span/>')
									.addClass('label label-danger field-error')
									.html(errors[key])
							);
						}
						this.$('.btn-save').button('reset');
						return false;
					},

					clearErrors: function() {
						this.$('.invalid').removeClass('invalid').trigger('clear-invalid');
						this.$('.field-error').remove();
					},

					save: function(ev) {
						this.$('.btn-save').button('loading');
						this.clearErrors();
						this.doSave(ev);
					},

					doSave: function() {}
				})
			}
		}
	);

})(jQuery, app);
