(function($, app) {

    app.models.Order = Backbone.Model.extend({

        url:function () {
            if (this.isNew()) {
                return app.config.baseUrl + 'api/order/'
            }
            else {
                return app.config.baseUrl + 'api/order/' + this.get('id');
            }
        },

        validation: {},

        getTotal: function(){
            var orders = this.get('orderItems');
            var total = 0;
            _.each(orders, function(order){
                total += order['price'] * order['quantity']
            });
            
            return total;
        }
    });

    app.models.OrdersCollection = Backbone.Collection.extend({
        url: app.config.baseUrl + 'api/orders/',
        model: app.models.Order
    });

})(jQuery, app);
