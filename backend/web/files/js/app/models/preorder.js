(function($, app) {

    app.models.Preorder = Backbone.Model.extend({

        url:function () {
            if (this.isNew()) {
                return app.config.baseUrl + 'preorder'
            }
            else {
                return app.config.baseUrl + 'preorder/' + this.get('id');
            }
        },

        getPreorderItems: function (params) {
            params = (params == undefined) ? '' : params;
            return $.ajax({
                url: app.config.baseUrl + 'api/preorder-item/view?id=' + this.get('id') + params
            })
        },

        validation: {}
    });

    app.models.PreorderCollection = Backbone.Collection.extend({
        url: app.config.baseUrl + 'preorder',
        model: app.models.Preorder
    });

})(jQuery, app);
