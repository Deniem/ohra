(function($, app) {

    app.models.Customer = Backbone.Model.extend({

        url:function () {
            if (this.isNew()) {
                return app.config.baseUrl + 'customer/'
            }
            else {
                return app.config.baseUrl + 'customer/' + this.get('id');
            }
        },

        validation: {},

        save: function(attrs, options) {
            options || (options = {});
            attrs || (attrs = _.clone(this.attributes));
            options.url = app.config.baseUrl + 'customer/update/' + this.attributes.id;
            options.type = 'POST';
            Backbone.Model.prototype.save.call(this, attrs, options);
        }
    });

})(jQuery, app);
