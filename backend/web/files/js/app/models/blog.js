(function($, app) {

    app.models.Blog = Backbone.Model.extend({

        url:function () {
            if (this.isNew()) {
                return app.config.baseUrl + 'api/blog'
            }
            else {
                return app.config.baseUrl + 'api/blog/' + this.get('id');
            }
        },

        validation: {}
    });

    app.models.BlogCollection = Backbone.Collection.extend({
        url: app.config.baseUrl + 'api/blog/',
        model: app.models.Blog
    });

})(jQuery, app);
