(function($, app) {

	app.models.ProductProperty = Backbone.Model.extend({

		url:function () {
			if (this.isNew()) {
				return app.config.baseUrl + 'api/product-property'
			}
			else {
				return app.config.baseUrl + 'api/product-property/' + this.get('id');
			}
		},

		validation: {}
	});

	app.models.ProductPropertiesCollection = Backbone.Collection.extend({
		url: app.config.baseUrl + 'api/product-property',
		model: app.models.Property
	});

})(jQuery, app);
