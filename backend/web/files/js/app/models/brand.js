(function($, app) {

    app.models.Brand = Backbone.Model.extend({

        url:function () {
            if (this.isNew()) {
                return app.config.baseUrl + 'api/brand'
            }
            else {
                return app.config.baseUrl + 'api/brand/' + this.get('id');
            }
        },

        validate: function(){
            var errors = [];

            if(!this.attributes.name){
                errors.push({'name': 'name', message: "Поле не должно быть пустым"});
            }

            return errors.length > 0 ? errors : false;
        },

        validation: {}
    });

    app.models.BrandCollection = Backbone.Collection.extend({
        url: app.config.baseUrl + 'api/brand/',
        model: app.models.Brand
    });

})(jQuery, app);
