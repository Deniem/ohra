(function($, app) {

    app.models.OrderItemDiscount = Backbone.Model.extend({

        url:function () {
            if (this.isNew()) {
                return app.config.baseUrl + 'api/order-item-discount'
            }
            else {
                return app.config.baseUrl + 'api/order-item-discount/' + this.get('id');
            }
        },

        validation: {}
    });

    app.models.OrderItemDiscountsCollection = Backbone.Collection.extend({
        url: app.config.baseUrl + 'api/order-item-discount',
        model: app.models.OrderItemDiscount
    });

})(jQuery, app);
