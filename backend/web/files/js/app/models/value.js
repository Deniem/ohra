(function($, app) {

	app.models.Value = Backbone.Model.extend({

		url:function () {
			if (this.isNew()) {
				return app.config.baseUrl + 'value'
			}
			else {
				return app.config.baseUrl + 'value/' + this.get('id');
			}
		}

	});

	app.models.ValuesCollection = Backbone.Collection.extend({
		url: app.config.baseUrl + 'value',
		model: app.models.Value
	});

})(jQuery, app);
