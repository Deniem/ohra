(function($, app) {

	app.models.User = Backbone.Model.extend({

		url:function () {
			if (this.isNew()) {
				return app.config.baseUrl + 'api/user'
			}
			else {
				return app.config.baseUrl + 'api/user/' + this.get('id');
			}
		}

	});

	app.models.ValuesCollection = Backbone.Collection.extend({
		url: app.config.baseUrl + 'api/user',
		model: app.models.User
	});

})(jQuery, app);
