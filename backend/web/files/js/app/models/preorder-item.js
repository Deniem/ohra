(function($, app) {

    app.models.PreorderItem = Backbone.Model.extend({

        url:function () {
            if (this.isNew()) {
                return app.config.baseUrl + 'preorder-item'
            }
            else {
                return app.config.baseUrl + 'preorder-item/' + this.get('id');
            }
        },
        
        validation: {}
    });

    app.models.PreorderItemCollection = Backbone.Collection.extend({
        
        url: function(){
            return app.config.baseUrl + 'api/preorder-item'
        },

        model: app.models.PreorderItem
    });

})(jQuery, app);
