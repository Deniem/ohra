(function($, app) {

	app.models.Property = Backbone.Model.extend({

		url:function () {
			if (this.isNew()) {
				return app.config.baseUrl + 'property'
			}
			else {
				return app.config.baseUrl + 'property/' + this.get('id');
			}
		},

		validation: {}
	});

	app.models.PropertiesCollection = Backbone.Collection.extend({
		url: app.config.baseUrl + 'property',
		model: app.models.Property
	});

})(jQuery, app);
