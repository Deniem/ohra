(function($, app) {

    app.models.OrderItem = Backbone.Model.extend({

        url:function () {
            if (this.isNew()) {
                return app.config.baseUrl + 'order-item'
            }
            else {
                return app.config.baseUrl + 'order-item/' + this.get('id');
            }
        },

        validation: {}
    });

    app.models.OrderItemsCollection = Backbone.Collection.extend({
        url: app.config.baseUrl + 'order-item/',
        model: app.models.OrderItem
    });

})(jQuery, app);
