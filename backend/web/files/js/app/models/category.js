(function($, app) {

	app.models.Category = Backbone.Model.extend({

		url:function () {
			if (this.isNew()) {
				return app.config.baseUrl + 'category'
			}
			else {
				return app.config.baseUrl + 'category/' + this.get('id');
			}
		},

		validation: {}
	});

	app.models.CategoryCollection = Backbone.Collection.extend({
		url: app.config.baseUrl + 'api/category',
		model: app.models.Category
	});

})(jQuery, app);
