(function($, app) {

	app.models.Settings = Backbone.Model.extend({

		url:function () {
			if (this.isNew()) {
				return app.config.baseUrl + 'api/settings'
			}
			else {
				return app.config.baseUrl + 'api/settings/' + this.get('id');
			}
		},

		validation: {}
	});

	app.models.SettingsCollection = Backbone.Collection.extend({
		url: app.config.baseUrl + 'api/settings',
		model: app.models.Settings
	});

})(jQuery, app);
