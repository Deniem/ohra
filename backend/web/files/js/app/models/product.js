(function($, app) {

	app.models.Product = Backbone.Model.extend({

		url:function () {
			if (this.isNew()) {
				return app.config.baseUrl + 'api/product'
			}
			else {
				return app.config.baseUrl + 'api/product/' + this.get('id');
			}
		},

		getIsLabels: function(){
			var line = "";
			if(this.attributes.is_action == 1) {
				line += '<div class="label label-info">Акционный</div>';
			}
			if(this.attributes.is_sale == 1) {
				line += '<div class="label label-warning">Распродажа</div>';
			}
			if(this.attributes.is_new == 1) {
				line += '<div class="label label-success">Новинка</div>';
			}
			
			return line;
		},

		getStatusLabel: function(){
			if(app.data.productStatus !== undefined){
				return line = '<div class="label label-' + app.data.productStatus.labels[this.attributes.status]
					+ '">' + app.data.productStatus.names[this.attributes.status] + '</div>';
			}
		},

		validation: {}
	});

	app.models.ProductCollection = Backbone.Collection.extend({
		url: app.config.baseUrl + 'api/product',
		model: app.models.Product
	});

})(jQuery, app);
