(function($, app) {

    app.models.Discount = Backbone.Model.extend({

        url:function () {
            if (this.isNew()) {
                return app.config.baseUrl + 'api/discount'
            }
            else {
                return app.config.baseUrl + 'api/discount/' + this.get('id');
            }
        },

        validation: {},

        calculateDate: function(){
            if(this.attributes.status == 'enabled'
                && this.attributes.start_date
                && !this.attributes.end_date){
                return 'C ' + this.attributes.start_date
            }else if(this.attributes.status == 'enabled'
                && !this.attributes.start_date
                && this.attributes.end_date){
                return 'Уже сейчас до ' + this.attributes.end_date;
            }else if(this.attributes.status == 'enabled'
                && this.attributes.start_date
                && this.attributes.end_date){
                return 'С ' + this.attributes.start_date
                + ' по ' + this.attributes.end_date;
            }else if(this.attributes.status !== 'enabled'){
                return 'Не активна';
            }else if(this.attributes.status == 'enabled'){
                return 'Всегда';
            }
        }
    });

    app.models.DiscountCollection = Backbone.Collection.extend({
        url: app.config.baseUrl + 'api/discount/',
        model: app.models.Discount
    });

})(jQuery, app);
