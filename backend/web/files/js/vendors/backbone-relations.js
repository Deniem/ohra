/*
 * Extensions for Backbone to accommodate our needs.
 *  - Support for the nested models
 *  - Support for the calculated properties
 *  - Backbone validation plugin
 *  - Support for pagination (compatible with Yii 2.0 REST)
 *  - Support for decorated properties
 *  - Wrapper for Underscore template function
 */
(function($) {

	/**
	 * Plugging in the Backbone validation plugin
	 * See https://github.com/thedersen/backbone.validation
	 */
	_.extend(Backbone.Model.prototype, Backbone.Validation.mixin);

	/**
	 * Adding a parser that supports nested models
	 *
	 * @param  {object} response  Raw model data that need parsing
	 *
	 * @returns  {object} Parsed data
	 */
	Backbone.Model.prototype.parse = function (response) {

		// Creating a copy of the original object
		response = $.extend({}, response);

		// If the model has declaration of the nested models and the parsed data has corresponding entries,
		// create nested objects.
		if (typeof this.model != 'undefined') {
			for (var key in this.model) {
				if (this.model.hasOwnProperty(key)) {
					var nestedClass = this.model[key];
					response[key] = new nestedClass(response[key], { parse: true });
				}
			}
		}
		return response;
	};

	/**
	 * The original Backbone toJSON method.
	 */
	var _BackboneModelToJSON = Backbone.Model.prototype.toJSON;

	/**
	 * Wrapping the original toJSON method
	 *
	 * @param  {object}  options  Serialization options
	 *
	 * @returns {object}  Serialized data
	 */
	Backbone.Model.prototype.toJSON = function (options) {
		options = options || {};

		// Getting plain model attributes
		var response = _BackboneModelToJSON.apply(this, arguments);

		// Iteration variable
		var key;

		// Replacing nested models with their serialized representation.
		if (typeof this.model != 'undefined') {
			for (key in this.model) {
				if (this.model.hasOwnProperty(key) && response.hasOwnProperty(key)) {
					var nestedClass = this.model[key];
					if (response[key]) {
						response[key] = response[key].toJSON(options);
					}
				}
			}
		}

		// Decorating properties if the model support decorations and if the options have decorations enabled.

		// TODO: Remove everything related to model.decorations as soon as we get rid of 'lists'
		if (options.decorate) {
			var keys = Object.keys(response);
			var decorations = this.decorations || {};
			for (var i = 0; i < keys.length; i++ ) {
				var key = keys[i];
				if (response.hasOwnProperty(key)) {
					if (decorations.hasOwnProperty(key)) {
						response[key] = decorations[key](response[key], options['empty'] || '');
					}
					else if (typeof response[key] == 'string'){
						response[key] = app.formatting.html(response[key], options['empty']);
					}
				}
			}
		}

		// Evaluating calculated properties
		if (typeof this.properties != 'undefined') {
			for (key in this.properties) {
				response[key] = this.property(key);
			}
		}

		return response;
	};

	/**
	 * Evaluates the property and returns its value
	 *
	 * @param  {string}  name  Property name
	 *
	 * @returns {*}  Calculated value
	 */
	Backbone.Model.prototype.property = function(name) {
		if (typeof this.properties != 'undefined') {
			if (typeof this.properties[name] != 'undefined') {
				return this.properties[name].call(this);
			}
		}
	};

	/**
	 * The original Backbone fetch method.
	 */
	var _BackboneCollectionFetch = Backbone.Collection.prototype.fetch;

	/**
	 * Wrapping Collection.fetch method to parse pagination information reported by Yii REST.
	 * It parses X-Pagination-* HTTP headers and calculates the rest of the pagination-related numbers.
	 * The pagination-related data is stored in collection's 'pagination' property.
	 *
	 * @param  {object} options
	 *
	 * @returns {*}
	 */
	Backbone.Collection.prototype.fetch = function(options) {
		options = options || {};
		var success = options.success;

		options.success = function(collection, models, request) {
			var pageHeader = request.xhr.getResponseHeader('X-Pagination-Current-Page');
			if (pageHeader !== null) {
				var page = parseInt(pageHeader);
				var pageSize = parseInt(request.xhr.getResponseHeader('X-Pagination-Per-Page'));
				var itemFirst = collection.length ? (page - 1) * pageSize + 1 : 0;

				collection.pagination = {
					page: page,
					pageSize: pageSize,
					pageCount: parseInt(request.xhr.getResponseHeader('X-Pagination-Page-Count')),
					itemCount: parseInt(request.xhr.getResponseHeader('X-Pagination-Total-Count')),
					itemFirst: itemFirst,
					itemLast: itemFirst + collection.length - 1
				};
			}
			if (success) success(collection, models, request);
		}
		return _BackboneCollectionFetch.call(this, options);
	};

	/**
	 * Streamlines usage of Underscore template() function. Instead of getting HTML of the template, processing it and
	 * storing resulting HTML in the destination element, you just specify the selector of the template tag and the
	 * selector of the destination element.
	 *
	 * Also, it triggers 'post-render' app event for post-processing the created HTML.
	 *
	 * @param  {string}  selector  jQuery selector of the template element (relative to the current view)
	 *
	 * @returns  {function}  Template function
	 */
	Backbone.View.prototype.createTemplate = function(selector) {
		var template = _.template(this.$(selector).html());
		return $.proxy(function(target) {
			var $target = this.$(target);
			$target.html(template.apply(this, Array.prototype.slice.call(arguments, 1)));

			$(app).trigger('post-render', [$target]);
		}, this);
	};

})(jQuery);
