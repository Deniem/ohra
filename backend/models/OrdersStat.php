<?php
/**
 * Created by PhpStorm.
 * User: deniem
 * Date: 16.08.16
 * Time: 16:33
 * Allow us to place all Orders statistic logic in one place
 */

namespace backend\models;

use common\models\db\base\OrderBase;
use common\models\db\Order;
use yii\base\Model;
use yii\helpers\ArrayHelper;

class OrdersStat
{

	public static function totalCost($items)
	{
		$prices = [];
		foreach ($items as $item) {
			$prices[] = $item->price;
		}
		return ($prices && count($prices)) > 0 ? array_sum($prices) : 0;
	}

}