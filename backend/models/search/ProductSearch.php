<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\db\Product;

/**
 * ArticleSearch represents the model behind the search form about `common\models\Article`.
 */
class ProductSearch extends Product
{
	public $isInSearch = false;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			[['id_category', 'price'], 'integer'],
			['name', 'string', 'max' => 255],
			['status', 'string'],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = Product::find();

		if (!($this->load($params) && $this->validate())) {
			return $query;
		}

		$this->isInSearch = true;

		$query->with('discount');
		$query->andFilterWhere([
			'id_category' => $this->id_category
		]);

		if ($this->status) {
			$query->andFilterWhere([
				'status' => $this->status != 'all' ? $this->status : ''
			]);
			if (count($params['ProductSearch']) <= 1) $this->isInSearch = false;
		}


		$query->andFilterWhere([
			'price' => $this->price
		]);

		$query->andFilterWhere(['like', 'name', $this->name]);


		return $query;
	}
}
