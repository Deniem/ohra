<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\db\Category;

/**
 * ArticleSearch represents the model behind the search form about `common\models\Article`.
 */
class CategorySearch extends Category
{

	public $isInSearch = false;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
		return [
			['id_parent', 'integer'],
			['name', 'string', 'max' => 255]
		];
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}

	/**
	 * Creates data provider instance with search query applied
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{


		$query = Category::find();

		if (!($this->load($params) && $this->validate())) {
			return $query;
		}

		if ($this->id_parent < 0) {
			$query->andWhere('id_parent is NULL');
		} else {
			$query->andFilterWhere([
				'id_parent' => $this->id_parent
			]);
		}

		if ($this->status) {
			$query->andFilterWhere([
				'status' => $this->status != 'all' ? $this->status : ''
			]);
		}

		$query->andFilterWhere(['like', 'name', $this->name]);
		$this->isInSearch = true;
		return $query;
	}
}
