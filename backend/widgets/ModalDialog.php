<?php
namespace backend\widgets;

use yii\base\Widget;

class ModalDialog extends Widget
{
    public $name;
    public $title;
    public $message;
    public $modalId;
    public $backBoneDataViewType;
    

    public function run()
    {
        return $this->render(
            'modal/' . strtolower($this->name),[
                'title' => $this->title,
                'message' => $this->message,
                'modalId' => $this->modalId,
                'backBoneDataViewType' => $this->backBoneDataViewType
            ]
        );
    }
}
