<div class="modal fade" id="add-property-value" data-view-type="modals.addPropertyValue">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>
        <script type="text/template" class="modal-template">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Новое значение для характеристики <strong><i>"<%= prop.name %>"</i></strong></h4>
            </div>
            <div class="modal-body">
                <form class="values">
                    <div class="row">
                        <div class="col-sm-12">
                            <h4>Новое значение</h4>
							<input type="text" class="form-control" name="name" value="">
							<input type="hidden" name="id_property" value="<%= prop.id %>">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button"  data-loading-text="Сохранение..." autocomplete="off" class="btn btn-primary btn-save">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
            </div>
        </script>
    </div>
</div>