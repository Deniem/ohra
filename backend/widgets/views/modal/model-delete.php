<div class="modal fade" id="<?= $modalId ?>" data-view-type="<?= $backBoneDataViewType ?>">
    <div class="modal-dialog modal-lg">
        <div class="modal-content"></div>
        <script type="text/template" class="modal-template">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span><span class="sr-only">Close</span>
                </button>
                <h3 class="modal-title"><?= $title ?></h3>
            </div>
            <div class="modal-body">
                <h4><?= $message ?></h4>
            </div>
            <div class="modal-footer">
                <button type="button" data-loading-text="Deleting..."
                        autocomplete="off" class="btn btn-danger deleteBtn">
                    Удалить
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
            </div>
        </script>
    </div>
</div>