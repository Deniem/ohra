<?php
namespace backend\components\controllers\page;

use Yii;
use yii\base\Action;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Base class for all page controllers in the project
 *
 * @package app\components
 */
class BaseController extends Controller
{
    /**
     * @var  array
     */
    public $pageData = [];
	public $notifications;

    /**
     * @var  Session
     */
    public $session;

    /**
     * Before the action is being executed, this function prepares session object.
     *
     * @param   Action  $action  Action to be executed
     *
     * @return  bool  True to proceed to the action; false to stop processing the request
     *
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {

        if (parent::beforeAction($action))
        {
	        $this->notifications = Yii::$app->notifications;
			$this->pageData['notifications'] =$this->notifications->getNotifications();

            if(!Yii::$app->user->isGuest){
                $this->pageData['user']['id'] = Yii::$app->user->identity->id;
                $this->pageData['user']['status'] = Yii::$app->user->identity->status;
                $this->pageData['user']['email'] = Yii::$app->user->identity->email;
                $this->pageData['user']['username'] = Yii::$app->user->identity->username;
            }

            return true;
        }
        return false;

    }

    /**
     * By default all controllers inside the modules must allow access only to the users who have Access permission to
     * the module. Controller must explicitly override this method if it needs to grant access to some or all of its
     * actions to other users or API calls.
     *
     * @return  array  An array describing restrictions
     */
    /*public function behaviors()
    {
        if ($this->module && $this->module->hasMethod('getAppCode'))
        {
            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => [$this->module->getAppCode() . ':Access']
                        ]
                    ]
                ]
            ];
        }
    }*/
}
