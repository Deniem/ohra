<?php

namespace backend\controllers;

use common\models\db\Discount;
use League\Flysystem\Exception;
use Yii;
use common\components\controllers\page\BaseController;
use backend\models\search\ProductSearch;
use \common\models\db\Product;
use \common\models\db\Category;
use common\models\db\Brand;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;

/**
 * ArticleController implements the CRUD actions for Article model.
 */
class ProductController extends BaseController
{
	/**
	 * Lists all Article models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new ProductSearch();

		$productsQuery = $searchModel->search(Yii::$app->request->queryParams);

		$countQuery = clone $productsQuery;
		$pageSize = Yii::$app->request->get('per-page');
		$pageSize = $pageSize ? $pageSize : Yii::$app->params['defaultPageSize'];
		$pages = new Pagination([
			'totalCount' => $countQuery->count(),
			'pageSize' => $pageSize
		]);
		$products = $productsQuery->offset($pages->offset)
			->limit($pages->limit)
			->all();

		$categoriesFilter = ['-1' => 'Корневая'];
		$categoriesFilter += \yii\helpers\ArrayHelper::map(\common\models\db\Category::find()->all(), 'id', 'name');

		return $this->render('index', [
			'searchModel' => $searchModel,
			'products' => $products,
			'categoriesFilter' => $categoriesFilter,
			'pages' => $pages,
			'categoriesFilter' => \yii\helpers\ArrayHelper::map(\common\models\db\Category::find()->all(), 'id', 'name')
		]);
	}

	/**
	 * Creates a new Article model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Product();

		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			$this->notifications->setNotifications($this->notifications->success, 'Товар '. $model->name . ' был успешно создан');
			return $this->redirect(['index']);
		} else {
			$dataPickerClientOptions =  $this->getDefaultDataPickerOptions();

			$this->pageData['product'] = $model->toArray();
			$this->pageData['discount'] = null;
			$this->pageData['discountDateTime'] = $dataPickerClientOptions;
			return $this->render('create', [
				'model' => $model,
				'categories' => Category::findAll(['status'=>Category::$STATUS_ACTIVE]),
				'brands' => Brand::find()->all(),
				'dataPickerClientOptions' => $dataPickerClientOptions,
			]);
		}
	}

	/**
	 * Updates an existing Article model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
		
		$discount = Discount::find()
			->where(['prod_id' => $model->id])
			->asArray()
			->one();

		$dataPickerClientOptions =  $this->getDefaultDataPickerOptions();

		$this->pageData['product'] = $model->toArray();
		$this->pageData['discount'] = $discount;
		$this->pageData['discountDateTime'] = $dataPickerClientOptions;

		if (Yii::$app->request->post()) {
			$params = Yii::$app->request->post();
			$model->load($params);
			if ($model->save()) {
				$this->notifications->setNotifications($this->notifications->success, 'Товар "'.$model->name.'" успешно сохранен.');
				return $this->redirect(['index']);
			}
		} else {
			$this->notifications->setNotifications($this->notifications->error, 'Товар "'.$model->name.'" не обновлен.');
			return $this->render('update', [
				'model' => $model,
				'categories' => Category::findAll(['status'=>Category::$STATUS_ACTIVE]),
				'brands' => Brand::find()->all(),
				'dataPickerClientOptions' => $dataPickerClientOptions,
			]);
		}
	}

	/**
	 * Deletes an existing Article model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$model = $this->findModel($id);
		$model->status = Product::$STATUS_DELETED;

		if (!$model->save()) {
			$this->notifications->setNotifications($this->notifications->error, 'Товар "'.$model->name.'" не был удален.');
		} else {
			$this->notifications->setNotifications($this->notifications->success, 'Товар "'.$model->name.'" помечен как удаленный.');
		}

		return $this->redirect(['index']);
	}

	/**
	 * Finds the Article model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Product the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Product::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	private function getDefaultDataPickerOptions(){
		return [
			'format' => 'YYYY-MM-DD HH:mm:ss',
			'allowInputToggle' => false,
			'sideBySide' => true,
			'locale' => 'ru-ru	',
			'widgetPositioning' => [
				'horizontal' => 'auto',
				'vertical' => 'auto'
			]
		];
	}
}
