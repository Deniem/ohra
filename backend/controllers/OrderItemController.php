<?php

namespace backend\controllers;


use common\models\db\OrderItem;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;

class OrderItemController extends ActiveController
{
    public $modelClass = 'common\models\db\OrderItem';

    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['prepareDataProvider'] = [$this, 'prepareData'];

        $actions['create']['class'] = '\backend\controllers\api\orderitem\CreateAction';
        $actions['update']['class'] = '\backend\controllers\api\orderitem\UpdateAction';
        $actions['delete']['class'] = '\backend\controllers\api\orderitem\DeleteAction';
        return $actions;
    }

    public function prepareData(){

        $orderItems = OrderItem::find();
        $params = \Yii::$app->request->get();

        if (isset($params['orderId']) && $params['orderId'] != '') {
            $orderItems->where(['order_id' => $params['orderId']]);
            $counter = clone $orderItems;
            return new ActiveDataProvider([
                'query' => $orderItems,
                'pagination' => [
                    'pageSize' => $counter->count(),
                ],
            ]);
        }else{
            throw new BadRequestHttpException();
        }

    }

}