<?php
/**
 * Created by PhpStorm.
 * User: bbyte
 * Date: 6/13/16
 * Time: 5:44 PM
 */

namespace backend\controllers;


use common\components\controllers\page\BaseController;
use common\models\db\Order;
use common\models\db\OrderOptions;
use yii\data\Pagination;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\ServerErrorHttpException;


class OrderController extends BaseController
{
    public function actionIndex(){
        $provider = null;
		$orderStatusFilter = strtolower(\Yii::$app->request->get('order-status-filter'));
        if($orderStatusFilter){
            if($orderStatusFilter == 'active'){
                $provider = $this->showActiveOrders();
            }elseif ($orderStatusFilter == 'all'){
                $provider = Order::find();
            }else{
                $provider = Order::find()
                    ->where(['order.status' => $orderStatusFilter]);
            }
        }else{
            $provider = $this->showActiveOrders();
        }

		$this->pageData['orderStatusFiler'] = $orderStatusFilter;

        $provider = $provider->with('customers');
        $countc = clone $provider;
		
        $pages = new Pagination([
            'totalCount' => $countc->count(),
            'pageSize' => 10
        ]);
        $provider = $provider->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        return $this->render('index', [
            'orders' => $provider,
            'pagination' => $pages
        ]);
    }

    public function actionUpdate($id){
        $order = $this->findModel($id);
		$this->pageData['order'] = $order->toArray();
		$this->pageData['order']['statusLabels'] = OrderOptions::getStatusOptions();
        if(\Yii::$app->request->post()){
            $order->load(\Yii::$app->request->post());
            if ($order->save()) {
                $this->notifications->setNotifications($this->notifications->success, 'Заказ был успешно обновлен.');
                return $this->redirect(['order/update','id'=>$id]);
            }else{
                $this->notifications->setNotifications($this->notifications->error, 'Не удалось обновить заказ. Попробуйте позже');
                return $this->redirect(['orders']);
            }
        }else{
            return $this->render('update', [
                'model' => $order
            ]);
        }
    }

    public function actionDelete($id){
        $model = $this->findModel($id);
        
        foreach ($model->orderItems as $item){
            $item->delete();
        }
        $model->delete();
        $this->notifications->setNotifications($this->notifications->success, 'Заказ был успешно удален.');
        return $this->redirect(['index']);
    }

    private function findModel($id){
        $order = Order::find()
            ->where(['order.id' => $id])
            ->with('orderItems')
            ->one();

        if($order !== null){
            return $order;
        }else{
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

	public function actionStatus($id){
		$params = \Yii::$app->request->getBodyParams();

		$order = Order::findOne($id);
		$order->status = $params['status'];

		if (!$order->save()) {
			throw new ServerErrorHttpException();
		}
		\Yii::$app->response->format = Response::FORMAT_JSON;
		return $order;
	}

    private function showActiveOrders(){
        // show new orders
        return $provider = Order::find()
          /*  ->leftJoin('order_items', 'order_items.order_id = order.id')
            ->where(['!=','order.status', 'complete'])
            ->andWhere(['!=','order.status', 'rejected'])*/;
    }

}