<?php
namespace backend\controllers\api;


use yii\rest\ActiveController;

class BrandController extends ActiveController
{
    public $modelClass = 'common\models\db\base\BrandBase';

    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['prepareDataProvider'] = [$this, 'prepareData'];

        $actions['create']['class'] = '\backend\controllers\api\brand\CreateAction';
        $actions['update']['class'] = '\backend\controllers\api\brand\UpdateAction';
        $actions['delete']['class'] = '\backend\controllers\api\brand\DeleteAction';
        return $actions;
    }
}