<?php
namespace backend\controllers\api\settings;

use common\models\db\Settings;

use Yii;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;

class DeleteAction extends \yii\rest\DeleteAction
{
	/**
	 * Creates a new model.
	 *
	 * @return  \yii\db\ActiveRecordInterface  The model newly created
	 *
	 * @throws  \Exception  If there is any error when creating the model
	 */
	public function run($id)
	{
		throw new ServerErrorHttpException('Unable to delete settings');
		$model = Settings::findone(['id'=>$id]);

		if (!$model) throw new ServerErrorHttpException('Unable to find settings');

		if (!$model->delete()) {
			throw new ServerErrorHttpException('Unable to delete settings');
		}
		return $model;
	}


}
