<?php
namespace backend\controllers\api\productProperty;

use common\models\db\ProductProperty;
use common\models\db\Property;

use Yii;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;

class UpdateAction extends \yii\rest\UpdateAction
{
	/**
	 * Creates a new model.
	 *
	 * @return  \yii\db\ActiveRecordInterface  The model newly created
	 *
	 * @throws  \Exception  If there is any error when creating the model
	 */
	public function run($id)
	{
        $params = Yii::$app->request->getBodyParams();

		$property = ProductProperty::findOne($id);

        $property->setAttributes($params);
        if ($property->validate()) {
            if (!$property->save()) {
                throw new ServerErrorHttpException('Unable to save new property');
            }
            return $property;
        } else {
            throw new ServerErrorHttpException('Invalid params');
        }
	}

}
