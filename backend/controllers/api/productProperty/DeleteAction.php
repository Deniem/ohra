<?php
namespace backend\controllers\api\productProperty;

use common\models\db\ProductProperty;

use Yii;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;

class DeleteAction extends \yii\rest\CreateAction
{
	/**
	 * Creates a new model.
	 *
	 * @return  \yii\db\ActiveRecordInterface  The model newly created
	 *
	 * @throws  \Exception  If there is any error when creating the model
	 */
	public function run($id)
	{
		$model = ProductProperty::findone(['id'=>$id]);

		if (!$model) throw new ServerErrorHttpException('Unable to find property');

		if (!$model->delete()) {
			throw new ServerErrorHttpException('Unable to delete property');
		}
		return $model;
	}


}
