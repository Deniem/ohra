<?php
namespace backend\controllers\api\productProperty;

use common\models\db\ProductProperty;

use common\models\db\Property;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;

class CreateAction extends \yii\rest\CreateAction
{
	/**
	 * Creates a new model.
	 *
	 * @return  \yii\db\ActiveRecordInterface  The model newly created
	 *
	 * @throws  \Exception  If there is any error when creating the model
	 */
	public function run()
	{
        $params = Yii::$app->request->getBodyParams();
        $model = new ProductProperty();

		if (isset($params['id_property'])) {
			$oldRelations = ProductProperty::findBySql('select * from product_property 
					where id_value
 					IN (select id from value where id_property=:id_property)
 					and id_product =:id_product ')
				->params([
					':id_property' => $params['id_property'],
					':id_product' => $params['id_product']
				])
				->all();
			foreach ($oldRelations as $oldRelation) {
				$oldRelation->delete();
			}
		}

		if (isset($params['id_value']) && $params['id_value'] != 0) {
			$model->setAttributes($params);
			if ($model->validate()) {
				if (!$model->save()) {
					throw new ServerErrorHttpException('Unable to save new product-property relation');
				}
				return $model;
			} else {
				throw new ServerErrorHttpException('Invalid params');
			}
		}


	}

}
