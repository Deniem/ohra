<?php
namespace backend\controllers\api;


use yii\rest\ActiveController;

class OrderController extends ActiveController
{
    public $modelClass = 'common\models\db\base\OrderBase';

    public function actions()
    {
        $actions = parent::actions();
        $actions['delete']['class'] = '\backend\controllers\api\order\DeleteAction';
        return $actions;
    }

}