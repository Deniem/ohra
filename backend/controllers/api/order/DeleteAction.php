<?php

namespace backend\controllers\api\order;

use common\models\db\Order;
use Yii;
use yii\web\ServerErrorHttpException;

class DeleteAction extends \yii\rest\DeleteAction
{
    public function run($id)
    {
        $order = Order::find()
            ->where(['order.id' => $id])
            ->with('orderItems')
            ->one();

        if($order !== null){
            foreach ($order->orderItems as $item){
                $item->delete();
            }

            if ($order->delete() === false) {
                throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
            }

            Yii::$app->getResponse()->setStatusCode(204);
        }
    }
}
