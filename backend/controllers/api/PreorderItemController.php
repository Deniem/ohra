<?php
/**
 * Created by PhpStorm.
 * User: bitybite
 * Date: 7/13/16
 * Time: 1:59 AM
 */

namespace backend\controllers\api;


use yii\rest\ActiveController;

class PreorderItemController extends ActiveController
{

    public $modelClass = 'common\models\db\base\PreorderItemBase';

    public function actions()
    {
        $actions = parent::actions();

        $actions['view']['class'] = '\backend\controllers\api\preorderItem\ViewAction';
        return $actions;
    }

}