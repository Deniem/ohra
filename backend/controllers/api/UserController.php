<?php

namespace backend\controllers\api;

use common\models\db\User;
use Yii;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

class UserController extends ActiveController
{

	public $modelClass = 'backend\models\User';


	public function actions()
	{
		$actions = parent::actions();

		$actions['index']['prepareDataProvider'] = [$this, 'prepareData'];

		$actions['create']['class'] = '\backend\controllers\api\settings\CreateAction';
		$actions['update']['class'] = '\backend\controllers\api\settings\UpdateAction';
		$actions['delete']['class'] = '\backend\controllers\api\settings\DeleteAction';
		return $actions;
	}

	public function prepareData(){
		$query = Settings::find();
		$params = Yii::$app->request->get();

		if (isset($params['type']) && $params['type'] != '') {
			$type = $params['type'];
			$query->where(['type' => $type]);
		}

		return new ActiveDataProvider([
			'query' => $query
		]);
	}

}
