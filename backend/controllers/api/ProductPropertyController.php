<?php

namespace backend\controllers\api;

use common\models\db\ProductProperty;
use Yii;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

class ProductPropertyController extends ActiveController
{

	public $modelClass = 'common\models\db\base\ProductPropertyBase';


	public function actions()
	{
		$actions = parent::actions();

		$actions['index']['prepareDataProvider'] = [$this, 'prepareData'];

		$actions['create']['class'] = '\backend\controllers\api\productProperty\CreateAction';
		$actions['update']['class'] = '\backend\controllers\api\productProperty\UpdateAction';
		$actions['delete']['class'] = '\backend\controllers\api\productProperty\DeleteAction';
		return $actions;
	}

	public function prepareData(){
		$query = ProductProperty::find();
		$params = Yii::$app->request->get();

		if (isset($params['idProduct']) && $params['idProduct'] != '') {
			$idProduct = $params['idProduct'];
			$query->where(['id_product' => $idProduct]);
		}

		return new ActiveDataProvider([
			'query' => $query
		]);
	}

}
