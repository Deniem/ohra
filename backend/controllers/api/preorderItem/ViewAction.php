<?php
namespace backend\controllers\api\preorderItem;

use common\models\db\PreorderItem;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;

class ViewAction extends \yii\rest\ViewAction
{
    public function run($id)
    {

        $provider = new ActiveDataProvider([
            'query' => PreorderItem::find()
                ->where(['preorder_id' => $id])
                ->with('product')
                ->asArray(),
            'pagination' => [
                'defaultPageSize' => 5
            ]
        ]);

        return [
            'pagination' => $provider->pagination,
            'items' => $provider->getModels()
        ];
    }
}