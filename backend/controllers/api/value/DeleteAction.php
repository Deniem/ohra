<?php
namespace backend\controllers\api\value;

use common\models\db\Value;

use Yii;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;

class DeleteAction extends \yii\rest\DeleteAction
{
	/**
	 * Creates a new model.
	 *
	 * @return  \yii\db\ActiveRecordInterface  The model newly created
	 *
	 * @throws  \Exception  If there is any error when creating the model
	 */
	public function run($id)
	{

		$model = Value::findone(['id'=>$id]);
		$model->name = null;

		if (!$model) throw new ServerErrorHttpException('Unable to find value');

		if (!$model->save()) {
			throw new ServerErrorHttpException('Unable to delete value');
		}
		return $model;
	}


}
