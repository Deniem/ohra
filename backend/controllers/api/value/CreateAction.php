<?php
namespace backend\controllers\api\value;

use common\models\db\Value;
use common\models\db\ProductProperty;

use Yii;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;

class CreateAction extends \yii\rest\CreateAction
{
	/**
	 * Creates a new model.
	 *
	 * @return  \yii\db\ActiveRecordInterface  The model newly created
	 *
	 * @throws  \Exception  If there is any error when creating the model
	 */
	public function run()
	{
        $params = Yii::$app->request->getBodyParams();

        $model = new Value();
		$model->setAttributes($params);

        if ($model->validate()) {
            if (!$model->save()) {
                throw new ServerErrorHttpException('Unable to save new value');
            }
            return $model;
        } else {
            throw new ServerErrorHttpException('Invalid params');
        }
	}

}
