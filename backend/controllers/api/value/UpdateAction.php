<?php
namespace backend\controllers\api\value;

use common\models\db\Value;

use Yii;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;

class UpdateAction extends \yii\rest\UpdateAction
{
	/**
	 * Creates a new model.
	 *
	 * @return  \yii\db\ActiveRecordInterface  The model newly created
	 *
	 * @throws  \Exception  If there is any error when creating the model
	 */
	public function run($id)
	{
        $params = Yii::$app->request->getBodyParams();

        $model = Value::findOne($id);

        $model->setAttributes($params);

        if ($model->validate()) {
            if (!$model->save()) {
                throw new ServerErrorHttpException('Unable to save new property');
            }
            return $model;
        } else {
            throw new ServerErrorHttpException('Invalid params');
        }
	}

}
