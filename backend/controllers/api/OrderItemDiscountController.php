<?php
/**
 * Created by PhpStorm.
 * User: bitybite
 * Date: 8/8/16
 * Time: 5:58 PM
 */

namespace backend\controllers\api;


use yii\rest\ActiveController;

class OrderItemDiscountController extends ActiveController
{
    public $modelClass = 'common\models\db\base\OrderItemDiscountBase';

    public function actions()
    {
        $actions['index']['class'] = '\backend\controllers\api\orderItemDiscount\ViewAction';
        $actions['create']['class'] = '\backend\controllers\api\orderItemDiscount\CreateAction';
        $actions['delete']['class'] = '\backend\controllers\api\orderItemDiscount\DeleteAction';
//        $actions['update']['class'] = '\backend\controllers\api\orderItemDiscount\UpdateAction';
        return $actions;
    }
}