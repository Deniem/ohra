<?php
/**
 * Created by PhpStorm.
 * User: bitybite
 * Date: 8/10/16
 * Time: 7:31 PM
 */

namespace backend\controllers\api;


use common\models\db\Category;
use common\models\db\Product;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\rest\ActiveController;

class CategoryController extends ActiveController
{
    public $modelClass = 'common\models\db\Category';

    public function actions()
    {
        $actions = parent::actions();
        $actions['index']['prepareDataProvider'] = [$this, 'prepareData'];
        unset($actions['view']);
        return $actions;
    }

    public function prepareData()
    {
        //Select categories that have not inner categories
        $query = Category::find()
            ->select(['id', 'name'])
            ->where(['status' => Category::$STATUS_ACTIVE])
            ->andWhere(['not', ['id' =>
                Category::find()
                    ->select(['id_parent'])
                    ->where(['>' , 'id_parent', 0])
                    ->asArray()
                ]
            ]);

        return new ActiveDataProvider([
            'query' => $query
        ]);
    }

    public function actionView($id){

        $query = Product::find()
            ->select(['id', 'name'])
            ->where(['status' => Product::$STATUS_ACTIVE])
            ->andWhere(['id_category' => $id]);

        return new ActiveDataProvider([
            'query' => $query
        ]);
    }
}