<?php

namespace backend\controllers\api;


use yii\rest\ActiveController;

class BlogController extends ActiveController
{

    public $modelClass = 'common\models\db\Blog';

    public function actions()
    {
        $actions = parent::actions();
        
//        $actions['create']['class'] = '\backend\controllers\api\blog\CreateAction';
//        $actions['update']['class'] = '\backend\controllers\api\blog\UpdateAction';
        $actions['delete']['class'] = '\backend\controllers\api\blog\DeleteAction';
        return $actions;
    }
}