<?php
/**
 * Created by PhpStorm.
 * User: bitybite
 * Date: 8/10/16
 * Time: 8:38 PM
 */

namespace backend\controllers\api;


use common\models\db\Product;
use yii\data\ActiveDataProvider;
use yii\rest\ActiveController;

class ProductController  extends ActiveController
{
    public $modelClass = 'common\models\db\Product';

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['view']);
        return $actions;
    }

    public function actionView($id)
    {
        return Product::find()
            ->where(['id' => $id])
            ->with('brand', 'discount', 'category',
                'productProperties', 'productValues')
            ->asArray()
            ->one();
    }
}