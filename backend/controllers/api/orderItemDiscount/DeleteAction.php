<?php

namespace backend\controllers\api\orderItemDiscount;


class DeleteAction extends \yii\rest\DeleteAction
{
    public $modelClass = 'common\models\db\base\OrderItemDiscountBase';
    public function run($id)
    {
        parent::run($id);
    }


}