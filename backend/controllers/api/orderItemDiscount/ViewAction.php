<?php
namespace backend\controllers\api\orderItemDiscount;

use common\models\db\OrderItemDiscount;

class ViewAction extends \yii\rest\ViewAction
{
    public $modelClass = 'common\models\db\base\OrderItemDiscountBase';

    public function run($orderItemsIds)
    {
        if(strpos($orderItemsIds, ';')){
            $orderItemsIds = explode(';', $orderItemsIds);
        }
        $model = OrderItemDiscount::find()
            ->where(['order_item_id' => $orderItemsIds])
            ->all();
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        return $model;
    }

}