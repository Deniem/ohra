<?php

namespace backend\controllers\api\orderItemDiscount;


class CreateAction extends \yii\rest\CreateAction
{
    public $modelClass = 'common\models\db\base\OrderItemDiscountBase';
    public function run()
    {
        return parent::run();
    }

}