<?php
namespace backend\controllers\api\discount;


use Yii;
use yii\web\ServerErrorHttpException;

class UpdateAction extends \yii\rest\UpdateAction
{
    public $modelClass = 'common\models\db\base\DiscountBase';

    public function run($id)
    {
        return parent::run($id);
    }

}