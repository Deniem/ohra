<?php
namespace backend\controllers\api\discount;


use Yii;

class CreateAction extends \yii\rest\CreateAction
{
    public $modelClass = 'common\models\db\base\DiscountBase';

    public function run()
    {
        return parent::run();
    }

}