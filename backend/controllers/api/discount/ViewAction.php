<?php

namespace backend\controllers\api\discount;

use common\models\db\Discount;

class ViewAction extends \yii\rest\ViewAction
{
    public $modelClass = 'common\models\db\base\DiscountBase';

    public function run($prod_id)
    {
        if(strpos($prod_id, ';')){
            $prod_id = explode(';', $prod_id);
        }
        $model = Discount::find()
        ->where(['prod_id' => $prod_id])
        ->all();
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        return $model;
    }

}