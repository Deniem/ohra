<?php

namespace backend\controllers\api\discount;


class DeleteAction extends \yii\rest\DeleteAction
{
    public $modelClass = 'common\models\db\base\DiscountBase';

    public function run($id)
    {
        parent::run($id);
    }

}