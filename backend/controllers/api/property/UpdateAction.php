<?php
namespace backend\controllers\api\property;

use common\models\db\Property;

use Yii;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;

class UpdateAction extends \yii\rest\UpdateAction
{
	/**
	 * Creates a new model.
	 *
	 * @return  \yii\db\ActiveRecordInterface  The model newly created
	 *
	 * @throws  \Exception  If there is any error when creating the model
	 */
	public function run($id)
	{
        $params = Yii::$app->request->getBodyParams();

        if ($this->isDuplicated($params)) {
            throw new ServerErrorHttpException('Duplicated entry or empty params');
        }

       return parent::run($id);
	}

    private function isDuplicated($params){
        $property = Property::findOne(['name' => $params['name'],'id_unit' => $params['id_unit'], 'id_category' => $params['id_category'], 'is_filter' => $params['is_filter']]);
        return $property !== null;
    }
}
