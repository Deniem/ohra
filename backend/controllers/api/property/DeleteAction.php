<?php
namespace backend\controllers\api\property;

use common\models\db\Property;

use Yii;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;

class DeleteAction extends \yii\rest\DeleteAction
{
	/**
	 * Creates a new model.
	 *
	 * @return  \yii\db\ActiveRecordInterface  The model newly created
	 *
	 * @throws  \Exception  If there is any error when creating the model
	 */
	public function run($id)
	{
		/*$model = Property::findone(['id'=>$id]);

		if (!$model) throw new ServerErrorHttpException('Unable to find property');

		if (!$model->delete()) {
			throw new ServerErrorHttpException('Unable to delete property');
		}
		return $model;*/

		return parent::run($id);
	}
}
