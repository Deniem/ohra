<?php
/**
 * Created by PhpStorm.
 * User: bitybite
 * Date: 8/8/16
 * Time: 9:34 AM
 */

namespace backend\controllers\api;


use yii\rest\ActiveController;

class DiscountController extends ActiveController
{
    public $modelClass = 'common\models\db\base\DiscountBase';

    public function actions()
    {
        $actions['index']['class'] = '\backend\controllers\api\discount\ViewAction';
        $actions['create']['class'] = '\backend\controllers\api\discount\CreateAction';
        $actions['update']['class'] = '\backend\controllers\api\discount\UpdateAction';
        $actions['delete']['class'] = '\backend\controllers\api\discount\DeleteAction';
        return $actions;
    }
}