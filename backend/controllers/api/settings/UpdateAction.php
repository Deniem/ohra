<?php
namespace backend\controllers\api\settings;

use common\models\db\Property;

use common\models\db\Settings;
use Yii;
use yii\web\BadRequestHttpException;
use yii\web\ServerErrorHttpException;

class UpdateAction extends \yii\rest\UpdateAction
{
	/**
	 * Creates a new model.
	 *
	 * @return  \yii\db\ActiveRecordInterface  The model newly created
	 *
	 * @throws  \Exception  If there is any error when creating the model
	 */
	public function run($id)
	{
        $params = Yii::$app->request->getBodyParams();

        return parent::run($id);
	}
}
