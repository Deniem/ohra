<?php
/**
 * Created by PhpStorm.
 * User: bbyte
 * Date: 8/4/16
 * Time: 5:24 PM
 */

namespace backend\controllers;


use backend\components\controllers\page\BaseController;
use common\models\db\Order;

class StatisticController extends BaseController
{

    public function actionOrders(){

		$orders = Order::find()
			->with('customers', 'orderItems.product')
			->where(['status' => Order::$STATUS_COMPLETE])
			->orderBy('updated_at', 'DESC')
			->all();
		return $this->render('orders', [
			'orders' => $orders
		]);
    }

    public function actionWatched(){
        return $this->render('watched');
    }

    public function actionLists(){
        return $this->render('lists');
    }


}