<?php

namespace backend\controllers;
use common\components\controllers\page\BaseController;

class CabinetController extends BaseController
{
    public function actionIndex()
    {
        return $this->render('index');
    }

}
