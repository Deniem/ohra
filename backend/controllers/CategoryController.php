<?php

namespace backend\controllers;

use common\components\controllers\page\BaseController;

use common\models\db\Property;
use Yii;
use yii\base\Action;
use yii\base\Exception;
use yii\filters\AccessControl;
use backend\models\search\CategorySearch;
use \common\models\db\Category;
use yii\web\NotFoundHttpException;
use yii\data\Pagination;



/**
 * ArticleController implements the CRUD actions for Article model.
 */
class CategoryController extends BaseController
{

	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'allow' => true,
						'actions' => ['login'],
						'roles' => ['?'],
					],
					[
						'allow' => true,
						'roles' => ['@']
					]
				]
			]
		];
	}


	public function beforeAction($action)
	{
//        AssetManager::register('site');
		return parent::beforeAction($action);
	}
	/**
	 * Lists all Article models.
	 * @return mixed
	 */
	public function actionIndex()
	{
		$searchModel = new CategorySearch();
		$categoriesQuery = $searchModel->search(Yii::$app->request->queryParams);

		$countQuery = clone $categoriesQuery;
		$pageSize = Yii::$app->params['defaultPageSize'];
		$pages = new Pagination([
			'totalCount' => $countQuery->count(),
			'pageSize' => $pageSize
		]);
		$categories = $categoriesQuery->offset($pages->offset)
			->limit($pages->limit)
			->all();
		$categoriesFilter = ['-1' => 'Корневая'];
		$categoriesFilter += \yii\helpers\ArrayHelper::map(\common\models\db\Category::find()->all(), 'id', 'name');

		return $this->render('index', [
			'searchModel' => $searchModel,
			'categories' => $categories,
			'pages' => $pages,
			'categoriesFilter' => $categoriesFilter
		]);
	}

	/**
	 * Creates a new Article model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate()
	{
		$model = new Category();
		if (Yii::$app->request->post()) {
			$params = Yii::$app->request->post();
			$params['Category']['show_on_main']  =  isset($params['Category']['show_on_main']) ? 1 : 0;
			$model->load($params);
			if ($model->save()) {
				$this->notifications->setNotifications($this->notifications->success, 'Категория "'.$model->name.'" успешно создана.');
				return $this->redirect(['index']);

			}
			$this->pageData['debug']['model']['errors'] = $model->errors;
			$this->notifications->setNotifications($this->notifications->error, 'Произошла ошибка. Попробуйте позже.');
		}
		return $this->render('create', [
			'model' => $model,
			'categories' => Category::findAll(['status'=>Category::$STATUS_ACTIVE]),
		]);
	}

	/**
	 * Updates an existing Article model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionUpdate($id)
	{
		$model = $this->findModel($id);
        $properties = Property::findAll(['id_category'=>$id]);

		$this->pageData['category'] = $model->toArray();

		if (Yii::$app->request->post()) {
			$params = Yii::$app->request->post();
			$params['Category']['show_on_main']  =  isset($params['Category']['show_on_main']) ? 1 : 0;
			$model->load($params);
			if ($model->save()) {
				$this->notifications->setNotifications($this->notifications->success, 'Категория "'.$model->name.'" успешно обновлена.');
				return $this->redirect(['index']);
			}
			$this->notifications->setNotifications($this->notifications->error, 'Произошла ошибка. Попробуйте позже.');
		} else {
			return $this->render('update', [
				'model' => $model,
				'properties' => $properties,
				'categories' => Category::findAll(['status'=>Category::$STATUS_ACTIVE]),
			]);
		}
	}

	/**
	 * Deletes an existing Article model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 * @param integer $id
	 * @return mixed
	 */
	public function actionDelete($id)
	{
		$model = $this->findModel($id);
		$products = $model->products;

		$errorMsg = '';

		if (count($products) == 0 && !$model->hasChilds()) {
			$model->status = Category::$STATUS_DELETED;
			$this->notifications->setNotifications($this->notifications->success, 'Категория "'.$model->name.'" помечена как удаленная.');
			if (!$model->save()) {
				$this->notifications->setNotifications($this->notifications->success, 'Произошла ошибка. Попробуйте позже.');
			}
		} else {
			$errorMsg .= count($products) ? ' товары ' : ' ';
			$errorMsg .= $model->hasChilds() ? ' подкатегории.' : '.';
			$this->notifications->setNotifications($this->notifications->error, 'В категории "'.$model->name. '" есть ' . $errorMsg);
		}
		return $this->redirect(['index']);
	}

	public function actionFilter(){
		$categories = Category::find()->all();
		$this->pageData['categories'] = $categories;
		return $this->render('filter-list');
	}

	/**
	 * Finds the Article model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return Article the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Category::findOne($id)) !== null) {
			return $model;
		} else {
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}
