<?php

namespace backend\controllers;

use common\components\controllers\page\BaseController;
use common\models\db\Brand;
use yii\data\ActiveDataProvider;
use yii\web\BadRequestHttpException;

class BrandController extends BaseController
{
    public function actionCreate()
    {
        $model = new Brand();

        if(\Yii::$app->request->post()){
            if ($model->load(\Yii::$app->request->post()) && $model->save()) {
                $this->notifications->setNotifications($this->notifications->success, 'Бренд "'.$model->name.'" успешно сохранен.');
                return $this->redirect(['index']);
            }else{
                $this->notifications->setNotifications($this->notifications->error, 'Ошибка сохранения бренда');
                return $this->redirect('create');
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    public function actionIndex()
    {
        $brands = new ActiveDataProvider([
            'query' => Brand::find(),
            'pagination' => [
                'pageSize' => 10
            ]
        ]) ;

        return $this->render('index', [
			'brands' => $brands
		]);
    }

    public function actionUpdate($id)
    {
        $brand = Brand::find()
            ->where(['id' => $id])
            ->one();
        
        if($brand === null){
            throw new BadRequestHttpException();
        }

        $this->pageData['brand'] = $brand->toArray();
        if(\Yii::$app->request->post()){
            if ($brand->load(\Yii::$app->request->post()) && $brand->save()) {
                $this->notifications->setNotifications($this->notifications->success, 'Бренд "'.$brand->name.'" успешно сохранен.');
                return $this->redirect(['/brand']);
            }else{
                $this->notifications->setNotifications($this->notifications->error, 'Ошибка сохранения бренда "'.$brand->name);
                return $this->render('update', [
                    'model' => $brand
                ]);
            }
        }else{
            return $this->render('update', [
                'model' => $brand
            ]);
        }
    }

}
