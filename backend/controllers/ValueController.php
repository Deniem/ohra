<?php

namespace backend\controllers;

use Yii;
use common\models\db\Value;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

class ValueController extends ActiveController
{

	public $modelClass = 'common\models\db\base\Value';


	public function actions()
	{
		$actions = parent::actions();

		$actions['index']['prepareDataProvider'] = [$this, 'prepareData'];

		$actions['create']['class'] = '\backend\controllers\api\value\CreateAction';
		$actions['update']['class'] = '\backend\controllers\api\value\UpdateAction';
		$actions['delete']['class'] = '\backend\controllers\api\value\DeleteAction';
		return $actions;
	}

	public function prepareData(){
		$query = Value::find();
		$params = Yii::$app->request->get();


		if (isset($params['idCategory']) && $params['idCategory'] != '') {
			$idCategory = $params['idCategory'];
			$query->where(['id_category' => $idCategory]);
		}

		return new ActiveDataProvider([
			'query' => $query
		]);

	}

}
