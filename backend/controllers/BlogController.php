<?php
namespace backend\controllers;


use backend\components\controllers\page\BaseController;
use common\models\db\Blog;
use common\models\db\ContentPages;
use yii\data\ActiveDataProvider;
use yii\web\BadRequestHttpException;

class BlogController extends BaseController
{

    public function actionIndex(){

        $model = new ActiveDataProvider([
            'query' => Blog::find()
                ->orderBy(['published_at' => SORT_DESC]),
            'pagination' => [
                'pageSize' => 10
            ]
        ]);

        return $this->render('index', [
            'model' => $model
        ]);

    }

    public function actionUpdate($id){
        $model = Blog::find()
            ->where(['id' => $id])
            ->one();

        if($model === null){
            throw new BadRequestHttpException();
        }

        if(\Yii::$app->request->post()){
            if ($model->load(\Yii::$app->request->post()) && $model->save()) {
                $this->notifications->setNotifications($this->notifications->success, 'Запись "'.$model->title.'" успешно сохранена.');
                return $this->redirect(['/blog/index']);
            }else{
                $this->notifications->setNotifications($this->notifications->error, 'Ошибка сохранения записи "'.$model->title);
                return $this->render('update', [
                    'model' => $model
                ]);
            }
        }else{
            return $this->render('update', [
                'model' => $model
            ]);
        }
    }

    public function actionCreate(){
        $model = new Blog();

        if(\Yii::$app->request->post()){
            if ($model->load(\Yii::$app->request->post()) && $model->save()) {
                $this->notifications->setNotifications($this->notifications->success, 'Запись "'.$model->title.'" успешно сохранена.');
                return $this->redirect(['index']);
            }else{
                $this->notifications->setNotifications($this->notifications->error, 'Ошибка сохранения записи "'.$model->title);
                return $this->redirect('create');
            }
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
}