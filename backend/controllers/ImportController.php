<?php
namespace backend\controllers;


use common\components\controllers\page\BaseController;

use common\models\db\Product;
use ruskid\csvimporter\CSVReader;
use ruskid\csvimporter\MultipleUpdateStrategy;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use common\models\db\Settings;
use ruskid\csvimporter\CSVImporter;
use yii\web\ServerErrorHttpException;
use yii\web\UploadedFile;
use common\models\db\Category;
/**
 * Site controller
 */
class ImportController extends BaseController
{
    public $layout = 'main';

    private $file,
            $importFieldCount,
            $errors,
            $importDir;


    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }

    public function beforeAction($action)
    {
        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionImportInsert(){

        $this->importDir = Yii::getAlias('@webroot') . '/files/import';
        $this->file = UploadedFile::getInstanceByName('file');
        $importer = new CSVImporter();
        $importer->setData(new CSVReader([
            'filename' => $this->file->tempName,
            'fgetcsvOptions' => [
                'delimiter' => ";",
            ],
            'startFromLine' => 0,
        ]));

        $this->importFieldCount = 0;
        $this->errors = [];

        $csvHeaders = [];
        $importData = $importer->getData();
        $articles = [];
        $groups = [];

        foreach ($importData[0] as $key => $header) {
            if (trim($header) != '' && !empty($header)) {
                $this->importFieldCount++;
                $csvHeaders[] = $header;
            }
        };

        for ($i = 1; $i < count($importData); $i++) {
            if (isset($importData[$i][1]) && isset($importData[$i][2])) {
                $articles[] = '"' . trim($importData[$i][1]) . '"';
                $groups[] = '"' . trim($importData[$i][2]) . '"';
            }
        }

        $groupsToImport = Category::find()->where('`name` IN (' . implode(', ', array_unique($groups, SORT_STRING)) . ')')->count();
        $productsToImport = Product::find()->where('`article` IN (' . implode(', ', $articles) . ')')->count();

        if (!$this->file->saveAs($this->importDir . '/import.csv')) {
            throw  new ServerErrorHttpException('Cannot save import file on server');
        }

        return json_encode([
            'importInfo' => [
                'groups' => count(array_unique($groups, SORT_STRING)) - $groupsToImport,
                'products' => count($articles) - $productsToImport,
            ],
            'errors' => $this->errors,
            'fileInfo' => [
                'path' => $this->file->tempName,
                'rowCount' => count($importer->getData()) - 1,
                'csvHeaders' => $csvHeaders
            ],
        ]);

    }

    public function actionImportExecute(){

        $_file = Yii::getAlias('@webroot') . '/files/import/import.csv';
        if (!file_exists($_file)) throw new ServerErrorHttpException('Error while proceeding file');
        $importer = new CSVImporter();
        $importer->setData(new CSVReader([
            'filename' => $_file,
            'fgetcsvOptions' => [
                'delimiter' => ";",
            ],
        ]));

        $errors = [];
        $groups = [
            'total' => 0,
            'new' => 0
        ];
        $product = Product::find();

        $strategy = new MultipleUpdateStrategy([
            'className' => Product::className(),
            'csvKey' => function ($line) use ($errors) {
                if (!isset($line[1])) {
                    $errors[] = $line;
                    return '';
                }
                return $line[1];
            },
            'rowKey' => function ($row) {
                return $row['id'];
            },
            'skipImport' => function ($line) use ($errors, $product) {
                if (empty($line) || count($line) <= 1) return true;
                $productExist = $product->where(['article' => $line[1]])->one();
                return $productExist;
            },
            'configs' => [
                [
                    'attribute' => 'name',
                    'value' => function ($line) {
                        return trim($line[0]);
                    }
                ],
                [
                    'attribute' => 'article',
                    'value' => function ($line) {
                        return trim($line[1]);
                    },
                    'unique' => true
                ],
                [
                    'attribute' => 'id_category',
                    'value' => function ($line) use ($groups) {
                        $name = trim($line[2]);
                        $category = Category::findOne(['name'=>$name]);
                        $groups['total'] += 1;
                        if (!$category) {
                            $groups['new'] += 1;
                            $category = new Category([
                                'name' => $name,
                                'status' => Category::$STATUS_ACTIVE,
                                'show_on_main' => 0,
                            ]);
                            if (!$category->save()) {
                                throw new ServerErrorHttpException('Cannot save or update category');
                            }
                        }
                        return $category->id;
                    }
                ],
                [
                    'attribute' => 'count_left',
                    'value' => function ($line) {
                        return intval($line[3]);
                    }
                ],
                [
                    'attribute' => 'price_eur',
                    'value' => function ($line) {
                        return floatval($line[4]) > 0 ? floatval($line[4]) : null;
                    }
                ],
                [
                    'attribute' => 'price_usd',
                    'value' => function ($line) {
                        return floatval($line[5]) > 0 ? floatval($line[5]) : null;
                    }
                ],
            ]
        ]);

        $result = $importer->import($strategy);
        return json_encode($result);
    }

}
