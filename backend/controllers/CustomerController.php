<?php
/**
 * Created by PhpStorm.
 * User: bbyte
 * Date: 6/24/16
 * Time: 2:07 PM
 */

namespace backend\controllers;


use common\components\controllers\page\BaseController;
use common\models\db\Category;
use common\models\db\Order;
use common\models\db\OrderOptions;
use common\models\db\Preorder;
use common\models\db\Product;
use frontend\models\Customer;
use yii\data\ActiveDataProvider;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

class CustomerController extends BaseController
{

    public function actionIndex(){

        $customers = null;

        $req = \Yii::$app->request->get();
        if(array_key_exists('customer-status-filter', $req)){
            switch ($req['customer-status-filter']){
                case 'active':
                    $customers = $this->showActiveUsers();
                    break;
                case 'deleted':
                    $customers = Customer::find()->where(['status' => 0 ]);
                    break;
                case 'not_verified':
                    $customers = Customer::find()->where(['status' => 1 ]);
                    break;
                case 'all':
                    $customers = Customer::find();
                    break;
            }
        }else{
            $customers = $this->showActiveUsers();
        }


        $customers = new ActiveDataProvider([
            'query' => $customers
        ]);


        
        return $this->render('index', [
            'customers' => $customers
        ]);
    }

    private function showActiveUsers(){
        return Customer::find()
            ->where(['status' => 10 ]);
    }
    
    
    public function actionView($id){
        
        $customer = Customer::find()
            ->leftJoin('order', 'order.customer_id=customer.id')
            ->leftJoin('order_items', 'order_items.order_id=order.id')
            ->where(['customer.id' => $id])
            ->asArray()
            ->one();

		if (!$customer) throw new NotFoundHttpException('Пользователь не найден.');

        $favorites = Product::find()
            ->leftJoin('customer_favorite', 'customer_favorite.product_id = product.id')
            ->where(['customer_favorite.customer_id' => $id])
            ->asArray()
            ->all();

        $compare = Product::find()
            ->leftJoin('customer_compare', 'customer_compare.product_id = product.id')
            ->where(['customer_compare.customer_id' => $id])
            ->asArray()
            ->all();

        $orders = Order::find()
            ->where(['customer_id' => $id])
            ->with('orderItems')
            ->asArray()
            ->all();

        $preorders = Preorder::find()
            ->where(['customer_id' => $id])
            ->orderBy(['is_active' => SORT_DESC, 'updated_at' => SORT_DESC])
            ->asArray()
            ->all();

        $categories = Category::find()
            ->asArray()
            ->all();
        
        $this->pageData['customer'] = $customer;
        $this->pageData['orders'] = $orders;
        $this->pageData['customerFavorite'] = $favorites;
        $this->pageData['customerCompare'] = $compare;
        $this->pageData['categories'] = $categories;
        $this->pageData['preorders'] = $preorders;
        $this->pageData['orderOptions'] = OrderOptions::getAllOptions();
        $this->pageData['customerStatus'] = Customer::getStatusLabels();
        $this->pageData['productStatus'] = Product::getStatusOptions();

        return $this->render('view', [
            'customer' => $customer,
            'orders' => $orders
        ]);
    }
    
    public function actionUpdate($id){
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $model = Customer::find()
            ->where(['id' => $id])
            ->one();

        if ($model !== null) {
            if (\Yii::$app->request->post()) {
                $model->load(['Customer' => \Yii::$app->request->post()]);
                if ($model->save() !== false) {
                    $this->notifications->setNotifications($this->notifications->success, 'Пользователь был "'.$model->username.'" успешно обновлен.');
                }
            }else{
                throw new BadRequestHttpException();
            }
        }else{
            throw new ServerErrorHttpException('Unable to find customer');
        }

        return $model->toArray();
    }

}