<?php

namespace backend\controllers;

use common\components\controllers\page\BaseController;
use common\models\db\ContentPages;
use League\Uri\Components\Query;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class ContentPagesController extends BaseController
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['login'],
                        'roles' => ['?'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@']
                    ]
                ]
            ]
        ];
    }


    public function beforeAction($action)
    {
//        AssetManager::register('site');
        return parent::beforeAction($action);
    }

    public function actionCreate()
    {
        $model = new ContentPages();
        if($model->load(Yii::$app->request->post()) && $model->save()){
	        $this->notifications->setNotifications($this->notifications->success, 'Страница "'.$model->title.'" успешно создана.');
            return $this->redirect(['index']);
        }else{
            $parent_pages = ContentPages::find()
                ->select('id, title')
                ->where(['id_parent' => 0])
                ->andWhere(['status' => 1])
                ->asArray()->all();

            $parent_pages = [0 => 'Без вложенности'] + ArrayHelper::map($parent_pages, 'id', 'title');

            return $this->render('create', [
                'model' => $model,
                'parents' => $parent_pages
            ]);
        }
    }

    public function actionDelete($id)
    {
        $model = ContentPages::find()
            ->where(['id' => $id])
            ->andWhere(['can_delete' => 1])
            ->one();

	    if (!$model->delete()) {
		    $this->notifications->setNotifications($this->notifications->error, 'Произошла ошибка. Попробуйте позже.');
	    }
	    $this->notifications->setNotifications($this->notifications->success, 'Страница "'.$model->title.'" успешно удалена.');
        return $this->redirect(['index']);
    }

    public function actionIndex()
    {
        $contentPages = ContentPages::find()->orderBy('id')->all();

        return $this->render('index',[
            'contentPages' => $contentPages
        ]);
    }

    public function actionUpdate()
    {
        $id = Yii::$app->request->get('id');
        $model = ContentPages::find()
            ->where(['id' => $id])
            ->one();

        if($model->load(Yii::$app->request->post())){
            if($model->update() !== false){
	            $this->notifications->setNotifications($this->notifications->success, 'Страница "'.$model->title.'" успешно обновлена.');
                return $this->redirect(['index']);
            }
        }else if(Yii::$app->request->get()){
            $parent_pages = ContentPages::find()
                ->select('id, title')
//                ->where(['id_parent' => 0])
                ->andWhere(['!=', 'id', $id])
                ->andWhere(['status' => 1])
                ->asArray()->all();

            $parent_pages = [0 => 'Без вложенности'] + ArrayHelper::map($parent_pages, 'id', 'title');

            return $this->render('update', [
                'model' => $model,
                'parents' => $parent_pages
            ]);
        }else{
            throw new NotFoundHttpException('Page not found');
        }
    }

}
