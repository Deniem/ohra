<?php

namespace backend\controllers;

use Yii;
use common\models\db\Property;
use yii\rest\ActiveController;
use yii\data\ActiveDataProvider;

class PropertyController extends ActiveController
{

	public $modelClass = 'common\models\db\Property';


	public function actions()
	{
		$actions = parent::actions();

		$actions['index']['prepareDataProvider'] = [$this, 'prepareData'];

		$actions['create']['class'] = '\backend\controllers\api\property\CreateAction';
		$actions['update']['class'] = '\backend\controllers\api\property\UpdateAction';
//		$actions['delete']['class'] = '\backend\controllers\api\property\DeleteAction';
		return $actions;
	}

	public function prepareData(){
		$query = Property::find();
		$params = Yii::$app->request->get();

		if (isset($params['id_category']) && $params['id_category'] != '') {
			$idCategory = $params['id_category'];
			$query->where(['id_category' => $idCategory]);
		}

		return new ActiveDataProvider([
			'query' => $query
		]);
	}

}
