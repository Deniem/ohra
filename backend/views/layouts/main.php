<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
$currentController = Yii::$app->controller->id;
AppAsset::register($this);
\conquer\momentjs\MomentjsAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php if (!Yii::$app->user->isGuest): ?>
<!--        <meta http-equiv="refresh" content="--><?php //echo Yii::$app->params['sessionTimeoutSeconds'];?><!--;"/>-->
    <?php endif; ?>
    <?php $this->head() ?>
	<script type="application/javascript">
		var app = {
			config: {
				baseUrl: <?= \yii\helpers\Json::encode(\Yii::$app->homeUrl,0) ?>
			},
			data: <?= \yii\helpers\Json::encode(\Yii::$app->controller->pageData,0)  ?>
		};
	</script>
</head>
<body class="page-body">
<?php $this->beginBody() ?>
<div class="page-container">
    <div class="sidebar-menu">
        <div class="sidebar-menu-inner">
            <header class="logo-env">
                <!-- logo -->
                <div class="logo">
                    <a href="<?=Url::to('/')?>">
                        <img src="<?=Url::base(true)?>/images/logo@2x.png" width="120"alt=""/>
                    </a>
                </div>
                <!-- logo collapse icon -->
                <div class="sidebar-collapse">
                    <a href="#" class="sidebar-collapse-icon">
                        <i class="entypo-menu"></i>
                    </a>
                </div>
                <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
                <div class="sidebar-mobile-menu visible-xs">
                    <a href="#" class="with-animation">
                        <i class="entypo-menu"></i>
                    </a>
                </div>
            </header>
            <ul id="main-menu" class="main-menu">
                <li>
                    <a href="<?=Url::to(['/'])?>">
                        <i class="entypo-gauge"></i><span class="title">Главная</span>
                    </a>
                </li>
                <li>
                    <a href="<?=Yii::getAlias('@frontendUrl');?>" target="_blank">
                        <i class="entypo-monitor"></i><span class="title">Главная магазин</span>
                    </a>
                </li>
				<li class="has-sub <?= (in_array($currentController,['content-pages', 'blog'])) ? 'opened' : '';?>">
					<a href="#" target="_blank">
						<i class="entypo-feather"></i><span class="title">Контент</span>
					</a>
					<ul>
						<li>
							<a href="<?= Url::to(['/content-pages']);?>">
								<i class="entypo-book-open"></i><span class="title">Страницы</span>
							</a>
						</li>
						<li>
							<a href="<?= Url::to(['/blog']);?>">
								<i class="entypo-newspaper"></i><span class="title">Блог</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="has-sub <?= (in_array($currentController,['product', 'category','brand'])) ? 'opened' : '';?>">
					<a href="#" target="_blank">
						<i class="entypo-basket"></i><span class="title">Каталог</span>
					</a>
					<ul>
						<li>
							<a href="<?=Url::to(['/category']);?>">
								<i class="entypo-archive"></i><span class="title">Категории</span>
							</a>
						</li>
						<li>
							<a href="<?= Url::to(['/product']);?>">
								<i class="entypo-box"></i><span class="title">Товары</span>
							</a>
						</li>
						<li>
							<a href="<?= Url::to(['/brand']);?>">
								<i class="entypo-tag"></i><span class="title">Бренды</span>
							</a>
						</li>
						<li>
							<a href="<?= Url::to(['/discounts']);?>">
								<i class="entypo-bookmark"></i><span class="title">Скидки</span>
							</a>
						</li>
						<li>
							<a href="<?= Url::to(['/progress']);?>">
								<i class="entypo-traffic-cone"></i><span class="title">Товары месяца</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="has-sub <?= (in_array($currentController,['order'])) ? 'opened' : '';?>">
					<a href="#" target="_blank">
						<i class="entypo-credit-card"></i><span class="title">Продажи</span>
					</a>
					<ul>
						<li>
							<a href="<?= Url::to(['/orders']);?>">
								<i class="entypo-basket"></i><span class="title">Заказы</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="has-sub <?= (in_array($currentController,['statistic'])) ? 'opened' : '';?>">
					<a href="#" target="_blank">
						<i class="entypo-chart-bar"></i><span class="title">Статистика</span>
					</a>
					<ul>
						<li>
							<a href="<?= Url::to(['/statistic/orders']);?>">
								<i class="entypo-basket"></i><span class="title">Продажи</span>
							</a>
						</li>
						<li>
							<a href="<?= Url::to(['/statistic/watched']);?>">
								<i class="entypo-eye"></i><span class="title">Просмотры</span>
							</a>
						</li>
						<li>
							<a href="<?= Url::to(['/statistic/lists']);?>">
								<i class="entypo-book"></i><span class="title">Пользовательские списки</span>
							</a>
						</li>
					</ul>
				</li>
				<li class="has-sub <?= (in_array($currentController,['customer'])) ? 'opened' : '';?>">
					<a href="#" target="_blank">
						<i class="entypo-link"></i><span class="title">Связь</span>
					</a>
					<ul>
						<li>
							<a href="<?= Url::to(['/customers']);?>">
								<i class="entypo-user"></i><span class="title">Клиенты</span>
							</a>
						</li>
						<li>
							<a href="<?= Url::to(['/progress']);?>">
								<i class="entypo-traffic-cone"></i><span class="title">Сообщения</span>
							</a>
						</li>
						<li>
							<a href="<?= Url::to(['/progress']);?>">
								<i class="entypo-traffic-cone"></i><span class="title">Обратный звонок</span>
							</a>
						</li>
						<li>
							<a href="<?= Url::to(['/progress']);?>">
								<i class="entypo-traffic-cone"></i><span class="title">Отзывы</span>
							</a>
						</li>
					</ul>
                </li>
                <li class="">
					<a href="/import">
						<i class="entypo-database"></i><span class="title">Импорт</span>
					</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="main-content">
        <div class="row"> <!-- Profile Info and Notifications -->
            <div class="col-md-6 col-sm-8 clearfix">
                <ul class="user-info pull-left pull-none-xsm"> <!-- Profile Info -->
                    <li class="profile-info dropdown">
                        <a href="<?=Url::to(['/cabinet'])?>">
                            <img src="<?=Url::base(true)?>/images/thumb-1@2x.png" alt="" class="img-circle" width="44"/>
                            <?=strtoupper(Yii::$app->user->identity->username)?>
                        </a>
                    </li>
                </ul>


                <!--<ul class="user-info pull-left pull-right-xs pull-none-xsm">
                    <li class="notifications dropdown">
	                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
		                    <i class="entypo-attention"></i>
		                    <span class="badge badge-info">3</span>
	                    </a>
                        <ul class="dropdown-menu">
                            <li class="top"><p class="small">
		                            <a href="#" class="pull-right">Перейти к заказам</a>
                                    У Вас <strong>3</strong> новых заказа
                                </p></li>
                            <li>
                                <ul class="dropdown-menu-list scroller">
                                    <li class="unread notification-success">
	                                    <a href="#">
		                                    <i class="entypo-user-add pull-right"></i>
		                                   <span class="line">
			                                    <strong>Красивый деревянный диван</strong>
			                                   и целая пачка другой мебели
		                                    </span>
		                                    <span class="line small">21-11-2016 13:55:32</span>
	                                    </a>
                                    </li>
                                    <li class="unread notification-secondary"><a href="#">
	                                    <i class="entypo-heart pull-right"></i>
		                                     <span class="line">
			                                    <strong>Ваш диван скрипит!</strong>
		                                    </span>
		                                    <span class="line small">10-04-2016 16:00:32</span>

	                                    </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="external"><a href="#">Просмотреть все уведомления</a></li>
                        </ul>
                    </li>
                </ul>-->


            </div>
            <div class="col-md-6 col-sm-4 clearfix hidden-xs">
                <ul class="list-inline links-list pull-right">
                    <li>
                        <a class="btn btn-link" href="<?=Url::to(['site/settings'])?>">
                            Настройки <i class="glyphicon glyphicon-cog"></i>
                        </a>
                    </li>
                    <li class="sep"></li>
                    <li>
                        <form method="POST" action="<?=Url::to(['site/logout'])?>">
                            <button class="btn btn-link" type="submit">
                                Выход <i class="entypo-logout right"></i>
                            </button>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
        <hr/>
        <br/>
		<!--CONTENT PLACEHOLDER-->
        <div class="row">
            <div class="col-md-12">
                <?=$content?>
            </div>
        </div>
		<!--CONTENT PLACEHOLDER-->
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
