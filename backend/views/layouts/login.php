<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
	<script type="application/javascript">
		var app = {
			config: {
				baseUrl: <?= \yii\helpers\Json::encode(\Yii::$app->homeUrl,0) ?>
			},
			data: <?= \yii\helpers\Json::encode(\Yii::$app->controller->pageData,0)  ?>
		};

	</script>
</head>
<body class="page-body login-page login-form-fall">
<?php $this->beginBody() ?>
<div class="login-container">
    <div class="login-header login-caret">
        <div class="login-content">
            <a href="<?=Url::to(['site/index'])?>" class="logo">
                <img src="<?=Url::base(true)?>/images/logo@2x.png" width="120" alt="" />
            </a>
            <p class="description">Выполните вход в систему, для доступа к админ панели!</p>
        </div>
    </div>
    <div class="login-progressbar">
        <div></div>
    </div>

    <?=$content?>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>


