<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;
?>
<div class="site-error">

	<div class="page-error-404">
		<h1>In progress</h1>
		<div class="error-symbol"><i class="entypo-traffic-cone"></i></div>
	</div>
	<div class="row">
		<div class="col-md-12 text-center">
			<p>Страница находится в стадии разработки</p>
		</div>
	</div>
</div>
