<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="login-form">
    <div class="login-content">
        <div class="form-login-error">
            <h3>Invalid login</h3>
        </div>
        <form method="post" action="<?=Url::to(['site/login']);?>" role="form" id="login-form">
            <div class="form-group field-loginform-username required has-success">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="entypo-user"></i>
                    </div>
                    <input type="text" class="form-control" name="LoginForm[username]" id="username" placeholder="Логин" autocomplete="off" />
                </div>
            </div>
            <div class="form-group field-loginform-password required has-success">
                <div class="input-group">
                    <div class="input-group-addon">
                        <i class="entypo-key"></i>
                    </div>
                    <input type="password" class="form-control" name="LoginForm[password]" id="password" placeholder="Пароль" autocomplete="off" />
                </div>
            </div>
            <div class="form-group">
                <button type="submit" name="login-button" class="btn btn-primary btn-block btn-login">
                    <i class="entypo-login"></i>
                    Войти
                </button>
            </div>
        </form>
    </div>
</div>