<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>
	<div class="page-error-404">
		<div class="error-symbol"><i class="entypo-attention"></i></div>
		<div class="error-text">
			<?= nl2br(Html::encode($message)) ?>
		</div>
	</div>
</div>
