<?php
use yii\helpers\Url;
use yii\helpers\Html;

/* @var $this yii\web\View */

$this->title = 'OHRA Dashboard';
?>
<h2>
	<i class="entypo-feather"></i>
	Контент
</h2>
<hr>
<div class="row">
	<div class="col-sm-3">
		<a href="<?=Url::to(['/content-pages']);?>">
			<div class="tile-title tile">
				<div class="icon"><i class="entypo-book-open"></i></div>
				<div class="title">
					<h3>Страницы</h3>
					<p>Контентовые страницы</p>
				</div>
			</div>
		</a>
	</div>
	<div class="col-sm-3">
		<a href="<?=Url::to(['/blog']);?>">
			<div class="tile-title tile">
				<div class="icon"><i class="entypo-newspaper"></i></div>
				<div class="title">
					<h3>Блог</h3>
					<p>Новости сайта</p>
				</div>
			</div>
		</a>
	</div>
</div>
<br/>
<h2>
	<i class="entypo-basket"></i>
	Каталог
</h2>
<hr>
<div class="row">
	<div class="col-sm-3">
		<a href="<?=Url::to(['/category']);?>">
			<div class="tile-title tile-blue">
				<div class="icon"><i class="entypo-archive"></i></div>
				<div class="title">
					<h3>Категории</h3>
					<p>Категории товаров</p>
				</div>
			</div>
		</a>
	</div>
	<div class="col-sm-3">
		<a href="<?=Url::to(['/product'])?>">
			<div class="tile-title tile-blue">
				<div class="icon"><i class="entypo-box"></i></div>
				<div class="title">
					<h3>Товары</h3>
					<p>Список товаров</p>
				</div>
			</div>
		</a>
	</div>
	<div class="col-sm-3">
		<a href="<?=Url::to(['/brand']);?>">
			<div class="tile-title tile-blue">
				<div class="icon"><i class="glyphicon glyphicon-tag"></i></div>
				<div class="title">
					<h3>Бренды</h3>
					<p>Список брендов</p>
				</div>
			</div>
		</a>
	</div>
</div>
<br/>
<h2>
	<i class="entypo-credit-card"></i>
	Продажи
</h2>
<hr>
<div class="row">
	<div class="col-sm-3">
		<a href="<?=Url::to(['/orders','order-status-filter'=>'created']);?>">
			<div class="tile-title tile-cyan">
				<div class="icon"><i class="glyphicon glyphicon-shopping-cart"></i></div>
				<div class="title">
					<h3>Заказы</h3>
					<p>Список заказов</p>
				</div>
			</div>
		</a>
	</div>
</div>
<h2>
	<i class="entypo-link"></i>
	Связь
</h2>
<hr>
<div class="row">
	<div class="col-sm-3">
		<a href="<?=Url::to(['/customers']);?>">
			<div class="tile-title tile-green">
				<div class="icon"><i class="glyphicon glyphicon-user"></i></div>
				<div class="title">
					<h3>Клиенты</h3>
					<p>Список покупателей</p>
				</div>
			</div>
		</a>
	</div>
	<div class="col-sm-3">
		<a href="<?=Url::to(['/progress']);?>">
			<div class="tile-title tile-green">
				<div class="icon"><i class="glyphicon glyphicon-send"></i></div>
				<div class="title">
					<h3>Сообщения</h3>
					<p>Сообщения формы обратной связи</p>
				</div>
			</div>
		</a>
	</div>
	<div class="col-sm-3">
		<a href="<?=Url::to(['/progress']);?>">
			<div class="tile-title tile-green">
				<div class="icon"><i class="glyphicon glyphicon-phone"></i></div>
				<div class="title">
					<h3>Обратный звонок</h3>
					<p>Сообщения формы связи с менеджером</p>
				</div>
			</div>
		</a>
	</div>
	<div class="col-sm-3">
		<a href="<?=Url::to(['/progress']);?>">
			<div class="tile-title tile-green">
				<div class="icon"><i class="glyphicon glyphicon-thumbs-up"></i></div>
				<div class="title">
					<h3>Отзывы</h3>
					<p>Список отзывов о товарах</p>
				</div>
			</div>
		</a>
	</div>
</div>
<br/>
