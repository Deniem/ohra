<?php

use yii\helpers\Url;
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'OHRA Settings';
?>
<div data-view-type="settings.List">
	<h2>Настройки сайта</h2>
	<hr>
	<div class="settings-list-placeholder"></div>
	<script type="text/template" id="settings-list-template">
		<div class="form-horizontal form-groups-bordered validate">
			<div class="row">
				<div class="col-md-12">

					<div class="panel panel-primary" data-collapsed="<%= (settings.global && settings.global.length > 0 )? '0' : '1' %>">

						<div class="panel-heading">
							<div class="panel-title">
								Глобальные настройки
							</div>

							<div class="panel-options">
								<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
								<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
							</div>
						</div>
						<div class="panel-body">
							<% _.each(settings.global, function(item) { %>
									<div class="form-group">
										<label for="field-1" class="col-sm-3 control-label"><%= item.label %></label>
										<div class="col-sm-6">
											<% if (item.value_type == 'multiline') { %>
											<textarea data-validate="<%= item.value_type + ',' + (item.required ? 'required' :'') %>"
													  class="form-control settings-input"
													  name="<%= item.name %>"
													  data-id="<%= item.id %>"
													  rows="8"
												><%= item.value %></textarea>
											<% } else { %>
											<input type="<%= item.value_type %>"
												   data-validate="<%= item.value_type + ',' + (item.required ? 'required' :'') %>"
												   class="form-control settings-input"
												   name="<%= item.name %>"
												   data-id="<%= item.id %>"
												   value="<%= item.value %>">
											<% } // endIf %>
											<span class="description"><%= item.description %></span>
										</div>
										<div class="col-sm-3">
											<button disabled data-loading-text="Сохраняем" class=" btn btn-save btn-success"><i class="entypo-floppy"></i> </button>
										</div>
									</div>
							<% }) //endEach %>
						</div>
					</div>

				</div>
			</div>

			<div class="row">
				<div class="col-md-6">

					<div class="panel panel-primary" data-collapsed="<%=(settings.frontend && settings.frontend.length > 0) ? '0' : '1' %>">

						<div class="panel-heading">
							<div class="panel-title">
								Настройки сайта
							</div>

							<div class="panel-options">
								<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
								<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
							</div>
						</div>

						<div class="panel-body">
							<% _.each(settings.frontend, function(item) { %>
							<div class="form-group">
								<label for="field-1" class="col-sm-3 control-label"><%= item.label %></label>
								<div class="col-sm-6">
									<% if (item.value_type == 'multiline') { %>
										<textarea data-validate="<%= item.value_type + ',' + (item.required ? 'required' :'') %>"
												  class="form-control settings-input"
												  name="<%= item.name %>"
												  data-id="<%= item.id %>"
											rows="8"><%- item.value %></textarea>
									<% } else { %>
									<input type="<%= item.value_type %>"
										   data-validate="<%= item.value_type + ',' + (item.required ? 'required' :'') %>"
										   class="form-control settings-input"
										   name="<%= item.name %>"
										   data-id="<%= item.id %>"
										   value="<%= item.value %>">
									<% } // endIf %>
									<span class="description"><%= item.description %></span>
								</div>
								<div class="col-sm-3">
									<button disabled data-loading-text="Сохраняем"  class=" btn btn-save btn-success"><i class="entypo-floppy"></i> </button>
								</div>
							</div>
							<% }) //endEach %>
						</div>

					</div>

				</div>
				<div class="col-md-6">
					<div class="panel panel-primary" data-collapsed="<%= (settings.backend && settings.backend.length > 0 )? '0' : '1' %>">

						<div class="panel-heading">
							<div class="panel-title">
								Админ панель
							</div>

							<div class="panel-options">
								<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
								<a href="#" data-rel="reload"><i class="entypo-arrows-ccw"></i></a>
							</div>
						</div>

						<div class="panel-body">
							<div class="panel-body">
								<% _.each(settings.backend, function(item) { %>
								<div class="form-group">
									<label for="field-1" class="col-sm-3 control-label"><%= item.label %></label>
									<div class="col-sm-6">
										<% if (item.value_type == 'multiline') { %>
											<textarea
												data-validate="<%= item.value_type + ',' + (item.required ? 'required' :'') %>"
												class="form-control settings-input"
												name="<%= item.name %>"
												data-id="<%= item.id %>"
												rows="5"><%- item.value %></textarea>
										<% } else { %>
											<input type="<%= item.value_type %>"
												   data-validate="<%= item.value_type + ',' + (item.required ? 'required' :'') %>"
												   class="form-control settings-input"
												   name="<%= item.name %>"
												   data-id="<%= item.id %>"
												   value="<%= item.value %>">
										<% } // endIf %>
										<span class="description"><%= item.description %></span>
									</div>
									<div class="col-sm-3">
										<button disabled data-loading-text="Сохраняем"  class="btn btn-success">Сохранить</button>
									</div>
								</div>
								<% }) //endEach %>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</script>

	</div>