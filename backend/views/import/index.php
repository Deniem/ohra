<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$this->title = 'Импорт данных';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row" data-view-type="import.index">
    <div class="col-md-12 col-sm-12 col-xs-6">
        <div class="row">
            <div class="col-md-12">
                <?= Breadcrumbs::widget([
                    'homeLink' => [
                        'label' => 'Главная',
                        'url' => Yii::$app->getHomeUrl(),
                        'itemprop' => 'url',
                    ],
                    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]);?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-8">
                <h1><?=$this->title?></h1>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4">
                <br/>
                <button class="dropzone btn btn-success btn-icon icon-left upload-file-btn" >
                </button>
            </div>
        </div>
        <!--STEP 1-->
        <br/>
        <div class="row pre-upload-placeholder"></div>
        <script type="text/template" id="pre-upload-template">
            <div class="col-md-12">
                <div class=" alert alert-info">
                    Загрузите файл для импорта
                    <small>(в формате <b>.csv</b>)</small>
                </div>
            </div>
        </script>
        <!--STEP 2-->
        </br>
        <div class="row file-info-placeholder"></div>
        <script type="text/template" id="file-info-template">
            <% if (info && info !== null) { %>
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title">Информация о файле:</h4>
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Тип</th>
                                    <th>Описание</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><b>Заголовки</b></td>
                                    <td>
                                        <% _.each (info.fileInfo.csvHeaders, function(header) { %>
                                        <label class="label label-info label-xs">
                                            <%= header %>
                                        </label>
                                        <% }) //endEach %>
                                    </td>
                                </tr>
                                <tr>
                                    <td><b>Всего записей</b></td>
                                    <td><b><%= info.fileInfo.rowCount %></b></td>
                                </tr>
                                <tr>
                                    <td><b>Группы</b></td>
                                    <td><%= info.importInfo.groups %></td>
                                </tr>
                                <tr>
                                    <td><b>Продукты</b></td>
                                    <td><%= info.importInfo.products %></td>
                                </tr>
                                <tr>
                                    <td><b>Ошибки</b></td>
                                    <td class="text-danger">
                                        <%= info.errors %>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="panel-footer">
                            <button class="btn btn-success btn-import-accept">Продолжить</button>
                            <button class="btn btn-danger btn-import-decline">Отмена</button>
                        </div>
                    </div>
                </div>
            <% } //endIf %>
        </script>
        <div class="row post-upload-placeholder"></div>
        <script type="text/template" id="post-upload-template">
            <div class="col-md-12">
                <h4>Результат:</h4><br/>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Добавлено</th>
                            <th>Обновлено</th>
                            <th>Не обновлено</th>
                            <th>Ошибка</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><%= data.new %></td>
                            <td><%= data.updated %></td>
                            <td><%= data.unchanged %></td>
                            <td><%= 0 %></td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </script>
        <!--STEP 3-->
    </div>
</div>