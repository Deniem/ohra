<?php

use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

$this->title = 'Создать бренд';
$this->params['breadcrumbs'][] = [
    'label'=> 'Бренды',
    'url' => Url::to(['/brand']),
    'itemprop' => 'url',
];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
    <div class="col-md-12">
        <?= Breadcrumbs::widget([
            'homeLink' => [
                'label' => 'Главная',
                'url' => Yii::$app->getHomeUrl(),
                'itemprop' => 'url',
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);?>
    </div>
</div>
<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-9">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-title">
                    <h3><?= $this->title ?></h3>
                </div>
            </div>
            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
