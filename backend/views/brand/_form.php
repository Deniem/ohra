<?php
use trntv\filekit\widget\Upload;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<form role="form" action="<?= Url::to() ?>" method="POST" class="form-horizontal form-groups-bordered"
data-view-type="brand.BrandItem" id="brand-form">
    <div class="panel-body">
        <div class="form-group">
            <label for="name" class="col-sm-4 control-label">Имя</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="name"
                       name="<?= Html::getInputName($model, 'name') ?>" value="<?= $model->name ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="h1" class="col-sm-4 control-label">Заголовок</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="h1" name="<?= Html::getInputName($model, 'h1') ?>" value="<?= $model->h1 ?>">
            </div>
        </div>

        <div class="form-group">
            <label for="description" class="col-sm-4 control-label">Описание</label>
            <div class="col-sm-8">
                <textarea name="<?= Html::getInputName($model, 'description') ?>"
                          id="description" rows="4"
                          class="col-sm-12 form-control"><?= $model->description ?></textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="w0" class="col-sm-4 control-label">Логотип</label>
            <div class="col-sm-8">
                <?= Upload::widget([
                    'model' => $model,
                    'attribute' => 'thumbnail',
                    'url' => ['/file-storage/upload'],
                    'maxFileSize' => 5000000, // 5 MiB
                ]) ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-4 control-label" for="<?=Html::getInputName($model, 'is_top_brand');?>">Показывать на главной</label>
            <div class="col-sm-8">
                <div class="checkbox">
                    <label>

                        <input type="checkbox" class="status-checkbox" <?= ($model->is_top_brand) ? 'checked': ''?>>
                        <input type="hidden"
                               name="<?=Html::getInputName($model, 'is_top_brand');?>"
                               class="status-input">
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="w0" class="col-sm-4 control-label">Контент</label>
            <div class="col-sm-8">
                <?= yii\imperavi\Widget::widget([
                    'model' => $model,
                    'attribute' => 'content',
                    'options' => [
                        'lang' => 'ru',
                    ],
                ]) ?>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <div class="row">
            <div class="col-md-12 text-right">
                <button class="btn btn-primary" type="submit"><?= $model->isNewRecord ? 'Создать' : 'Обновить' ?></button>
                <a href="<?=Url::to('/brand');?>" class="btn btn-danger">Отмена</a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</form>
