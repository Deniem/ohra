<?php
use common\components\widgets\Pager;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

$this->title = 'Бренды';
$this->params['breadcrumbs'][] = 'Каталог';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
    <div class="col-md-12 col-sm-12" data-view-type="brand.MainList">
        <div class="row">
            <div class="col-md-12">
                <?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]);?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-10 col-sm-10 col-xs-10">
                <h1><?=$this->title?></h1>
            </div>
            <div class="col-md-2 col-sm-2 col-xs-2 text-right">
                <br/>
                <a class="btn btn-success" href="<?= Url::to(['brand/create']); ?>">
                    <i class="entypo-plus"></i>
                </a>
            </div>
        </div>
        <br/>

        <div class="row">
            <div class="col-md-12 col-sm-12">
               <?php if(count($brands->models) > 0): ?>
                   <table class="table table-striped table-bordered table-hover text-center font-size-16 responsive">
                       <thead>
                       <tr>
                           <th class="text-center">Превью</th>
                           <th class="text-center">Название</th>
                           <th class="text-center">Заголовок</th>
                           <th class="text-center">Описание</th>
                           <th class="text-center">На главной?</th>
                           <th class="text-center">Действия</th>
                       </tr>
                       </thead>
                       <tbody>
                       <?php foreach ($brands->models as $brand): ?>
                           <tr>
                               <td class="middle-align"><img src="<?= Yii::$app->helpers->formattingProperty($brand, 'previewSrc')?>"
                                                             alt="<?=$brand->name ?>"></td>
                               <td class="middle-align"><?= Yii::$app->helpers->formattingProperty($brand, 'name') ?></td>
                               <td class="middle-align"><?= Yii::$app->helpers->formattingProperty($brand, 'h1') ?></td>
                               <td class="middle-align"><?= Yii::$app->helpers->formattingProperty($brand, 'description')?></td>
                               <td class="middle-align">
                                   <?php if($brand->is_top_brand == 1):?>
                                   <div class="label label-success"><i class="entypo-check"></i></div>
                                   <?php endif?>
                               </td>
                               <td class="middle-align" data-id="<?=$brand->id?>">
                                   <nobr>
                                       <a title="Редактировать" class="btn btn-primary" href="<?= Url::to(['brand/update', 'id' => $brand->id]) ?>">
                                           <i class="entypo-pencil"></i>
                                       </a>
                                       <button title="Удалить" class="btn btn-danger brand-delete-action">
                                           <i class="entypo-cancel"></i>
                                       </button>
                                   </nobr>
                               </td>
                           </tr>
                       <?php endforeach; ?>
                       </tbody>
                   </table>
                <?php else: ?>
                   <div class="alert alert-info">
                       Товаров не найдено
                   </div>
                <?php endif; ?>
            </div>
        </div>

        <div class="row">
        <div class="col-md-6 ">
            Показано <?= $brands->pagination->pageSize < $brands->pagination->totalCount
                ? $brands->pagination->pageSize
                : $brands->pagination->totalCount ?> из <?= $brands->pagination->totalCount ?> записей
        </div>
        <div class="col-md-6 text-right">
            <?= Pager::widget([
                'pages' => $brands->pagination,
                'viewTemplate' => 'pager/backend'
            ]);	?>
        </div>
    </div>

    </div>
</div>


<?= \backend\widgets\ModalDialog::widget([
    'name'=>'model-delete',
    'title' => 'Удалить бренд',
    'message' => 'Вы действительно хотите удалить бренд?',
    'modalId' => 'brand-delete',
    'backBoneDataViewType' => 'modals.DeleteBrand'
])?>

