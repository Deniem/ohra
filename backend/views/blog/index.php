<?php

use common\components\widgets\Pager;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

$this->title = 'Блог';
$this->params['breadcrumbs'][] = 'Контент';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
    <div class="col-md-12 col-sm-12" data-view-type="blog.MainList">
        <div class="row">
            <div class="col-md-12">
                <?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-8 col-sm-8 col-xs-8">
                <h1><?= $this->title ?></h1>
            </div>
            <div class="col-md-4 col-sm-4 col-xs-4 text-right">
                <br/>
                <a class="btn btn-success btn-lg btn-icon icon-left" href="<?= Url::to(['blog/create']); ?>">
                    Добавить запись
                    <i class="entypo-plus"></i>
                </a>
            </div>
        </div>
        <br/>


        <div class="row">
            <div class="col-md-12 col-sm-12">
                <?php if (count($model->models) > 0): ?>
                    <table class="table table-striped table-bordered table-hover text-center font-size-16 responsive">
                        <thead>
                        <tr>
                            <th class="text-center">Заголовок</th>
                            <th class="text-center">Создана/Обновлена</th>
                            <th class="text-center">Опубликована</th>
                            <th class="text-center">Статус</th>
                            <th class="text-center">Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($model->models as $post): ?>
                            <tr>
                                <td class="middle-align"><?= Yii::$app->helpers->formattingProperty($post, 'title') ?></td>
                                <td class="middle-align">
                                    <?= $post->updated_at
                                        ? Yii::$app->helpers->formattingProperty($post, 'updated_at')
                                        : Yii::$app->helpers->formattingProperty($post, 'created_at')
                                    ?>
                                </td>
                                <td class="middle-align"><?= Yii::$app->helpers->formattingProperty($post, 'published_at') ?></td>
                                <td class="middle-align"><?= $post->getStatusLabel()[$post->status]?></td>
                                <td class="middle-align" data-id="<?= $post->id ?>">
                                    <nobr>
                                        <a title="Редактировать" class="btn btn-primary btn-lg"
                                           href="<?= Url::to(['blog/update', 'id' => $post->id]) ?>">
                                            <i class="entypo-pencil"></i>
                                        </a>
                                        <button title="Удалить" class="btn btn-danger btn-lg blog-delete-action">
                                            <i class="entypo-cancel"></i>
                                        </button>
                                    </nobr>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php else: ?>
                    <div class="alert alert-info">
                        Записей не найдено
                    </div>
                <?php endif; ?>
            </div>
        </div>


        <div class="row">
            <div class="col-md-6 ">
                Показано <?= $model->pagination->pageSize < $model->pagination->totalCount
                    ? $model->pagination->pageSize
                    : $model->pagination->totalCount ?> из <?= $model->pagination->totalCount ?> записей
            </div>
            <div class="col-md-6 text-right">
                <?= Pager::widget([
                    'pages' => $model->pagination,
                    'viewTemplate' => 'pager/backend'
                ]); ?>
            </div>
        </div>

    </div>
</div>


<?= \backend\widgets\ModalDialog::widget([
    'name'=>'model-delete',
    'title' => 'Удалить запись',
    'message' => 'Вы действительно хотите удалить запись блога?',
    'modalId' => 'blog-delete',
    'backBoneDataViewType' => 'modals.DeleteBlog'
])?>



