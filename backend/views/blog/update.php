<?php
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

$this->title = $model->title;
$this->params['breadcrumbs'][] = [
    'label' => 'Блог',
    'url' => Url::to(['/blog']),
    'itemprop' => 'url',
];
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="row">
    <div class="col-md-12">
        <?= Breadcrumbs::widget([
            'homeLink' => [
                'label' => 'Главная',
                'url' => Yii::$app->getHomeUrl(),
                'itemprop' => 'url',
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]); ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h1><?= $this->title ?></h1>
    </div>
</div>
<br/>


<div class="row">
    <div class="col-md-1"></div>
    <div class="col-md-9">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="panel-title">
                    <h3>Редактировать запись</h3>
                </div>
            </div>

            <?= $this->render('_form', [
                'model' => $model,
            ]) ?>
        </div>
    </div>
</div>
