<?php
use trntv\filekit\widget\Upload;
use trntv\yii\datetime\DateTimeWidget;
use yii\helpers\Html;
use yii\helpers\Url;

//data-view-type="blog.BlogItem"
?>

<form role="form" action="<?= Url::to() ?>" method="POST" class="form-horizontal form-groups-bordered"
      id="blog-form">
    <div class="panel-body">
        <div class="form-group">
            <label for="h1" class="col-sm-4 control-label">Название</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="h1"
                       name="<?= Html::getInputName($model, 'h1') ?>" value="<?= $model->h1 ?>">
            </div>
        </div>

        <div class="form-group">
            <label for="title" class="col-sm-4 control-label">Заголовок</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="title" name="<?= Html::getInputName($model, 'title') ?>"
                       value="<?= $model->title ?>">
            </div>
        </div>

        <div class="form-group">
            <label for="w0" class="col-sm-4 control-label">Обложка</label>
            <div class="col-sm-8">
                <?= Upload::widget([
                    'model' => $model,
                    'attribute' => 'thumbnail',
                    'url' => ['/file-storage/upload'],
                    'maxFileSize' => 5000000, // 5 MiB
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <label for="field-1" class="col-sm-4 control-label">Описание</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="description"
                       name="<?= Html::getInputName($model, 'description') ?>" value="<?= $model->description ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="field-1" class="col-sm-4 control-label">Ключевые слова</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="keywords"
                       name="<?= Html::getInputName($model, 'keywords') ?>" value="<?= $model->keywords ?>">
            </div>
        </div>

        <div class="form-group">
            <label for="w0" class="col-sm-4 control-label">Контент</label>
            <div class="col-sm-8">
                <?= yii\imperavi\Widget::widget([
                    'model' => $model,
                    'attribute' => 'content',
                    'options' => [
                        'lang' => 'ru',
                        'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi']),
                        'minHeight' => 300
                    ],
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <div class="text-right col-sm-4">
                <label for="published_at" class="control-label label-bootstrap-height">Дата публикации: </label>
            </div>
            <div class="col-sm-8">
                <?= DateTimeWidget::widget([
                    'name' => Html::getInputName($model, 'published_at'),
                    'model' => $model,
                    'phpDatetimeFormat' => 'yyyy-MM-dd HH:mm',
                    'momentDatetimeFormat' => 'YYYY-MM-DD HH:mm:ss',
                    'clientOptions' => [
                        'format' => 'YYYY-MM-DD HH:mm:ss',
                        'allowInputToggle' => false,
                        'sideBySide' => true,
                        'locale' => 'ru-ru	',
                        'widgetPositioning' => [
                            'horizontal' => 'auto',
                            'vertical' => 'auto'
                        ],
                    ],
                    'options' => [
                        'class' => 'form-control discount-date'
                    ]
                ]); ?>
            </div>
        </div>

        <div class="form-group">
            <label for="parent_id" class="col-sm-4 control-label">Статус</label>
            <div class="col-md-8 col-sm-6 col-xs-4">
                <select class="form-control" name="<?= Html::getInputName($model, 'status') ?>">
                    <?php $statusLabels = $model->getStatusLabel() ?>
                    <?php foreach ($statusLabels as $status => $label): ?>
                        <option <?= $model->status == $status ? 'selected' : '' ?>
                            value="<?= $status ?>"><?= $label ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
        </div>

        <div class="form-group">
            <label for="action" class="col-sm-4 control-label">Ссылка страницы</label>
            <div class="col-sm-8">
                <input disabled type="text" maxlength="255" class="form-control"
                       id="action"
                       value="<?= $model->action ?>">

            </div>
        </div>
    </div>
    <div class="panel-footer">
        <div class="row">
            <div class="col-md-12 text-right">
                <button class="btn btn-primary"
                        type="submit"><?= $model->isNewRecord ? 'Создать' : 'Обновить' ?></button>
                <a href="<?= Url::to('/blog'); ?>" class="btn btn-danger">Отмена</a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</form>
