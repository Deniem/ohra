<div data-view-type="orders.newOrderItem">
    <div class="new-order-item-placeholder"></div>

    <script type="text/template" id="new-order-item-template"
            data-id-order="<?= $model->id ?>">
        <% if(newItemButtonClicked == true) { %>
        <div class="panel panel-primary">

            <div class="panel-heading">
                <div class="panel-title">
                    <h4>Добавить товар в заказ</h4>
                </div>
            </div>

            <div class="panel-body">
                <form name="order-item-add-prod" id="order-item-add-prod">
                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3 text-right">
                                <label for="new-item-category" class="control-label label-bootstrap-height">
                                    Категория
                                </label>
                            </div>
                            <div class="col-sm-9">
                                <select class="form-control" name="new-item-category" id="new-item-category">
                                    <option value="none">выберите категорию</option>
                                    <% _.each(categories, function(cat, key){%>
                                    <option <%= selectedCategory
                                            ? selectedCategory == cat.id ? 'selected' : ''
                                            : '' %>
                                        value="<%= cat.id %>">
                                        <%= cat.name %>
                                    </option>
                                    <% }) //endEach %>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="row">
                            <div class="col-sm-3 text-right">
                                <label for="new-item-product" class="control-label label-bootstrap-height">
                                    Продукт
                                </label>
                            </div>
                            <div class="col-sm-9">
                                <select class="form-control" name="new-item-product" id="new-item-product">
                                    <option value="none>">
                                        <%= products.length > 0 ? 'выберите продукт' : '-' %>
                                    </option>
                                    <% _.each(products, function(prod, key){%>
                                    <option <%= selectedProduct !== null
                                        ? selectedProduct.get('id') == prod.id ? 'selected' : ''
                                        : '' %>
                                        value="<%= prod.id %>">
                                        <%= prod.name %>
                                    </option>
                                    <% }) //endEach %>
                                </select>
                            </div>
                        </div>
                    </div>

                    <% if(selectedProduct){%>
                    <hr>
                    <div class="form-group">
                        <div class="row">
                            <% if(selectedProduct.get('loading')) { %>
                            <div class="col-md-12 text-center">
                                <div class="loader-spin"></div>
                            </div>
                            <% } else { %>
                          <div class="col-sm-12">
                              <div class="row">
                                  <div class="col-sm-6 text-right">
                                      <% if(selectedProduct.get('thumbnail_base_url')
                                      && selectedProduct.get('thumbnail_path')) { %>
                                      <img src="<%= selectedProduct.get('thumbnail_base_url') + '/'
                                    + selectedProduct.get('thumbnail_path') %>"
                                           alt="selectedProduct.get('name')"
                                           class="img-responsive">
                                      <% } else {%>
                                      <div class="text-center">
                                        <span class="label label-danger">
                                            Без изображения
                                        </span>
                                      </div>
                                      <% } %>
                                  </div>
                                  <div class="col-sm-6">
                                      <div class="row">
                                          <div class="col-sm-12">
                                              <span><b>Название: </b><%=selectedProduct.get('name')%></span>
                                          </div>
                                      </div>
                                      <div class="row">
                                          <div class="col-sm-12">
                                              <span><b>Категория: </b><%=selectedProduct.get('category')['name']%></span>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <br>
                              <div class="row">
                                      <div class="col-sm-3 text-right">
                                          <b>Свойства: &nbsp;</b>
                                      </div>
                                      <div class="col-sm-9">
                                          <% if(selectedProduct.get('is_new') == 1) {%>
                                          <span class="label label-success">Новинка</span>
                                          <% } %>
                                          <% if(selectedProduct.get('is_action') == 1) {%>
                                          <span class="label label-info">Акционный</span>
                                          <% } %>
                                          <% if(selectedProduct.get('is_sale') == 1) {%>
                                          <span class="label label-warning">Распродажа</span>
                                          <% } %>
                                          <% if(selectedProduct.get('top_sale') == 1) {%>
                                          <span class="label label-danger">Топ продаж</span>
                                          <% } %>
                                          <%
                                          if( selectedProduct.get('is_new') == 0
                                              && selectedProduct.get('is_action') == 0
                                              && selectedProduct.get('is_sale') == 0
                                              && selectedProduct.get('top_sale') == 0 ){ %>
                                          <span class="label label-warning">Свойств нет</span>
                                          <% } %>
                                      </div>
                              </div>

                              <br>
                              <div class="row">
                                  <div class="col-sm-3 text-right">
                                      <b>Скидочный?</b>
                                  </div>
                                  <div class="col-sm-3">
                                      <% if(selectedProduct.get('discount') !== null) { %>
                                          <span class="label label-primary">
                                              -<%=selectedProduct.get('discount')['value'] %>
                                              <%= selectedProduct.get('discount')['type'] == 'percentage'
                                                    ? '%' : 'грн' %>
                                          </span>
                                      <% } else { %>
                                      <span class="label label-primary">Нет</span>
                                      <% } %>
                                  </div>
                                  <div class="col-sm-3 text-right">
                                      <b>Активная?</b>
                                  </div>
                                  <div class="col-sm-3">
                                      <% if(selectedProduct.get('discountActivityState')) { %>
                                          <span class="label label-primary">Да</span>
                                      <% } else { %>
                                         <span class="label label-danger">Нет</span>
                                      <% } %>
                                  </div>


                              </div>

                              <br>
                              <div class="row">
                                  <div class="col-sm-3 text-right">
                                      <b>Цена:</b>
                                  </div>
                                  <div class="col-sm-3">
                                      <%=parseFloat(selectedProduct.get('price')).toFixed(2)%> грн
                                  </div>

                                  <% if(selectedProduct.get('discount')) { %>
                                      <div class="col-sm-3 text-right">
                                          <b>Со скидкой:</b>
                                      </div>
                                      <div class="col-sm-3">
                                          <%=parseFloat(selectedProduct.get('discount')['price']).toFixed(2)%> грн
                                      </div>
                                  <% } %>
                              </div>


                              <br>
                              <div class="row">
                                  <div class="col-sm-3 label-bootstrap-height text-right">
                                      <label for="new-item-productCount"><b>Количество:</b></label>
                                  </div>
                                  <div class="col-sm-3">
                                      <input type="number" class="form-control"
                                             value="<%= selectedProduct.get('quantity')
                                              ? selectedProduct.get('quantity')
                                              : 1 %>"
                                             min="1"
                                          step="1" name="new-item-productCount" id="new-item-productCount">
                                  </div>
                              </div>

                              <hr>
                              <div class="row">
                                  <div class="col-sm-3 text-right">
                                      <b>Общая сума: </b>
                                  </div>
                                  <div class="col-sm-9 text-right">
                                      <%= selectedProduct.get('finalCost') %> грн
                                  </div>
                              </div>

                          </div>
                            <% } %>
                        </div>
                    </div>
                    <% } %>
                </form>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-12 error-text">
                        <% if(selectedProduct !== null
                            && selectedProduct.get('alreadyFound')){ %>
                        <div class="alert alert-danger">
                            Продукт уже был добавлен в заказ
                        </div>
                        <% } %>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 text-right">
                        <button type="button" <%= selectedProduct === null
                         ? 'disabled'
                         : selectedProduct.get('alreadyFound')
                            ? 'disabled'
                            : '' %>
                                class="btn btn-primary btn-addNewOrderItem">
                            Добавить
                        </button>
                        <button type="button" class="btn btn-danger btn-cancel-add">
                            Отмена
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <% } else { %>
        <% } %>
    </script>
</div>
