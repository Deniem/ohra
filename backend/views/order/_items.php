<?php
use yii\helpers\Url;

?>

<div data-view-type="orders.ordersList">
    <div class="items-list-placeholder items-list"></div>

    <script type="text/template" id="items-list-template" data-id-order="<?= $model->id ?>">
        <div class="panel-body">
        <% if (items.length > 0) { %>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-6">
                <table class="table table-bordered table-hover text-center font-size-16 responsive">
                    <thead>
                    <tr class="row th-center">
                        <th >#</th>
                        <th >Товар</th>
                        <th >Количество</th>
                        <th >Цена, шт</th>
                        <th >Скидка</th>
                        <th >Со скидкой</th>
                        <th >Опции</th>
                    </tr>
                    </thead>
                    <tbody id="order-items-list">
                    <%_.each(items, function(prop, key){ %>
                    <% var dis = discounts.findWhere({'order_item_id': prop.id});%>
                    <tr class="row" data-id="<%= prop.id %>" data-product-id="<%= prop.product_id %>"
                        <% if(dis){ %>
                            data-discount-id="<%= dis.get('id') %>"
                        <% } %>
                    >
                        <td><%= key + 1%></td>
                        <td>
                            <a href="<?=Url::to(['product/update'])?>/<%= prop.product_id %>">
                                <%= prop.name %>
                            </a>
                        </td>
                        <td>
                            <input type="number" step="1" class="form-control quantityItem" min="1"
                                   value="<%= prop.quantity %>">
                        </td>
                        <td><%= parseFloat(prop.price).toFixed(2) %></td>
                        <td>
                            <div class="label label-info">
                                <%= dis
                                ? '-' + dis.get('value') +  ( dis.get('type') == 'percentage' ? '%' : 'грн')
                                : '<i class="entypo-cancel"></i>' %>
                            </div>
                        </td>
                        <td>
                            <div class="label label-success">
                                <%= dis ? parseFloat(dis.get('price')).toFixed(2)
                                        : parseFloat(prop.price).toFixed(2) %>
                                 грн
                            </div>
                        </td>
                        <td>
                            <button title="Удалить" class="btn btn-danger btn-sm deleteItem">
                                <i class="entypo-cancel"></i>
                            </button>
                        </td>
                    </tr>
                    <% }) //endEach %>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">Сумма заказа: </div>
                <div class="col-md-6 text-right"><%= total %></div>
            </div>
        </div>
        <% } else {%>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-6">
                <div class="col-sm-12 alert alert-warning" role="alert">Товаров в заказе нет</div>
            </div>
        </div>
        <% }%>
    </div>
    <div class="panel-footer">
        <div class="row">
            <div class="col-sm-12 text-right">
                <button type="button" <%= !newOrderProductButtonAvailability ? 'disabled' : '' %>
                        class="btn btn-success btn-new-order-item">
                    Добавить товар
                </button>
            </div>
        </div>
    </div>
    </script>
</div>
