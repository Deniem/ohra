<?php

use common\models\db\OrderOptions;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

$this->title = "Редактирование заказа";
$this->params['breadcrumbs'][] = [
	'label'=> 'Заказы',
	'url' => Url::to(['/orders']),
	'itemprop' => 'url',
];
$this->params['breadcrumbs'][] = $this->title;

$orderOptions = OrderOptions::getAllOptions();

?>
<div class="row">
	<div class="col-md-12">
		<?= Breadcrumbs::widget([
			'homeLink' => [
				'label' => 'Главная',
				'url' => Yii::$app->getHomeUrl(),
				'itemprop' => 'url',
			],
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]);?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<h1><?=$this->title?></h1>
	</div>
</div>
<br/>
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-success">
            <div class="panel-heading">
                <div class="panel-title">
                    <h3>Заказ</h3>
                </div>
            </div>
            <?= $this->render('_form',[
                'model' => $model,
                'orderOptions' => $orderOptions
            ]);?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-info">
            <div class="panel-heading ">
                <div class="panel-title">
                    <h3>Товары в заказе</h3>
                </div>
            </div>
            <?= $this->render('_items',[
                'model' => $model
            ]);?>
        </div>
        <?= $this->render('_newOrderItem',[
            'model' => $model
        ]);?>
    </div>
</div>