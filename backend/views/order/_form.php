<?php
use common\models\db\OrderOptions;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div data-view-type="order.Update">
	<form role="form" action="<?= Url::to() ?>" method="POST" class="form-horizontal form-groups-bordered">
		<div class="panel-body">
			<div class="row">
				<div class="col-md-12">
					<div class="btn-toolbar btn-group-xs " role="toolbar" aria-label="...">
						<?php foreach($orderOptions['status'] as $k=>$v): ?>
							<button data-loading-text="Сохраняем..." type="button" data-status="<?=$k?>" class="btn btn-status btn-<?=$orderOptions['statusLabels'][$k]?>"><?=$v?></button>
						<?php endforeach; ?>
					</div>
				</div>
			</div>
			<br/>
			<div class="form-group">
				<label for="order-status" class="col-sm-4 control-label">Статус</label>
				<div class="col-sm-8">
					<select class="form-control" name="Order[status]" id="order-status" required>
						<?php foreach($orderOptions['status'] as $k=>$v): ?>
							<option value="<?= $k?>" <?= $model->status == $k ? 'selected': '' ?>>
								<?=$v?>
							</option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="order-delivery" class="col-sm-4 control-label">Доставка</label>
				<div class="col-sm-8">
					<select class="form-control" name="Order[delivery]" id="order-delivery" required>
						<?php foreach($orderOptions['delivery'] as $k=>$v): ?>
							<option value="<?= $k?>" <?= $model->delivery == $k ? 'selected': '' ?>>
								<?=$v?>
							</option>
						<?php endforeach; ?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="order-payment_method" class="col-sm-4 control-label">Способ оплаты</label>
				<div class="col-sm-8">
					<select class="form-control" name="Order[payment_method]" id="order-payment_method" required>
						<?php foreach($orderOptions['payment_method'] as $k=>$v): ?>
							<option value="<?= $k?>" <?= $model->payment_method == $k ? 'selected': '' ?>>
								<?=$v?>
							</option>
						<?php endforeach;?>
					</select>
				</div>
			</div>
			<div class="form-group">
				<label for="created_at" class="col-sm-4 control-label">Создана</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" readonly id="created_at" name="<?= Html::getInputName($model, 'created_at') ?>" value="<?= $model->created_at ?>">
				</div>
			</div>
			<div class="form-group">
				<label for="updated_at" class="col-sm-4 control-label">Обновлена</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" readonly id="updated_at" name="<?= Html::getInputName($model, 'updated_at') ?>" value="<?= $model->updated_at ?>">
				</div>
			</div>

			<div class="form-group text-center">
				<hr>
				<div class="col-sm-12 alert alert-info" role="alert">
					Контактные данные
				</div>
			</div>
			<div class="form-group">
				<label for="name" class="col-sm-4 control-label">Ф.И.О.</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="name" name="<?= Html::getInputName($model, 'name') ?>" value="<?= $model->name ?>">
				</div>
			</div>
			<div class="form-group">
				<label for="city" class="col-sm-4 control-label">Город</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="city" name="<?= Html::getInputName($model, 'city') ?>" value="<?= $model->city ?>">
				</div>
			</div>
			<div class="form-group">
				<label for="address" class="col-sm-4 control-label">Адресс</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="address" name="<?= Html::getInputName($model, 'address') ?>" value="<?= $model->address ?>">
				</div>
			</div>
			<div class="form-group">
				<label for="phone" class="col-sm-4 control-label">Телефон</label>
				<div class="col-sm-8">
					<input type="text" class="form-control" id="phone" name="<?= Html::getInputName($model, 'phone') ?>" value="<?= $model->phone ?>">
				</div>
			</div>
			<div class="form-group">
				<label for="email" class="col-sm-4 control-label">email</label>
				<div class="col-sm-8">
					<input type="email" class="form-control" id="email" name="<?= Html::getInputName($model, 'email') ?>" value="<?= $model->email ?>">
				</div>
			</div>
			<div class="form-group">
				<label for="notes" class="col-sm-4 control-label">Примечания</label>
				<div class="col-sm-8">
					<?= yii\imperavi\Widget::widget([
						'model' => $model,
						'attribute' => 'notes',
						'options' => [
							'lang' => 'ru',
						],
					]) ?>
				</div>
			</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button class="btn btn-primary" type="submit"><?= $model->isNewRecord ? 'Создать' : 'Обновить' ?></button>
					<a href="<?=Url::to(['/order']);?>" class="btn btn-danger">Отмена</a>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</form>
</div>