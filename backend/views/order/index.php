<?php

use common\models\db\OrderOptions;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use yii\widgets\LinkPager;


$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;
$orderOptions = OrderOptions::getAllOptions();
?>

<div class="row" data-view-type="order.order_filter">
    <div class="col-md-12 col-sm-12 col-xs-6">
        <div class="row">
            <div class="col-md-12">
                <?= Breadcrumbs::widget(
					['homeLink' => [
						'label' => 'Главная',
						'url' => Yii::$app->getHomeUrl(),
						'itemprop' => 'url',
					],
					'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]);?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <h1><?=$this->title?></h1>
            </div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
				<div class="row orders-filter-list-placeholder orders-filter-list "></div>
			</div>
        </div>

        <br/>
        <script type="text/template" id="orders-filter-list-template">
			<div class="col-md-6"></div>
			<div class="col-md-6">
				<div class="form-group">
					<label for="order-status">Статус</label>
					<select class="form-control order-status-filter" name="order-status-filter" id="order-status">
						<option  <%= (app.data.orderStatusFiler == "all>") ? 'selected' : '' %> value="all">Все</option>
						<?php foreach($orderOptions['status'] as $k=>$v): ?>
							<option <%= (app.data.orderStatusFiler == "<?=$k?>") ? 'selected' : '' %> value="<?= $k?>" ><?=$v?></option>
						<?php endforeach?>
					</select>
				</div>
			</div>
        </script>
        <div class="row">
            <div class="col-md-12">
                <?php if (count($orders) > 0): ?>
                    <table class="table table-striped table-bordered table-hover text-center font-size-16 responsive">
                        <thead>
                        <tr class="row ">
							<th class="col-md-2">Имя</th>
							<th class="col-md-2">Телефон</th>
							<th class="col-md-2">Адрес</th>
							<th class="col-md-2">Способ оплаты</th>
							<th class="col-md-2">Доставка</th>
							<th class="col-md-2">Создана</th>
							<th class="col-md-2">Статус</th>
                            <th class="col-md-2">Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach($orders as $order): ?>
                            <tr class="row">
								<td class="middle-align text-left">
									<a href="<?=Url::to(['customer/view','id'=>$order->customer_id])?>">
										<i class="entypo-eye"></i>
										<?= Yii::$app->helpers->formattingProperty($order, 'name')?>
									</a>
								</td>
                                <td class="middle-align text-left">
									<a href="<?=$order->phone ? 'tel:'.$order->phone : '#'?>">
										<i class="entypo-phone"></i>
										<?= Yii::$app->helpers->formattingProperty($order, 'phone')?>
									</a>
								</td>
								<td class="middle-align text-left">
									<?= Yii::$app->helpers->formattingProperty($order, 'city')?>,
									<?= Yii::$app->helpers->formattingProperty($order, 'address')?>
								</td>
								<td class="middle-align text-left"><?= $orderOptions['payment_method'][$order->payment_method]?></td>
								<td class="middle-align text-left"><?= $orderOptions['delivery'][$order->delivery]?></td>

								<td class="middle-align"><?= Yii::$app->helpers->formattingProperty($order, 'created_at')?></td>
								<td class="middle-align"><div class="label label-<?=$orderOptions['statusLabels'][$order->status]?> "><?= $orderOptions['status'][$order->status]?></div></td>
                                <td class="middle-align" data-id="<?=$order->id?>">
									<nobr>
										<a title="Редактировать" class="btn btn-primary btn-lg" href="<?= Url::to(['order/update', 'id' => $order->id]) ?>">
											<i class="entypo-pencil"></i>

										</a>
										<button title="Удалить" class="btn btn-danger btn-lg order-delete-btn">
											<i class="entypo-cancel"></i>
										</button>
									</nobr>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>

                    <?= LinkPager::widget(['pagination' => $pagination]); ?>

                <?php else:?>
					<div class="col-md-12 alert alert-info">
						<h3>Заказы отсутсвуют</h3>
					</div>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>

<?= \backend\widgets\ModalDialog::widget([
	'name'=>'model-delete',
	'title' => 'Удалить заказ',
	'message' => 'Вы действительно хотите удалить заказ?',
	'modalId' => 'order-delete',
	'backBoneDataViewType' => 'modals.DeleteOrder'
])?>

