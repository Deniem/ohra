<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

$this->title = "Создание товара";
$this->params['breadcrumbs'][] = [
	'label'=> 'Продукты',
	'url' => Url::to(['/product']),
	'itemprop' => 'url',
];
$this->params['breadcrumbs'][] = $this->title;
$productOptions = \common\models\db\Product::getStatusOptions();
?>

<div class="row">
	<div class="col-md-12">
		<?= Breadcrumbs::widget([
			'homeLink' => [
				'label' => 'Главная',
				'url' => Yii::$app->getHomeUrl(),
				'itemprop' => 'url',
			],
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]);?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<h1><?=$this->title?></h1>
	</div>
</div>
<br/>
<div class="row">
	<div class="col-md-6">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<div class="panel-title">
					<h3>Новый Товар</h3>
				</div>
			</div>
		    <?= $this->render('_form', [
		        'model' => $model,
			    'categories' => $categories,
			    'brands' => $brands,
			    'productOptions' => $productOptions,
		    ]) ?>
		</div>
	</div>
	<div class="col-md-6">
		<div class="panel panel-info">
			<div class="panel-heading">
				<div class="panel-title">
					<h3>Характеристики товара</h3>
				</div>
			</div>
			<?= $this->render('_properties',[
				'model' => $model
			]);?>
		</div>
		<div class="panel panel panel-success">
			<div class="panel-heading">
				<div class="panel-title">
					<h3>Параметры скидок</h3>
				</div>
			</div>
			<?= $this->render('_discount', [
				'model' => $model,
				'dataPickerClientOptions' => $dataPickerClientOptions
			])?>
		</div>
	</div>
</div>
