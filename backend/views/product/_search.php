<?php

use yii\helpers\Html;
use yii\helpers\Url;
$productOptions = \common\models\db\Product::getStatusOptions();
?>

<div class="row">
	<div class="col-md-12">
		<div class="panel panel-sm panel-gray" data-collapsed="<?=$searchModel->isInSearch ? '0' : '1'?>">
			<div class="panel-heading panel-heading-sm">
				<div class="panel-title">
					<div class="row">
						Быстрый поиск: &nbsp;
						<input type="text" name="ProductSearch" class="input-xs">
					</div>
				</div>
				<div class="panel-options">
					<button type="submit" class="btn btn-info btn-xs">
						<i class="entypo-search"></i>
					</button>
					<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
				</div>
			</div>
			<div class="panel-body panel-body-sm">
				<form method="get" class="search-bar" action="<?=Url::to();?>">
					<div class="row">
						<div class="col-md-12">
							<table class="table table-bordered responsive">
								<tbody>
								<tr class="row">
									<td class=" text-center align-middle">
										<h4>Искать по:</h4>
									</td>
									<td class="col-md-3">
										<input type="text" class="form-control" value="<?=$searchModel->name?>" name="<?= Html::getInputName($searchModel, 'name') ?>" placeholder="Название">
									</td>
									<td class="">
										<select class="selectboxit form-control" name="<?= Html::getInputName($searchModel, 'id_category') ?>" data-first-option="false">
											<option>Категория</option>
											<?php foreach ($categoriesFilter as $id => $category):?>
												<option <?= $searchModel->id_category == $id ? 'selected' : '' ?> value="<?=$id?>"><?=$category?></option>
											<?php endforeach;?>
										</select>
									</td>
									<td>
										<select name="ProductSearch[status]" class="selectboxit">
											<option value="all">Все</option>
											<?php foreach ($productOptions['statuses'] as $status): ?>
												<option <?=$searchModel->status == $status ? 'selected' : '' ?> value="<?= $status ?>">
													<?= $productOptions['names'][$status] ?>
												</option>
											<?php endforeach; ?>
										</select>
										</td>
									<td class=" text-center">
										<nobr>
											<button title="Найти" type="submit" class="btn btn-info ">
												<i class="entypo-search"></i>
											</button>
											<a title="Сбросить" href="<?=Url::to(['/product'])?>" class="btn btn-primary">
												<i class="entypo-cancel"></i>
											</a>
										</nobr>
									</td>
								</tr>
								</tbody>
							</table>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>



