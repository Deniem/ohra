<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\db\Units;

$units = Units::find()->all();

?>
<div class="product-property-list" data-view-type="product.propertyList">
	<div class="panel-body">
		<div class="property-list-placeholder"></div>
	</div>
	<div class="panel-footer">
		<div class="clear"></div>
	</div>
	<script type="text/template" id="property-list-template">
		<% if (properties.length > 0){ %>
		<table class=" table table-hover responsive">
			<thead>
			<tr class="row th-center">
				<th class="col-md-2">Характеристика</th>
				<th class="col-md-5">Значение</th>
				<th class="col-md-4">Действия</th>
			</tr>
			</thead>
			<tbody>
			<%_.each(properties, function(prop, key){ %>
			<tr data-id="<%= prop.id %>" data-key="<%=key%>" class="row property-line">
				<td>
					<p>
						<% if (prop.is_filter) { %>
							<i title="Является фильтром" class="fa fa-filter"></i>
						<% } //endif; %>
						<%= prop.name + ', ' + prop.unit.short_name %></p>
				</td>
				<td>
					<% if (prop.values.length > 0) { %>
						<select name="id_value" class="form-control prop-input">
							<option value="0" class="">Нет</option>
							<% _.each(prop.values, function(value, idx) { %>
								<option class=""
								<%= productProperty.findWhere({id_value:value.id}) != null ? "selected" : "" %>
								value="<%=value.id%>" >
									<%= value.name %>
								</option>
							<% }) //endEach %>
						</select>
					<% } else { %>
						<p>Нет значений, добавьте новое...</p>
					<% } //endIf %>
				</td>
				<td class="text-center">
					<button class="btn btn-success btn-add">Добавить</button>
				</td>
			</tr>
			<% }) //endEach %>
			</tbody>
		</table>
		<% } else { %>
		<div class="alert alert-info">
			Для категории данного товара характеристики не выбраны
		</div>
		<% } //endif %>
	</script>
</div>