<?php

use trntv\filekit\widget\Upload;
use yii\helpers\Html;
use yii\helpers\Url;


?>
<div data-view-type="product.Form">
	<form role="form" action="<?= Url::to() ?>" method="POST" class="form-horizontal form-groups-bordered">
		<div class="panel-body">
				<div class="form-group">
					<label for="field-1" class="col-sm-4 control-label">Название</label>
					<div class="col-sm-8">
						<input type="text" class="form-control" id="name" name="<?= Html::getInputName($model, 'name') ?>" value="<?= $model->name ?>">
					</div>
				</div>
				<div class="form-group">
					<label for="w0" class="col-sm-4 control-label">Обложка</label>
					<div class="col-sm-8">
						<?= Upload::widget([
							'model' => $model,
							'attribute' => 'thumbnail',
							'url' => ['/file-storage/upload'],
							'maxFileSize' => 5000000, // 5 MiB
						]) ?>
					</div>
				</div>
				<div class="form-group">
					<label for="w0" class="col-sm-4 control-label">Фото</label>
					<div class="col-sm-8">
						<?=  Upload::widget([
							'model' => $model,
							'attribute' => 'photos',
							'url' => ['/file-storage/upload-product-image'],
							'maxFileSize' => 5000000, // 5 MiB
							'multiple' => true,
							'maxNumberOfFiles' => 100
						]); ?>
					</div>
				</div>
				<div class="form-group">
					<label for="field-1" class="col-sm-4 control-label">Описание</label>
					<div class="col-sm-8">
						<?= yii\imperavi\Widget::widget([
							'model' => $model,
							'attribute' => 'description',
							'options' => [
								'lang' => 'ru',
							],
						]) ?>
					</div>
				</div>
				<div class="form-group">
					<label for="field-1" class="col-sm-4 control-label">Цена</label>
					<div class="col-sm-8">
						<input type="text" class="form-control product-price"
							   name="<?= Html::getInputName($model, 'price') ?>" value="<?= $model->price ?>">
					</div>
				</div>
                <div class="form-group">
					<label for="field-1" class="col-sm-4 control-label">Артикул</label>
					<div class="col-sm-8">
						<input type="text" class="form-control product-article"
							   name="<?= Html::getInputName($model, 'article') ?>" value="<?= $model->article ?>">
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" for="ProductSearch[status]">Статус</label>
					<div class="col-sm-8">
						<select name="<?= Html::getInputName($model, 'status') ?>" class="selectboxit" id="product-status-filter">
							<?php foreach ($productOptions['statuses'] as $status): ?>
								<option <?=$model->status == $status ? 'selected' : '' ?> value="<?= $status ?>">
									<?= $productOptions['names'][$status] ?>
								</option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" for="ProductSearch[status]">Акционный / Новинка / Распродажа</label>
					<div class="col-sm-8">
						<div class="checkbox">
							<label>
								<?= Html::checkbox(Html::getInputName($model, 'is_action'), $model->is_action, ['class'=>'status-checkbox'])?>
								<input type="hidden" value="<?=$model->is_action ? $model->is_action : '0';?>" name="<?=Html::getInputName($model, 'is_action');?>" class="status-input">
								Акционный
							</label>
						</div>
						<div class="checkbox">
							<label>
								<?= Html::checkbox(Html::getInputName($model, 'is_new'), $model->is_new, ['class'=>'status-checkbox'])?>
								<input type="hidden" value="<?=$model->is_new ? $model->is_new : '0';?>" name="<?=Html::getInputName($model, 'is_new');?>" class="status-input">
								Новинка
							</label>
						</div>
						<div class="checkbox">
							<label>
								<?= Html::checkbox(Html::getInputName($model, 'is_sale'), $model->is_sale, ['class'=>'status-checkbox'])?>
								<input type="hidden" value="<?=$model->is_sale ? $model->is_sale : '0';?>" name="<?=Html::getInputName($model, 'is_sale');?>" class="status-input">
								Распродажа
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-4 control-label" for="ProductSearch[top_sale]">Топ продаж</label>
					<div class="col-sm-8">
						<div class="checkbox">
							<label>
								<?= Html::checkbox(Html::getInputName($model, 'top_sale'), $model->top_sale, ['class' => 'top-checkbox']) ?>
								<input type="hidden" value="<?= $model->top_sale ? $model->top_sale : '0'; ?>"
									   name="<?= Html::getInputName($model, 'top_sale'); ?>" class="top-input">
								Топ продаж
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="parent_id" class="col-sm-4 control-label">Категория</label>
					<div class="col-sm-8">
						<select name="<?= Html::getInputName($model, 'id_category') ?>" class="selectboxit" data-first-option="false">
							<option>Выберите категорию</option>
							<?php foreach ($categories as $category): ?>
								<option value="<?= $category->id ?>" <?= $category->hasChilds() ? 'disabled' : ''?> <?= $category->id == $model->id_category ? 'selected' : '' ?>>
									<?= $category->name ?>
								</option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
			<div class="form-group">
					<label for="parent_id" class="col-sm-4 control-label">Бренд</label>
					<div class="col-sm-8">
						<select name="<?= Html::getInputName($model, 'brand_id') ?>" class="selectboxit" data-first-option="false">
							<option>Выберите бренд</option>
							<?php foreach ($brands as $brand): ?>
								<option value="<?= $brand->id ?>" <?= $brand->id == $model->brand_id? 'selected' : '' ?>>
									<?= $brand->name ?>
								</option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
		</div>
		<div class="panel-footer">
			<div class="row">
				<div class="col-md-12 text-right">
					<button class="btn btn-primary" type="submit"><?= $model->isNewRecord ? 'Создать' : 'Обновить' ?></button>
					<a href="<?=Url::to('/product');?>" class="btn btn-danger">Отмена</a>
				</div>
				<div class="clear"></div>
			</div>
		</div>
	</form>
</div>