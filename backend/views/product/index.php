<?php

use common\components\widgets\Pager;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

$this->title = 'Товары';
$this->params['breadcrumbs'][] = 'Каталог';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
	[
		'label' => 'Превью',
		'value' => 'preview',
		'column-class' => 'category-preview-column'
	],
	[
		'label' => 'Имя',
		'attribute' => 'name',
		'value' => 'name'
	],
	[
		'label' => 'Category',
		'attribute' => 'id_category',
		'value' => 'category.name',
		'filter' => $categoriesFilter
	]
];

$productOptions = \common\models\db\Product::getStatusOptions();

?>

<div class="row" data-view-type="product.List">
	<div class="col-md-12 col-sm-12">
		<div class="row">
			<div class="col-md-12">
				<?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]);?>
			</div>
		</div>
		<div class="row ">
			<div class="col-md-8 col-sm-8 col-xs-8">
				<h1><?=$this->title?></h1>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-2">
				<div class="row">
					<form role="form" action="<?=Url::to()?>">
						<div class="form-group">
							<label for="ProductSearch[status]">Статус</label>
							<select name="ProductSearch[status]" class="selectboxit" id="product-status-filter">
								<option value="all">Все</option>
								<?php foreach ($productOptions['statuses'] as $status): ?>
									<option <?=$searchModel->status == $status ? 'selected' : '' ?> value="<?= $status ?>">
										<?= $productOptions['names'][$status] ?>
									</option>
								<?php endforeach; ?>
							</select>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-2 text-right">
				<br/>
				<a class="btn btn-success" href="<?= Url::to(['product/create']); ?>">
					<i class="entypo-plus"></i>
				</a>
			</div>

		</div>
		<br/>

		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
						<?= $this->render('_search', [
							'searchModel' => $searchModel,
							'categoriesFilter' => $categoriesFilter
						]); ?>
					</div>
				</div>
				<?php if ($products) : ?>
					<table class="table table-striped table-bordered table-hover text-center font-size-16 responsive">
						<thead>
						<tr class="row">
							<th class="col-sm-3">Превью</th>
							<th>Название</th>
							<th>Категория</th>
							<th>Бренд</th>
							<th>Цена</th>
							<th>Акционный/Новинка/Распродажа</th>
							<th>Топ продаж</th>
							<th>Скидка</th>
							<th>Статус</th>
							<th width="1%">Действия</th>
						</tr>
						</thead>
						<tbody>
						<?php foreach($products as $product): ?>
							<tr class="row">
								<td ><?= Yii::$app->helpers->formattingProperty($product, 'preview')?></td>
								<td class="middle-align text-left">
									<a href="<?=Url::to(['product/update','id'=>$product->id]);?>">
										<?= Yii::$app->helpers->formattingProperty($product, 'name')?>
									</a>
								</td>
								<td class="middle-align text-left">
									<a href="<?=Url::to(['category/update','id'=>$product->id_category]);?>">
										<?= Yii::$app->helpers->formattingProperty($product, 'category.name')?>
									</a>
								</td>
								<td class="middle-align text-left">
									<a href="<?=Url::to(['brand/update','id'=>$product->brand_id]);?>">
										<?= Yii::$app->helpers->formattingProperty($product, 'brand.name')?>
									</a>
								</td>
								<td class="middle-align text-left"><?= Yii::$app->helpers->formattingProperty($product, 'price')?></td>
								<td class="middle-align">
									<?php if ($product->is_new):?>
										<div class="label label-success">Новинка</div>
									<?php endif;?>
									<?php if ($product->is_action):?>
										<div class="label label-info">Акционный</div>
									<?php endif;?>
									<?php if ($product->is_sale):?>
										<div class="label label-warning">Распродажа</div>
									<?php endif;?>
								</td>
								<td class="middle-align">
									<div class="label label-<?=$product->top_sale ? 'success' : 'danger'?>">
										<i class="entypo-<?= Yii::$app->helpers->formattingProperty($product, 'top_sale') ? 'check' : 'cancel';?>"></i>
									</div>
								</td>
								<td class="middle-align">
									<?php if($product->discount): ?>
										<div class="label label-info">
											<?= $product->discount->type == 'percentage'
												? '-' . $product->discount->value . "%"
												: '-' . $product->discount->value . " грн"?>
										</div>
									<?php else: ?>
										<div class="label label-default">
											<i class="entypo-minus"></i>
										</div>
									<?php endif ?>
								</td>
								<td class="middle-align ">
									<div class="label label-<?=$productOptions['labels'][$product->status]?>">
										<?=$productOptions['names'][$product->status]?>
									</div>
								</td>
								<td class="middle-align">
									<nobr>
										<a title="Редактировать" class="btn btn-primary " href="<?= Url::to(['product/update', 'id' => $product->id]) ?>">
											<i class="entypo-pencil"></i>
										</a>
										<?php if ($product->status != $product::$STATUS_DELETED) : ?>
											<a title="Удалить" class="btn btn-danger " href="<?= Url::to(['product/delete', 'id' => $product->id]) ?>">
												<i class="entypo-cancel"></i>
											</a>
										<?php endif;?>
									</nobr>
								</td>
							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				<?php else: ?>
					<div class="alert alert-info">
						Товаров не найдено
					</div>
				<?php endif;?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 ">
				Показано <?= $pages->pageSize < $pages->totalCount ? $pages->pageSize : $pages->totalCount ?> из <?= $pages->totalCount ?> записей
			</div>
			<div class="col-md-6 text-right">
				<?= Pager::widget([
					'pages' => $pages,
					'viewTemplate' => 'pager/backend'
				]);	?>
			</div>
		</div>
</div>


