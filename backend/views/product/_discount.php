<?php
use trntv\yii\datetime\DateTimeWidget;

?>

<div data-view-type="product.Discount">
    <div class="product-discount-placeholder"></div>

    <script type="text/template" id="product-discount-template">
        <div class="panel-body">
            <% if(discount !== null){ %>
            <form action="" id="discount-form">
            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3 text-right">
                        <label for="discount-price" class="control-label label-bootstrap-height">
                            Активность
                        </label>
                    </div>
                    <div class="col-sm-9">
                        <div class="checkbox">
                            <label>
                                <input class="discount-status-checkbox" name="status"
                                       value="<%= discount.get('status') == 'active' ? 1 : 0 %>"
                                <%= discount.get('status') == 'enabled' ? 'checked' : '' %>
                                type="checkbox">
                                <input type="hidden" value="<%= discount.get('status') == 'active' ? 1 : 0 %>"
                                       name="status" id="stat-val"
                                       class="discount-status-input">
                                Скидка активная
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3 text-right">
                        <label class="control-label">Период действия</label>
                    </div>
                    <div class="col-sm-9">
                        <div class="row">
                            <div class="text-right col-sm-1">
                                <label for="discount-value" class="control-label label-bootstrap-height">С: </label>
                            </div>
                            <div class="col-sm-10">
                                <?php $disTimeStart = DateTimeWidget::begin([
                                    'name' => 'start_date',
                                    'clientOptions' => $dataPickerClientOptions,
                                    'phpDatetimeFormat' => 'yyyy-MM-dd HH:mm',
                                    'momentDatetimeFormat' => 'YYYY-MM-DD HH:mm:ss',
                                    'options' => [
                                        'class' => 'form-control discount-date',
                                        ]
                                    ]);
                                DateTimeWidget::end(); ?>
                                <input type="hidden" id="disTimeStart"
                                       value="<?=$disTimeStart->containerOptions['id'] ?>">
                            </div>

                        </div>
                        <div class="row">
                            <div class="text-right col-sm-1">
                                <label for="discount-value" class="control-label label-bootstrap-height">По: </label>
                            </div>
                            <div class="col-sm-10">
                                <?php $disTimeEnd = DateTimeWidget::begin([
                                    'name' => 'end_date',
                                    'phpDatetimeFormat' => 'yyyy-MM-dd HH:mm',
                                    'momentDatetimeFormat' => 'YYYY-MM-DD HH:mm:ss',
                                    'clientOptions' => $dataPickerClientOptions,
                                    'options' => [
                                        'class' => 'form-control discount-date'
                                    ]
                                ]);
                                DateTimeWidget::end(); ?>
                                <input type="hidden" id="disTimeEnd"
                                       value="<?=$disTimeEnd->containerOptions['id'] ?>">
                            </div>
                        </div>
                    </div>
                </div>

               <% if (discount.get('start_date') || discount.get('end_date')
                || discount.get('status')){ %>
                <div class="row">
                    <div class="col-sm-3 text-right">
                        <label class="control-label label-bootstrap-height">Будет активна:</label>
                    </div>
                    <div class="col-sm-9 label-bootstrap-height">
                        <span><%= discount.calculateDate() %></span>
                    </div>
                </div>
                <% } %>
            </div>

            <hr>

            <div class="form-group">
                <div class="row">
                    <div class="text-right col-sm-3">
                        <label for="discount-value" class="control-label label-bootstrap-height">Скидка</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="number" class="form-control" id="discount-value" name="value"
                               value="<%= discount.get('value') ? parseFloat(discount.get('value')).toFixed(2)
                               : parseFloat(0).toFixed(2) %>"
                               min="0">
                    </div>
                    <div class="col-sm-1">
                        <label for="discount-type" class="control-label label-bootstrap-height">Тип</label>
                    </div>
                    <div class="col-sm-3">
                        <select class="form-control" name="type" id="discount-type" required>
                            <option value="percentage">%</option>
                            <option <%= discount.get('type') == 'sum' ? 'selected' : '' %>
                                    value="sum">сумма</option>
                        </select>
                    </div>
                    <div class="col-sm-1"></div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3 text-right">
                        <label for="discount-origin-price" class="control-label label-bootstrap-height">Оригинальная
                            цена</label>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="discount-origin-price" name="origin-price"
                               required readonly
                               value="<%= discount.get('origin-price')
                               ? discount.get('origin-price')
                               : 0.00 %>">
                    </div>
                    <div class="col-sm-1 label-bootstrap-height">грн</div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-sm-3 text-right">
                        <label for="discount-price" class="control-label label-bootstrap-height">
                            Со скидкой
                        </label>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="discount-price" name="price"
                               required readonly
                               value="<%= discount.get('price')
                               ? parseFloat(discount.get('price')).toFixed(2)
                               : product.get('price')
                                    ? parseFloat(product.get('price')).toFixed(2)
                                    : 0
                                %>">
                    </div>
                    <div class="col-sm-1 label-bootstrap-height">грн</div>
                </div>
            </div>
            </form>
        </div>
        <div class="panel-footer">
            <div class="row">
                <div class="col-sm-12 text-right">
                    <button class="btn btn-primary btn-discount-submit" type="button">Сохранить</button>
                    <button class="btn btn-danger btn-discount-delete" type="button">Удалить</button>
                </div>
            </div>
        </div>
            <% } else {%>
            <div class="col-md-6">
                <h4>Скидок для данного товара не существует</h4>
            </div>
            <div class="text-right col-md-6">
                <button class="btn btn-success btn-add-discount" type="button">
                    Добавить
                </button>
            </div>
            <% }%>
        </div>
    </script>
</div>
