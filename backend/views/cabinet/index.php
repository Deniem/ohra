<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$this->title = 'Личный кабинет';
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="row" data-view-type="cabinet.Settings">
	<div class="col-md-12 col-sm-12 col-xs-6">
		<div class="row">
			<div class="col-md-12">
				<?= Breadcrumbs::widget([
					'homeLink' => [
						'label' => 'Главная',
						'url' => Yii::$app->getHomeUrl(),
						'itemprop' => 'url',
					],
					'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]);?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-sm-8 col-xs-8">
				<h1><?=$this->title?></h1>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-4 text-right">
				<br/>
				<a class="btn btn-success btn-lg btn-icon icon-left" href="<?= Url::to(['content-pages/create']); ?>">Добавить страницу<i class="entypo-plus"></i></a>
			</div>
		</div>
		<br/>
		<div class="row">
			<div class="col-md-6">
				<div class="panel panel-primary">
						<ul class="nav nav-tabs">
							<li class="active">
								<a data-toggle="tab" href="#basics">
									<span class="entypo-vcard"></span>
									Доступ
								</a>
							</li>
							<li>
								<a data-toggle="tab" href="#advanced">
									<span class="entypo-lock"></span>
									Личные данные
								</a>
							</li>
						</ul>
					<div class="panel-body">
						<div class="tab-content custom-tab-content">
							<div id="basics" class="tab-pane fade in active">
								<div class="form-group">
									<label>Логин</label>
									<input type="text" name="" class="form-control" value="" disabled>
								</div>
								<br/>
								<div class="form-group">
									<label>E-mail</label>
									<input type="email" name="" class="form-control" value="" required="true" disabled>
								</div>
								<br/>
								<div class="form-group">
									<label>Пароль </label>
									<input type="password" name="" class="form-control" value="" required="true" disabled>
								</div>
							</div>
							<div id="advanced" class="tab-pane fade">

							</div>
						</div>
					</div>
					<div class="panel-footer">
						<button class="btn btn-success" data-loading-text="Сохраняем">Сохранить</button>
					</div>
				</div>
			</div>
			<div class="col-md-6">

			</div>
		</div>
	</div>
</div>