<?php
use yii\helpers\Url;
?>

<div class="panel-body" data-view-type="customer.customer_order_filter">
    <div class="customer-order-filter-placeholder"></div>
    <br/>

    <script type="text/template" id="customer-order-filter-template">
        <div class="row text-right">
            <div class="col-md-2">
                <label class="control-label label-bootstrap-height" for="order-status"> Статус</label>
            </div>
            <div class="col-md-4 form-group">
                <select class="form-control order-status-filter" name="customer-order-filter" id="customer-order-filter">
                    <?php foreach ($orderOptions['status'] as $k => $v): ?>
                        <option value="<?= $k ?>"><?= $v ?></option>
                    <?php endforeach ?>
                    <option value="all">Все</option>
                </select>
            </div>
            <div class="col-md-6"></div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <% if (orders.length > 0) { %>
                <table class="table table-bordered table-hover text-center font-size-16 responsive">
                    <thead>
	                    <tr class="row">
	                        <th>Имя получателя</th>
	                        <th>Дата заказа</th>
	                        <th>Статус</th>
	                        <th>Сумма заказа</th>
	                        <th width="1%">Опции</th>
	                    </tr>
                    </thead>
                    <tbody>
	                   <% _.each(orders.models, function(order){%>
	                       <tr style="cursor: pointer;" class="row customer-order" data-view-id="<%=order.get('id')%>">
	                           <td><%= order.get('name') %></td>
	                           <td><%= order.get('created_at') %></td>
	                           <td>
		                            <div class="label label-<%=orderOptions.statusLabels[order.get('status')]%>">
			                           <%= orderOptions.status[order.get('status')] %>
		                            </div>
	                           </td>
	                           <td><%= order.getTotal()%></td>
	                           <td>
		                           <nobr>
		                               <a title="Изменить" class="btn btn-primary btn-lg" href="<?=Url::to(['order/update']);?>/<%=order.get('id')%>">
		                                   <i class="entypo-pencil"></i>
		                               </a>
		                           </nobr>
	                           </td>
	                       </tr>
	                    <% });%>
                    </tbody>
                </table>
                <% } else{ %>
                <br>
                <div class="alert alert-warning" role="alert">Заказы с данным фильтром отсутствуют</div>
                <% }%>
            </div>
        </div>
   </script>
</div>
