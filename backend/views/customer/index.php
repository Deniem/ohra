<?php
use frontend\models\Customer;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

$this->title = 'Клиенты';
$this->params['breadcrumbs'][] = $this->title;
$customerStatus = Customer::getStatusLabels();
$statusLabels = Customer::getStatusLabelColors();
?>


<div class="row" data-view-type="customer.customers_list">
    <div class="col-md-12 col-sm-12 col-xs-6">
        <div class="row">
            <div class="col-md-12">
                <?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]); ?>
            </div>
        </div>
		<div class="row">
			<div class="col-md-6">
				<h1><?=$this->title?></h1>
			</div>
			<div class="col-md-2"></div>
			<div class="col-md-4">
				<div class="col-md-12 customers-status-filter-placeholder"></div>
			</div>
		</div>
        <div class="row" >

            <script type="text/template" id="customers-status-filter-template">
				<div class="col-md-6"></div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="order-status">Статус</label>
						<select class="form-control customer-status-filter" name="customer-status-filter" id="customer-status">
							<option value="active">Активные</option>
							<option value="deleted">Удаленные</option>
							<option value="not_verified">Не подтвержденные</option>
							<option value="all">Все</option>
						</select>
					</div>
                </div>
                <div class="col-md-8">
                </div>
            </script>
        </div>
    </div>
        <main class="col-md-12">
            <?php if (count($customers->getModels()) > 0): ?>
                <table class="table table-striped table-bordered table-hover text-center font-size-16 responsive">
                    <thead>
                    <tr class="row">
                        <th >Пользователь</th>
                        <th >e-mail</th>
                        <th >Статус</th>
                        <th  width="1%">Опции</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($customers->getModels() as $customer): ?>
                        <tr class="row">
                            <td class="middle-align text-left">
								<a title="Просмотр" href="<?= Url::to(['customer/view', 'id' => $customer->id]) ?>">
									<i class="entypo-eye"></i>
									<?= Yii::$app->helpers->formattingProperty($customer, 'username') ?>
								</a>
							</td>
                            <td class="middle-align text-left">
								<a href="mailto:<?=$customer->email;?>">
									<i class="entypo-mail"></i>
									<?= Yii::$app->helpers->formattingProperty($customer, 'email') ?></a>
							</td>
                            <td class="middle-align">
								<div class="label label-<?=$statusLabels[$customer->status]?>" ><?= $customerStatus[$customer->status] ?></div>
							</td>
                            <td class="middle-align">
                                <a title="Просмотр" class="btn btn-primary btn-lg" href="<?= Url::to(['customer/view', 'id' => $customer->id]) ?>">
                                    <i class="entypo-eye"></i>
                                </a>
                            </td>
                        </tr>

                    <?php endforeach; ?>
                    </tbody>
                </table>
            <?php else:?>
                    <div class="col-md-12 alert alert-info">
                        <h3>Пользователи отсутсвуют</h3>
                    </div>
            <?php endif;?>
        </main>
</div>

