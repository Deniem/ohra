<div class="panel-body" data-view-type="customer.order_items">
    <div class="row">
        <div class="col-md-12 order-items-placeholder">
            Нажмите на заказ для просмотра деталей
        </div>

        <script type="text/template" id="order-items-template">
            <div class="row">
                <div class="form-group text-center">
                    <div class="col-sm-12 alert alert-success" role="alert">Заказ</div>
                </div>
            </div>
            <div class="row">
                <div class="form-group">
                    <label for="order-item-status" class="col-sm-4 control-label">Статус</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" readonly id="order-item-status" name="status"
                               value="<%= orderOptions.status[model.get('status')] %>">
                    </div>
                </div>
            </div><br>
            <div class="row">
                <div class="form-group">
                    <label for="order-item-delivery" class="col-sm-4 control-label">Способ доставки</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" readonly id="order-item-delivery" name="delivery"
                               value="<%= orderOptions.delivery[model.get('delivery')] %>">
                    </div>
                </div>
            </div><br>
            <div class="row">
                <div class="form-group">
                    <label for="order-item-payment" class="col-sm-4 control-label">Способ оплаты</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" readonly id="order-item-payment" name="payment"
                               value="<%= orderOptions.payment_method[model.get('payment_method')] %>">
                    </div>
                </div>
            </div><br>
            <div class="row">
                <div class="form-group">
                    <label for="order-item-created_at" class="col-sm-4 control-label">Создана</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" readonly id="order-item-created_at" name="created_at"
                               value="<%= model.get('created_at') %>">
                    </div>
                </div>
            </div><br>
            <div class="row">
                <div class="form-group">
                    <label for="order-item-updated_at" class="col-sm-4 control-label">Обновленна</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" readonly id="order-item-updated_at" name="updated_at"
                               value="<%= model.get('updated_at') %>">
                    </div>
                </div>
            </div><br>

            <% if( model.get('orderItems').length > 0) { %>
            <div class="row">
                <div class="form-group text-center">
                    <div class="col-sm-12 alert alert-warning" role="alert">Товары</div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-6">
                    <table class="table table-bordered table-hover text-center font-size-16 responsive">
                        <thead>
                        <tr class="row th-center">
                            <th class="col-md-1">#</th>
                            <th class="col-md-8">Товар</th>
                            <th class="col-md-1">Количество</th>
                            <th class="col-md-2">Цена, шт</th>
                        </tr>
                        </thead>
                        <tbody>
                        <%_.each(model.get('orderItems'), function(prop, key){ %>
                        <tr class="row" data-id="<%= prop.id %>">
                            <td><%= key + 1%></td>
                            <td>
                                <a href="<?=\yii\helpers\Url::to(['product/update']);?>/<%= prop.product_id%>">
                                    <%= prop.name %>
                                </a>
                            </td>
                            <td>
                                <input type="number" step="1" min="1"
                                       readonly
                                       class="form-control quantityItem"
                                       value="<%= prop.quantity %>">
                            </td>
                            <td><%= prop.price %></td>
                        </tr>
                        <% }) //endEach %>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-right">
                    Сумма : <%= model.getTotal()%>
                </div>
            </div>
            <% } else {%>
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-6">
                    <div class="col-sm-12 alert alert-warning" role="alert">Товаров в заказе нет</div>
                </div>
            </div>
            <% }%>

            <div class="row">
                <div class="form-group text-center">
                    <div class="col-sm-12 alert alert-info" role="alert">Контактные данные</div>
                </div>
            </div><br>
            <div class="row">
                <div class="form-group">
                    <label for="order-item-name" class="col-sm-4 control-label">Ф.И.О.</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" readonly id="order-item-name" name="name"
                               value="<%= model.get('name') %>">
                    </div>
                </div>
            </div><br>
            <div class="row">
                <div class="form-group">
                    <label for="order-item-city" class="col-sm-4 control-label">Город</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" readonly id="order-item-city" name="city"
                               value="<%= model.get('city') %>">
                    </div>
                </div>
            </div><br>
            <div class="row">
                <div class="form-group">
                    <label for="order-item-address" class="col-sm-4 control-label">Адресс</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" readonly id="order-item-address" name="address"
                               value="<%= model.get('address') %>">
                    </div>
                </div>
            </div><br>
            <div class="row">
                <div class="form-group">
                    <label for="order-item-phone" class="col-sm-4 control-label">Телефон</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" readonly id="order-item-phone" name="phone"
                               value="<%= model.get('phone') %>">
                    </div>
                </div>
            </div><br>
            <div class="row">
                <div class="form-group">
                    <label for="order-item-email" class="col-sm-4 control-label">email</label>
                    <div class="col-sm-8">
                        <input type="email" class="form-control" readonly id="order-item-email" name="email"
                               value="<%= model.get('email') %>">
                    </div>
                </div>
            </div><br>
            <div class="row">
                <div class="form-group">
                    <label for="order-item-notes" class="col-sm-4 control-label">Примечания</label>
                    <div class="col-md-8">
                        <textarea class="form-control" readonly name="notes" id="order-item-notes"
                                  rows="5"><%= model.get('notes') %></textarea>
                    </div>
                </div>
            </div><br>
        </script>
    </div>
</div>
