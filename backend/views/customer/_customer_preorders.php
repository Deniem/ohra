<div class="row">
    <% if(preorderCollection.models.length > 0 ) { %>
    <div class="col-md-12">
        <table
            class="table table-bordered table-hover text-center font-size-16 responsive">
            <thead>
            <tr class=" th-center">
                <th>#</th>
                <th>Последняя активность</th>
                <th>Статус</th>
            </tr>
            </thead>
            <tbody>
            <%_.each(preorderCollection.models, function(prop, key){%>
            <tr class="pointer-cursor show-preopred-items" data-id="<%= prop.id %>">
                <td><%= key + 1%></td>
                <td><%= prop.get('updated_at')%></td>
                <td><% if(prop.get('is_active') == 1){ %>
                    <div class="label label-success">активная</div>
                    <% } else {%>
                    <div class="label label-warning">завершена</div>
                    <% } %>
                </td>
            </tr>
            <% }) //endEach %>
            </tbody>
        </table>
    </div>
    
    <% if(pageCount > 1) {%>
        <div class="col-md-12 text-center">
            <ul class="pagination-ul">
                <% for(var i=1; i<= pageCount; i++){ %>
                <li class="page-item <%= (currentPage == i) ? 'active' : '' %>"
                    data-page="<%= i %>" data-per-page="<%= pageSize %>">
                    <%= i %>
                </li>
                <% } %>
            </ul>    
        </div>
    <% } %>
    
    <% } else { %>
    <div class="col-md-12">
        <h4>Пользователь еще не добавил ничего в корзину</h4>
    </div>
    <% } %>
    
</div>