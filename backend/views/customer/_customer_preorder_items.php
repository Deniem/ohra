<div class="row">
    <div class="col-md-12">
        <table
            class="table table-bordered table-hover text-center font-size-16 responsive">
            <thead>
            <tr class=" th-center">
                <th>#</th>
                <th>Товар</th>
                <th>Цена</th>
                <th>Кол-во.</th>
                <th>Тип</th>
                <th>Статус</th>
                <th>Куплен?</th>
            </tr>
            </thead>
            <tbody>
            <%_.each(itemsCollection.models, function(prop, key){%>
            <% var product = new app.models.Product(prop.get('product')) %>
            <tr class="pointer-cursor show-preopred-items" data-id="<%= prop.id %>">
                <td><%= key + 1%></td>
                <td>

                    <a href="<%= app.config.baseUrl + 'product/update/' + prop.get('product_id') %>">
                        <%= prop.get('name')%>
                    </a>
                </td>
                <td><%= prop.get('price')%></td>
                <td><%= prop.get('quantity') %></td>
                <td><%= product.getIsLabels() %></td>
                <td><%= product.getStatusLabel() %></td>
                <td>
                    <input type="checkbox" class="active" <%= prop.get('is_order_confirmed') == 1 ? 'checked' : ''%>
                    title="is_order_confirmed" onclick="return false">
                </td>
            </tr>
            <% }) //endEach %>
            </tbody>
        </table>
    </div>

    <% if(pageCount > 1) {%>
    <div class="col-md-12 text-center">
        <ul class="pagination-ul">
            <% for(var i=1; i<= pageCount; i++){ %>
            <li class="page-item <%= (currentPage == i) ? 'active' : '' %>"
                data-page="<%=i%>" data-per-page="<%= pagination['defaultPageSize'] %>">
                <%= i %>
            </li>
            <% } %>
        </ul>
    </div>
    <% } %>
</div>