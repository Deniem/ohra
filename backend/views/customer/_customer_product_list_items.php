<div class="panel-body">
    <div class="col-md-12">
        <% if(itemsCollection.models.length > 0) { %>
        <table class="table table-bordered table-hover text-center font-size-16 responsive">
            <thead>
            <tr class="th-center">
                <th>#</th>
                <th>Товар</th>
                <th>Категория</th>
                <th>Цена, шт</th>
                <th>Тип</th>
                <th>Статус</th>
            </tr>
            </thead>
            <tbody>
            <%_.each(itemsCollection.models, function(prop, key){%>
            <tr data-id="<%= prop.id %>">
                <td><%= key + 1%></td>
                <td><a href="<?=\yii\helpers\Url::to(['product/update']);?>/<%= prop.get('id')%>"><%= prop.get('name')%></a></td>
                <td><%= categoryCollection.findWhere({id: prop.get('id_category')}).get('name')%></td>
                <td><%= prop.get('price')%></td>
                <td><%= prop.getIsLabels() %></td>
                <td><%= prop.getStatusLabel() %></td>
            </tr>
            <% }) //endEach %>
            </tbody>
        </table>
        <% } else {%>
            <h4>Отсутствуют</h4>
        <% } %>
    </div>
</div>