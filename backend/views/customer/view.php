<?php

use common\models\db\OrderOptions;
use common\models\db\Product;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

$this->title = "Просмотр пользователя " . $customer['username'];
$this->params['breadcrumbs']['customer'] = [
    'label' => 'Клиенты',
    'url' => ['customer/index']
];
$this->params['breadcrumbs'][] = $customer['username'];

$orderOptions = OrderOptions::getAllOptions();
?>

<div class="row">
    <div class="col-md-12">
        <?= Breadcrumbs::widget(['links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]); ?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-default">

                <div data-view-type="customer.customerNavTabs">

                    <div class="customer-nav-tab-placeholder">
                    </div>

                    <script type="text/template" id="customer-nav-tab-template">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#user-details">
                                    <span class="glyphicon glyphicon-user"></span>
                                    &nbsp;Пользователь
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#user-favorite">
                                    <span class="glyphicon glyphicon-heart"></span>
                                    &nbsp;Любимое
                                    <% if(favoriteCount > 0) {%>
                                    <span class="badge badge-secondary"><%= favoriteCount %></span>
                                    <% } %>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#user-compare">
                                    <span class="glyphicon glyphicon-retweet"></span>
                                    &nbsp;В сравнении
                                    <% if(compareCount > 0) {%>
                                     <span class="badge badge-warning"><%= compareCount%></span>
                                    <% } %>
                                </a>
                            </li>
                            <li>
                                <a data-toggle="tab" href="#user-cart">
                                    <span class="glyphicon glyphicon-shopping-cart"></span>
                                    &nbsp;Корзина
                                </a>
                            </li>
                        </ul>
                    </script>

                </div>

                <div class="tab-content custom-tab-content">
                    <div id="user-details" class="tab-pane fade in active" data-view-type="customer.customer">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h3>Параметры пользователя</h3>
                            </div>
                        </div>

                        <div class="customer-params-placeholder">
                        </div>

                        <script type="text/template" id="customer-params-template">
                        <form role="form" action="<?= Url::to() ?>" method="POST" class="form-horizontal form-groups-bordered">
                            <div class="panel-body">

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="customer-username" class="col-sm-4 control-label">Никнейм</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" readonly id="customer-username"
                                                   name="customer-username"
                                                   value="<%= customer.username%>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="customer-email" class="col-sm-4 control-label">email</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" readonly id="customer-email"
                                                   name="customer-email"
                                                   value="<%= customer.email%>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="customer-account" class="col-sm-4 control-label">Статус</label>
                                        <div class="col-sm-8">
                                            <select class="form-control customer-account-filter" name="customer-account-filter"
                                                    id="customer-account">
                                                <% _.each(customerStatus, function(status_val, status_key){%>
                                                <option value="<%= status_key%>"
                                                <% if(status_key == customer.status){%>
                                                selected
                                                <% } %>
                                                >
                                                <%= status_val%></option>
                                                <% }); %>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="customer-id" class="col-sm-4 control-label">Id</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" readonly id="customer-id"
                                                   name="customer-id" value="<%= customer.id%>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="customer-created" class="col-sm-4 control-label">Создан</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" readonly id="customer-created"
                                                   name="customer-created"
                                                   value="<%= customer.created_at %>">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="customer-updated" class="col-sm-4 control-label">Обновлен</label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" readonly id="customer-updated"
                                                   name="customer-updated"
                                                   value="<%= customer.updated_at %>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        </script>
                    </div>

                    <div id="user-favorite" class="tab-pane fade" data-view-type="customer.customerFavorite">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h3>Избранные товары</h3>
                            </div>
                        </div>

                        <div class="customer-favorite-placeholder">
                        </div>

                        <script type="text/template" id="customer-favorite-template">
                            <?= $this->render('_customer_product_list_items'); ?>
                        </script>
                    </div>
                    <div id="user-compare" class="tab-pane fade" data-view-type="customer.customerCompare">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h3>Товары в сравнениии</h3>
                            </div>
                        </div>

                        <div class="customer-compare-placeholder">
                        </div>

                        <script type="text/template" id="customer-compare-template">
                            <?= $this->render('_customer_product_list_items'); ?>
                        </script>

                    </div>
                    <div id="user-cart" class="tab-pane fade">
                        <div class="panel-heading">
                            <div class="panel-title">
                                <h3>История корзины</h3>
                            </div>
                        </div>

                        <div class="panel-body">
                          <div class="row">
                              <div class="col-md-4 col-sm-12 col-xs-12">
                                  <div data-view-type="customer.customerPreorder">
                                      <div class="customer-preorder-placeholder"></div>

                                      <script type="text/template" id="customer-preorder-template">
                                        <?= $this->render('_customer_preorders'); ?>
                                      </script>
                                  </div>
                              </div>
                              <div class="col-md-8 col-sm-12 col-xs-12">
                                  <div data-view-type="customer.PreorderItems">
                                      <div class="customer-preorder-items-placeholder">
                                          <p>Нажмите на элемент слева для просмотра содержимого</p>
                                      </div>

                                      <script type="text/template" id="customer-preorder-items-template">
                                        <?= $this->render('_customer_preorder_items'); ?>
                                      </script>
                                  </div>
                              </div>
                          </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <div class="row text-right">
                        <div class="col-md-12">
                            <a class="btn btn-success btn-lg btn-icon icon-left"
                               href="<?= Url::to(['/progress']); ?>">
                                Написать письмо
                                <i class="entypo-mail"></i>
                            </a>
                        </div>
                    </div>
                </div>



        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <h3>Заказы</h3>
                </div>
            </div>
            <?= $this->render('_customer_orders', [
                'customer' => $customer,
                'orders' => $orders,
                'orderOptions' => $orderOptions
            ]); ?>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="panel-title">
                    <h3>Детали заказа</h3>
                </div>
            </div>
            <?= $this->render('_customer_order_prop', [
                'customer' => $customer,
                'orders' => $orders,
                'orderOptions' => $orderOptions
            ]); ?>
        </div>
    </div>
</div>
