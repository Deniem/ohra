<?php
use common\models\db\ContentPages;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<form role="form" action="<?= Url::to() ?>" method="POST" class="form-horizontal form-groups-bordered">
    <div class="panel-body">
        <div class="form-group">
            <label for="field-1" class="col-sm-4 control-label">Название</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="title" name="<?= Html::getInputName($model, 'title') ?>" value="<?= $model->title ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="field-1" class="col-sm-4 control-label">Заголовок страницы</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="h1" name="<?= Html::getInputName($model, 'h1') ?>" value="<?= $model->h1 ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="field-1" class="col-sm-4 control-label">Описание</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="description" name="<?= Html::getInputName($model, 'description') ?>" value="<?= $model->description ?>">
            </div>
        </div>
        <div class="form-group">
            <label for="field-1" class="col-sm-4 control-label">Ключевые слова</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="keywords" name="<?= Html::getInputName($model, 'keywords') ?>" value="<?= $model->keywords ?>">
            </div>
        </div>

        <div class="form-group">
            <label for="w0" class="col-sm-4 control-label">Контент</label>
            <div class="col-sm-8">
                <?= yii\imperavi\Widget::widget([
                    'model' => $model,
                    'attribute' => 'content',
                    'options' => [
                        'lang' => 'ru',
                        'imageUpload' => Yii::$app->urlManager->createUrl(['/file-storage/upload-imperavi']),
                        'minHeight' => 300
                    ],
                ]) ?>
            </div>
        </div>

        <div class="form-group">
            <label for="field-1" class="col-sm-4 control-label">Опубликовать страницу</label>
            <div class="col-sm-8">
                <?= Html::checkbox(Html::getInputName($model, 'status'), $model->status)?>
            </div>
        </div>

        <?=Html::hiddenInput(Html::getInputName($model, 'can_delete'), $model->can_delete)?>


        <div class="form-group">
            <label for="action" class="col-sm-4 control-label">Ссылка страницы</label>
            <div class="col-sm-8">
                <input <?=$model->can_delete ? '' : 'disabled'?> type="text" maxlength="255" class="form-control" id="action"
                       name="<?= Html::getInputName($model, 'action')?>"
                value="<?=$model->action?>">

            </div>
        </div>
        
        <div class="form-group">
            <label for="field-1" class="col-sm-4 control-label">Вложенность</label>
            <div class="col-sm-8">
                <?php echo Html::activeDropDownList($model, 'id_parent', $parents, [
                    'class' => 'selectboxit',
                ]);?>
            </div>
        </div>
    </div>
    <div class="panel-footer">
        <div class="row">
            <div class="col-md-12 text-right">
                <button class="btn btn-primary" type="submit"><?= $model->isNewRecord ? 'Создать' : 'Обновить' ?></button>
                <a href="<?=Url::to(['/content-pages']);?>" class="btn btn-danger">Отмена</a>
            </div>
            <div class="clear"></div>
        </div>
    </div>
</form>
