<?php
/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\Breadcrumbs;

$this->title = 'Контент';
$this->params['breadcrumbs'][] = $this->title;

?>


<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-6">
        <div class="row">
            <div class="col-md-12">
                <?= Breadcrumbs::widget([
					'homeLink' => [
						'label' => 'Главная',
						'url' => Yii::$app->getHomeUrl(),
						'itemprop' => 'url',
					],
					'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],]);?>
            </div>
        </div>
        <div class="row">
			<div class="col-md-8 col-sm-8 col-xs-8">
				<h1><?=$this->title?></h1>
			</div>
			<div class="col-md-4 col-sm-4 col-xs-4 text-right">
				<br/>
				<a class="btn btn-success btn-lg btn-icon icon-left" href="<?= Url::to(['content-pages/create']); ?>">Добавить страницу<i class="entypo-plus"></i></a>
			</div>
        </div>
        <br/>
        <div class="row">
            <div class="col-md-12">
                <?php if (count($contentPages) > 0): ?>
                <table class="table table-striped table-bordered table-hover text-center font-size-16 responsive">
                    <thead>
                        <tr class="row ">
                            <th>H1</th>
							<th>Создана / Обновлена</th>
							<th>Статус</th>
							<th width="1%">Действия</th>
						</tr>
                    </thead>
                    <tbody>
                    <?php foreach($contentPages as $page): ?>
                        <tr class="row">
                            <td class="middle-align text-left">
								<a href="<?=Url::to(['content-pages/update', 'id' => $page->id]);?>">
									<?= Yii::$app->helpers->formattingProperty($page, 'h1')?>
								</a>
							</td>
							<td class="middle-align"><?= Yii::$app->helpers->formattingProperty($page, 'created_at')?></td>
							<td class="middle-align">
								<div class="label label-<?=$page->status ? 'success': 'default'?>">
									<?= Yii::$app->helpers->formattingProperty($page, 'status') ? 'Активная' : 'Неактивная'?>
								</div>
							</td>
                            <td class="middle-align">
								<nobr>
									<a class="btn btn-primary btn-lg" title="Редактировать" href="<?= Url::to(['content-pages/update', 'id' => $page->id]) ?>">
										<i class="entypo-pencil"></i>
									</a>
									<a class="btn btn-info btn-lg" target="_blank" title="Просмотреть" href="<?= Yii::getAlias('@frontendUrl').'/'.$page->action ?>">
										<i class="entypo-eye"></i>
									</a>
									<?php if($page->can_delete == 1): ?>
										<a class="btn btn-danger btn-lg" title="Удалить" href="<?= Url::to(['content-pages/delete', 'id' => $page->id]) ?>">
											<i class="entypo-cancel"></i>
										</a>
									<?php endif;?>
								</nobr>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody>
                </table>
                <?php else:?>
                    <div class="row">
                        <div class="col-md-12 alert alert-info">
                            <h3>Не создано ни одной страницы!</h3>
                        </div>
                    </div>
                <?php endif;?>
            </div>
        </div>
    </div>
</div>
