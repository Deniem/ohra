<?php

use yii\helpers\Url;
use yii\widgets\Breadcrumbs;
use backend\models\OrdersStat;

$this->title = 'Продажи';
$this->params['breadcrumbs'][] = [
    'label'=> 'Статистика'
];

$this->params['breadcrumbs'][] = $this->title;
?>

	<div class="row">
    <div class="col-md-12 col-sm-12">
        <?= Breadcrumbs::widget([
            'homeLink' => [
                'label' => 'Главная',
                'url' => Yii::$app->getHomeUrl(),
                'itemprop' => 'url',
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h1><?=$this->title?></h1>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-12">
		<?php if (count($orders) > 0): ?>
        <table class="table table-bordered table-striped table-hover text-center font-size-16 responsive">
			<thead>
				<tr>
					<th>Дата реализации</th>
					<th>Клиент</th>
					<th>Сумма заказа</th>
					<th width="1%">Действия</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($orders as $order): ?>
						<tr>
							<td class="text-left middle-align"><?=date('d-m-Y',strtotime($order->updated_at))?></td>
							<td class="text-left middle-align">
								<a target="_blank" href="<?=Url::to(['customer/view', 'id'=>$order->customer_id])?>"><?=$order->customers->username?></a>
							</td>
							<td class="text-left middle-align">
								<?= OrdersStat::totalCost($order->orderItems).' грн.'?>
							</td>
							<td>
								<button title="Подробнее" class="btn btn-lg btn-info"><i class="entypo-eye"></i></button>
							</td>
						</tr>
				<?php endforeach;?>
			</tbody>
		</table>
		<?php else:?>
			<div class="alert alert-info">
				Нет заказов для сбора статистики
			</div>
		<?php endif;?>
    </div>
</div>

