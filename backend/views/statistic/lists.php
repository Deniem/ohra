<?php

use yii\helpers\Url;
use yii\widgets\Breadcrumbs;

$this->title = 'Пользовательские списки';
$this->params['breadcrumbs'][] = [
    'label'=> 'Статистика'
];

$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <?= Breadcrumbs::widget([
            'homeLink' => [
                'label' => 'Главная',
                'url' => Yii::$app->getHomeUrl(),
                'itemprop' => 'url',
            ],
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]);?>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <h1><?=$this->title?></h1>
    </div>
</div>
<br/>
<div class="row">
    <div class="col-md-12">
        Пользовательские списки
    </div>
</div>

