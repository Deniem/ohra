<?php

use yii\helpers\Html;
use yii\helpers\Url;
use trntv\filekit\widget\Upload;

?>
<form role="form" action="<?= Url::to() ?>" method="POST" class="form-horizontal form-groups-bordered">
<div class="panel-body">
		<div class="form-group">
			<label for="field-1" class="col-sm-4 control-label">Название</label>
			<div class="col-sm-8">
				<input type="text" class="form-control" id="name" name="<?= Html::getInputName($model, 'name') ?>" value="<?= $model->name ?>">
			</div>
		</div>
		<div class="form-group">
			<label for="w0" class="col-sm-4 control-label">Обложка</label>
			<div class="col-sm-8">
				<?= Upload::widget([
					'model' => $model,
					'attribute' => 'thumbnail',
					'url' => ['/file-storage/upload'],
					'maxFileSize' => 5000000, // 5 MiB
				]) ?>
			</div>
		</div>
		<div class="form-group">
			<label for="parent_id" class="col-sm-4 control-label">Вложенность</label>
			<div class="col-sm-8">
				<select name="<?= Html::getInputName($model, 'id_parent') ?>" class="selectboxit" data-first-option="false">
					<option>Выберите категорию</option>
					<option <?= !$model->id_parent ? 'selected' : ''?> value="">Корневая</option>
					<?php foreach ($categories as $category): ?>
						<?php if ($category->id != $model->id):?>
							<option <?=($category->products || ($category->id_parent == $model->id && $model->id != null)) ? 'disabled' : '' ?> value="<?= $category->id ?>" <?= $category->id == $model->id_parent ? 'selected' : '' ?>>
								<?= $category->name ?>
							</option>
						<?php endif;?>
					<?php endforeach; ?>
				</select>
			</div>
		</div>
		<?php if ($model->id && $model->id_parent == null): ?>
			<div class="form-group">
				<label for="parent_id" class="col-sm-4 control-label">Показывать в шапке</label>
				<div class="col-md-8 col-sm-6 col-xs-4">
					<input type="checkbox" name="<?= Html::getInputName($model, 'show_on_main') ?>" class="form-control" <?=$model->show_on_main ? 'checked' : ''?>>
				</div>
			</div>
		<?php endif;?>
		<?php if ($model->id): ?>
			<div class="form-group">
				<label for="parent_id"  class="col-sm-4 control-label">Статус</label>
				<div class="col-md-8 col-sm-6 col-xs-4">
						<select class="form-control" name="<?= Html::getInputName($model, 'status') ?>">
						<?php foreach (\common\models\db\Category::getStatusLabels() as $status => $label): ?>
							<option <?=$model->status == $status ? 'selected' : ''?> value="<?=$status?>"><?=$label?></option>
						<?php endforeach;?>
					</select>
				</div>
			</div>
		<?php endif;?>
</div>
<div class="panel-footer">
	<div class="row">
		<div class="col-md-12 text-right">
			<button class="btn btn-primary" type="submit"><?= $model->isNewRecord ? 'Создать' : 'Обновить' ?></button>
			<a href="<?=Url::to(['/category']);?>" class="btn btn-danger">Отмена</a>
		</div>
		<div class="clear"></div>
	</div>
</div>
</form>
