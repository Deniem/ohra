<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

$this->title = 'Настройка фильтров каталога';
$this->params['breadcrumbs'][] = [
	'label'=> 'Категории',
	'url' => Url::to(['/category']),
	'itemprop' => 'url',
];
$this->params['breadcrumbs'][] = $this->title;
?>
<div data-view-type="category.FilterList">
	<div class="row">
		<div class="col-md-12">
			<?= Breadcrumbs::widget([
				'homeLink' => [
					'label' => 'Главная',
					'url' => Yii::$app->getHomeUrl(),
					'itemprop' => 'url',
				],
				'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
			]);?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
			<h1><?=$this->title?></h1>
		</div>
	</div>
	<br/>
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
			<div class="panel panel-default">
				<div class="panel-heading">
					<div class="panel-title">
						<h3>Редактировать фильтры</h3>
					</div>
				</div>
				<div class="panel-body property-list-placeholder">
					<script type="text/template" id="property-list-template">
						<% _.each(categories, function(category, cat_idx){ %>
							<% if (category.properties.length > 0) { %>
								<div class="panel panel-default" data-cat-id="<%= cat_idx %>" data-collapsed="<%= category.collapsed %>">
									<div class="panel-heading">
										<h3 class="panel-title"><%= category.name %></h3>
										<div class="panel-options">
											<a href="#" class="collapse-category" data-rel="collapse"><i class="entypo-down-open"></i></a>
										</div>
									</div>

									<div class="panel-body">
										<table class="table">
											<thead>
												<tr class="row">
													<th>Фильтр?</th>
													<th>Характеристика</th>
													<th>Действия</th>
												</tr>
											</thead>
											<% _.each(category.properties, function(prop, idx) { %>
											<tbody>
												<tr class="row" data-cat-id="<%=cat_idx%>" data-prop-id="<%= idx %>">
													<td>
														<% if (prop.is_filter) { %>
														<i class="fa fa-filter"></i>
														<% } //endIf %>
													</td>
													<td>
														<%= prop.name %>
													</td>
													<td>
														<button class="btn btn-switch-filter btn-<%= prop.is_filter ? 'danger' : 'success'%>">
															<%= prop.is_filter ? 'Выключить'  : 'Включить'%>
														</button>
													</td>
												</tr>
											</tbody>
											<% }) //endEach %>
										</table>
									</div>
								</div>
							<% } //endIf %>
						<% }) //endEach %>
					</script>
				</div>
				<div class="panel-footer">
					<a href="<?=Url::to(['/category'])?>" class="btn btn-primary">Назад</a>
				</div>
			</div>
		</div>
	</div>
</div>