<?php

use yii\helpers\Url;
use common\components\widgets\Pager;
use yii\widgets\Breadcrumbs;

$this->title = 'Категории';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
	[
		'label' => 'Preview',
		'value' => 'preview',
		'column-class' => 'category-preview-column'
	],
	[
		'label' => 'Name',
		'attribute' => 'name',
		'value' => 'name'
	],
	[
		'label' => 'Parent Group',
		'attribute' => 'id_parent',
		'value' => 'parentName',
		'filter' => $categoriesFilter
	]
];
?>
<div class="row">
	<div class="col-md-12 col-sm-12">
		<div class="row">
			<div class="col-md-12">
				<?= Breadcrumbs::widget([
					'homeLink' => [
						'label' => 'Главная',
						'url' => Yii::$app->getHomeUrl(),
						'itemprop' => 'url',
					],
					'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
				]);?>
			</div>
		</div>
		<div class="row ">
			<div class="col-md-8 col-sm-8 col-xs-8">
				<h1><?=$this->title?></h1>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-2">
				<div class="row">
					<form role="form" action="<?=Url::to()?>">
						<div class="form-group">
							<label for="CategorySearch['status']">Статус</label>
							<select onChange="$(this).closest('form').submit()" name="CategorySearch['status']" class="form-control" id="product-status-filter">
								<option value="all">Все</option>
								<?php foreach (\common\models\db\Category::getStatusLabels() as $status => $label): ?>
									<option <?=$searchModel->status == $status ? 'selected' : ''?> value="<?=$status?>"><?=$label?></option>
								<?php endforeach;?>
							</select>
						</div>
					</form>
				</div>
			</div>
			<div class="col-md-2 col-sm-2 col-xs-2 text-right">
				<br/>
				<a title="Настройка фильтров" class="btn btn-primary" href="<?= Url::to(['category/filter']); ?>"><i class="fa fa-filter"></i></a>
				<a title="Новая категория" class="btn btn-success" href="<?= Url::to(['category/create']); ?>"><i class="entypo-plus"></i></a>
			</div>
		</div>
		<br/>
		<div class="row">
			<div class="col-md-12">
				<div class="row">
					<div class="col-md-12">
						<?= $this->render('_search', [
							'searchModel' => $searchModel,
							'categoriesFilter' => $categoriesFilter
						]); ?>
					</div>
				</div>
				<?php if ($categories): ?>
				<table class="table table-bordered table-striped table-hover text-center font-size-16">
					<thead>
						<tr class="row ">
							<th class="col-sm-3">Превью</th>
							<th>Название</th>
							<th>В шапке?</th>
							<th>Родитель</th>
							<th width="1%">Действия</th>
						</tr>
					</thead>
					<tbody>
					<?php foreach($categories as $category): ?>
						<tr class="row">
							<td class="middle-align"><?= Yii::$app->helpers->formattingProperty($category, 'previewFill')?></td>
							<td class="middle-align text-left">
								<a href="<?=Url::to(['category/update/', 'id'=>$category->id])?>">
									<?= Yii::$app->helpers->formattingProperty($category, 'name')?>
								</a>
							</td>
							<td class="middle-align">
								<div class="label label-<?=$category->show_on_main ? 'success' : 'danger'?>">
									<i class="entypo-<?= Yii::$app->helpers->formattingProperty($category, 'show_on_main') ? 'check' : 'cancel';?>"></i>
								</div>
							</td>
							<td class="middle-align"><?= Yii::$app->helpers->formattingProperty($category, 'parentName')?></td>
							<td class="middle-align">
								<nobr>
									<a title="Редактировать" class="btn btn-primary " href="<?= Url::to(['category/update', 'id' => $category->id]) ?>">
										<i class="entypo-pencil"></i>
									</a>
									<a title="Удалить" class="btn btn-danger" href="<?= Url::to(['category/delete', 'id' => $category->id]) ?>">
										<i class="entypo-cancel"></i>
									</a>
								</nobr>
							</td>
						</tr>
					<?php endforeach;?>
					</tbody>
				</table>
				<?php else: ?>
					<div class="well well-default">
						<h3>Еще не создано ни единой категории</h3>
					</div>
				<?php endif;?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 ">
					Показано <?= $pages->pageSize < $pages->totalCount ? $pages->pageSize : $pages->totalCount ?> из <?= $pages->totalCount ?> записей
			</div>
			<div class="col-md-6 text-right">
				<?= Pager::widget([
					'pages' => $pages,
					'viewTemplate' => 'pager/backend'
				]);	?>
			</div>
		</div>
	</div>
</div>
