<?php
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;

$this->title = "Создание категории";
$this->params['breadcrumbs'][] = [
	'label'=> 'Категории',
	'url' => Url::to(['/category']),
	'itemprop' => 'url',
];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
	<div class="col-md-12">
		<?= Breadcrumbs::widget([
			'homeLink' => [
				'label' => 'Главная',
				'url' => Yii::$app->getHomeUrl(),
				'itemprop' => 'url',
			],
			'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
		]);?>
	</div>
</div>
<div class="row">
	<div class="col-md-12">
		<h1><?=$this->title?></h1>
	</div>
</div>
<br/>
<div class="row">
	<div class="col-md-2"></div>
	<div class="col-md-8">
		<div class="panel panel-primary">
			<div class="panel-heading">
				<div class="panel-title">
					<h3>Новая категория</h3>
				</div>
			</div>
			<?= $this->render('_form', [
			    'model' => $model,
			    'categories' => $categories,
			]) ?>
		</div>
	</div>
</div>
