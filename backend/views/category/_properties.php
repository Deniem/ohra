<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\db\Units;

$units = Units::find()->all();

?>
<div class="panel-body" data-view-type="category.propertyList">
	<div class="property-list-placeholder property-list"></div>
	<script type="text/template" id="property-list-template">
		<% if (properties.length > 0){ %>
		<table class=" table table-hover responsive">
			<thead>
				<tr class="row th-center">
					<th>Название</th>
					<th>Еденицы измерения</th>
					<th>Фильтр?</th>
					<th>Действия</th>
				</tr>
			</thead>
			<tbody>
			<%_.each(properties, function(prop, key){ %>
				<tr data-id="<%= prop.id %>" data-key="<%=key%>" data-category-id="<%= prop.id_category %>" class="row property-line">
					<td>
						<input type="text" value="<%= prop.name %>" name="name" class="form-control prop-input">
					</td>
					<td>
						<select id="unit_list" name="id_unit" class="form-control prop-input">
							<?php foreach ($units as $unit):?>
								<option <%= prop.id_unit == <?=$unit->id?> ? 'selected' : '' %> value="<?=$unit->id?>" >
									<?=$unit->name?>
								</option>
							<?php endforeach;?>
						</select>
					</td>
					<td>
						<input class="form-control prop-input" name="is_filter" type="checkbox" <%= prop.is_filter ? 'checked' : '' %> >
					</td>
					<td class="text-center">
						<button type="button" disabled class="btn btn-primary btn-update">Обновить</button>
						<button type="button" <%= prop.values.length ? 'disabled' :'' %> class="btn btn-danger btn-delete">Удалить</button>
					</td>
				</tr>
			<% }) //endEach %>
			</tbody>
		</table>
		<% } else { %>
		<div data-prev-html=""  class="alert alert-info loader">
			Для данной категории характеристики не выбраны
		</div>
		<% } //endif %>
	</script>
	<hr>
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-info">
				<div class="panel-heading">
					<div class="panel-title">
						<h4>Добавить новую характеристику</h4>
					</div>
				</div>
				<form role="form" id="add-property-form" class="add-property-form">
					<div class="panel-body">
						<table class="table">
							<thead>
								<tr class="row">
									<th>Название</th>
									<th>Фильтр?</th>
									<th>Еденицы измерения</th>
								</tr>
							</thead>
							<tbody>
								<tr class="row">
									<td>
										<input type="text" class="form-control" id="name" name="name" value="">
									</td>
									<td>
										<input class="form-control" name="is_filter" type="checkbox">
									</td>
									<td>
										<select id="unit_list" name="id_unit" class="form-control">
											<?php foreach ($units as $unit):?>
												<option  value="<?=$unit->id?>"><?=$unit->name?></option>
											<?php endforeach;?>
										</select>
									</td>
								</tr>
							</tbody>
						</table>
						<!--<div class="form-group">
							<label for="field-1" class="col-sm-2 control-label">Название</label>
							<div class="col-sm-6">

							</div>
							<div class="col-sm-6">

							</div>
							<label for="" class="col-sm-2 control-label">Ед.</label>
							<div class="col-sm-2">

							</div>
						</div>-->
					</div>
					<div class="panel-footer">
						<div class="row text-right">
							<div class="col-md-6"></div>
							<div class="col-md-6">
								<input type="hidden" class="form-control" id="category_id" name="id_category" value="<?=$model->id?>">
								<button
									data-loading-text="Сохраняем"
									class="btn btn-success btn-save">
									<i class="glyphicon glyphicon-plus"></i> Добавить
								</button>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<div class="panel-footer">
	<div class="clear"></div>
</div>
