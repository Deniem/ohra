<?php

namespace backend\assets;

use Yii;
use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{

	public $basePath = '@webroot';
	public $baseUrl = '@web';

	public $css = [
        'css/bootstrap.css',
        'css/datatables.css',
        'css/neon-core.css',
        'css/neon-forms.css',
        'css/neon-theme.css',
        'css/custom.css',
        'css/font-icons/entypo/css/entypo.css',
		'css/font-icons/font-awesome/css/font-awesome.min.css',
        'js/selectboxit/jquery.selectBoxIt.css',
        'js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css',
        'http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic',
    ];
	public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
	public $js = [

		'js/gsap/TweenMax.min.js',
		'js/gsap/main-gsap.js',
		'js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js',
		'js/bootstrap.js',
		'js/joinable.js',
		'js/resizeable.js',
		'js/neon-api.js',
		'js/cookies.min.js',
		'js/jvectormap/jquery-jvectormap-1.2.2.min.js',
		'js/jvectormap/jquery-jvectormap-europe-merc-en.js',
		'js/jquery.sparkline.min.js',
		'js/jquery.validate.min.js',
		'js/jquery.nestable.js',
		'js/rickshaw/vendor/d3.v3.js',
		'js/rickshaw/rickshaw.min.js',
		'js/raphael-min.js',
		'js/morris.min.js',
		'js/toastr.js',
		'js/selectboxit/jquery.selectBoxIt.min.js',
		'js/select2/select2.min.js',
		'js/neon-custom.js',
		'js/neon-login.js',
		'js/support.js',

		/*not theme files*/
		'files/js/vendors/underscore-min.js',
		'files/js/vendors/backbone-min.js',
		'files/js/vendors/backbone-validation.js',
		'files/js/vendors/backbone-relations.js',
		'files/js/vendors/dropzone.js',
		'files/js/vendors/loadingoverlay.min.js',

		/*JS BACKBONE APPLICATION*/
		'files/js/app/app.js',
		'files/js/app/global.js',
		'files/js/app/modal.js',
		'files/js/app/helpers.js',

		/*js models*/
		'files/js/app/models/brand.js',
		'files/js/app/models/property.js',
		'files/js/app/models/discount.js',
		'files/js/app/models/order-item-discount.js',
		'files/js/app/models/value.js',
		'files/js/app/models/product.js',
		'files/js/app/models/product-property.js',
		'files/js/app/models/category.js',
		'files/js/app/models/settings.js',
		'files/js/app/models/order-item.js',
		'files/js/app/models/order.js',
		'files/js/app/models/preorder.js',
		'files/js/app/models/preorder-item.js',
		'files/js/app/models/customer.js',
		'files/js/app/models/user.js',
		'files/js/app/models/blog.js',

		/*views*/
		'files/js/app/views/brand/brand.js',
		'files/js/app/views/category/category.js',
		'files/js/app/views/product/product.js',
		'files/js/app/views/order/order.js',
		'files/js/app/views/order-item/order-item.js',
		'files/js/app/views/customer/customer.js',
		'files/js/app/views/cabinet/cabinet.js',
		'files/js/app/views/blog/blog.js',
		'files/js/app/views/import/import.js',

		/*widgets*/
		'files/js/app/views/widgets/category-property-list.js',
		'files/js/app/views/widgets/product-property-list.js',
		'files/js/app/views/widgets/settings-list.js',

		/*modals*/
		'files/js/app/views/modals/add-property-value.js',
		'files/js/app/views/modals/brand-delete.js',
		'files/js/app/views/modals/order-delete.js',
		'files/js/app/views/modals/blog-delete.js',
	];
}


