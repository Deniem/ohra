<?php

use yii\db\Migration;

class m160801_152212_product_add_top_sale_property extends Migration
{
    public function up()
    {
        $this->addColumn('product', 'top_sale',  "tinyint(1) NOT NULL DEFAULT 0");
    }

    public function down()
    {
        echo "m160801_152212_product_add_top_sale_property cannot be reverted.\n";

        return false;
    }
}
