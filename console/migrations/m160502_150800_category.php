<?php

use yii\db\Migration;
use yii\db\Schema;

class m160502_150800_category extends Migration
{
    public function up()
    {
	    $this->createTable('category',[
		    'id'=> Schema::TYPE_PK,
		    'id_parent' => 'int(11) DEFAULT NULL',
		    'name' => 'varchar(255) NOT NULL'
	    ]);
	    $this->addForeignKey('fk_category', 'category', 'id_parent', 'category', 'id');
    }

    public function down()
    {
        echo "m160502_150800_category cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
