<?php

use yii\db\Migration;

class m160508_184504_backend_admin_user extends Migration
{
    public function up()
    {
	    $this->delete('user',[
		    'username' => 'admin',
		    'email' => 'admin@admin.com'
	    ]);

		$this->insert('user',[
			'username' => 'admin',
			'auth_key' => '0DOoC6RLr_LMH1hNy7rvP3rE6h3Tsq8_',
			'password_hash' => '$2y$13$hRdH8pyuxJD4.ldga/aIuOzRh2w7EmhjjN59hw668V7.oEZUQf/DW',
			'email' => 'admin@admin.com',
			'status' => '10',
			'created_at' => '1462733046',
			'updated_at' => '1462733046',
			'role' => '10',
		]);
    }

    public function down()
    {
        echo "m160508_184504_backend_admin_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
