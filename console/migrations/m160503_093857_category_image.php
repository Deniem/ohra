<?php

use yii\db\Migration;

class m160503_093857_category_image extends Migration
{
    public function up()
    {
		$this->addColumn('category', 'thumbnail_base_url', 'varchar(1024) DEFAULT NULL');
		$this->addColumn('category', 'thumbnail_path', 'varchar(1024) DEFAULT NULL');
    }

    public function down()
    {
        echo "m160503_093857_category_image cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
