<?php

use yii\db\Migration;

class m160707_120002_customerFavorite_remove extends Migration
{
    public function up()
    {
        if (\Yii::$app->db->schema->getTableSchema('customer_favorite', true) !== null) {
            $this->dropForeignKey('fk_customer_fav_customer_id', 'customer_favorite');
            $this->dropForeignKey('fk_customer_fav_product_id', 'customer_favorite');
            $this->dropForeignKey('fk_customer_compare_customer_id', 'customer_favorite');
            $this->dropForeignKey('fk_customer_compare_product_id', 'customer_favorite');
            $this->dropTable('customer_favorite');
        }


    }

    public function down()
    {
        echo "m160707_120002_customerFavorite_add_pk cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migavoriteration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
