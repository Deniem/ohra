<?php

use yii\db\Migration;

class m160607_085850_content_pages_add_field_IdParent extends Migration
{
    public function up()
    {
        $this->addColumn('content_pages', 'id_parent', $this->integer(11)->notNull()->defaultValue(0));
    }

    public function down()
    {
        echo "m160607_085850_content_pages_add_field_IdParent cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
