<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tbl_discount`.
 */
class m160804_163804_create_tbl_discount extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('discount', [
            'id' => $this->primaryKey(),
            'prod_id' => $this->integer(11)->notNull(),
            'price' => 'FLOAT(19, 2) NOT NULL ',
            'value' => 'FLOAT(9, 2) NOT NULL',
            'type' => "enum('percentage', 'sum') NOT NULL",
            'status' => "enum('enabled', 'disabled')",
            'start_date' => 'timestamp NOT NULL',
            'end_date' => 'timestamp NOT NULL'
        ]);
        
        $this->addForeignKey('fk_discount_product_id', 'discount', 'prod_id', 'product' , 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tbl_discount');
    }
}
