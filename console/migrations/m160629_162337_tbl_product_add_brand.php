<?php

use yii\db\Migration;

class m160629_162337_tbl_product_add_brand extends Migration
{
    public function up()
    {
        $this->insert('brand', [
            'id' => 1,
            'name' => 'test brand'
            ]
        );
        $this->addColumn('product', 'brand_id' , $this->integer(11)->notNull()->defaultValue(1));
        $this->addForeignKey('fk_brand_id', 'product', 'brand_id', 'brand', 'id');
    }

    public function down()
    {
        echo "m160629_162337_tbl_product_add_brand cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
