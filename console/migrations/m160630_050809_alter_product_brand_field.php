<?php

use yii\db\Migration;

class m160630_050809_alter_product_brand_field extends Migration
{
    public function up()
    {
        $this->alterColumn('product', 'brand_id', $this->integer(11)->defaultValue(null));
    }

    public function down()
    {
        echo "m160630_050809_alter_product_brand_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
