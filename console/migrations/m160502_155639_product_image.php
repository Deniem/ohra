<?php

use yii\db\Migration;
use yii\db\Schema;

class m160502_155639_product_image extends Migration
{
    public function up()
    {
	    $this->createTable('product_image',[
		    'id'=> Schema::TYPE_PK,
		    'id_product' => 'int(11) NOT NULL',
		    'base_url' => 'varchar(1024) DEFAULT NULL',
		    'path' => 'varchar(1024) DEFAULT NULL'
	    ]);
	    $this->addForeignKey('fk_productImage_product', 'product_image', 'id_product', 'product', 'id');
    }

    public function down()
    {
        echo "m160502_155639_product_image cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
