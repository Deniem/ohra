<?php

use yii\db\Migration;

class m160429_140033_alter_user_table_with_roles extends Migration
{
    public function up()
    {
        $this->addColumn("user","role","enum('10','5','1') NOT NULL DEFAULT '1'");
    }

    public function down()
    {
        echo "m160429_140033_alter_user_table_with_roles cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
