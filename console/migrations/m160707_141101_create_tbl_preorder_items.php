<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tbl_preorder_items`.
 */
class m160707_141101_create_tbl_preorder_items extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('preorder_items', [
            'id' => $this->primaryKey()->notNull(),
            'preorder_id' => $this->integer(11)->defaultValue(null),
            'name' => $this->char(255)->defaultValue(null),
            'price' => 'FLOAT(19, 2) NOT NULL ',
            'product_id' => $this->integer(11)->defaultValue(null),
            'quantity' => $this->integer(11)->defaultValue(1)
        ]);

        $this->addForeignKey('fk_preorder_product_id', 'preorder_items', 'product_id', 'product', 'id');
        $this->addForeignKey('fk_preorder_id', 'preorder_items', 'preorder_id', 'preorder', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tbl_preorder_items');
    }
}
