<?php

use yii\db\Migration;

class m160623_102127_tbl_product_add_slug extends Migration
{
    public function up()
    {
        $table = Yii::$app->db->schema->getTableSchema('product');
        if($table->getColumn('slug') === null){
            echo "Will create column slug";
            $this->addColumn('product', 'slug', $this->char(255)->defaultValue(null));
        }else{
            echo "slug column already exist";
        }
    }

    public function down()
    {
        echo "m160623_102127_tbl_product_add_slug cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
