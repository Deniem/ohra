<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tbl_customercompare`.
 */
class m160707_123301_create_tbl_customerCompare extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('customer_compare', [
            'id' => $this->primaryKey(11),
            'customer_id' => $this->integer(11)->notNull(),
            'product_id' => $this->integer(11)->notNull()
        ]);

        $this->addForeignKey('fk_customer_compare_customer_id', 'customer_compare', 'customer_id',
            'customer', 'id');
        $this->addForeignKey('fk_customer_compare_product_id', 'customer_compare', 'product_id',
            'product', 'id');

    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tbl_customercompare');
    }
}
