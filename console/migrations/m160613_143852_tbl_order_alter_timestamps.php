<?php

use yii\db\Migration;

class m160613_143852_tbl_order_alter_timestamps extends Migration
{
    public function up()
    {
        $this->alterColumn('order', 'created_at', 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP');
        $this->alterColumn('order', 'updated_at', 'timestamp NULL');
    }

    public function down()
    {
        echo "m160613_143852_tbl_order_alter_timestamps cannot be reverted.\n";

        return false;
    }
    
}
