<?php

use yii\db\Migration;

class m160808_144359_create_tbl_order_item_discount extends Migration
{
    public function up()
    {
        $this->createTable('order_items_discount', [
            'id' => $this->primaryKey(),
            'order_item_id' => $this->integer(11)->notNull(),
            'price' => 'FLOAT(19, 2) NOT NULL ',
            'value' => 'FLOAT(9, 2) NOT NULL',
            'type' => "enum('percentage', 'sum') NOT NULL",
            'status' => "enum('enabled', 'disabled')",
            'start_date' => 'timestamp NULL',
            'end_date' => 'timestamp NULL'
        ]);

        $this->addForeignKey('fk_discount_order_items_id',
            'order_items_discount', 'order_item_id',
            'order_items' , 'id');
    }

    public function down()
    {
        echo "m160808_144359_create_tbl_order_item_discount cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
