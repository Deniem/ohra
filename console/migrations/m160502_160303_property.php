<?php

use yii\db\Migration;
use yii\db\Schema;

class m160502_160303_property extends Migration
{
    public function up()
    {
	    $this->createTable('property',[
		    'id'=> Schema::TYPE_PK,
		    'name' => 'varchar(255) NOT NULL',
		    'id_category' => 'int(11) NOT NULL'
	    ]);
	    $this->addForeignKey('fk_property_category', 'property', 'id_category', 'category', 'id');
    }

    public function down()
    {
        echo "m160502_160303_property cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
