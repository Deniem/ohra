<?php

use yii\db\Migration;

class m170301_131134_product_us_eur_prict_article extends Migration
{
    public function up()
    {
        $table = 'product';
        $this->addColumn($table, 'price_usd', 'decimal(11,2) NULL');
        $this->addColumn($table, 'price_eur', 'decimal(11,2) NULL');
        $this->addColumn($table, 'article', ' varchar(30) NOT NULL');

        $this->createIndex('product_article_idx', $table, 'article', true);
    }

    public function down()
    {
        echo "m170301_131134_product_us_eur_prict_article cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
