<?php

use yii\db\Migration;
use yii\db\Schema;

class m160502_153559_feature extends Migration
{
    public function up()
    {
	    $this->createTable('feature',[
		    'id'=> Schema::TYPE_PK,
		    'name' => 'varchar(255) NOT NULL',
		    'status' => 'enum("active", "not active") DEFAULT "active"'
	    ]);
    }

    public function down()
    {
        echo "m160502_153559_feature cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
