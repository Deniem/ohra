<?php

use yii\db\Migration;

class m160808_144621_alter_table_discount_start_date extends Migration
{
    public function up()
    {
        $this->alterColumn('discount', 'start_date', 'timestamp NULL');
    }

    public function down()
    {
        echo "m160808_144621_alter_table_discount_start_date cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
