<?php

use yii\db\Migration;

class m160817_133016_create_table_blog_page extends Migration
{
    public function up()

    {
        $this->createTable('blog', [
            'id' => $this->primaryKey(),
            'h1' => $this->text()->defaultValue(null),
            'title' => $this->text()->defaultValue(null),
            'keywords' => $this->text()->defaultValue(null),
            'description' => $this->text()->defaultValue(null),
            'content' => $this->text()->defaultValue(null),
            'created_at' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at' => 'timestamp NULL',
            'published_at' => 'timestamp NULL',
            'status' => "enum('draft', 'active', 'inactive', 'archive')",
            'action' => $this->text()->defaultValue(null),
            'thumbnail_base_url' => 'varchar(1024) DEFAULT NULL',
            'thumbnail_path' => 'varchar(1024) DEFAULT NULL',
        ]);
    }
    public function down()
    {
        echo "m160817_133016_create_table_blog_page cannot be reverted.\n";

        return false;
    }
}
