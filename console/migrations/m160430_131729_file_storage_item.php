<?php

use yii\db\Migration;
use yii\db\Schema;

class m160430_131729_file_storage_item extends Migration
{
    public function up()
    {
	    $this->createTable('file_storage_item',[
		    'id'=> Schema::TYPE_PK,
		    'component' => 'varchar(255) NOT NULL',
		    'base_url' => 'varchar(1024) NOT NULL',
		    'path' => 'varchar(1024) NOT NULL',
		    'type' => 'varchar(255) DEFAULT NULL',
		    'size' => 'int(11) DEFAULT NULL',
		    'name' => 'varchar(255) DEFAULT NULL',
		    'upload_ip' => 'varchar(15) DEFAULT NULL',
		    'created_at' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP'
	    ]);
    }

    public function down()
    {
        echo "m160430_131729_file_storage_item cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
