<?php

use yii\db\Migration;

class m160721_115946_settings_base_values extends Migration
{
    public function up()
    {
		$defaultValues = [
			'admin_email' => 'admin@admin.com',
			'contact_address' => 'Украина, г. Херсон, ул. Вишнёвая, 60',
			'contact_phone_1' => '+38 050 65-73-828',
			'contact_phone_2' => '+38 0552 42-72-62',
			'about_text' => 'Ohra — профессионалы, люди, которые любят своё дело. Компания вложила им в руки лучшие материалы и инструменты для реализации проектов вашей будущей мебели.',
			'socials_vk' => 'http://vk.com',
			'socials_fb' => 'https://www.facebook.com/',
			'socials_gplus' => 'https://plus.google.com/',
			'copyright_text' => '© 2016 OHRA',
			'callback_link' => 'http://otzyv.ua',
			'header_phone' => '0-800-600-006',
		];

		foreach ($defaultValues as $name => $value) {
			echo $this->db->createCommand('update settings set value = "' . $value . '" where name = "' . $name . '" ')->execute();

		}

	}

    public function down()
    {
        echo "m160721_115946_settings_base_values cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
