<?php

use yii\db\Migration;

class m160707_115955_customerCompare_remove extends Migration
{
    public function up()
    {

        if (\Yii::$app->db->schema->getTableSchema('customer_compare',true) !== null) {
            $this->dropTable('customer_compare');
        }

     
    }

    public function down()
    {
        echo "m160707_115955_customerCompare_add_pk cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
