<?php

use yii\db\Migration;

class m160609_091250_tbl_category_add_slug extends Migration
{
    public function up()
    {
            $this->addColumn('category', 'slug', 'string');
    }

    public function down()
    {
        echo "m160609_091250_tbl_category_add_slug cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
