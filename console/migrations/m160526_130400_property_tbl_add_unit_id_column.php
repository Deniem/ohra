<?php

use yii\db\Migration;

class m160526_130400_property_tbl_add_unit_id_column extends Migration
{
    public function up()
    {
        $this->addColumn("property","id_unit","int(10) NOT NULL DEFAULT 1");
        $this->addForeignKey('fk_property_units','property','id_unit','units','id','RESTRICT','RESTRICT');
    }

    public function down()
    {
        echo "m160526_130400_property_tbl_add_unit_id_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
