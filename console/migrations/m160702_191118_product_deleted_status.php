<?php

use yii\db\Migration;

class m160702_191118_product_deleted_status extends Migration
{
    public function up()
    {
		$this->alterColumn('product', 'status', 'enum("active", "not active", "conditionally active", "deleted") NOT NULL DEFAULT "not active"');
    }

    public function down()
    {
        echo "m160702_191118_product_deleted_status cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
