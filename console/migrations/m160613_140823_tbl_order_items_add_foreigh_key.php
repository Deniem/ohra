<?php

use yii\db\Migration;

class m160613_140823_tbl_order_items_add_foreigh_key extends Migration
{
    public function up()
    {
        $this->addForeignKey('fk_order_id', 'order_items', 'order_id', 'order', 'id');
    }

    public function down()
    {
        echo "m160613_140823_tbl_order_items_add_foreigh_key cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
