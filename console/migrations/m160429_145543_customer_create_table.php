<?php

use yii\db\Migration;
use yii\db\Schema;
class m160429_145543_customer_create_table extends Migration
{
    public function up()
    {
        $this->createTable('customer',[
            'id'=>Schema::TYPE_PK,
            'username' => 'varchar(255) NOT NULL',
            'auth_key' => 'varchar(32) NOT NULL',
            'password_hash' => 'varchar(255) NOT NULL',
            'password_hash' => 'varchar(255) NOT NULL',
            'email' => 'varchar(255) NOT NULL',
            'status' => 'smallint(6) NOT NULL DEFAULT 1',
            'created_at' => 'int(11) NOT NULL',
            'updated_at' => 'int(11) NOT NULL',
            'UNIQUE KEY `username` (`username`)',
            'UNIQUE KEY `email` (`email`)'
        ]);
    }

    public function down()
    {
        echo "m160429_145543_customer_create_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
