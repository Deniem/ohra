<?php

use yii\db\Migration;

class m160707_161740_add_preorder_item_is_active extends Migration
{
    public function up()
    {
        $this->addColumn('preorder_items', 'is_active', $this->boolean()->notNull()->defaultValue(false) );
    }

    public function down()
    {
        echo "m160707_161740_add_preorder_item_is_active cannot be reverted.\n";

        return false;
    }
    
}
