<?php

use yii\db\Migration;

class m160506_144704_product_image extends Migration
{
    public function up()
    {
	    $this->addColumn('product', 'thumbnail_base_url', 'varchar(1024) DEFAULT NULL');
	    $this->addColumn('product', 'thumbnail_path', 'varchar(1024) DEFAULT NULL');
    }

    public function down()
    {
        echo "m160506_144704_product_image cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
