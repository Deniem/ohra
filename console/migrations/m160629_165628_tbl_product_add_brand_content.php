<?php

use yii\db\Migration;

class m160629_165628_tbl_product_add_brand_content extends Migration
{
    public function up()
    {
        $this->addColumn('brand', 'content', $this->text());
    }

    public function down()
    {
        echo "m160629_165628_tbl_product_add_brand_content cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
