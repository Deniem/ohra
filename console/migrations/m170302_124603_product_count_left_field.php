<?php

use yii\db\Migration;

class m170302_124603_product_count_left_field extends Migration
{
    public function up()
    {
        $this->addColumn('product', 'count_left', 'int(10) NOT NULL default 0');
    }

    public function down()
    {
        echo "m170302_124603_product_count_left_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
