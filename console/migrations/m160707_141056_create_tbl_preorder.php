<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tbl_preorder`.
 */
class m160707_141056_create_tbl_preorder extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('preorder', [
            'id' => $this->primaryKey(),
            'created_at' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at' => 'timestamp NULL',
            'name' => $this->text(),
            'delivery' => 'enum("courier", "self-pickup", "shipping_service") NOT NULL DEFAULT "shipping_service"',
            'payment_method' => 'enum("cash_in office", "c.o.d.", "cash_shipping_service") NOT NULL DEFAULT "cash_shipping_service"',
            'phone' => $this->char(255)->defaultValue(null),
            'city' => $this->text(),
            'address' => $this->text(),
            'email' => $this->char(255)->defaultValue(null),
            'notes' => $this->text(),
            'customer_id' => $this->integer(11),
            'is_active' => $this->boolean()->notNull()->defaultValue(false)
        ]);

        $this->addForeignKey('fk_preorder_customer_id', 'preorder', 'customer_id', 'customer', 'id', 'RESTRICT','RESTRICT');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tbl_preorder');
    }
}
