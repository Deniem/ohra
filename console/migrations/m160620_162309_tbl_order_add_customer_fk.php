<?php

use yii\db\Migration;

class m160620_162309_tbl_order_add_customer_fk extends Migration
{
    public function up()
    {
        $this->addForeignKey('fk_customer_id', 'order', 'customer_id', 'customer', 'id', 'RESTRICT','RESTRICT');
    }

    public function down()
    {
        echo "m160620_162309_tbl_order_add_customer_fk cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
