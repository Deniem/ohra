<?php

use yii\db\Migration;

class m160624_134959_tbl_customer_change_date_format extends Migration
{
    public function up()
    {
		$this->alterColumn('customer','created_at','timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP');
		$this->alterColumn('customer', 'updated_at', 'timestamp NULL DEFAULT NULL');
	}

    public function down()
    {
        echo "m160624_134959_tbl_customer_change_date_format cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
