<?php

use yii\db\Migration;
use yii\db\Schema;

class m160526_130258_property_units_table_create extends Migration
{
    public function up()
    {
        $this->createTable('units',[
            'id'=> Schema::TYPE_PK,
            'name' => 'varchar(255) NOT NULL',
            'short_name' => 'varchar(255)'
        ]);

        $this->insert('units',['name' => 'нет','short_name' => '']);
    }

    public function down()
    {
        echo "m160526_130258_property_units_table_create cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
