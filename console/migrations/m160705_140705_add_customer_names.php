<?php

use yii\db\Migration;

class m160705_140705_add_customer_names extends Migration
{
    public function up()
    {
        $this->addColumn('customer', 'first_name', $this->char(255));
        $this->addColumn('customer', 'last_name', $this->char(255));
        $this->addColumn('customer', 'middle_name', $this->char(255));
    }

    public function down()
    {
        echo "m160705_140705_add_customer_names cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
