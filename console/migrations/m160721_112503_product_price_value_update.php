<?php

use yii\db\Migration;

class m160721_112503_product_price_value_update extends Migration
{
    public function up()
    {
		$this->alterColumn('product','price',' decimal(11,2) NOT NULL DEFAULT 0');
    }

    public function down()
    {
        echo "m160721_112503_product_price_value_update cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
