<?php

use yii\db\Migration;

class m160713_001819_add_tbl_preorder_order_confirmed_value extends Migration
{
    public function up()
    {
        $this->addColumn('preorder', 'is_order_confirmed', $this->boolean()->notNull()->defaultValue(false));
    }

    public function down()
    {
        echo "m160713_001819_add_tbl_preorder_order_confirmed_value cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
