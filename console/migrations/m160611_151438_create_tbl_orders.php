<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tbl_orders`.
 */
class m160611_151438_create_tbl_orders extends Migration
{
    /**
     * @inheritdoc
     * C.O.D - cash on delivery - наложенный платеж
     * delete phone, city, address, email after user order implementation
     */
    public function up()
    {
        $this->createTable('order', [
            'id' => $this->primaryKey()->notNull(),
            'created_at' => $this->integer(11)->defaultValue(null),
            'updated_at' => $this->integer(11)->defaultValue(null),
            'name' => $this->text(),
            'delivery' => 'enum("courier", "self-pickup", "shipping_service") NOT NULL DEFAULT "shipping_service"',
            'payment_method' => 'enum("cash_in office", "c.o.d.", "cash_shipping_service") NOT NULL DEFAULT "cash_shipping_service"',
            'phone' => $this->char(255)->defaultValue(null),
            'city' => $this->text(),
            'address' => $this->text(),
            'email' => $this->char(255)->defaultValue(null),
            'notes' => $this->text(),
            'status' => 'enum("created", "viewed", "verified", "pending", "in_process", "sent", "rejected", "complete") 
                NOT NULL DEFAULT "created"',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tbl_orders');
    }
}
