<?php

use yii\db\Migration;
use yii\db\Schema;

class m160506_125515_settings_table extends Migration
{
    public function up()
    {

        $this->createTable('settings',[
            'id'=> Schema::TYPE_PK,
            'name' => 'varchar(255) NOT NULL',
            'description' => 'text NOT NULL',
            'value' => 'varchar(255) NULL',
            'type' => "enum('global','backend','frontend')",
            'value_type' => "enum('checkbox','text','number','email','url','tel')",
            'required' => 'tinyint(1) NOT NULL DEFAULT 0'
        ]);

    }

    public function down()
    {
        echo "m160506_125515_settings_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
