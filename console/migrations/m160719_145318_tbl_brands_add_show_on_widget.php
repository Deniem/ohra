<?php

use yii\db\Migration;

class m160719_145318_tbl_brands_add_show_on_widget extends Migration
{
    public function up()
    {
        $this->addColumn('brand', 'is_top_brand', $this->boolean()->defaultValue(false));
    }

    public function down()
    {
        echo "m160719_145318_tbl_brands_add_show_on_widget cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
