<?php

use yii\db\Migration;
use yii\db\Schema;

class m160502_155031_product_feature extends Migration
{
    public function up()
    {
	    $this->createTable('product_feature',[
		    'id'=> Schema::TYPE_PK,
			'id_product' => 'int(11) NOT NULL',
			'id_feature' => 'int(11) NOT NULL'
	    ]);
	    $this->addForeignKey('fk_productFeature_product', 'product_feature', 'id_product', 'product', 'id');
	    $this->addForeignKey('fk_productFeature_feature', 'product_feature', 'id_feature', 'feature', 'id');
    }

    public function down()
    {
        echo "m160502_155031_product_feature cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
