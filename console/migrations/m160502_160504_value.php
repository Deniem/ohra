<?php

use yii\db\Migration;
use yii\db\Schema;

class m160502_160504_value extends Migration
{
    public function up()
    {
	    $this->createTable('value',[
		    'id'=> Schema::TYPE_PK,
		    'name' => 'varchar(255) NOT NULL',
		    'id_property' => 'int(11) NOT NULL'
	    ]);
	    $this->addForeignKey('fk_value_property', 'value', 'id_property', 'property', 'id');
    }

    public function down()
    {
        echo "m160502_160504_value cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
