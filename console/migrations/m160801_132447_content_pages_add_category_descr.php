<?php

use yii\db\Migration;

class m160801_132447_content_pages_add_category_descr extends Migration
{
    public function up()
    {
        $this->insert('content_pages', [
            'h1' => 'Каталог',
            'title' => 'Каталог',
            'description' => 'каталог',
            'keywords' => 'Каталог',
            'content' => 'Контент каталог страницы по умолчанию.
                Есть мебель, которая покоряет с первого взгляда, она удивляет своим дизайном или функциональностью.
                 Всё потому, что у неё есть характер, неповторимый и интересный. Мы собрали множество необычных и 
                 очень харизматичных предметов мебели и декора, которые способны разнообразить интерьер, удивить и 
                 влюбить в себя. Среди предметов интерьера есть особенно стильные и очаровательные, которые способны 
                 кардинально преобразить любое помещение. Именно благодаря таким вещам дом или квартира выглядят уютными 
                 и теплыми, а у офиса появиться собственный стиль. В таком доме хочется жить и отдыхать, а в офисе 
                 работать.',
            'status' => 1,
            'action' => 'catalog',
            'can_delete' => 0,
            'id_parent' => 0
        ]);
    }

    public function down()
    {
        echo "m160801_132447_content_pages_add_category_descr cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
