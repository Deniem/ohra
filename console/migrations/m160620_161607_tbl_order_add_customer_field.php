<?php

use yii\db\Migration;

class m160620_161607_tbl_order_add_customer_field extends Migration
{
    public function up()
    {
        $this->addColumn('order', 'customer_id', $this->integer(11)->notNull());
    }

    public function down()
    {
        echo "m160620_161607_tbl_order_add_customer_field cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
