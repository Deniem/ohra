<?php

use yii\db\Migration;

class m160603_133956_units_list extends Migration
{
    public function up()
    {
        $table = 'units';
        $list = [
          ['name'=>'метры', 'short_name'=>'м'],
          ['name'=>'сантиметры', 'short_name'=>'см'],
          ['name'=>'миллиметры', 'short_name'=>'мм'],
          ['name'=>'граммы', 'short_name'=>'гр'],
          ['name'=>'килограммы', 'short_name'=>'кг'],
          ['name'=>'метры квадратные', 'short_name'=>'м<sup>2</sup>']
        ];

        foreach ($list as $unit){
            $this->insert($table,[
               'name' => $unit['name'], 'short_name' => $unit['short_name']
            ]);
        }
    }

    public function down()
    {
        echo "m160603_133956_units_list cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
