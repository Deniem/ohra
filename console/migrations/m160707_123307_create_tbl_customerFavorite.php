<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tbl_customerfavorite`.
 */
class m160707_123307_create_tbl_customerFavorite extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('customer_favorite', [
            'id' => $this->primaryKey(11),
            'customer_id' => $this->integer(11)->notNull(),
            'product_id' => $this->integer(11)->notNull()
        ]);

        $this->addForeignKey('fk_customer_fav_customer_id', 'customer_favorite', 'customer_id',
            'customer', 'id');
        $this->addForeignKey('fk_customer_fav_product_id', 'customer_favorite', 'product_id',
            'product', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tbl_customerfavorite');
    }
}
