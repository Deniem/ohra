<?php

use yii\db\Migration;

class m160718_135916_category_show_on_main_statuses extends Migration
{
    public function up()
    {
		$this->addColumn('category','status',"enum('deleted','active') NOT NULL default 'active'");
		$this->addColumn('category','show_on_main',"tinyint(1) NOT NULL DEFAULT 1");
    }

    public function down()
    {
        echo "m160718_135916_category_show_on_main_statuses cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
