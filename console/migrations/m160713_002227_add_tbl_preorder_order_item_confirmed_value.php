<?php

use yii\db\Migration;

class m160713_002227_add_tbl_preorder_order_item_confirmed_value extends Migration
{
    public function up()
    {
        $this->addColumn('preorder_items', 'is_order_confirmed', $this->boolean()->notNull()->defaultValue(false));
    }

    public function down()
    {
        echo "m160713_002227_add_tbl_preorder_order_item_confirmed_value cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
