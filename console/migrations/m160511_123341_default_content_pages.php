<?php

use yii\db\Migration;

class m160511_123341_default_content_pages extends Migration
{
    public function up()
    {
        $pages = [
          'about' => [
              'h1' => 'О нас',
              'title' => 'О нас титл',
              'description' => 'О нас дескр',
              'keywords' => 'О нас кейв',
              'content' => 'Контент О Нас',
              'status' => 1,
              'action' => 'about',
              'can_delete' => 0,
          ],
          'brands' => [
              'h1' => 'Бренды',
              'title' => 'Бренды титл',
              'description' => 'Бренды дескр',
              'keywords' => 'Бренды кейв',
              'content' => 'Бренды',
              'status' => 1,
              'action' => 'brands',
              'can_delete' => 0,
          ],
          'sales' => [
              'h1' => 'Акции',
              'title' => 'Акции тайтл',
              'description' => 'Акции описание',
              'keywords' => 'Акции ключевые слова',
              'status' => 1,
              'content' => 'Акции контент',
              'action' => 'sales',
              'can_delete' => 0,
          ],
          'blog' => [
              'h1' => 'Блог',
              'title' => 'Блог тайтл',
              'description' => 'Блог описание',
              'keywords' => 'Блог ключевые слова',
              'content' => 'Блог контент',
              'status' => 1,
              'action' => 'blog',
              'can_delete' => 0,
          ],
          'payment_delivery' => [
              'h1' => 'Доставка и оплата',
              'title' => 'Доставка и оплата тайтл',
              'description' => 'Доставка и оплата описание',
              'keywords' => 'Доставка и оплата ключевые слова',
              'content' => 'Доставка и оплата контент',
              'status' => 1,
              'action' => 'payment_delivery',
              'can_delete' => 0,
          ],
          'guarantees' => [
              'h1' => 'Гарантии',
              'title' => 'Гарантии и возврат тайтл',
              'description' => 'Гарантии описание ',
              'keywords' => 'Гарантии ключевые слова',
              'content' => 'Гарантии контент',
              'status' => 1,
              'action' => 'guarantees',
              'can_delete' => 0,
          ],
          'contacts' => [
              'h1' => 'Контакты',
              'title' => 'Контакты тайтл',
              'description' => 'Контакты описание',
              'keywords' => 'Контакты ключевые слова',
              'content' => 'Контакты контент',
              'status' => 1,
              'action' => 'contacts',
              'can_delete' => 0,
          ],
          'partnership' => [
              'h1' => 'Сотрудничество',
              'title' => 'Сотрудничество тайтл',
              'description' => 'Сотрудничество описание',
              'keywords' => 'Сотрудничество ключевые слова',
              'content' => 'Сотрудничество контент',
              'status' => 1,
              'action' => 'partnership',
              'can_delete' => 0,
          ],
          'how_to' => [
              'h1' => 'Как оформить заказ',
              'title' => 'Как оформить заказ тайтл',
              'description' => 'Как оформить заказ описание',
              'keywords' => 'Как оформить заказ ключевые слова',
              'content' => 'Как оформить заказ контент',
              'status' => 1,
              'action' => 'how-to',
              'can_delete' => 0,
          ],
          'advices' => [
              'h1' => 'Советы по выбору',
              'title' => 'Советы по выбору',
              'description' => 'помощь в выбору',
              'keywords' => 'ключевые слова',
              'content' => 'Советы по выбору контент',
              'status' => 1,
              'action' => 'advices',
              'can_delete' => 0,
          ],
            'Loyalty program' => [
                'h1' => 'Программа лояльности',
                'title' => 'Программа лояльности тайтл',
                'description' => 'Программа лояльности дескр',
                'keywords' => 'Программа лояльности кейв',
                'content' => 'Программа лояльности контент',
                'status' => 1,
                'action' => 'loyalty-program',
                'can_delete' => 0,
            ],
        ];
        foreach ($pages as $page)
        {
            $this->insert('content_pages',$page);
        }
    }

    public function down()
    {
        echo "m160511_123341_default_content_pages cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
