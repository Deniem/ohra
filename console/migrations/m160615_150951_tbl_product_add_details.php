<?php

use yii\db\Migration;

class m160615_150951_tbl_product_add_details extends Migration
{
    public function up()
    {
        $this->addColumn('product', 'description', $this->text());
    }

    public function down()
    {
        echo "m160615_150951_tbl_product_add_details cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
