<?php

use yii\db\Migration;

class m160621_142309_settings_label_column extends Migration
{
    public function up()
    {
		$this->addColumn('settings','label','nvarchar(255) NOT NULL');
		$this->alterColumn('settings','value_type',"enum('checkbox','text','number','email','url','tel','multiline')");
    }

    public function down()
    {
        echo "m160621_142309_settings_label_column cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
