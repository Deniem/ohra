<?php

use yii\db\Migration;
use yii\db\Schema;

class m160502_153814_product extends Migration
{
    public function up()
    {
	    $this->createTable('product',[
		    'id'=> Schema::TYPE_PK,
		    'name' => 'varchar(255) NOT NULL',
		    'price' => 'DECIMAL(5,2) NOT NULL',
		    'id_category' => 'int(11) NOT NULL',
		    'status' => 'enum("active", "not active", "conditionally active") NOT NULL DEFAULT "not active"',
	    ]);
	    $this->addForeignKey('fk_product_category', 'product', 'id_category', 'category', 'id');
    }

    public function down()
    {
        echo "m160502_153814_product cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
