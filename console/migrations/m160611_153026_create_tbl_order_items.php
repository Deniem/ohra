<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation for table `tbl_order_items`.
 */
class m160611_153026_create_tbl_order_items extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('order_items', [
            'id' => $this->primaryKey()->notNull(),
            'order_id' => $this->integer(11)->defaultValue(null),
            'name' => $this->char(255)->defaultValue(null),
            'price' => 'FLOAT(19, 2) NOT NULL ',
            'product_id' => $this->integer(11)->defaultValue(null),
            'quantity' => $this->integer(11)->defaultValue(1)
        ]);

        $this->addForeignKey('fk_product_id', 'order_items', 'product_id', 'product', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tbl_order_items');
    }
}
