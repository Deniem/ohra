<?php

use yii\db\Migration;

class m160705_095230_additional_settings extends Migration
{
    public function up()
    {
		$this->insert('settings',[
			'name' => 'callback_link',
			'label' => 'Otzyv.ua',
			'description' => 'Ссылка в подвале сайта на сервис Otzyv.ua',
			'value' => 'http://otzyv.ua',
			'type' => 'frontend',
			'value_type' => 'url',
			'required' => '1',
		]);

		$this->insert('settings',[
			'name' => 'header_phone',
			'label' => 'Телефон горячей линии',
			'description' => 'Телефон в шапке сайта',
			'value' => '0-800-600-006',
			'type' => 'frontend',
			'value_type' => 'tel',
			'required' => '1',
		]);
    }

    public function down()
    {
        echo "m160705_095230_additional_settings cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
