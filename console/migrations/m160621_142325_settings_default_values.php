<?php

use yii\db\Migration;

class m160621_142325_settings_default_values extends Migration
{
    public function up()
    {
		$TYPE_GLOBAL = 'global';
		$TYPE_FRONT = 'frontend';
		$TYPE_BACK = 'backend';

		$VALUE_TYPE_CHECKBOX = 'checkbox';
		$VALUE_TYPE_TEXT = 'text';
		$VALUE_TYPE_NUMBER = 'number';
		$VALUE_TYPE_EMAIL = 'email';
		$VALUE_TYPE_URL = 'url';
		$VALUE_TYPE_TEL = 'tel';
		$VALUE_TYPE_MULTILINE = 'multiline';

		$values = [
			[
				'name' => 'admin_email',
				'label' => 'Email администратора',
				'description' => 'На эту почту будут приходить сообщения',
				'value' => 'admin@admin.com',
				'type' => $TYPE_GLOBAL,
				'value_type' => $VALUE_TYPE_EMAIL,
				'required' => '1',
			],
			[
				'name' => 'contact_address',
				'label' => 'Адрес',
				'description' => 'Адрес в разделе контакты',
				'value' => '',
				'type' => $TYPE_FRONT,
				'value_type' => $VALUE_TYPE_TEXT,
				'required' => '1',
			],
			[
				'name' => 'contact_phone_1',
				'label' => 'Контактный телефон',
				'description' => 'Телефон в разделе контакты выше',
				'value' => '',
				'type' => $TYPE_FRONT,
				'value_type' => $VALUE_TYPE_TEL,
				'required' => '0',
			],
			[
				'name' => 'contact_phone_2',
				'label' => 'Контактный телефон',
				'description' => 'Телефон в разделе контакты ниже',
				'value' => '',
				'type' => $TYPE_FRONT,
				'value_type' => $VALUE_TYPE_TEL,
				'required' => '0',
			],
			[
				'name' => 'about_text',
				'label' => 'О нас',
				'description' => 'Текст О нас в подвале страницы',
				'value' => '',
				'type' => $TYPE_FRONT,
				'value_type' => $VALUE_TYPE_MULTILINE,
				'required' => '0',
			],
			[
				'name' => 'socials_vk',
				'label' => 'Вконтакте',
				'description' => 'Адресс группы ВК',
				'value' => '',
				'type' => $TYPE_FRONT,
				'value_type' => $VALUE_TYPE_URL,
				'required' => '0',
			],
			[
				'name' => 'socials_fb',
				'label' => 'Facebook',
				'description' => 'Адресс группы Facebook',
				'value' => '',
				'type' => $TYPE_FRONT,
				'value_type' => $VALUE_TYPE_URL,
				'required' => '0',
			],
			[
				'name' => 'socials_gplus',
				'label' => 'Google+',
				'description' => 'Адресс группы Google+',
				'value' => '',
				'type' => $TYPE_FRONT,
				'value_type' => $VALUE_TYPE_URL,
				'required' => '0',
			],
			[
				'name' => 'copyright_text',
				'label' => 'Копирайт',
				'description' => 'Текст копирайта в подвале страницы',
				'value' => '',
				'type' => $TYPE_FRONT,
				'value_type' => $VALUE_TYPE_URL,
				'required' => '0',
			]
		];

		foreach ($values as $value) {
			$this->insert('settings',[
				'name'=>$value['name'],
				'description'=>$value['description'],
				'label'=>$value['label'],
				'value'=>$value['value'],
				'type'=>$value['type'],
				'value_type'=>$value['value_type'],
				'required'=>$value['required'],
			]);
		}

    }

    public function down()
    {
        echo "m160621_142325_settings_default_values cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
