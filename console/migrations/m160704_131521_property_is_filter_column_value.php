<?php

use yii\db\Migration;

class m160704_131521_property_is_filter_column_value extends Migration
{
    public function up()
    {
		$this->addColumn('property','is_filter','tinyint(1) NOT NULL DEFAULT 1');
    }

    public function down()
    {
        echo "m160704_131521_property_is_filter_column_value cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
