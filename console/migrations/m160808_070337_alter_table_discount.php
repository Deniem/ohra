<?php

use yii\db\Migration;

class m160808_070337_alter_table_discount extends Migration
{
    public function up()
    {
        $this->alterColumn('discount', 'end_date', 'timestamp NULL');
    }

    public function down()
    {
        echo "m160808_070337_alter_table_discount cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
