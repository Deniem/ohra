<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tbl_brand`.
 */
class m160629_160918_create_tbl_brand extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('brand', [
            'id' => $this->primaryKey(),
            'name' => 'varchar(255) NOT NULL',
            'slug' => 'varchar(255) NULL',
            'thumbnail_base_url' => 'varchar(1024) DEFAULT NULL',
            'thumbnail_path' => 'varchar(1024) DEFAULT NULL',
            'h1' => 'varchar(255) NULL',
            'description' => $this->text()->defaultValue(null)
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('brand');
    }
}
