<?php

use yii\db\Migration;

/**
 * Handles the creation for table `tbl_user_product_compare`.
 */
class m160701_205500_create_tbl_customer_product_compare extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('customer_compare', [
            'customer_id' => $this->integer(11)->notNull(),
            'product_id' => $this->integer(11)->notNull()
        ]);
        $this->addForeignKey('fk_customer_compare_customer_id', 'customer_favorite', 'customer_id',
            'customer', 'id');
        $this->addForeignKey('fk_customer_compare_product_id', 'customer_favorite', 'product_id',
            'product', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tbl_user_product_compare');
    }
}
