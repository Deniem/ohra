<?php

use yii\db\Migration;

class m160721_134907_share_content_page extends Migration
{
    public function up()
    {
		$page = [
			'h1' => 'Распродажа',
			'title' => 'Распродажа',
			'description' => 'Распродажа',
			'keywords' => 'Распродажа',
			'content' => 'Распродажа',
			'status' => 1,
			'action' => 'share',
			'can_delete' => 0,
		];
		$this->insert('content_pages',$page);
    }

    public function down()
    {
        echo "m160721_134907_share_content_page cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
