<?php

use yii\db\Migration;

class m160703_192734_product_additional_statuses extends Migration
{
    public function up()
    {
		$this->addColumn('product','is_action', 'tinyint(1) NOT NULL default 0');
		$this->addColumn('product','is_sale', 'tinyint(1) NOT NULL default 0');
		$this->addColumn('product','is_new', 'tinyint(1) NOT NULL default 0');
    }

    public function down()
    {
        echo "m160703_192734_product_additional_statuses cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
