<?php

use yii\db\Migration;
use yii\db\Schema;

class m160502_160624_product_property extends Migration
{
    public function up()
    {
	    $this->createTable('product_property',[
		    'id'=> Schema::TYPE_PK,
		    'id_product' => 'int(11) NOT NULL',
		    'id_value' => 'int(11) NOT NULL'
	    ]);
	    $this->addForeignKey('fk_productProperty_product', 'product_property', 'id_product', 'product', 'id');
	    $this->addForeignKey('fk_productProperty_value', 'product_property', 'id_value', 'value', 'id');
    }

    public function down()
    {
        echo "m160502_160624_product_property cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
