<?php

use yii\db\Migration;
use yii\db\Schema;

class m160511_115350_content_pages_table_create extends Migration
{
    public function up()
    {
        $this->createTable('content_pages',[
            'id' => Schema::TYPE_PK,
            'h1' => 'text NOT NULL',
            'title' => 'text NULL',
            'description' => 'text NULL',
            'keywords' => 'text NULL',
            'content' => 'text NULL',
            'created_at' => 'timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'updated_at' => 'timestamp NULL',
            'status' => 'tinyint NOT NULL DEFAULT 0 ',
            'action' => 'varchar(255) NOT NULL',
            'can_delete' => 'tinyint(1) UNSIGNED NOT NULL DEFAULT 1',
        ]);
    }

    public function down()
    {
        echo "m160511_115350_content_pages_table_create cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
