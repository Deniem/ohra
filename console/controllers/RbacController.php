<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\components\rbac\UserRoleRule;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;
        $auth->removeAll();
        $dashboard = $auth->createPermission('dashboard');
        $dashboard->description = 'backend';
        $auth->add($dashboard);
        $rule = new UserRoleRule();
        $auth->add($rule);
        $user = $auth->createRole('user');
        $user->description = 'Simple user';
        $user->ruleName = $rule->name;
        $auth->add($user);
        $manager = $auth->createRole('manager');
        $manager->description = 'Backend auth allowed manager';
        $manager->ruleName = $rule->name;
        $auth->add($manager);
        $auth->addChild($manager, $user);
        $auth->addChild($manager, $dashboard);
        $admin = $auth->createRole('admin');
        $admin->description = 'Application administrator';
        $admin->ruleName = $rule->name;
        $auth->add($admin);
        $auth->addChild($admin, $manager);
    }
}