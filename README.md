OHRA
===============================

How to config server:
_______________________________


1) /etc/hosts add
    - 127.0.0.1 <sitename>
    - 127.0.0.1 backend.<sitename>
    - 127.0.0.1 storage.<sitename>

2) /etc/apache2/site-available => create conf file with following options:

    ```
        <VirtualHost *:80>
           ServerName <sitename>
           DocumentRoot "<path_to_site>/frontend/web/"

           <Directory "<path_to_site>/frontend/web/">
               # use mod_rewrite for pretty URL support
               RewriteEngine on
               # If a directory or a file exists, use the request directly
               RewriteCond %{REQUEST_FILENAME} !-f
               RewriteCond %{REQUEST_FILENAME} !-d
               # Otherwise forward the request to index.php
               RewriteRule . index.php

               # use index.php as index file
               DirectoryIndex index.php

               # ...other settings...
           </Directory>
        </VirtualHost>

        <VirtualHost *:80>
           ServerName backend.<sitename>
           DocumentRoot "<path_to_site>/backend/web/"

           <Directory "<path_to_site>/backend/web/">
               # use mod_rewrite for pretty URL support
               RewriteEngine on
               # If a directory or a file exists, use the request directly
               RewriteCond %{REQUEST_FILENAME} !-f
               RewriteCond %{REQUEST_FILENAME} !-d
               # Otherwise forward the request to index.php
               RewriteRule . index.php

               # use index.php as index file
               DirectoryIndex index.php

               # ...other settings...
           </Directory>
        </VirtualHost>

        <VirtualHost *:80>
                ServerName storage.<sitename>
                DocumentRoot <path_to_site>/storage/web/

                <Directory "<path_to_site>/storage/web/">
                    # use mod_rewrite for pretty URL support
                    RewriteEngine on
                    # If a directory or a file exists, use the request directly
                    RewriteCond %{REQUEST_FILENAME} !-f
                    RewriteCond %{REQUEST_FILENAME} !-d
                    # Otherwise forward the request to index.php
                    RewriteRule . index.php
                    # use index.php as index file
                    DirectoryIndex index.php
                </Directory>
        </VirtualHost>


    ```
-------------------

How to run project:
_______________________________

```
1) Run composer install command
2) Create own database
3) Copy db.dist file to db file => type your credentials
4) Run php yii migrate
5) Copy env.dist file to env file => type your environment variables
6) For yii2-file-kit component change access permission to 775 on this folders
 `storage/cache` and `storage/web/source`

```
-------------------

Components:
_______________________________

```
1) yii2-file-kit (https://github.com/trntv/yii2-starter-kit):
	This kit is designed to automate routine processes of uploading files, their saving and storage. It includes:
		- File upload widget (based on Blueimp File Upload https://github.com/blueimp/jQuery-File-Upload)
		- Component for storing files (built on top of flysystem https://github.com/thephpleague/flysystem)
		- Actions to download, delete, and view (download) files
		- Behavior for saving files in the model and delete files when you delete a model
```