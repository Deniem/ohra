<?php
namespace frontend\components;

use common\models\db\Order;

class CustomerOrderStatus
{
    public static function getOrderStatus(Order $order){
        $message = $status = $label = "";
        switch ($order->status){
            case Order::$STATUS_CREATED:
            case Order::$STATUS_VIEWED:
            case Order::$STATUS_VERIFIED:
            case Order::$STATUS_PENDING:
                $message = "Заказ находится в обработке";
                $status = "В обработке";
                $label = 'info';
                break;
            case Order::$STATUS_IN_PROCESS:
                $message = "Заказ в данный момент комплектуется ";
                $status = "Комплектуется";
                $label = 'warning';
                break;
            case Order::$STATUS_SENT:
                $message = "Заказ передан в службу доставки";
                $status = "Отправлен";
                $label = 'primary';
                break;
            case Order::$STATUS_REJECTED:
                $message = "Заказ отклонен. Свяжитесь с нами для уточнения причин";
                $status = "Отклонен";
                $label = 'danger';
                break;
            case Order::$STATUS_COMPLETE:
                $message = "Заказ успешно выполнен";
                $status = "Завершен";
                $label = 'success';
                break;
        }

        return [
            'message' => $message,
            'status' => $status,
            'label' => $label
        ];
    }
}