<?php
/**
 * Created by PhpStorm.
 * User: bbyte
 * Date: 8/2/16
 * Time: 4:30 PM
 */

namespace frontend\assets;


use yii\web\AssetBundle;

class ResetAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';


    //Must be before any bootstrap styles
    public $css = [
        'css/reset.css'
    ];

}