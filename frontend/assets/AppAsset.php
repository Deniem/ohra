<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
//        'css/reset.css',
        'css/main.css',
        'css/custom.css',
        'css/font-icons/entypo/css/entypo.css',
        'css/font-icons/font-awesome/css/font-awesome.min.css',
		'css/toastr.min.css',
        'css/jquery-ui.min.css'
    ];
    public $js = [
        'js/carouFredSel.min.js',
        'js/init.js',
        'js/custom.js',
        'js/search-filter.js',
        'js/toastr.js',
        'js/jquery-ui.min.js',

        /* backbone assets */
        'files/js/vendors/underscore-min.js',
        'files/js/vendors/backbone-min.js',
        'files/js/vendors/backbone-validation.js',
        'files/js/vendors/backbone-relations.js',
        'files/js/app/app.js',
        'files/js/app/global.js',
        'files/js/app/helpers.js',

        /* backbone views */
        'files/js/app/views/cart.js',
        'files/js/app/views/cart_nav.js',
        'files/js/app/views/main/main.js',
        'files/js/app/views/product.js',
        'files/js/app/views/cabinet.js',

        /* backbone models */
        'files/js/app/models/cart.js',
        'files/js/app/models/product.js',
        'files/js/app/models/newUser.js',
        'files/js/app/models/order.js',
        'files/js/app/models/cabinet.js',

    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
