(function ($, app) {

    $.extend(true,
        app.views, {
            cabinet: {
                settingsPersonal: Backbone.View.extend({

                    events: {
                        'click .personal-upd-btn': 'updatePersonal',
                        'focus #personal-last_name': 'clearInput',
                        'focus #personal-first_name': 'clearInput',
                        'focus #personal-middle_name': 'clearInput'
                    },

                    updatePersonal: function (ev) {
                        ev.preventDefault();
                        this.clearInputs();
                        this.cabinet = new app.models.cabinetPersonal();
                        this.cabinet.set({
                            first_name: $('#personal-first_name').val(),
                            last_name: $('#personal-last_name').val(),
                            middle_name: $('#personal-middle_name').val(),
                            user_id: app.data.user.id
                        });

                        var err = this.cabinet.validate();

                        if (err != false) {
                            this.validateAction(err);
                        } else {
                            this.cabinet.changePersonalData();
                        }
                    },

                    validateAction: function (errors) {
                        app.helpers.validateAction(errors, '#personal-');
                    },

                    clearInput: function (ev) {
                        app.helpers.clearInput(ev);
                    },

                    clearInputs: function () {
                        app.helpers.clearInputs('#personal_data');
                    }
                }),

                settingPassword: Backbone.View.extend({
                    events: {
                        'click .password-upd-btn': 'updatePassword',
                        'focus #password-old_pass': 'clearInput',
                        'focus #password-new_pass': 'clearInput',
                        'focus #password-new-pass_confirm': 'clearInput'
                    },

                    updatePassword: function (ev) {
                        ev.preventDefault();
                        this.clearInputs();
                        this.cabinet = new app.models.cabinetPassword();
                        this.cabinet.set({
                            old_pass: $('#password-old_pass').val(),
                            new_pass: $('#password-new_pass').val(),
                            new_pass_confirm: $('#password-new_pass_confirm').val(),
                            user_id: app.data.user.id
                        });

                        var err = this.cabinet.validate();
                        var self = this;

                        if (err != false) {
                            this.validateAction(err);
                        } else {
                            $.when(this.cabinet.changePassword())
                                .done(function (resp) {
                                    if (resp.status == 'success') {
                                        app.helpers.notifications.show('success', resp.message);
                                    } else {
                                        self.validateAction([{
                                            'name': 'old_pass',
                                            message: resp.message
                                        }]);
                                    }
                                })
                                .fail(function(){
                                    app.helpers.notifications.show('error', 'Не удалось обновить данные, попробуйте позже');
                                });
                        }
                    },

                    validateAction: function (errors) {
                        app.helpers.validateAction(errors, '#password-');
                    },

                    clearInput: function (ev) {
                        app.helpers.clearInput(ev);
                    },

                    clearInputs: function () {
                        app.helpers.clearInputs('#password_change');
                    }

                }),

                orders: Backbone.View.extend({

                    events: {
                        'click .customer-order' : 'orderView'
                    },

                    initialize: function(){
                        console.log("sss");
                    },

                    orderView: function(ev){
                        var parent = $(ev.target).closest('li.customer-order');
                        var order_id = parent.attr('data-order-id');
                        var detail = parent.find('ul.customer-order-detail');
                        var orderContainer = parent.find('div.customer-order-container');

                        if(detail.hasClass('customer-order-detail__active')){
                            orderContainer.removeClass('customer-order__active');
                            detail.removeClass('customer-order-detail__active');
                        }else{
                            orderContainer.addClass('customer-order__active');
                            detail.addClass('customer-order-detail__active');
                        }
                    }
                })
            }
        });

})(jQuery, app);
	