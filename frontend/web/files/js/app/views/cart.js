(function ($, app) {

    $.extend(true,
        app.views, {
            cartView: {
                cartItem: Backbone.View.extend({

                    events: {
                        'click .deleteItem': 'deleteItem',
                        'change .cart-position-quantity' : 'changeQuantity'
                    },

                    initialize: function () {
                        this.itemsCollection = new app.models.CartCollection(app.data.cart);
                        this.template = this.createTemplate('#cart-list-template');

                        this.render();
                    },

                    render: function () {
                        this.totalCost();
                        this.template('.cart-list-placeholder',{
                            items: this.itemsCollection.toJSON(),
                            total : self.totalCost
                        });
                    },

                    deleteItem: function (ev) {
                        var el = $(ev.target);
                        var id = el.closest('tr').attr('data-id');
                        var self = this;
                        this.itemsCollection.get(id).destroy({
                            success: function(resp){
                                update_cart();
                                self.render();
                            }
                        });
                    },


                    
                    changeQuantity: function (ev) {
                        var el = $(ev.target);
                        var id = el.closest('tr').attr('data-id');

                        this.itemsCollection.get(id).set({'quantity': el.val()});
                        var self = this;
                        this.itemsCollection.get(id).save(null,{
                            success: function(model, resp, opts){
                                self.render();
                            }
                        });
                    },

                    totalCost: function () {
                        var total = 0;
                        _.each(this.itemsCollection.models, function (el) {
                            if(el.get('discountActive')){
                                total += el.get('discount')['price'] * el.get('quantity');
                            }else{
                                total += el.get('price') * el.get('quantity');
                            }
                        });
                        self.totalCost = total;
                    }
                }),

                user_type: Backbone.View.extend({

                    events: {
                        'click #login-btn': 'loginAction',
                        'click #signup-button': 'signupAction',
                        'click #login-screen-btn': 'signupFormAction',
                        'click #already-register-btn': 'loginFormAction',

                        'focus #signup-username ' : 'clearInput',
                        'click #signup-email ' : 'clearInput',
                        'click #signup-password' : 'clearInput',
                        'click #login-name' : 'clearInput',
                        'click #login-pass' : 'clearInput',
                        'blur #signup-username' : 'checkUserName'
                    },

                    initialize: function () {
                        this.newUser = new app.models.newUser();
                        this.template = this.createTemplate('#cart-confirm-user-type-template');
                        this.render();
                    },

                    render: function () {
                        this.template('.cart-confirm-user-type-placeholder');
                    },
                    
                    signupAction: function(){
                        this.clearInputs();
                        this.grabParams();
                        var self = this;

                        var onSuccessCheck = function () {
                            $.ajax({
                                type: "POST",
                                url: '/site/signup',
                                data: {
                                    _csrf: $('meta[name=csrf-token]').attr("content"),
                                    no_redirect: true,
                                    'SignupForm': {
                                        username: self.newUser.get('username'),
                                        email: self.newUser.get('email'),
                                        password: self.newUser.get('password'),
                                        rememberMe: 1
                                    }
                                },
                                success: function (resp) {
                                    console.log(resp);
                                    var res = jQuery.parseJSON(resp);
                                    if (res.status == "success") {
                                        location.reload();
                                    }
                                },
                                error: function () {
                                    console.log('nope')
                                }
                            });

                        };

                        this.newUser.validate(onSuccessCheck, self.validateAction);
                    },

                    grabParams: function(){
                        this.newUser.set({
                            username: $('#signup-username').val(),
                            email: $('#signup-email').val(),
                            password: $('#signup-password').val()
                        });
                    },

                    validateAction: function(errors){
                        _.each(errors, function(el){
                            var elmnt = $("#signup-" + el.name);
                            elmnt.closest('.form-group').addClass('has-error');
                            elmnt.parent()
                                .after("<div class='errors'><div class='col-md-4'></div>" +
                                    "<div class='col-md-8 help-block'>" + el.message + "</div></div>");
                        });
                    },

                    showError: function (elmnt, err, noLabel) {
                        elmnt.closest('.form-group').addClass('has-error');
                        if(noLabel === undefined){
                            elmnt.parent()
                                .after("<div class='errors col-md-12'><div class='col-md-4'></div>" +
                                    "<div class='col-md-8 help-block'>" + err + "</div></div>");
                        }
                    },

                    clearInput: function(ev){
                        $(ev.target).closest('.form-group').removeClass('has-error');
                        $(ev.target).parent().next('.errors').remove();
                    },
                    
                    clearInputs: function(){
                        $('#login-screen, #already-register').find('input').each(function(ind, el){
                            $(el).closest('.form-group').removeClass('has-error');
                            $(el).parent().next('.errors').remove();
                        });
                    },

                    checkUserName: function(ev){
                        this.grabParams();
                        var self = this;
                        $.when(app.helpers.checkUsername(this.newUser.get('username'))
                            .done(function (data) {
                                if (data.status !== 'ok') {
                                    self.showError($(ev.target), data.message);
                                }
                            }));
                    },
                    
                    loginAction: function(){
                        this.clearInputs();
                        var name = $('#login-name').val();
                        var pass = $('#login-pass').val();
                        var self = this;
                        $.ajax({
                            type: "POST",
                            url: '/site/login',
                            data: {
                                _csrf: $('meta[name=csrf-token]').attr("content"),
                                no_redirect: true,
                                'LoginForm':{
                                    username: name,
                                    password: pass,
                                    rememberMe: 1
                                }
                            },
                            success: function(resp){
                                console.log("resp" + resp);
                                var res = jQuery.parseJSON(resp);
                                if(res.status == "success"){
                                    location.reload();
                                }else if(res.status == "error"){
                                    console.log(res);
                                    self.showError($('#login-name'), "Неправильный логин или пароль");
                                    self.showError($('#login-pass'), "", true);
                                }
                            },
                            error: function(){
                                console.log('nope')
                            }
                        });
                    },

                    signupFormAction: function () {
                        var loginScreen = $('#login-screen');
                        loginScreen.css({
                            'display': 'block'
                        });
                        $('#login-screen-btn').addClass('active');

                        var alreadyRegister = $('#already-register');
                        alreadyRegister.css({
                            'display': 'none'
                        });
                        $('#already-register-btn').removeClass('active');
                    },

                    loginFormAction: function (ev) {
                        var loginScreen = $('#login-screen');
                        loginScreen.css({
                            'display': 'none'
                        });
                        $('#login-screen-btn').removeClass('active');

                        var alreadyRegister = $('#already-register');
                        alreadyRegister.css({
                            'display': 'block'
                        });
                        $('#already-register-btn').addClass('active');
                        // this.render();
                    }
                }),

                confirm: Backbone.View.extend({

                    events:{
                        'click #submit-order-btn': 'submitAction',
                        'focus #order-city' : 'clearInput',
                        'focus #order-address' : 'clearInput',
                        'focus #order-email' : 'clearInput',
                        'focus #order-name' : 'clearInput',
                        'focus #order-phone' : 'clearInput',
                        'keyup #order-phone' : 'digitsOnly',
                        'keypress #order-phone' : 'digitsOnly'
                    },

                    submitAction: function (ev) {
                        ev.preventDefault();
                        this.clearInputs();
                        this.grabParam();
                        var err = this.order.validate();
                        if(err != false){
                            this.validateAction(err);
                        }else{
                            var self = this;
                            $.ajax({
                                type: "POST",
                                url: '/cart/order',
                                data: self.order.toJSON(),
                                success: function (resp) {
                                    update_cart();
                                },
                                error: function (resp) {
                                    console.log(resp)
                                }
                            });
                        }
                    },

                    initialize: function () {
                        this.template = this.createTemplate('#cart-confirm-template');
                        this.order = new app.models.order();
                        this.render();
                    },

                    grabParam: function(){
                        this.order.set({
                            city: $('#order-city').val(),
                            address: $('#order-address').val(),
                            email: $('#order-email').val(),
                            name: $('#order-name').val(),
                            phone: $('#order-phone').val(),
                            notes: $('#order-notes').val(),
                            payment: $('#order-payment').find(':selected').val(),
                            delivery: $('#order-delivery').find(':selected').val()
                        });
                    },

                    digitsOnly: function(ev){
                        var el = $(ev.target);
                        el.val(el.val().replace(/[^\d]/,''));
                    },
                    
                    getFullUserName: function () {
						if (app.data.user.last_name) {
							return (app.data.user.last_name === null ? " " : app.data.user.last_name) + " " +
								(app.data.user.first_name === null ? " " : app.data.user.first_name) + " " +
								(app.data.user.middle_name === null ? " " : app.data.user.middle_name);
						} else {
							return '';
						}

                    },

                    validateAction: function(errors){
                        _.each(errors, function(el){
                            var elmnt = $("#order-" + el.name);
                            elmnt.closest('.form-group').addClass('has-error');
                            elmnt.parent()
                                .after("<div class='errors'><div class='col-md-4'></div>" +
                                    "<div class='col-md-8 help-block'>" + el.message + "</div></div>");
                        });
                    },

                    clearInput: function(ev){
                        $(ev.target).closest('.form-group').removeClass('has-error');
                        $(ev.target).parent().next('.errors').remove();
                    },

                    clearInputs: function(){
                        $('.cart-confirm-placeholder').find('input').each(function(ind, el){
                            $(el).closest('.form-group').removeClass('has-error');
                            $(el).parent().next('.errors').remove();
                        });
                    },

                    render: function () {
                        this.template('.cart-confirm-placeholder', {
							userName: this.getFullUserName()
						});
                    }
                })
            }
        });

})(jQuery, app);
	