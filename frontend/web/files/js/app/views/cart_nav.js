(function ($, app) {

    $.extend(true,
        app.views, {
            cartNav: {
                Main: Backbone.View.extend({

                    events: {
                    },

                    initialize: function () {
                        var self = this;
                        $.when(
                            this.getFavCount(),
                            this.getCompareCount()
                        )
                            .done(function( resp1, resp2 ) {
                                self.doFavRender('add', resp1[0]['favorite']);
                                self.doCompareRender('add', resp2[0]['compare']);
                        });

                        this.listenTo(app.event_bus, 'update-cart-fav-count', this.doFavRender);
                        this.listenTo(app.event_bus, 'update-cart-compare-count', this.doCompareRender);
                        this.checkCart();
                    },

                    render: function () {
                    },
                    
                    getFavCount: function(){
                        return $.ajax({
                            type: "GET",
                            url: '/customer-favorite/'
                        });
                    },

                    getCompareCount: function(){
                        return $.ajax({
                            type: "GET",
                            url: '/customer-compare/'
                        });
                    },

                    checkCart: function(){
                        var number = $('.b-header__option--basket').attr('data-number');
                      if (number == 0){
                          $('.b-header__option--basket').removeAttr('data-number');
                      }
                    },

                    doCompareRender: function(action, number){
                        var shareOption = $('.b-header__option--share');
                        var aside = $('.aside-compare');

                        if (number === undefined) {
                            var numLike = shareOption.attr('data-number');
                            if (numLike.length == 0) {
                                numLike = 0;
                                if (action == 'add') {
                                    shareOption.addClass('b-header__option--active');
                                    shareOption.attr('data-number', ++numLike);
                                    aside.after('<span class="badge brand-label">'+ (numLike) +'</span>');
                                }
                                return;
                            }

                            if (action == 'add') {
                                shareOption.addClass('b-header__option--active');
                                shareOption.attr('data-number', ++numLike);
                                aside.next().text(numLike);
                                aside.next().css({'display': 'block'});
                            } else if (action == 'delete') {
                                shareOption.attr('data-number', --numLike);
                                if (numLike == 0) {
                                    shareOption.removeClass('b-header__option--active');
                                    aside.next().css({'display': 'none'});
                                    return;
                                }
                                shareOption.attr('data-number', numLike);
                                aside.next().text(numLike);
                            }
                        } else {
                            if (number != 0) {
                                shareOption.attr('data-number', number);
                                shareOption.addClass('b-header__option--active');
                                aside.next().text(number);
                            }
                        }

                    },

                    doFavRender: function (action, number) {
                        var headerLike = $('.b-header__option--like');
                        var aside = $('.aside-fav');

                        if(number === undefined){
                            var numLike = headerLike.attr('data-number');
                            if(numLike.length == 0){
                                numLike = 0;
                                if(action == 'add'){
                                    headerLike.addClass('b-header__option--active');
                                    headerLike.attr('data-number', ++numLike);
                                    aside.after('<span class="badge brand-label">'+ (numLike) +'</span>');
                                }
                                return;
                            }

                            if(action == 'add'){
                                headerLike.addClass('b-header__option--active');
                                headerLike.attr('data-number', ++numLike);
                                aside.next().text(numLike);
                                aside.next().css({'display': 'block'});
                            }else if(action == 'delete'){
                                headerLike.attr('data-number', --numLike);
                                if(numLike == 0){
                                    headerLike.removeClass('b-header__option--active');
                                    aside.next().css({'display': 'none'});
                                    return;
                                }
                                headerLike.attr('data-number',numLike);
                                aside.next().text(numLike);
                            }
                        }else{
                            if( number != 0) {
                                headerLike.attr('data-number',number);
                                headerLike.addClass('b-header__option--active');
                                aside.next().text(number);
                            }
                        }

                    }
                })

            }
        });

})(jQuery, app);
