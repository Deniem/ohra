(function ($, app) {

    $.extend(true,
        app.views, {
            main: {
                Novelty: Backbone.View.extend({

                    events: {
                        'click .add-to-fav': 'addToFav',
                        'click .add-to-compare': 'addToCompare',
                        'click .add-to-cart': 'addToCart'
                    },

                    initialize: function () {
                        this.listenTo(app.event_bus, 'updateLabel', this.updateLabel);
                    },
                    
                    addToCart: function(ev){
                        var data = $(ev.target).data();
                        var model = new app.models.Cart({
                            id: data.id
                        }, {parse:true});

                        ev.preventDefault();

                        model.set('quantity', 1);
                        var self = this;

                        if($(ev.target).hasClass('active')){
                            model.destroy({
                                success: function(resp){
                                    update_cart();
                                    app.helpers.notifications.show('info','Товар удален из корзины');
                                    app.event_bus.trigger('updateLabel', model.get('id'), self.cid, '.add-to-cart');
                                    // app.event_bus.trigger('update-cart-items-count', action);
                                },
                                error: function () {
                                    app.helpers.notifications.show('warning','Что-то пошло не так. Попробуйте, пожалуйста еще раз ');
                                }
                            })
                        }else{
                            model.save(null, {
                                success: function (resp) {
                                    update_cart();
                                    app.helpers.notifications.show('success','Товар успешно добавлен в корзину ');
                                    app.event_bus.trigger('updateLabel', model.get('id'), self.cid, '.add-to-cart');
                                    // app.event_bus.trigger('update-cart-items-count', action);
                                },
                                error: function () {
                                    app.helpers.notifications.show('warning','Что-то пошло не так. Попробуйте, пожалуйста еще раз ');
                                }
                            })
                        }
                    },

                    addToCompare: function(ev){
                        ev.preventDefault();
                        var self = this;

                        var model = new app.models.Product(
                            {id: $(ev.target).data().id},
                            {parse: true}
                        );
                        var action  = false;
                        var message = "";

                        var onSuccess = function () {
                            message.call();

                            app.event_bus.trigger('update-cart-compare-count', action);
                            app.event_bus.trigger('updateLabel', model.get('id'), self.cid, '.add-to-compare');

                            //cabinet self remove action
                            var container =  $(ev.target).closest('div.container-novelty');
                            if (container.attr('data-view-removable') == 'true'
                                && container.attr('data-view-action') == 'compare') {
                                $(ev.target).closest('li.b-novelty__item').remove();
                            }
                        };

                        var onFail = function () {
                            app.helpers.notifications.show('error','Не удалось выполнить действие. Попробуйте позже')
                        };

                        if($(ev.target).hasClass('active')){
                            action = 'delete';
                            message = function () {
                                app.helpers.notifications.show('info','Товар был убран из списка сравнения')
                            };
                            $.when(model.removeFromCompare())
                                .done(onSuccess(action, message))
                                .fail(function(){onFail()});
                        }else{
                            action = 'add';
                            message = function () {
                                app.helpers.notifications.show('success','Товар был добавлен в список сравнения')
                            };
                            $.when(model.addToCompare())
                                .done(onSuccess(action, message))
                                .fail(function(){onFail()});
                        }
                    },

                    addToFav: function(ev){
                        ev.preventDefault();
                        var self = this;

                        var model = new app.models.Product(
                            {id: $(ev.target).data().id},
                            {parse: true}
                        );
                        var action  = false;
                        var message = "";

                        var onSuccess = function () {
                            message.call();

                            app.event_bus.trigger('update-cart-fav-count', action);
                            app.event_bus.trigger('updateLabel', model.get('id'), self.cid, '.add-to-fav');

                            //cabinet self remove action
                            var container =  $(ev.target).closest('div.container-novelty');
                            if (container.attr('data-view-removable') == 'true'
                                && container.attr('data-view-action') == 'favorite') {
                                $(ev.target).closest('li.b-novelty__item').remove();
                            }
                        };

                        var onFail = function(){
                            app.helpers.notifications.show('error','Не удалось выполнить действие. Попробуйте позже')
                        };

                        if($(ev.target).hasClass('active')){
                            action = 'delete';
                            message = function () {
                                app.helpers.notifications.show('info','Товар был убран из списка избранного')
                            };
                            $.when(model.removeFromFavorite())
                                .done(onSuccess(action, message))
                                .fail(function(){onFail()});
                        }else{
                            action = 'add';
                            message = function () {
                                app.helpers.notifications.show('success','Товар был добавлен в список избранного')
                            };
                            $.when(model.addToFavorite())
                                .done(onSuccess(action, message))
                                .fail(function(){onFail()});
                        }
                    },

                    /*
                     *  Update all add product's widget buttons with specified params, where
                     *  model - app.Models.Product
                     *  cid - view.cid
                     *  selector - .add-to-cart / .add-to-fav / .add-to-compare
                     */
                    updateLabel: function(id, cid, selector){
                        var elements = $(selector);
                        var self = this;

                        $.each(elements, function (indx, val) {
                            if($(val).attr('data-id') == id){
                                if(cid == self.cid){
                                    $(val).toggleClass('active');
                                }
                            }
                        });
                    }
                    
                })
            }
        });

})(jQuery, app);
	