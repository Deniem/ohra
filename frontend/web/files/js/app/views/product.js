(function ($, app) {

    $.extend(true,
        app.views, {
            product: {
                Item: Backbone.View.extend({

                    events: {
                        'click .add-to-cart' : 'addToCart',
                        'click .add-to-favorite' : 'addToFavorite',
                        'click .add-to-compare' : 'addToCompare'
                    },

                    initialize: function () {
                        this.model = new app.models.Product(app.data.product);
                        this.template = this.createTemplate('#product-item-template');
                        this.render();
                    },

                    render: function () {
                        this.template('.product-item-placeholder',{
                            model: this.model.toJSON()
                        });
                    },

                    addToCart: function(){
                        var cart = new app.models.Cart();
                        cart.set('product', this.model);
                        cart.save(null, {
                            success: function(){
                                update_cart();
                            }
                        });
                    },
                    
                    addToFavorite: function(ev){
                        this.model.addToFavorite();
                        $(ev.target).css({'pointer-events': 'none'});
                        app.event_bus.trigger('update-cart-fav-count', 'add');
                    },

                    addToCompare: function (ev) {
                        this.model.addToCompare();
                        $(ev.target).css({'pointer-events': 'none'});
                        app.event_bus.trigger('update-cart-compare-count', 'add');
                    }
                })

            }
        });

})(jQuery, app);
	