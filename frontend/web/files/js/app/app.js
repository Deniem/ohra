var app = app || {};

(function($, app) {

	$.extend(app, {

		models: {
			/**
			 * A namespace for the models that represent input forms and don't have corresponding database entities
			 */
			forms: {}
		},

		/**
		 * A namespace for application's views
		 */
		views: {},

		/**
		 * A namespace for application's events
		 */
		events: _.extend({}, Backbone.Events)

	});


})(jQuery, app);