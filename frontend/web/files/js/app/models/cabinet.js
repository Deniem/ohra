(function($, app) {

    app.models.cabinetPersonal = Backbone.Model.extend({

        url:function () {
            if (this.isNew()) {
                return app.config.baseUrl + 'api/user/'
            }
            else {
                return app.config.baseUrl + 'api/user/' + this.get('id');
            }
        },

        validate: function(){
            var errors = [];

            if(!this.attributes.first_name){
                errors.push({'name': 'first_name', message: "Поле не должно быть пустым"});
            }

            if(!this.attributes.last_name){
                errors.push({'name': 'last_name', message: "Поле не должно быть пустым"});
            }
            
            if(!this.attributes.middle_name){
                errors.push({'name': 'middle_name', message: "Поле не должно быть пустым"});
            }
            
            return errors.length > 0 ? errors : false;
        },
        
        changePersonalData: function(){
            $.ajax({
                type: "POST",
                url: app.config.baseUrl + 'api/user/update-personal',
                data: {
                    first_name : this.attributes.first_name,
                    last_name : this.attributes.last_name,
                    middle_name : this.attributes.middle_name,
                    user_id: this.attributes.user_id
                },
                success: function (resp) {
                    if(resp.status == 'success'){
                        app.helpers.notifications.show('success', resp.message);
                    }else{
                        if(resp.hasOwnProperty('message')){
                            app.helpers.notifications.show('error', resp.message);
                        }
                    }
                },
                error: function () {
                    app.helpers.notifications.show('error', 'Не удалось обновить данные, попробуйте позже');
                }
            });
        }
    });

    app.models.cabinetPassword = Backbone.Model.extend({

        url:function () {
            if (this.isNew()) {
                return app.config.baseUrl + 'api/user/'
            }
            else {
                return app.config.baseUrl + 'api/user/' + this.get('id');
            }
        },

        validate: function() {
            var errors = [];

            if(!this.attributes.old_pass){
                errors.push({'name': 'old_pass', message: "Поле не должно быть пустым"});
            }

            if (!this.attributes.new_pass) {
                errors.push({'name': 'new_pass', message: "Заполните поле пароля"});
            } else if (this.attributes.new_pass.length < 6 || this.attributes.new_pass.length > 255) {
                errors.push({'name': 'new_pass', message: "Пароль неправильной длины"});
            }

            if (!this.attributes.new_pass_confirm) {
                errors.push({'name': 'new_pass_confirm', message: "Поле не должно быть пустым"});
            }

            if(this.attributes.new_pass !== this.attributes.new_pass_confirm){
                errors.push({'name': 'new_pass', message: "Пароли не совпадают"});
            }

            return errors.length > 0 ? errors : false;
        },
        
        changePassword: function(){
            return $.ajax({
                type: "POST",
                url: app.config.baseUrl + 'api/user/update-password',
                data: {
                    old_pass: this.attributes.old_pass,
                    new_pass: this.attributes.new_pass,
                    new_pass_confirm: this.attributes.new_pass_confirm,
                    user_id: this.attributes.user_id
                }
            });
                
        }
    });

})(jQuery, app);
