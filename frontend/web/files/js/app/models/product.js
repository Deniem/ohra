(function($, app) {

    app.models.Product = Backbone.Model.extend({

        url:function () {
            if (this.isNew()) {
                return app.config.baseUrl + 'cart/'
            }
            else {
                return app.config.baseUrl + 'cart/' + this.get('id');
            }
        },

        addToFavorite: function(){
            var prod_id = this.get('id');
            $.ajax({
                type: "PUT",
                url: app.config.baseUrl + 'customer-favorite/' + prod_id,
                data: {
                    _csrf: $('meta[name=csrf-token]').attr("content"),
                    id: prod_id
                }
            });
        },

        removeFromFavorite: function(){
            var prod_id = this.get('id');
            $.ajax({
                type: "DELETE",
                url: app.config.baseUrl + 'customer-favorite/' + prod_id,
                data: {
                    _csrf: $('meta[name=csrf-token]').attr("content"),
                    id: prod_id
                }
            });
        },

        addToCompare: function () {
            var prod_id = this.get('id');

            $.ajax({
                type: "PUT",
                url: app.config.baseUrl + 'customer-compare/' + prod_id,
                data: {
                    _csrf: $('meta[name=csrf-token]').attr("content"),
                    id: prod_id
                }
            });
        },

        removeFromCompare: function(){
            var prod_id = this.get('id');

            $.ajax({
                type: "DELETE",
                url: app.config.baseUrl + 'customer-compare/' + prod_id,
                data: {
                    _csrf: $('meta[name=csrf-token]').attr("content"),
                    id: prod_id
                }
            });
        }

    });

    app.models.ProductCollection = Backbone.Collection.extend({
        url: + app.config.baseUrl + 'cart/',
        model: app.models.Product
    });

})(jQuery, app);
