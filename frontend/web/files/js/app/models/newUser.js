(function ($, app) {

    app.models.newUser = Backbone.Model.extend({

        url: function () {
            if (this.isNew()) {
                return app.config.baseUrl + 'api/user/'
            }
            else {
                return app.config.baseUrl + 'api/user/' + this.get('id');
            }
        },

        validate: function (succCall, errCall) {
            var errors = [];
            var self =this;

            var inner = function () {
                var r = $.Deferred();
                if (!self.attributes.username) {
                    errors.push({'name': 'username', message: "Заполните поле логина"});
                } else if (self.attributes.username.length > 255) {
                    errors.push({'name': 'username', message: "Слишком большая длина"});
                } else if (self.attributes.username.length < 3) {
                    errors.push({'name': 'username', message: "Слишком короткое поле"});
                } else {

                $.when(app.helpers.checkUsername(self.attributes.username))
                    .done(function(data){
                        if (data.status !== 'ok') {
                            errors.push({'name': 'username', message: data.message});
                        }
                    });
                }

                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                if (!self.attributes.email) {
                    errors.push({'name': 'email', message: "Заполните поле почтового адресса"});
                } else if (!re.test(self.attributes.email)) {
                    errors.push({'name': 'email', message: "Адресс неподходящего формата"});
                }

                if (!self.attributes.password) {
                    errors.push({'name': 'password', message: "Заполните поле пароля"});
                } else if (self.attributes.password.length < 6 || self.attributes.password.length > 255) {
                    errors.push({'name': 'password', message: "Пароль неправильной длины"});
                }

                //waiting for checkUsername function
                setTimeout(function () {
                    r.resolve();
                }, 500);
                return r;
            };


            inner().done(function(){
                errors = errors.length == 0 ? false : errors;
                if(errors == false){
                    if(typeof succCall == "function"){
                        succCall();
                    }
                }else{
                    if(typeof succCall == "function"){
                        errCall(errors);
                    }
                }
            }).fail(function(){
               console.log('ooops');
            });
        }
    });

})(jQuery, app);
