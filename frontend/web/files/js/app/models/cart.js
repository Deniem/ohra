(function($, app) {

    app.models.Cart = Backbone.Model.extend({

        url:function () {
            if (this.isNew()) {
                return app.config.baseUrl + 'cart/create/'
            }
            else {
                return app.config.baseUrl + 'cart/' + this.get('id');
            }
        },
        
        models: {
            product: app.models.Product
        },
        quantity: 0 
    });

    app.models.CartCollection = Backbone.Collection.extend({
        url: app.config.baseUrl + 'cart/',
        model: app.models.Cart
    });

})(jQuery, app);
