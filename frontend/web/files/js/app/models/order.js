(function($, app) {

    app.models.order = Backbone.Model.extend({

        url:function () {
            if (this.isNew()) {
                return app.config.baseUrl + 'api/user/'
            }
            else {
                return app.config.baseUrl + 'api/user/' + this.get('id');
            }
        },

        validate: function(){
            var errors = [];

            if(!this.attributes.city){
                errors.push({'name': 'city', message: "Заполните поле города"});
            }

            if(!this.attributes.address){
                    errors.push({'name': 'address', message: "Заполните поле адресса"});
            }

            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

            if(!this.attributes.email){
                errors.push({'name': 'email', message: "Заполните поле почтового адресса"});
            }else if(!re.test(this.attributes.email)){
                errors.push({'name': 'email', message: "Адресс неподходящего формата"});
            }

            if(!this.attributes.name){
                errors.push({'name': 'name', message: "Заполните поле имени получателя"});
            }

            if(!this.attributes.phone){
                errors.push({'name': 'phone', message: "Заполните поле телефона"});
            }

            return errors.length > 0 ? errors : false;
        }
    });

})(jQuery, app);
