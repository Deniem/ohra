(function($, app) {

    $.extend(true, app, {

        helpers: {
			log:[],

			showLogger: function(){
				$.each(this.log, function(idx, line){
					console.log(line);
				})
			},

            collectForm: function(selector){
                selector = selector || '';
                var data = {};
				var form = $('form' + selector);
                $.each(form.serializeArray(), function(idx, o) {
                    if (typeof data[o.name] == 'undefined') {
                        data[o.name] = o.value;
                    }
                    else if (typeof data[o.name] == 'string') {
                        data[o.name] = [data[o.name], o.value];
                    }
                    else {
                        data[o.name].push(o.value);
                    }
                });

				if (form.find('input[type="checkbox"]').length > 0) {
					$.each(form.find('input[type="checkbox"]'), function(idx, item){
						data[$(item).attr('name')] = $(item).prop('checked') ? 1 : 0;
					});
				}

                return data;
            },

			clearForm: function(selector){
				if (!$('form' + selector).length) return;
				$('form' + selector)[0].reset();
			},

			showErrors: function(data, selector){
				if ( !$(selector).length ) return;
				if (data.length > 0) {
					$.each(data, $.proxy(function(idx, item){
						this.showErrors(item, selector);
					},this))
				} else {
					var errString = '<span class="err-string alert alert-sm alert-danger">' + data.msg + '<span>';
					$(selector).html(errString);
					setTimeout(function(){
						$(selector).find('.err-string').remove();
					}, 2000);
				}
			},

			showPleaseWait: function(selector, state) {
				if (!$(selector).length) return;

				var state = state == 'hide';

				if (state) {
					$($(selector).html()).show();
					$(selector).removeClass('loader-gif');
				} else {
					$($(selector).html()).hide();
					$(selector).addClass('loader-gif');
				}
			},

			checkUsername: function(username){
				return $.ajax({
					type: "GET",
					url: '/api/user/check',
					data: {
						username: username
					}
				});
			},

			clearInput: function(ev){
				$(ev.target).closest('.form-group').removeClass('has-error');
				$(ev.target).parent().next('.errors').remove();
			},

			clearInputs: function(target_id){
				$(target_id).find('input').each(function(ind, el){
					$(el).closest('.form-group').removeClass('has-error');
					$(el).parent().next('.errors').remove();
				});
			},

			validateAction: function(errors, target_id){
				_.each(errors, function(el){
					var elmnt = $(target_id + el.name);
					elmnt.closest('.form-group').addClass('has-error');
					elmnt.parent()
						.after("<div class='errors'><div class='col-md-4'></div>" +
							"<div class='col-md-8 help-block'>" + el.message + "</div></div>");
				});
			},

			notifications: {
				types: ['success','error','warning','info'],
				show: function(type, message){
					if (_.indexOf(this.types, type) >= 0 && $.trim(message) != '') {
						return toastr[type](message);
					}
				}
			}
        },

		event_bus :_({}).extend(Backbone.Events)

    });


})(jQuery, app);