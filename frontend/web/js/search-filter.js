$(document).ready(function(){
    filter_checkBox_click();
    setFilterCheckBoxesState();
});

function filter_checkBox_click() {

    var apply_filters = function(){
        var filters = {};
        $('.filter-checkbox:checked').each(function () {
            if(this.checked){
                var param_slug = $(this).attr('data-filter-slug');
                var value_slug = $(this).attr('name');

                if(filters[param_slug] !== undefined){
                    filters[param_slug] = filters[param_slug] + "," +value_slug;
                }else{
                    filters[param_slug] = value_slug;
                }
            }
        });

        filters['sort-by'] = $('.sort-by').find(":selected").val();
        generateURL(filters);
    };

    $('.filter-checkbox').click(apply_filters);
    $('.sort-by').change(apply_filters);
}

function setFilterCheckBoxesState() {
    var urlParams = parseURLParams();
    $('.filter-checkbox').each(function () {
        var param_slug = $(this).attr('data-filter-slug');
        if(urlParams.hasOwnProperty(param_slug)){
            if(urlParams[param_slug].indexOf(',') > -1){
                var values = urlParams[param_slug].split(',');
                var param_val = $(this).attr('name');
                for(var val in values){
                    if(values[val] == param_val){
                        $(this).attr('checked', true);
                    }
                }
            }else if(urlParams[param_slug] == $(this).attr('name')){
                $(this).attr('checked', true);
            }
        }
    });

    if(urlParams.hasOwnProperty('sort-by')){
        $('.sort-by').find('option').each(function(el, val){
            if($(val).val() == urlParams['sort-by']){
                $(val).prop('selected', 'selected');
            }
        });
    }
}

function generateURL(obj){
    var params = "?";
    for(var k in obj){
        if (obj.hasOwnProperty(k)) {
            params += "" + k + "=" + obj[k] + "&";
        }
    }

    params = params.substring(0, params.length-1);
    window.location.href = location.protocol + '//' + location.host + location.pathname + params;
}

function parseURLParams(){
    var obj = {};
    var pairs = location.search.substring(1).split('&');
    for(var i in pairs){
        var split = pairs[i].split('=');
        obj[decodeURIComponent(split[0])] = decodeURIComponent(split[1]);
    }

    return obj;
}