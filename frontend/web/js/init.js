$(window).load(function(){
	bmarkRender();
});

$(document).ready(function() {

	basket_render();

	//anchor
	var pageYLabel = 0;
	var checkScroll = true;
	var timeScroll = 800;

	window.onscroll = function() {
		var pageY = window.pageYOffset || document.documentElement.scrollTop;
		var innerHeight = document.documentElement.clientHeight;

		var path = $('.b-section__anchor').attr('data-path');
		if(checkScroll){
			switch (path) {
				case '':
					if (pageY > innerHeight) {
						$('.b-section__anchor').attr('data-path','up');
					}
					break;

				case 'up':
					if (pageY < innerHeight) {
						$('.b-section__anchor').attr('data-path','');
					}
					break;

				case 'down':
					if (pageY > innerHeight) {
						$('.b-section__anchor').attr('data-path','up');
					}
					break;
			}
		}
	}


	$('.b-section__anchor').click(function(e){
		var pageY = window.pageYOffset || document.documentElement.scrollTop;
		var path = $(this).attr('data-path');
		switch (path) {
			case 'up':
				$(this).attr('data-path','down');
				pageYLabel = pageY;
				//window.scrollTo(0, 0);
				checkScroll = false;
				$("html,body").animate({"scrollTop":0},timeScroll);

				window.setTimeout(function(){
					checkScroll = true;
				},timeScroll);

				break;

			case 'down':
				$(this).attr('data-path','up');
				checkScroll = false;
				$("html,body").animate({"scrollTop":pageYLabel},timeScroll);
				window.setTimeout(function(){
					checkScroll = true;
				},timeScroll);
				break;
		};
		e.preventDefault();

	});
	//-


	$('.b-header__botmenu-list').on('click','a.submenu_in',function(e){
		var $parent = $(this).closest('ul');
		var $subMenu = $(this).siblings('.b-header__submenu-list');
		$parent.find('.b-header__submenu-list').slideUp();
		$subMenu.slideDown();
		e.preventDefault();
	});

	$('.b-header__search-show').click(function(){
		$('.b-header__search_block').slideDown();
	});

	$('.top__button').click(function(){
		if ($(this).hasClass('mobile_active')){
			$(this).removeClass('mobile_active');
			$('.b-header__botmenu-list').slideUp();
		} else {
			$(this).addClass('mobile_active');
			$('.b-header__botmenu-list').slideDown();
		};


	});

	$('.b-category__block').hover(function(){
		$(this).addClass('active');
	});

	$('.b-searchForm__open').click(function(){
		$('.b-searchForm').slideToggle();
		$(this).hide();
		setTimeout(function(){
			$('.b-searchForm__search').focus();
		}, 400);

	});

	$(document).click(function(e) {
		basket_clicks(e);

		if($(e.target).closest(".b-header__botmenu-item").length ==0 &&
			$(e.target).closest(".b-header__botmenu-link").length ==0){
			$('.b-header__submenu-list').slideUp();
		}
		if($(e.target).closest(".b-header__search_block").length ==0 &&
			$(e.target).closest(".b-header__search-show").length ==0){
			$('.b-header__search_block').slideUp();
			$('.b-header__search_block').find('.b-header__search-input').val('');
		}
		if($(e.target).closest(".b-header__botmenu-list").length ==0 &&
			$(e.target).closest(".top__button").length ==0 &&
			$('.top__button').css('display') == 'block'){

			$('.b-header__botmenu-list').slideUp();
			$('.top__button').removeClass('mobile_active');
		}
		if($(e.target).closest(".b-searchForm__open").length ==0 &&
			$(e.target).closest(".b-searchForm").length ==0){
			$('.b-searchForm__open').show();
			$('.b-searchForm').slideUp();
			$('.b-searchForm').find('.b-searchForm__search').val('');
		}
		if($(e.target).closest(".b-filter__navBlock").length ==0){
			$('.b-filter__navContent').slideUp();
		}
	});


	if($('.b-mainslider__list').length > 0){
		$('.b-mainslider__list').carouFredSel({
			direction            : "left",
			responsive           : true,
			scroll : {
				items            : 1,
				duration        : 800,
				pauseOnHover    : true
			},

			prev	: {
				button	: ".b-mainslider__nav--left",
			},
			next	: {
				button	: ".b-mainslider__nav--right",
			}
		});
	};

	if($('.b-month__list').length > 0){
		$('.b-month__list').carouFredSel({
			direction            : "left",
			responsive           : true,
			scroll : {
				items            : 1,
				duration        : 1000,
				pauseOnHover    : true
			},
			prev	: {
				button	: ".b-month__nav--left",
			},
			next	: {
				button	: ".b-month__nav--right",
			}

		});
	};


	if($('.b-novelty__list').length > 0){
		$('.b-novelty__list').each(function () {
			var $slider = $(this),
				$navLeft = $(this).siblings('.b-novelty__nav--left'),
				$navRight = $(this).siblings('.b-novelty__nav--right');

			$slider.carouFredSel({
				direction            : "left",
				responsive           : true,
				scroll : {
					items            : 1,
					duration        : 1000,
					pauseOnHover    : true
				},
				items : {
					width       : 300,
					visible: {
						min: 1,
						max: 4
					}
				},
				prev	: {
					button	: $navLeft,
				},
				next	: {
					button	: $navRight,
				}
			});
		});
	}

	/************Catalog**layout***************/

	$('.b-filter__nav').on('click','.b-filter__navLink',function(e){
		$('.b-filter__navContent').slideUp();
		$(this).siblings('.b-filter__navContent').slideDown();
		e.preventDefault();
	});

	$('.b-filter__screenLink').click(function(e){
		$('.b-filter__screenLink').removeClass('active');
		$(this).addClass('active');
		e.preventDefault();
	});


	var costMin = $('#minCost').attr('data-min');
	var costMax = $('#maxCost').attr('data-max');
	$('#minCost').val(costMin);
	$('#maxCost').val(costMax);

	$("#sliderRange").slider({
		min: costMin,
		max: costMax/10,
		values: [costMin,costMax],
		range: true,
		stop: function(event, ui) {
			$("input#minCost").val($("#sliderRange").slider("values",0));
			$("input#maxCost").val($("#sliderRange").slider("values",1));

		},
		slide: function(event, ui){
			$("input#minCost").val($("#sliderRange").slider("values",0));
			$("input#maxCost").val($("#sliderRange").slider("values",1));
		}
	});

	$("input#minCost").change(function(){
		var value1=$("input#minCost").val();
		var value2=$("input#maxCost").val();

		if(parseInt(value1) > parseInt(value2)){
			value1 = value2;
			$("input#minCost").val(value1);
		}
		$("#sliderRange").slider("values",0,value1);
	});


	$("input#maxCost").change(function(){
		var value1=$("input#minCost").val();
		var value2=$("input#maxCost").val();

		if (value2 > costMax) { value2 = costMax; $("input#maxCost").val(costMax)}

		if(parseInt(value1) > parseInt(value2)){
			value2 = value1;
			$("input#maxCost").val(value2);
		}
		$("#sliderRange").slider("values",1,value2);
	});

	$('.b-sliderRange__input').keypress(function(event){
		var key, keyChar;
		if(!event) var event = window.event;

		if (event.keyCode) key = event.keyCode;
		else if(event.which) key = event.which;

		if(key==null || key==0 || key==8 || key==13 || key==9 || key==46 || key==37 || key==39 ) return true;
		keyChar=String.fromCharCode(key);

		if(!/\d/.test(keyChar))	return false;

	});

	$('.b-sliderRange__input').click(function(){
		$(this).val('');
	});

	$('.b-sliderRange__input').focusout(function(){

		if ( $(this).val() == ''){
			$(this).val('0');
		};
	});

	/************End*of*Catalog**layout***************/

	cabinet_list();

});

function cabinet_list(){
	if($('.b-header__cabinet').length > 0) {
		$('.b-header__cabinet-link').hover(function () {
			$('#cabinet_links').css({'display': 'block'});
		});

		$('#cabinet_links').mouseleave(function () {
			$('#cabinet_links').css({'display': 'none'});
		});
	}
}

function basket_render(){
	var basketOptionHeader = $('.b-header__option--basket');
    basketOptionHeader.click(function(){
		$('.b-basket').slideToggle();
	});

	var aside = $('.aside-cart');
    if( basketOptionHeader.attr('data-number') == 0){
		basketOptionHeader.removeAttr('data-number');
		aside.next().remove();
	}else{
		if(aside.parent().find('span').length > 0){
			aside.next().text(basketOptionHeader.attr('data-number'))
		}else{
			aside.parent().append('<span class="badge brand-label">' +
				basketOptionHeader.attr('data-number') + '</span>');
		}
	}

	$('.b-basket__list').on('click','.b-basket__close',function(){
		var productPrice = +($(this).closest('li').find('.b-basket__item-price span').text());
		var allPrice = ($('.b-basket__total-price span').text());
		var total = allPrice - productPrice;
		$('.b-basket__total-price span').text(total);

		var basketOptionHeader = $('.b-header__option--basket');
        var numBasket = +(basketOptionHeader.attr('data-number'));
		numBasket--;
		basketOptionHeader.attr('data-number',numBasket);
		var aside = $('.aside-cart');
        aside.next().text(numBasket);
		if( numBasket == 0){
			basketOptionHeader.removeAttr('data-number');
			aside.next().remove();
		}

		var self = this;
		var prod_id = $(this).closest('li').attr('data-product-id');
		$.ajax({
			url: '/cart/delete',
			type: 'GET',
			data: {id: prod_id},
			success: function(resp){
				$(self).closest('li').remove();
				update_cart();
				
				//update cart labels
				$.each($('.add-to-cart'), function(indx, val){
					if($(val).attr('data-id') == prod_id){
						$(val).toggleClass('active');
					}
				});
			}
		});
	});
}

function basket_clicks(e){
	if($(e.target).closest(".b-basket").length==0 &&
		$(e.target).closest(".b-header__option--basket").length==0 &&
		$(e.target).closest(".b-basket__close").length==0){
		$(".b-basket").slideUp();
	}
}

function update_cart(){
	var cart = $('#cart');
	$.ajax({
		url: '/cart/render',
		success: function (resp) {
			$('#cart').html(resp['cart_view']);
			
			$.when(getFavCount(), getCompareCount() ).done(function(resp1, resp2){
				app.event_bus.trigger('update-cart-fav-count', 'add', resp1[0]['favorite']);
				app.event_bus.trigger('update-cart-compare-count', 'add', resp2[0]['compare']);
			});
			
			basket_render();

			$(document).click(function(e) {
				basket_clicks(e);
			});
		},
		error: function(resp){
			console.log(resp);
		}
	});
}

function getFavCount(){
	return $.ajax({
		type: "GET",
		url: '/customer-favorite/'
	});
}

function getCompareCount(){
	return $.ajax({
		type: "GET",
		url: '/customer-compare/'
	});
}

function bmarkRender(){
	if($('.b-mark__list').length > 0){
		$('.b-mark__list').carouFredSel({
			direction            : "left",
			responsive           : true,
			scroll : {
				items            : 2,
				duration        : 1000,
				pauseOnHover    : true
			},
			items : {
				width       : 200,
				visible: {
					min: 1,
					max: 5
				}
			},
			pagination	: ".b-mark__paginator"
		});
	}
}