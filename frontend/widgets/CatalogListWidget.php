<?php
namespace app\widgets;


use common\models\db\Category;
use yii\base\Widget;
use yii\db\Query;

class CatalogListWidget extends Widget
{

    private $categories;
    public $name;

    public function init(){
        parent::init();

        $categories = [];

        switch ($this->name){
            case 'aside_categories_list':
                $this->categories = $this->findAsideCategories();
                break;
            case 'menu_categories_list':
                $this->categories = $this->findMenuCategories();
                break;
        }
    }

    private function findAsideCategories(){
        $catListSorted = [];

        $categories = Category::find()
            ->select('id, id_parent, name, slug')
            ->where(['status' => Category::$STATUS_ACTIVE])
            ->orderBy('ISNULL(id_parent) DESC, id ASC')
            ->asArray()
            ->all();

        foreach ($categories as $cat){
            if($cat['id_parent'] == null){
                $catListSorted[$cat['id']] = $cat;
            }else{
                $this->findInParent($cat, $catListSorted);
            }
        }

        return $catListSorted;
    }

    private function findMenuCategories(){
        $catListSorted = [];

        $categories = Category::find()
            ->select('id, id_parent, name, slug, show_on_main')
            ->where(['status' => Category::$STATUS_ACTIVE])
            ->orderBy('ISNULL(id_parent) DESC, id ASC')
            ->asArray()
            ->all();

        foreach ($categories as $cat){
            if($cat['show_on_main'] == true && $cat['id_parent'] == null){
                $catListSorted[$cat['id']] = $cat;
            }
        }

        foreach ($catListSorted as &$catParent){
            foreach ($categories as $cat){
                if($catParent['id'] == $cat['id_parent']){
                    $catParent['sub'][$cat['id']] = $cat;
                }
            }
        }

        return $catListSorted;
    }

    private function findInParent($currItem, &$array){
        foreach ($array as &$item) {
            if($item['id'] == $currItem['id_parent']){
                $item['sub'][$currItem['id']] = $currItem;
            }else if(array_key_exists('sub', $item)){
                $this->findInParent($currItem, $item['sub']);
            }else{
                continue;
            }
        }
    }

    public function run()
    {
        return $this->render(
            'catalog/' . $this->name, [
                'categories' => $this->categories
            ]
        );
    }
}