<?php
/**
 * Created by PhpStorm.
 * User: bitybite
 * Date: 6/30/16
 * Time: 5:30 AM
 */

namespace app\widgets;


use yii\base\Widget;

class SubCategoriesWidget extends Widget
{

    public $categories;

    public function run()
    {
        return $this->render(
            'catalog/subcategoryItems', [
                'categories' => $this->categories
            ]
        );
    }


}