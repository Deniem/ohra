<?php
/**
 * Created by PhpStorm.
 * User: bbyte
 * Date: 7/22/16
 * Time: 2:44 PM
 */

namespace app\widgets;


use yii\base\Widget;

class CatalogBreadCrumbsWidget extends Widget
{
    public $category;

    private function getParents(){
        $parents = [];

        if(!isset($this->category)){
            return [];
        }
        $currentParent = $this->category;
        do{
            $currentParent = $currentParent->parent;
            if($currentParent == null){
                break;
            } else{
                $parents[] = $currentParent;
            }
        }while($currentParent->id_parent !== null);
        return $parents =  array_reverse($parents);
    }

    public function run()
    {
        return $this->render('catalog/catalogBreadcrumbs', [
            'parents' => $this->getParents(),
            'category' => $this->category
        ]);
    }

}