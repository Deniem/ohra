<div id="blog-pages">
    <?php
    $format = 'Y-m-d H:i:s';
    ?>
    <?php foreach ($model->getModels() as $m): ?>
        <div class="row">
            <?php if ($m->thumbnail_base_url && $m->thumbnail_path): ?>
            <div class="col-sm-3">
                <a href="<?= \yii\helpers\Url::to(['/blog/slug', 'slug' => $m->action]) ?>">
                    <img src="<?= $m->getPreviewSrc() ?>" class="img-responsive"
                         alt="<?= $m->title ?>">
                </a>
            </div>
            <div class="col-sm-9">
                <?php else: ?>
                <div class="col-sm-12">
                <?php endif; ?>


                <div class="row">
                    <div class="col-sm-12">
                        <h2>
                            <a href="<?= \yii\helpers\Url::to(['/blog/slug', 'slug' => $m->action]) ?>">
                                <?= $m->title ?>
                            </a>
                        </h2>
                        <hr>
                    </div>
                </div>


                <div class="row">
                    <div class="col-sm-12">
                        <?= $m->description ?>
                    </div>
                </div>

                <br>
                <div class="row">
                    <div class="col-sm-6">
                        <?php if($m->published_at) : ?>
                        <span>
                            <span class="glyphicon glyphicon-time"></span>&nbsp;
                            <?= Yii::$app->formatter->asDate($m->published_at); ?>
                        </span>
                        <?php endif; ?>
                    </div>
                    <div class="col-sm-6 text-right">
                        <a href="<?= \yii\helpers\Url::to(['/blog/slug', 'slug' => $m->action]) ?>">
                            <button type="button" class="btn btn-primary">
                                Просмотр
                            </button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <br>
    <?php endforeach; ?>
</div>

