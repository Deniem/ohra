<?php
?>

<form action="" class="b-subscription">
    <h3 class="b-section__left-title">Подпишись на новинки в этой категории</h3>

    <div class="b-subscription__label-block">
        <input type="checkbox" id="check">
        <label class="b-subscription__label" for="check">Учитывать выбраные фильтры</label>
    </div>
    <div class="b-subscription__mail-block">
        <input type="mail" class="b-subscription__mail" placeholder="Адрес эл. почты">
        <button type="submit" class="b-subscription__button">Ok</button>
    </div>
</form>
