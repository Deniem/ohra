<?php
?>
<div class="row">
	<div class="panel panel-default">
		<div class="panel-heading">
			<h3 class="panel-title">Фильтр товаров</h3>
		</div>
		<div class="panel-body">
			<div class="col-md-12">
				<?php if (count($filters) == 0): ?>
					<div class="alert alert-warning col-md-12">Недостаточно товаров для фильтрации</div>
				<?php endif; ?>

				<?php foreach ($filters as $k => $v): ?>
					<div class="row">
						<div class="filter-products">
							<div class="col-md-12">
								<div class="row">
									<div class="col-md-12 filter-label-wrap">
										<label class="filter-label" for="checkbox-<?= $k ?>">
											<?= $v['property_name'] ?></label>
									</div>
								</div>
							</div>
							<br>
							<div class="col-md-12">
								<ul class="filter-list">
									<?php foreach ($v as $option_k => $option_v):
										if (!is_numeric($option_k)) continue; ?>
										<li>
											<input class="filter-checkbox" type="checkbox" id="checkbox-<?= $k ?>"
												   name="<?= $option_v['value_slug'] ?>"
												   data-filter-slug="<?= $k ?>">
											&nbsp;
											<label for="checkbox-<?= $option_v['value_slug'] ?>"
												   class="">
												<?= $option_v['value_name'] ?>
											</label>
										</li>
									<?php endforeach; ?>
								</ul>
							</div>
						</div>
					</div>
					<br/>
				<?php endforeach; ?>
			</div>
		</div>
	</div>
</div>