<?php
use yii\helpers\Url;

?>
<section class="b-breadcrumb">
    <ul class="b-breadcrumb__list">
        <li class="b-breadcrumb__item">
            <a href="<?= Url::home() ?>" class="b-breadcrumb__link">
                Главная
            </a>
        </li>
        <li class="b-breadcrumb__item">
            <a href="<?= Url::to(['/catalog']) ?>" class="b-breadcrumb__link
            <?= !isset($category) ? "b-breadcrumb__link--active" : "" ?>">
                Каталог
            </a>
        </li>
        <?php if (count($parents) > 0): ?>

            <?php foreach ($parents as $item): ?>
                <li class="b-breadcrumb__item">
                    <a href="<?= Url::to(['/catalog/'.$item->slug])?>" class="b-breadcrumb__link">
                        <?= $item->name ?>
                    </a>
                </li>
            <?php endforeach; ?>

        <?php endif; ?>
        <?php if(isset($category)): ?>
        <li class="b-breadcrumb__item">
            <a href="<?= Url::to(['/catalog/'.$category->slug])?>" class="b-breadcrumb__link b-breadcrumb__link--active">
                <?= $category->name ?>
            </a>
        </li>
        <?php endif; ?>

    </ul>
</section>

