<?php
use yii\helpers\Url;
?>

<ul class="b-header__botmenu-list">
    <?php

    foreach ($categories as $category): ?>
        <li class="b-header__botmenu-item">
            <?php if (array_key_exists('sub', $category)): ?>
                <a href="<?= Url::to(['catalog/' . $category['slug']]) ?>"
                   class="b-header__botmenu-link submenu_in">
                    <?= $category['name'] ?></a>
                <ul style="" class="b-header__submenu-list">
                    <?php foreach ($category['sub'] as $sub): ?>
                        <li class="li b-header__submenu-itemt">
                            <a href="<?= Url::to(['catalog/' . $sub['slug']]) ?>"
                               class="b-header__submenu-link">
                                <?= $sub['name'] ?>
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
            <?php else: ?>
                <a href="<?= Url::to(['catalog/' . $category['slug']]) ?>"
                   class="b-header__botmenu-link">
                    <?= $category['name'] ?></a>
            <?php endif; ?>

        </li>
    <?php endforeach; ?>
    <li class="b-header__botmenu-item">
        <a href="<?= Url::to(['/share']) ?>" class="b-header__botmenu-link b-header__botmenu-link--hot">Распродажа</a>
    </li>
</ul>