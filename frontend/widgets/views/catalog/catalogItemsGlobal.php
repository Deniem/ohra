<?php
use yii\helpers\Url;

?>

<ul class="b-categorys__list b-categorys__list--global">
    <li class="b-categorys__item">
        <div class="b-categorys__img">
            <img src="/img/product_global1.jpg" alt="">
        </div>
        <a class="b-categorys__link" href="<?=Url::to(['/share'])?>"><span>распродажи</span></a>
    </li>
    <li class="b-categorys__item">
        <div class="b-categorys__img">
            <img src="/img/product_global1.jpg" alt="">
        </div>
        <a class="b-categorys__link" href="<?= Url::to(['/sales'])?>"><span>Акции</span></a>
    </li>
    <li class="b-categorys__item">
        <div class="b-categorys__img">
            <img src="/img/product_global2.jpg" alt="">
        </div>
        <a class="b-categorys__link b-categorys__link--globalType1" href="<?= Url::to(['/blog'])?>"><span>Блог</span></a>
    </li>
</ul>
