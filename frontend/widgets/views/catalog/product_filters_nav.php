<?php
?>

<form class="b-filter">
    <div class="b-filter__line">
        <div class="b-filter__lineLeft">
            <ul class="b-filter__screenList">
                <li class="b-filter__screenItem">
                    <a href="#" class="b-filter__screenLink b-filter__screenLink--type1 active"></a>
                </li>
                <li class="b-filter__screenItem">
                    <a href="#" class="b-filter__screenLink b-filter__screenLink--type2"></a>
                </li>
            </ul>
        </div>
        <div class="b-filter__lineRight">
            <div class="b-filter__sort">
                <span class="b-filter__sortText">Сортировка</span>
                <div class="b-filter__select">
                    <select name="sort-by" class="sort-by">
                        <option value="popularity">Популярные</option>
                        <option value="new">Новинки</option>
                        <option value="action">Акционные</option>
                        <option value="sale">Распродажа</option>
                        <option value="from-cheap">От дешевым к дорогим</option>
                        <option value="from-expensive">От дорогим к дешевым</option>
                    </select>
                </div>
            </div>
        </div>
    </div>

    <nav class="b-filter__nav">
        <h3 class="b-filter__navTitle">Фильтры:</h3>
        <?php foreach ($filters as $k => $v): ?>
            <div class="b-filter__navBlock">
                <a href="#" class="b-filter__navLink"><?= $v['property_name'] ?></a>
                <div style="" class="b-filter__navContent">
                    <?php foreach ($v as $option_k => $option_v):
                        if (!is_numeric($option_k)) continue; ?>
                        <div class="b-filter__checkWrap">
                            <input id="checkbox-<?= $option_v['value_slug'] ?>" hidden="" type="checkbox"
                                   name="<?= $option_v['value_slug'] ?>"
                                   data-filter-slug="<?= $k ?>"
                                   class="filter-checkbox">
                            
                            <label for="checkbox-<?= $option_v['value_slug'] ?>"
                            class="">
                                <?= $option_v['value_name'] ?>
                            </label>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        <?php endforeach; ?>
    </nav>
</form>
