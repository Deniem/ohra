<?php
?>
<div class="row">
    <div class="col-md-12">
        <div class="b-categorys">
            <ul class="b-categorys__list">
                <?php foreach($categories as $cat): ?>
                    <li class="b-categorys__item">
                        <div class="b-categorys__img">
                            <?php if($cat->previewcatalog !== ''): ?>
                                <?= $cat->previewcatalog; ?>
                            <?php else: ?>
                                <img src="/img/product/product_1.jpg" alt="">
                            <?php endif;?>
                        </div>

                        <a class="b-categorys__link" href="/catalog/<?= $cat->slug?>">
                            <span><?= \yii\helpers\Html::decode($cat->name)?></span>
                        </a>
                    </li>
                <?php endforeach;	 ?>
            </ul>
        </div>
    </div>
</div>
