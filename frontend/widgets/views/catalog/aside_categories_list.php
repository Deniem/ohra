<?php
use yii\helpers\Url;

function draw_recursively($category)
{
    echo '<ul class="b-header__submenu-list">';
    foreach ($category as $sub) {
        if (array_key_exists('sub', $sub)) {
            echo '<li class="li b-header__submenu-itemt b-header__botmenu-item--active">
                    <a href="#" class="b-header__submenu-link submenu_in">' . $sub['name'] . '</a>';
            draw_recursively($sub['sub']);
            echo '</li>';
        } else {
            echo '<li class="li b-header__submenu-itemt">
                    <a href="/catalog/' . $sub['slug'] . '" class="b-header__submenu-link">' . $sub['name'] . '</a>
                </li>';
        }

    }
    echo '</ul>';
}

?>

<div class="b-section__left">
    <h3 class="b-section__left-title">
        <a class="filter-title" href="/catalog">Категории</a>
    </h3>

    <div class="b-header__bottommenu">
        <ul class="b-header__botmenu-list">
            <?php foreach($categories as $cat): ?>
                <?php if (array_key_exists('sub', $cat)): ?>
                    <li class="b-header__botmenu-item">
                        <a href="#" class="b-header__botmenu-link submenu_in"><?= $cat['name'] ?></a>
                        <?php draw_recursively($cat['sub']); ?>
                    </li>
                <?php else: ?>
                    <li class="b-header__botmenu-item">
                        <a href="/catalog/<?= $cat['slug']?>" class="b-header__botmenu-link"><?= $cat['name'] ?></a>
                    </li>
                <?php endif?>
            <?php endforeach ?>

            <li class="b-header__botmenu-item b-header__botmenu-item--option">
                <a href="<?= Url::to(['/share'])?>" class="b-header__botmenu-link b-header__botmenu-link--hot">Распродажа</a>
            </li>
            <li class="b-header__botmenu-item">
                <a href="<?= Url::to(['/sales'])?>" class="b-header__botmenu-link b-header__botmenu-link--hot">Акции</a>
            </li>
            <li class="b-header__botmenu-item">
                <a href="<?= Url::to(['/blog'])?>" class="b-header__botmenu-link b-header__botmenu-link--hot">Блог</a>
            </li>
        </ul>
    </div>

</div>