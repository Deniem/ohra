<section class="b-actions">
    <div class="container">
        <h2 class="b-actions__main-title">Акции</h2>
        <ul class="b-actions__list">
            <li class="b-actions__item">
                <div class="b-actions__wrap-img">
                    <img src="img/actions_1.jpg" alt="">
                </div>
                <div class="b-actions__option">
                    <span class="b-actions__date">15 Апреля, 2015</span> |
                    <a href="#" class="b-actions__coment">2 комментария</a>
                </div>
                <a href="#" class="b-actions__title">Диван за полцены</a>
                <div class="b-actions__text">Этот красавчик идеален для посиделок в кругу близких и друзей! И с 20 июля по 20 августа, в честь Дня рождения компании NICOLAS, его цена</div>
                <a href="#" class="b-actions__detal">Детали</a>
            </li>
            <li class="b-actions__item">
                <div class="b-actions__wrap-img">
                    <img src="img/actions_2.jpg" alt="">
                </div>
                <div class="b-actions__option">
                    <span class="b-actions__date">25 Марта, 2015</span> |
                    <a href="#" class="b-actions__coment">комментариев нет</a>
                </div>
                <a href="#" class="b-actions__title">Распродажа кухонь</a>
                <div class="b-actions__text">Отличный повод обновить кухню для жителей из Николаева, ведь мы устраиваем распродажу выставочных образцов кухонь со скидкой в 50%! Акция продолжается до конца года.</div>
                <a href="#" class="b-actions__detal">Детали</a>
            </li>
            <li class="b-actions__item">
                <div class="b-actions__wrap-img">
                    <img src="img/actions_3.jpg" alt="">
                </div>
                <div class="b-actions__option">
                    <span class="b-actions__date">20 Марта, 2015</span> |
                    <a href="#" class="b-actions__coment">10 комментариев</a>
                </div>
                <a href="#" class="b-actions__title">Скидки на технику — 60%</a>
                <div class="b-actions__text">Отличный повод обновить кухню для жителей из Николаева, ведь мы устраиваем распродажу выставочных образцов кухонь со скидкой в 50%! Акция продолжается до конца года.</div>
                <a href="#" class="b-actions__detal">Детали</a>
            </li>
        </ul>
        <?=\app\widgets\PartInject::widget(['name'=>'delivery-support-main-page'])?>

    </div>
</section>