<div class="col-sm-7">
    <div class="b-sale container-novelty" data-view-type="main.Novelty">
        <h2 class="b-section__title">Распродажа</h2>
        <ul class="b-sale__list">
	        <?php use frontend\controllers\CartController;

			foreach($models as $model): ?>
	            <li class="b-novelty__item b-novelty__item--sale">
	                <a class="b-novelty__img" href="<?= \yii\helpers\Url::to(['/product/'.$model->slug])?>">
	                    <img src="<?php if ($model->thumbnail_path) {
		                    echo Yii::$app->glide->createSignedUrl([
			                    'glide/index',
			                    'path' => $model->thumbnail_path,
			                    'w' => 113,
			                    'h' => 184,
			                    'fit' => 'crop'
		                    ], true);
	                    } else {
		                    echo '';
	                    }
	                    ?>" alt="">
	                </a>
	                <div class="b-novelty__detal">
	                    <div class="b-novelty__detal-left">
	                        <h3 class="b-novelty__name"><?= $model->name ?></h3>
	                        <span class="b-novelty__type"><?= $model->category->name ?></span>
	                    </div>
	                    <div class="b-novelty__detal-right">
	                        <div class="b-novelty__ptice-old"><span>4 600</span> грн</div><!-- Где взять price-old? -->
	                        <div class="b-novelty__ptice"><span><?= $model->discountActive ? $model->discount->price : $model->price ?></span> грн</div>
	                    </div>
	                </div>
	                <div class="b-novelty__hide-block">
	                    <ul class="b-novelty__option-list">
	                        <li class="b-novelty__option-item">
	                            <a href="" data-id="<?= $model->id?>"
								   class="b-novelty__option-link b-novelty__option-link--basket add-to-cart
								   <?= CartController::isInCart($model->id)?>"></a>
	                        </li>
	                        <li class="b-novelty__option-item">
	                            <a href=""  data-id="<?= $model->id?>"
								   class="b-novelty__option-link b-novelty__option-link--like add-to-fav
								   <?= CartController::isInFavorite($model->id) ?>"></a>
	                        </li>
	                        <li class="b-novelty__option-item">
	                            <a href="" data-id="<?= $model->id?>"
								   class="b-novelty__option-link b-novelty__option-link--share add-to-compare
								   <?= CartController::isInCompareList($model->id) ?>"></a>
	                        </li>
	                    </ul>
						<div class="b-novelty__size">
							<?php $widget->drawSizes($model->productValues)?>
						</div>
	                    <div class="b-novelty__total-price">
							<?php if($model->discountActive) :?>
								<div class="b-novelty__old-price"><span><?= $model->price ?></span> грн</div>
								<div class="b-novelty__new-price"><span><?= $model->discount->price ?></span> грн</div>
								<div class="b-novelty__benefit-price">выгода <span><?= $model->price - $model->discount->price?></span> грн</div>
								<!-- Что за выгода? -->
							<?php else: ?>
								<div class="b-novelty__new-price"><span><?= $model->price ?></span> грн</div>
							<?php endif; ?>
	                    </div>
	                </div>
	            </li>
	        <?php endforeach; ?>
        </ul>
    </div>
</div>
