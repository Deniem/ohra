<section class="b-section__item">
    <div class="container container-novelty">
        <div class="row">
            <!--Goods of the month-->
            <?=\app\widgets\PartInject::widget(['name'=>'goods-of-the-month-main-page'])?>
            <!--Goods of the month-->
            <!--sales-->
            <?=\app\widgets\PartInject::widget([
                'name'=>'sales-main-page',
                'models' => $models,
                'properties' => $properties
            ])?>
            <!--sales-->
        </div>
    </div>
</section>
