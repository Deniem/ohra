<?php
use yii\helpers\Url;

?>
<?php if (count($models) > 0): ?>
    <section class="b-mark">
        <div class="container">
            <ul class="b-mark__list">
                <?php foreach ($models as $brand): ?>
                    <li class="b-mark__item">
                        <a href="<?= Url::to(['brand/' . $brand->slug]) ?>" class="b-mark__link">
                            <img src="<?= $brand->previewSrc?>" alt="<?= $brand->name ?>">
                        </a>
                    </li>
                <?php endforeach; ?>

            </ul>
            <div class="b-mark__paginator"></div>
        </div>
    </section>

<?php endif; ?>

