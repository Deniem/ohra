<section class="b-category">
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="b-category__block b-category__block--type1" style="background-image: url(img/category_1.jpg)">
                    <div class="b-category__text-block1">
                        <div class="b-category__title1">Журнальне</div>
                        <div class="b-category__text1">столы</div>
                        <a href="#" class="b-category__button b-category__button--type1">Смотреть</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="b-category__block b-category__block--type2" style="background-image: url(img/category_2.jpg)">
                    <div class="b-category__text-block2">
                        <div class="b-category__text-content">
                            <div class="b-category__title2">распродажа</div>
                            <div class="b-category__text2">Классической мебели</div>
                        </div>
                        <a href="#" class="b-category__button b-category__button--type2">Смотреть</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>