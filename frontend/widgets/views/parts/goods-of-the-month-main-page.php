<div class="col-sm-5">
    <div class="b-month">
        <h2 class="b-section__title">Товары месяца</h2>
        <div class="b-month__slider">
            <ul class="b-month__list">
                <li class="b-month__item">
                    <div class="b-month__time">
                        <div class="b-month__time-item b-month__time-item--hours"><span>23</span> часа</div>
                        <div class="b-month__time-item b-month__time-item--minutes"><span>15</span> минут</div>
                        <div class="b-month__time-item b-month__time-item--cecond"><span>10</span> секунд</div>
                    </div>
                    <div class="b-month__img">
                        <img src="img/slider_img_1.jpg" alt="">
                    </div>
                    <div class="b-novelty__detal b-novelty__detal--month">
                        <div class="b-novelty__detal-left">
                            <h3 class="b-novelty__name b-novelty__name--month">Boston   </h3>
                            <span class="b-novelty__type b-novelty__type--month">Угловой диван</span>
                        </div>
                        <div class="b-novelty__detal-right">
                            <div class="b-novelty__ptice-old b-novelty__ptice-old--month"><span>36 000</span> грн</div>
                            <div class="b-novelty__ptice b-novelty__ptice--month"><span>29 000</span> грн</div>
                        </div>
                    </div>
                </li>
                <li class="b-month__item">
                    <div class="b-month__time">
                        <div class="b-month__time-item b-month__time-item--hours"><span>23</span> часа</div>
                        <div class="b-month__time-item b-month__time-item--minutes"><span>15</span> минут</div>
                        <div class="b-month__time-item b-month__time-item--cecond"><span>10</span> секунд</div>
                    </div>
                    <div class="b-month__img">
                        <img src="img/slider_img_1.jpg" alt="">
                    </div>
                    <div class="b-novelty__detal b-novelty__detal--month">
                        <div class="b-novelty__detal-left">
                            <h3 class="b-novelty__name b-novelty__name--month">Boston   </h3>
                            <span class="b-novelty__type b-novelty__type--month">Угловой диван</span>
                        </div>
                        <div class="b-novelty__detal-right">
                            <div class="b-novelty__ptice-old b-novelty__ptice-old--month"><span>36 000</span> грн</div>
                            <div class="b-novelty__ptice b-novelty__ptice--month"><span>29 000</span> грн</div>
                        </div>
                    </div>
                </li>
                <li class="b-month__item">
                    <div class="b-month__time">
                        <div class="b-month__time-item b-month__time-item--hours"><span>23</span> часа</div>
                        <div class="b-month__time-item b-month__time-item--minutes"><span>15</span> минут</div>
                        <div class="b-month__time-item b-month__time-item--cecond"><span>10</span> секунд</div>
                    </div>
                    <div class="b-month__img">
                        <img src="img/slider_img_1.jpg" alt="">
                    </div>
                    <div class="b-novelty__detal b-novelty__detal--month">
                        <div class="b-novelty__detal-left">
                            <h3 class="b-novelty__name b-novelty__name--month">Boston   </h3>
                            <span class="b-novelty__type b-novelty__type--month">Угловой диван</span>
                        </div>
                        <div class="b-novelty__detal-right">
                            <div class="b-novelty__ptice-old b-novelty__ptice-old--month"><span>36 000</span> грн</div>
                            <div class="b-novelty__ptice b-novelty__ptice--month"><span>29 000</span> грн</div>
                        </div>
                    </div>
                </li>
            </ul>
            <div class="b-month__nav b-month__nav--left"></div>
            <div class="b-month__nav b-month__nav--right"></div>
        </div>
    </div>
</div>