<section class="b-mainslider">
    <ul class="b-mainslider__list">
        <li class="b-mainslider__item" style="background-image: url(img/main_slider_1.jpg);">
            <div class="b-mainslider__text">
                <h3 class="b-mainslider__title">Украина, London</h3>
                <p class="b-mainslider__detal-text">Всё верно, теперь London может быть в каждом Украинском доме</p>
                <a href="#" class="b-mainslider__link">На ПМЖ</a>
            </div>
        </li>
        <li class="b-mainslider__item" style="background-image: url(img/main_slider_1.jpg);">
            <div class="b-mainslider__text">
                <h3 class="b-mainslider__title">Украина, London</h3>
                <p class="b-mainslider__detal-text">Всё верно, теперь London может быть в каждом Украинском доме</p>
                <a href="#" class="b-mainslider__link">На ПМЖ</a>
            </div>
        </li>
        <li class="b-mainslider__item" style="background-image: url(img/main_slider_1.jpg);">
            <div class="b-mainslider__text">
                <h3 class="b-mainslider__title">Украина, London</h3>
                <p class="b-mainslider__detal-text">Всё верно, теперь London может быть в каждом Украинском доме</p>
                <a href="#" class="b-mainslider__link">На ПМЖ</a>
            </div>
        </li>
    </ul>
    <div class="b-mainslider__nav b-mainslider__nav--left"></div>
    <div class="b-mainslider__nav b-mainslider__nav--right"></div>
</section>