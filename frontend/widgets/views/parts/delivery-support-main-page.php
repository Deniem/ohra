<div class="row">
    <div class="col-sm-6">
        <div class="b-actions__about b-actions__about--type1">
            <div class="b-actions__about-img">
                <img src="img/big_icon_1.png" alt="">
            </div>
            <div class="b-actions__about-textblock">
                <h3 class="b-actions__about-title">Бесплатная доставка</h3>
                <p class="b-actions__about-text">Мы сотрудничаем с лучшими партнерами в Украине по доставке товаров — Мiст Експрес и Нова Пошта.</p>
            </div>
        </div>
    </div>
    <div class="col-sm-6">
        <div class="b-actions__about b-actions__about--type2">
            <div class="b-actions__about-img">
                <img src="img/big_icon_2.png" alt="">
            </div>
            <div class="b-actions__about-textblock">
                <h3 class="b-actions__about-title">Поддержка 24/7</h3>
                <p class="b-actions__about-text">Ответим на любые вопросы на протяжении всего дня. А если что, и ночью :-)</p>
            </div>
        </div>
    </div>
</div>