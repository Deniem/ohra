<?php
?>

<div class="b-category">
    <ul class="b-category__list">
        <li class="b-novelty__item" style="width: 285px;">
            <a href="#" class="b-novelty__img b-novelty__img--sell">
                <img src="assets/img/product/product_3.jpg" alt="">
            </a>
            <div class="b-novelty__detal">
                <div class="b-novelty__detal-left">
                    <h3 class="b-novelty__name">Derby </h3>
                    <span class="b-novelty__type">Стул</span>
                </div>
                <div class="b-novelty__detal-right">
                    <div class="b-novelty__ptice"><span>6 000</span> грн</div>
                </div>
            </div>
            <div class="b-novelty__hide-block">
                <ul class="b-novelty__option-list">
                    <li class="b-novelty__option-item">
                        <a href="" class="b-novelty__option-link b-novelty__option-link--basket"></a>
                    </li>
                    <li class="b-novelty__option-item">
                        <a href="" class="b-novelty__option-link b-novelty__option-link--like"></a>
                    </li>
                    <li class="b-novelty__option-item">
                        <a href="" class="b-novelty__option-link b-novelty__option-link--share"></a>
                    </li>
                </ul>
                <div class="b-novelty__size">
                    <p class="b-novelty__size-text">Кровать: <span class="b-novelty__param b-novelty__param--type1"></span> 226 <span class="b-novelty__param b-novelty__param--type2"></span>    196−176 <span class="b-novelty__param b-novelty__param--type3"></span>  115</p>
                    <p class="b-novelty__size-text">Спальное место: <span class="b-novelty__param b-novelty__param--type1"></span>    180−160 <span class="b-novelty__param b-novelty__param--type3"></span>  200 <span class="b-novelty__param b-novelty__param--type4"></span></p>
                </div>
                <div class="b-novelty__total-price">
                    <div class="b-novelty__old-price"><span>50 000</span> грн  </div>
                    <div class="b-novelty__new-price"><span>40 000</span> грн</div>
                    <div class="b-novelty__benefit-price">выгода <span>10 000</span> грн</div>
                </div>
            </div>
        </li>
        <li class="b-novelty__item" style="width: 285px;">
            <!--if prodact has discount, add attribute data-discout="скидка" -->
            <a href="#" class="b-novelty__img" data-discout="50%">
                <img src="assets/img/product/product_4.jpg" alt="">
            </a>
            <div class="b-novelty__detal">
                <div class="b-novelty__detal-left">
                    <h3 class="b-novelty__name">Derby </h3>
                    <span class="b-novelty__type">Стул</span>
                </div>
                <div class="b-novelty__detal-right">
                    <div class="b-novelty__ptice-old"><span>6 000</span> грн</div>
                    <div class="b-novelty__ptice"><span>4 000</span> грн</div>
                </div>
            </div>
            <div class="b-novelty__hide-block">
                <ul class="b-novelty__option-list">
                    <li class="b-novelty__option-item">
                        <a href="" class="b-novelty__option-link b-novelty__option-link--basket"></a>
                    </li>
                    <li class="b-novelty__option-item">
                        <a href="" class="b-novelty__option-link b-novelty__option-link--like"></a>
                    </li>
                    <li class="b-novelty__option-item">
                        <a href="" class="b-novelty__option-link b-novelty__option-link--share"></a>
                    </li>
                </ul>
                <div class="b-novelty__size">
                    <p class="b-novelty__size-text">Кровать: <span class="b-novelty__param b-novelty__param--type1"></span> 226 <span class="b-novelty__param b-novelty__param--type2"></span>    196−176 <span class="b-novelty__param b-novelty__param--type3"></span>  115</p>
                    <p class="b-novelty__size-text">Спальное место: <span class="b-novelty__param b-novelty__param--type1"></span>    180−160 <span class="b-novelty__param b-novelty__param--type3"></span>  200 <span class="b-novelty__param b-novelty__param--type4"></span></p>
                </div>
                <div class="b-novelty__total-price">
                    <div class="b-novelty__old-price"><span>50 000</span> грн  </div>
                    <div class="b-novelty__new-price"><span>40 000</span> грн</div>
                    <div class="b-novelty__benefit-price">выгода <span>10 000</span> грн</div>
                </div>
            </div>
        </li>
        <li class="b-novelty__item" style="width: 285px;">
            <!--if prodact has discount, add attribute data-discout="скидка" -->
            <a href="#" class="b-novelty__img">
                <img src="assets/img/product/product_1.jpg" alt="">
            </a>
            <div class="b-novelty__detal">
                <div class="b-novelty__detal-left">
                    <h3 class="b-novelty__name">London </h3>
                    <span class="b-novelty__type">Стол</span>
                </div>
                <div class="b-novelty__detal-right">
                    <div class="b-novelty__ptice"><span>15 000</span> грн</div>
                </div>
            </div>
            <div class="b-novelty__hide-block">
                <ul class="b-novelty__option-list">
                    <li class="b-novelty__option-item">
                        <a href="" class="b-novelty__option-link b-novelty__option-link--basket"></a>
                    </li>
                    <li class="b-novelty__option-item">
                        <a href="" class="b-novelty__option-link b-novelty__option-link--like"></a>
                    </li>
                    <li class="b-novelty__option-item">
                        <a href="" class="b-novelty__option-link b-novelty__option-link--share"></a>
                    </li>
                </ul>
                <div class="b-novelty__size">
                    <p class="b-novelty__size-text">Кровать: <span class="b-novelty__param b-novelty__param--type1"></span> 226 <span class="b-novelty__param b-novelty__param--type2"></span>    196−176 <span class="b-novelty__param b-novelty__param--type3"></span>  115</p>
                    <p class="b-novelty__size-text">Спальное место: <span class="b-novelty__param b-novelty__param--type1"></span>    180−160 <span class="b-novelty__param b-novelty__param--type3"></span>  200 <span class="b-novelty__param b-novelty__param--type4"></span></p>
                </div>
                <div class="b-novelty__total-price">
                    <div class="b-novelty__old-price"><span>50 000</span> грн  </div>
                    <div class="b-novelty__new-price"><span>40 000</span> грн</div>
                    <div class="b-novelty__benefit-price">выгода <span>10 000</span> грн</div>
                </div>
            </div>
        </li>
        <li class="b-novelty__item" style="width: 285px;">
            <a href="#" class="b-novelty__img">
                <img src="assets/img/product/product_2.jpg" alt="">
            </a>
            <div class="b-novelty__detal">
                <div class="b-novelty__detal-left">
                    <h3 class="b-novelty__name">London </h3>
                    <span class="b-novelty__type">Стол</span>
                </div>
                <div class="b-novelty__detal-right">
                    <div class="b-novelty__ptice"><span>40 000</span> грн</div>
                </div>
            </div>
            <div class="b-novelty__hide-block">
                <ul class="b-novelty__option-list">
                    <li class="b-novelty__option-item">
                        <a href="" class="b-novelty__option-link b-novelty__option-link--basket"></a>
                    </li>
                    <li class="b-novelty__option-item">
                        <a href="" class="b-novelty__option-link b-novelty__option-link--like"></a>
                    </li>
                    <li class="b-novelty__option-item">
                        <a href="" class="b-novelty__option-link b-novelty__option-link--share"></a>
                    </li>
                </ul>
                <div class="b-novelty__size">
                    <p class="b-novelty__size-text">Кровать: <span class="b-novelty__param b-novelty__param--type1"></span> 226 <span class="b-novelty__param b-novelty__param--type2"></span>    196−176 <span class="b-novelty__param b-novelty__param--type3"></span>  115</p>
                    <p class="b-novelty__size-text">Спальное место: <span class="b-novelty__param b-novelty__param--type1"></span>    180−160 <span class="b-novelty__param b-novelty__param--type3"></span>  200 <span class="b-novelty__param b-novelty__param--type4"></span></p>
                </div>
                <div class="b-novelty__total-price">
                    <div class="b-novelty__old-price"><span>50 000</span> грн  </div>
                    <div class="b-novelty__new-price"><span>40 000</span> грн</div>
                    <div class="b-novelty__benefit-price">выгода <span>10 000</span> грн</div>
                </div>
            </div>
        </li>
        <li class="b-novelty__item" style="width: 285px;">
            <a href="#" class="b-novelty__img">
                <img src="assets/img/product/product_3.jpg" alt="">
            </a>
            <div class="b-novelty__detal">
                <div class="b-novelty__detal-left">
                    <h3 class="b-novelty__name">Derby </h3>
                    <span class="b-novelty__type">Стул</span>
                </div>
                <div class="b-novelty__detal-right">
                    <div class="b-novelty__ptice"><span>6 000</span> грн</div>
                </div>
            </div>
            <div class="b-novelty__hide-block">
                <ul class="b-novelty__option-list">
                    <li class="b-novelty__option-item">
                        <a href="" class="b-novelty__option-link b-novelty__option-link--basket"></a>
                    </li>
                    <li class="b-novelty__option-item">
                        <a href="" class="b-novelty__option-link b-novelty__option-link--like"></a>
                    </li>
                    <li class="b-novelty__option-item">
                        <a href="" class="b-novelty__option-link b-novelty__option-link--share"></a>
                    </li>
                </ul>
                <div class="b-novelty__size">
                    <p class="b-novelty__size-text">Кровать: <span class="b-novelty__param b-novelty__param--type1"></span> 226 <span class="b-novelty__param b-novelty__param--type2"></span>    196−176 <span class="b-novelty__param b-novelty__param--type3"></span>  115</p>
                    <p class="b-novelty__size-text">Спальное место: <span class="b-novelty__param b-novelty__param--type1"></span>    180−160 <span class="b-novelty__param b-novelty__param--type3"></span>  200 <span class="b-novelty__param b-novelty__param--type4"></span></p>
                </div>
                <div class="b-novelty__total-price">
                    <div class="b-novelty__old-price"><span>50 000</span> грн  </div>
                    <div class="b-novelty__new-price"><span>40 000</span> грн</div>
                    <div class="b-novelty__benefit-price">выгода <span>10 000</span> грн</div>
                </div>
            </div>
        </li>
        <li class="b-novelty__item" style="width: 285px;">
            <!--if prodact has discount, add attribute data-discout="скидка" -->
            <a href="#" class="b-novelty__img" data-discout="-25%">
                <img src="assets/img/product/product_4.jpg" alt="">
            </a>
            <div class="b-novelty__detal">
                <div class="b-novelty__detal-left">
                    <h3 class="b-novelty__name">Derby </h3>
                    <span class="b-novelty__type">Стул</span>
                </div>
                <div class="b-novelty__detal-right">
                    <div class="b-novelty__ptice-old"><span>6 000</span> грн</div>
                    <div class="b-novelty__ptice"><span>4 000</span> грн</div>
                </div>
            </div>
            <div class="b-novelty__hide-block">
                <ul class="b-novelty__option-list">
                    <li class="b-novelty__option-item">
                        <a href="" class="b-novelty__option-link b-novelty__option-link--basket"></a>
                    </li>
                    <li class="b-novelty__option-item">
                        <a href="" class="b-novelty__option-link b-novelty__option-link--like"></a>
                    </li>
                    <li class="b-novelty__option-item">
                        <a href="" class="b-novelty__option-link b-novelty__option-link--share"></a>
                    </li>
                </ul>
                <div class="b-novelty__size">
                    <p class="b-novelty__size-text">Кровать: <span class="b-novelty__param b-novelty__param--type1"></span> 226 <span class="b-novelty__param b-novelty__param--type2"></span>    196−176 <span class="b-novelty__param b-novelty__param--type3"></span>  115</p>
                    <p class="b-novelty__size-text">Спальное место: <span class="b-novelty__param b-novelty__param--type1"></span>    180−160 <span class="b-novelty__param b-novelty__param--type3"></span>  200 <span class="b-novelty__param b-novelty__param--type4"></span></p>
                </div>
                <div class="b-novelty__total-price">
                    <div class="b-novelty__old-price"><span>50 000</span> грн  </div>
                    <div class="b-novelty__new-price"><span>40 000</span> грн</div>
                    <div class="b-novelty__benefit-price">выгода <span>10 000</span> грн</div>
                </div>
            </div>
        </li>
        <li class="b-novelty__item" style="width: 285px;">
            <!--if prodact has discount, add attribute data-discout="скидка" -->
            <a href="#" class="b-novelty__img">
                <img src="assets/img/product/product_1.jpg" alt="">
            </a>
            <div class="b-novelty__detal">
                <div class="b-novelty__detal-left">
                    <h3 class="b-novelty__name">London </h3>
                    <span class="b-novelty__type">Стол</span>
                </div>
                <div class="b-novelty__detal-right">
                    <div class="b-novelty__ptice"><span>15 000</span> грн</div>
                </div>
            </div>
            <div class="b-novelty__hide-block">
                <ul class="b-novelty__option-list">
                    <li class="b-novelty__option-item">
                        <a href="#" class="b-novelty__option-link b-novelty__option-link--basket"></a>
                    </li>
                    <li class="b-novelty__option-item">
                        <a href="#" class="b-novelty__option-link b-novelty__option-link--like"></a>
                    </li>
                    <li class="b-novelty__option-item">
                        <a href="#" class="b-novelty__option-link b-novelty__option-link--share"></a>
                    </li>
                </ul>
                <div class="b-novelty__size">
                    <p class="b-novelty__size-text">Кровать: <span class="b-novelty__param b-novelty__param--type1"></span> 226 <span class="b-novelty__param b-novelty__param--type2"></span>    196−176 <span class="b-novelty__param b-novelty__param--type3"></span>  115</p>
                    <p class="b-novelty__size-text">Спальное место: <span class="b-novelty__param b-novelty__param--type1"></span>    180−160 <span class="b-novelty__param b-novelty__param--type3"></span>  200 <span class="b-novelty__param b-novelty__param--type4"></span></p>
                </div>
                <div class="b-novelty__total-price">
                    <div class="b-novelty__old-price"><span>50 000</span> грн  </div>
                    <div class="b-novelty__new-price"><span>40 000</span> грн</div>
                    <div class="b-novelty__benefit-price">выгода <span>10 000</span> грн</div>
                </div>
            </div>
        </li>
        <li class="b-novelty__item" style="width: 285px;">
            <a href="#" class="b-novelty__img">
                <img src="assets/img/product/product_2.jpg" alt="">
            </a>
            <div class="b-novelty__detal">
                <div class="b-novelty__detal-left">
                    <h3 class="b-novelty__name">London </h3>
                    <span class="b-novelty__type">Стол</span>
                </div>
                <div class="b-novelty__detal-right">
                    <div class="b-novelty__ptice"><span>40 000</span> грн</div>
                </div>
            </div>
            <div class="b-novelty__hide-block">
                <ul class="b-novelty__option-list">
                    <li class="b-novelty__option-item">
                        <a href="" class="b-novelty__option-link b-novelty__option-link--basket"></a>
                    </li>
                    <li class="b-novelty__option-item">
                        <a href="" class="b-novelty__option-link b-novelty__option-link--like"></a>
                    </li>
                    <li class="b-novelty__option-item">
                        <a href="" class="b-novelty__option-link b-novelty__option-link--share"></a>
                    </li>
                </ul>
                <div class="b-novelty__size">
                    <p class="b-novelty__size-text">Кровать: <span class="b-novelty__param b-novelty__param--type1"></span> 226 <span class="b-novelty__param b-novelty__param--type2"></span>    196−176 <span class="b-novelty__param b-novelty__param--type3"></span>  115</p>
                    <p class="b-novelty__size-text">Спальное место: <span class="b-novelty__param b-novelty__param--type1"></span>    180−160 <span class="b-novelty__param b-novelty__param--type3"></span>  200 <span class="b-novelty__param b-novelty__param--type4"></span></p>
                </div>
                <div class="b-novelty__total-price">
                    <div class="b-novelty__old-price"><span>50 000</span> грн  </div>
                    <div class="b-novelty__new-price"><span>40 000</span> грн</div>
                    <div class="b-novelty__benefit-price">выгода <span>10 000</span> грн</div>
                </div>
            </div>
        </li>
    </ul>
</div>
