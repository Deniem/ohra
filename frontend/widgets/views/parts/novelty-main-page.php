<section class="b-novelty">
    <div class="container container-novelty" data-view-type="main.Novelty">
        <h2 class="b-section__title"><?= $title ?></h2>

        <div class="b-novelty__slider b-novelty__slider--type1">
            <ul class="b-novelty__list js-product__list">
	            <?php use frontend\controllers\CartController;
				foreach($models as $model) : ?>
		            <li class="b-novelty__item">
			            <!--if prodact has discount, add attribute data-discout="скидка" -->
						<a class="b-novelty__img" href="<?= \yii\helpers\Url::to(['/product/'.$model->slug])?>">
				            <img src="<?php
				            if ($model->thumbnail_path) {
					            echo Yii::$app->glide->createSignedUrl([
						            'glide/index',
						            'path' => $model->thumbnail_path,
						            'w' => 267,
						            'h' => 275,
						            'fit' => 'crop'
					            ], true);
				            } else {
					            echo '';
				            }
				            ?>" alt="">
			            </a>
			            <div class="b-novelty__detal">
				            <div class="b-novelty__detal-left">
					            <h3 class="b-novelty__name"><?= $model->name ?></h3>
					            <span class="b-novelty__type"><?= $model->category->name ?></span>
				            </div>
				            <div class="b-novelty__detal-right">
					            <div class="b-novelty__ptice">
									<span>
										<?= $model->discountActive ? $model->discount->price : $model->price ?>
									</span> грн
								</div>
				            </div>
			            </div>
			            <div class="b-novelty__hide-block">
				            <ul class="b-novelty__option-list">
					            <li class="b-novelty__option-item">
						            <a href="#" data-id="<?= $model->id?>"
									   class="b-novelty__option-link b-novelty__option-link--basket add-to-cart
									   <?= CartController::isInCart($model->id)?>"></a>
					            </li>
					            <li class="b-novelty__option-item">
						            <a href="#" data-id="<?= $model->id?>"
									   class="b-novelty__option-link b-novelty__option-link--like add-to-fav
									   <?= CartController::isInFavorite($model->id) ?>"></a>
					            </li>
					            <li class="b-novelty__option-item">
						            <a href="#" data-id="<?= $model->id?>"
									   class="b-novelty__option-link b-novelty__option-link--share add-to-compare
									   <?= CartController::isInCompareList($model->id) ?>"></a>
					            </li>
				            </ul>

							<div class="b-novelty__size">
								<?php $widget->drawSizes($model->productValues)?>
				            </div>
				            <div class="b-novelty__total-price">
								<?php if($model->discountActive) :?>
									<div class="b-novelty__old-price"><span><?= $model->price ?></span> грн</div>
									<div class="b-novelty__new-price"><span><?= $model->discount->price ?></span> грн</div>
									<div class="b-novelty__benefit-price">выгода <span><?= $model->price - $model->discount->price?></span> грн</div>
									<!-- Что за выгода? -->
								<?php else: ?>
									<div class="b-novelty__new-price"><span><?= $model->price ?></span> грн</div>
								<?php endif; ?>
				            </div>
			            </div>
		            </li>
	            <?php endforeach; ?>

            </ul>
			<?php if(count($models) > 4): ?>
            <div class="b-novelty__nav b-novelty__nav--left"></div>
            <div class="b-novelty__nav b-novelty__nav--right"></div>
			<?php endif;?>
        </div>
    </div>
</section>
