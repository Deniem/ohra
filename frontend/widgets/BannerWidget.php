<?php
/**
 * Created by PhpStorm.
 * User: bbyte
 * Date: 7/8/16
 * Time: 4:44 PM
 */

namespace app\widgets;


use yii\base\Widget;

class BannerWidget extends Widget
{
    public $name;
    
    public function run()
    {
        return $this->render(
            'banner/' . $this->name
        );
    }


}