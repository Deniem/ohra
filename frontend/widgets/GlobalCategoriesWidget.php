<?php
/**
 * Created by PhpStorm.
 * User: bbyte
 * Date: 8/1/16
 * Time: 2:20 PM
 */

namespace app\widgets;


use yii\base\Widget;

class GlobalCategoriesWidget extends Widget
{
    public function run()
    {
        return $this->render(
            'catalog/catalogItemsGlobal'
        );
    }
}