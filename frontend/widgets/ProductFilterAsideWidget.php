<?php
/**
 * Created by PhpStorm.
 * User: bbyte
 * Date: 7/8/16
 * Time: 4:48 PM
 */

namespace app\widgets;


use yii\base\Widget;

class ProductFilterAsideWidget extends Widget
{
    public $name;
    public $filters;
    
    public function run()
    {
        return $this->render(
            'catalog/' . $this->name, [
                'filters' => $this->filters
            ]
        );
    }
}