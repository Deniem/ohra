<?php
namespace app\widgets;

use yii\base\Widget;

class PartInject extends Widget
{
    public $name;
    public $title;
    public $models;
    public $properties;

    public function run()
    {
        return $this->render(
            'parts/' . strtolower($this->name), [
                'models' => $this->models,
                'properties' => $this->properties,
                'widget' => $this,
                'title' => $this->title
            ]
        );
    }

    public function findInProperties($key){
        $needle = false;
        foreach ($this->properties as $property){
            if($property['id'] == $key){
                $needle = $property;
                break;
            }
        }
        return $needle;
    }

    public function drawSizes($values){
        $widthVal = false;
        $heightVal = false;
        $diameterVal = false;
        $remains= [];
        foreach ($values as $value){
            foreach ($this->properties as $property){
                if($property['id'] == $value->id_property){
                    $name = $property['name'];
                    if(($name == 'Ширина')){ $widthVal = $value->name;}
                    else if(($name == 'Высота')){ $heightVal = $value->name;}
                    else if(($name == 'Длина')){ $diameterVal = $value->name;}
                    else{
                        $remains[] =  '<p class="b-novelty__size-text">' . $property['name'] . ': '
                            . $value->name . ' ' . $property['units']['short_name'];
                    }
                }
            }
        }

        if ($widthVal || $heightVal || $diameterVal){
            echo $this->render(
                'parts/innerElements/sizes' , [
                    'width' => $widthVal,
                    'height' => $heightVal,
                    'diameter' => $diameterVal
                ]
            );
        }

        foreach ($remains as $remain){
            echo $remain;
        }
    }
}
