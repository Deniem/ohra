<?php
/**
 * Created by PhpStorm.
 * User: bbyte
 * Date: 8/17/16
 * Time: 5:24 PM
 */

namespace app\widgets;


use yii\base\Widget;

class BlogPageWidget extends Widget
{
    public $model;
    
    public function init()
    {
        parent::run();
    }

    public function run()
    {
        return $this->render(
            'blog/blogPageList', [
                'model' => $this->model
            ]
        );
    }


}