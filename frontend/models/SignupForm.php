<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\Customer;
use common\models\db\Settings;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required', 'message'=> 'Поле логина не может быть пустым'],
            ['username', 'unique', 'targetClass' => '\frontend\models\Customer', 'message' => 'Логин уже занят'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required', 'message'=> 'Поле пароля не может быть пустым'],
            ['email', 'email', 'message' => 'Неправильный формат почтового адресса'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\frontend\models\Customer', 'message' => 'Почтовый адрес уже используеться'],

            ['password', 'required', 'message'=>'Поле пароля не может быть пустым'],
            ['password', 'string', 'min' => 6],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username' => 'Логин',
            'email' => 'Email',
            'password' => 'Пароль',
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {

        if (!$this->validate()) {
            return null;
        }

        $customer = new Customer();
        $customer->username = $this->username;
        $customer->email = $this->email;
        $customer->setPassword($this->password);
        $customer->generateAuthKey();
		$customer->status = Customer::STATUS_ACTIVE;
        
        $mailer = Yii::$app->mailer->compose('verify/html',['authKey' => $customer->auth_key])
            ->setFrom(Settings::findOne(['name'=>'admin_email'])->value)
            ->setTo($customer->email)
            ->setSubject("Please verify your email at OHRA.com");

        return ($customer->save() && $mailer->send()) ? $customer : null;
    }
}
