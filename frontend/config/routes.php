<?php
return [
    '/' => 'site/index',
    '/signup' => 'site/signup',
    '/verify/<authKey>' => 'site/verify',
    'login' => 'site/login',
	'/cabinet' => '/cart/cabinet',
	'/cabinet/<slug>' => '/cart/cabinet-view',

	'/cart/order' => 'cart/order',
	'/cart/view' => 'cart/cart',
	
    '/catalog/' => '/catalog/list',
    '/catalog/list' => '/catalog/list',
	'/catalog/<slug>' => '/catalog/view',

	'/product/<slug>' => '/product/slug/',

	'/brands' => '/brand/index',
	'/brand/<slug>' => '/brand/slug',
	
	'/blog' => '/blog/index',
	'/blog/<slug>' => '/blog/slug',

	'/api/user/update-password' => '/api/user/update-password',
	'/api/user/update-personal' => '/api/user/update-personal',
	'/api/user/<name>' => '/api/user/check',

	['class' => 'yii\rest\UrlRule', 'pluralize' => false, 'controller' => 'cart'],
	['class' => 'yii\rest\UrlRule', 'pluralize' => false, 'controller' => 'customer-favorite'],
	['class' => 'yii\rest\UrlRule', 'pluralize' => false, 'controller' => 'customer-compare'],

	'/<slug>' => 'site/content-pages',
];
