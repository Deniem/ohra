<?php
use frontend\controllers\CartController;

$this->title = "Список избранного";
$session = Yii::$app->session;
$favList = CartController::getProductListFromSession('favorite')
?>

<main class="b-section__main">
    <section class="b-section__text">
        <div class="container">
            <div class="b-breadcrumb">
                <ul class="b-breadcrumb__list">
                    <li class="b-breadcrumb__item">
                        <a href="<?= \yii\helpers\Url::home()?>" class="b-breadcrumb__link">Главная</a>
                    </li>
                    <li class="b-breadcrumb__item">
                        <a href="<?= \yii\helpers\Url::to(['/cabinet'])?>" class="b-breadcrumb__link">Кабинет</a>
                    </li>
                    <li class="b-breadcrumb__item">
                        <a href="<?= \yii\helpers\Url::to(['/cabinet/favorite'])?>" class="b-breadcrumb__link b-breadcrumb__link--active">Любимое</a>
                    </li>
                </ul>
            </div>
            <h2 class="b-section__title"><?= $this->title ?></h2>
            <div class="row">
                <aside class="col-md-2">
                    <?= $this->render('@frontend/views/layouts/cabinet_aside') ?>
                </aside>
                <div class="col-md-10">
                    <div class="b-section__contentText">
                        <h3>Избранные товары</h3>
                    </div>
                    <?php if (count($favList) > 0): ?>
                    
                    <div class="row">
                        <div class="col-md-12">
                            <section class="b-novelty">
                                <div class="container-novelty" data-view-type="main.Novelty" data-view-removable="true"
                                     data-view-action="favorite">
                                    <?= $favList ? \app\widgets\PartInject::widget(['name' => 'goods-main-page',
                                        'models' => $favList,
                                        'properties' => $properties
                                    ]) : '' ?>
                                </div>
                            </section>
                        </div>
                    </div>

                </div>
                <?php else: ?>
                    <div class="row">
                        <div class="col-md-12">
                            <br>
                            <span class="alert alert-success">
                                <span>Список избранного пуст</span>
                            </span>
                        </div>
                    </div>
                    <br><br>
                <?php endif; ?>
            </div>
        </div>  
    </section>
</main>
