<?php
if (Yii::$app->user->isGuest) {
    Yii::$app->response->redirect('/cart/view');
}

$this->title = "Настройки";
?>

<main class="b-section__main">
    <section class="b-section__text">
        <div class="container">
            <div class="b-breadcrumb">
                <ul class="b-breadcrumb__list">
                    <li class="b-breadcrumb__item">
                        <a href="<?= \yii\helpers\Url::home()?>" class="b-breadcrumb__link">Главная</a>
                    </li>
                    <li class="b-breadcrumb__item">
                        <a href="<?= \yii\helpers\Url::to(['/cabinet'])?>" class="b-breadcrumb__link">Кабинет</a>
                    </li>
                    <li class="b-breadcrumb__item">
                        <a href="<?= \yii\helpers\Url::to(['/cabinet/settings'])?>" class="b-breadcrumb__link b-breadcrumb__link--active"><?= $this->title ?></a>
                    </li>
                </ul>
            </div>
            <h2 class="b-section__title"><?= $this->title ?></h2>
            <div class="row">
                <aside class="col-md-2">
                    <?= $this->render('@frontend/views/layouts/cabinet_aside', [
                        'orders' => $orders
                    ]) ?>
                </aside>
                <div class="col-md-10">
                    <form action="" role="form">
                        <div class="row" id="personal_data" data-view-type="cabinet.settingsPersonal">
                            <div class="col-md-12">
                                <div class="b-section__contentText">
                                    <h3>Личные данные</h3>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-4 text-right">
                                            <label for="personal-first_name" class="form-control-input-label">Имя</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="personal-first_name"
                                                   name="personal-first_name" value="<?= $customer->first_name?>">
                                        </div>
                                    </div>
                                    <br>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-4 text-right">
                                            <label for="personal-last_name" class="form-control-input-label">Фамилия</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="personal-last_name"
                                                   name="personal-last_name" value="<?= $customer->last_name?>">
                                        </div>
                                    </div>
                                    <br>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-4 text-right">
                                            <label for="personal-middle_name" class="form-control-input-label">Отчество</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="text" class="form-control" id="personal-middle_name"
                                                   name="personal-middle_name" value="<?= $customer->middle_name?>">
                                        </div>
                                    </div>
                                    <br>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-4 text-right">
                                        <button type="button" class="btn btn-success personal-upd-btn">Обновить</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <div class="row" id="password_change" data-view-type="cabinet.settingPassword">
                            <div class="col-md-12">
                                <div class="b-section__contentText">
                                    <h3>Смена пароля</h3>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-4 text-right">
                                            <label for="password-old_pass" class="form-control-input-label">Старый
                                                пароль</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="password" class="form-control" id="password-old_pass"
                                                   name="password-old_pass">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-4 text-right">
                                            <label for="password-new_pass" class="form-control-input-label">Новый
                                                пароль</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="password" class="form-control" id="password-new_pass"
                                                   name="password-new_pass">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-4 text-right">
                                            <label for="password-new_pass_confirm" class="form-control-input-label">Повторите
                                                пароль</label>
                                        </div>
                                        <div class="col-md-8">
                                            <input type="password" class="form-control" id="password-new_pass_confirm"
                                                   name="password-new_pass_confirm">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-8 col-md-offset-4 text-right">
                                        <button type="button" class="btn btn-success password-upd-btn">Обновить</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr>

                        <input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>"
                               value="<?=Yii::$app->request->csrfToken?>"/>
                    </form>
                </div>
            </div>
    </section>
</main>



