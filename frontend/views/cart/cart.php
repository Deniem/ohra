<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = "Просмотр корзины";
?>

<main class="b-section__main">
    <section class="b-section__text">
        <div class="container">
            <div class="b-breadcrumb">
                <ul class="b-breadcrumb__list">
                    <li class="b-breadcrumb__item">
                        <a href="<?= \yii\helpers\Url::home()?>" class="b-breadcrumb__link">Главная</a>
                    </li>
                    <li class="b-breadcrumb__item">
                        <a href="<?= \yii\helpers\Url::to(['/cabinet'])?>" class="b-breadcrumb__link">Кабинет</a>
                    </li>
                    <li class="b-breadcrumb__item">
                        <a href="<?= \yii\helpers\Url::to(['/cart/view'])?>" class="b-breadcrumb__link b-breadcrumb__link--active"><?= $this->title?></a>
                    </li>
                </ul>
            </div>
            <h2 class="b-section__title"><?= $this->title?></h2>

            <div class="row">
                <aside class="col-md-2">
                    <?= $this->render('@frontend/views/layouts/cabinet_aside') ?>
                </aside>
                <div class="col-md-10">
                    <div class="row">
                        <div class="col-md-12 b-section__contentText">
                            <h3 class="cancel-reset">Корзина</h3>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">

                            <form action="/cart/order" type="post" id="order-form"
                                  class="form-horizontal form-groups-bordered">

                                <div data-view-type="cartView.cartItem" class="row">
                                    <div class="cart-list-placeholder"></div>
                                    <script type="text/template" id="cart-list-template">
                                        <table class="table table-bordered table-hover text-center font-size-16 responsive">
                                            <thead>
                                            <tr>
                                                <th class="col-md-1 text-center">#</th>
                                                <th class="col-md-2 text-center">Изображение</th>
                                                <th class="col-md-4 text-center">Товар</th>
                                                <th class="col-md-2 text-center">Количество</th>
                                                <th class="col-md-2 text-center">Стоимость</th>
                                                <th class="col-md-1 text-center"></th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <%_.each(items, function(prop, key){ %>
                                            <tr data-id="<%= prop.id %>">
                                                <td><%= key + 1%></td>
                                                <% if(prop.thumbnail_base_url !== null && prop.thumbnail_path !== null) { %>
                                                <td><img src="<%= prop.thumbnail_base_url + '/' +  prop.thumbnail_path %>"
                                                         alt="<%= prop.name %>" class="img-responsive"></td>
                                                <% } else { %>
                                                <td></td>
                                                <% } %>
                                                <td><a href="/product/<%= prop.slug %>"><%= prop.name %></a></td>
                                                <td class="form-group">
                                                    <input type="number" step="1" min="1"
                                                           class="cart-position-quantity form-control"
                                                           value="<%= prop.quantity%>">
                                                </td>
                                                <td><%= prop.discountActive ?  parseFloat(prop.discount.price).toFixed(2)
                                                    : parseFloat(prop.price).toFixed(2) %></td>
                                                <td><a href="#" class="deleteItem">Удалить</a></td>
                                            </tr>
                                            <% }) //endEach %>
                                            </tbody>
                                        </table>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <p id="cart-cost" class="pull-right">Общая стоимость: <%=
                                                    parseFloat(total).toFixed(2) %></p>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <br>
                                    </script>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <a href="/cart/list" class="btn btn-success pull-right">Оформить заказ</a>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</main>
