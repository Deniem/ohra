<?php
$this->title = "Кабинет пользователя";

$cart = \Yii::$app->cart;
$cart_items_count = count($cart->getPositions());
?>

<main class="b-section__main">
    <section class="b-section__text">
        <div class="container">
            <div class="b-breadcrumb">
                <ul class="b-breadcrumb__list">
                    <li class="b-breadcrumb__item">
                        <a href="<?= \yii\helpers\Url::home()?>" class="b-breadcrumb__link">Главная</a>
                    </li>
                    <li class="b-breadcrumb__item">
                        <a href="<?= \yii\helpers\Url::to(['/cabinet'])?>" class="b-breadcrumb__link b-breadcrumb__link--active">Кабинет</a>
                    </li>
                </ul>
            </div>
            <h2 class="b-section__title">Кабинет пользователя</h2>
            <div class="row">
                <aside class="col-md-2">
                    <?= $this->render('@frontend/views/layouts/cabinet_aside', [
                        'orders' => $orders
                    ]) ?>
                </aside>
                <div class="col-md-10">
                    <?php if (Yii::$app->session->getFlash('order-status') !== null): ?>
                        <div class="row">
                            <div class="col md-12">
                                <div class="alert alert-info"><?= Yii::$app->session->getFlash('order-status')[0] ?></div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <?php if (Yii::$app->session->getFlash('error') !== null): ?>
                        <div class="row">
                            <div class="col md-12">
                                <div class="alert alert-info"><?= Yii::$app->session->getFlash('order-status')[0] ?></div>
                            </div>
                        </div>
                    <?php endif; ?>


                    <div class="row">
                       <div class="col-md-12">
                           <div class="row">
                               <div class="col-md-12 b-section__contentText">
                                   <h3 class="cancel-reset">Личные данные</h3>
                               </div>
                           </div>

                           <div class="row">
                               <div class="col-md-6">
                                   <div class="row">
                                       <div class="col-md-12">
                                           <div class="form-group">
                                               <label for="order-status"
                                                      class="col-sm-4 form-control-input-label text-right">Логин</label>
                                               <div class="col-sm-8 form-control-input-label-sm">
                                                   <input class="form-control" type="text"
                                                          value="<?= $customer->username ?>" disabled>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                                   <br>
                                   <div class="row">
                                       <div class="col-md-12">
                                           <div class="form-group">
                                               <label for="order-status"
                                                      class="col-sm-4 text-right form-control-input-label">Email</label>
                                               <div class="col-sm-8 form-control-input-label-sm">
                                                   <input class="form-control" type="text"
                                                          value="<?= $customer->email ?>" disabled>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                                   <br>
                                   <div class="row">
                                       <div class="col-md-12">
                                           <div class="form-group">
                                               <label for="order-status"
                                                      class="col-sm-4 text-right form-control-input-label-sm">Имя</label>
                                               <div class="col-sm-8 form-control-input-label-sm">
                                                   <?= $customer->first_name ?
                                                       $customer->first_name :
                                                       '<span class="label label-info">не указанно</span>' ?>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                                   <br>
                                   <div class="row">
                                       <div class="col-md-12">
                                           <div class="form-group">
                                               <label for="order-status"
                                                      class="col-sm-4 text-right form-control-input-label-sm">Фамилия</label>
                                               <div class="col-sm-8 form-control-input-label-sm">
                                                   <?= $customer->last_name ?
                                                       $customer->last_name :
                                                       '<span class="label label-info">не указанна</span>' ?>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                                   <br>
                                   <div class="row">
                                       <div class="col-md-12">
                                           <div class="form-group">
                                               <label for="order-status"
                                                      class="col-sm-4 text-right form-control-input-label-sm">Отчество</label>
                                               <div class="col-sm-8 form-control-input-label-sm">
                                                   <?= $customer->middle_name ?
                                                       $customer->middle_name :
                                                       '<span class="label label-info">не указанно</span>' ?>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                               <div class="col-md-6">
                                   <?php if ($cart_items_count > 0): ?>
                                       <div class="row">
                                           <div class="col-md-12">
                                               <br>
                                                <span class="alert alert-success">
                                                    <span class="glyphicon glyphicon-shopping-cart"></span>
                                                    <span>У вас есть незавершенные
                                                        <a href="/cart/view">покупки</a></span>
                                                </span>
                                           </div>
                                       </div><br>
                                   <?php endif; ?>
                               </div>
                           </div>
                       </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-8 col-md-offset-4">

                                <a class="btn btn-success btn-sm" href="/cabinet/settings#personal_data">Редактировать
                                    личные данные</a> <br>
                            </div>
                        </div>
                    </div>
                    <hr>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="col-md-8 col-md-offset-4">
                                <a class="btn btn-warning btn-sm" href="/cabinet/settings#password_change">Изменить
                                    пароль</a>
                                <br>
                                <br>
                                <a class="btn btn-danger btn-sm" href="/site/logout">Выход</a> <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</main>
