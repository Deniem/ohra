<?php
use common\models\db\OrderOptions;
use frontend\components\CustomerOrderStatus;
use frontend\controllers\CartController;

if (Yii::$app->user->isGuest) {
    Yii::$app->response->redirect('/cart/view');
}

$this->title = "Мои заказы";

$formatter = \Yii::$app->formatter;
$formatter->locale = 'ru-RU';

$orderOptions = OrderOptions::getAllOptions();
?>

<main class="b-section__main">
    <section class="b-section__text">
        <div class="container">
            <div class="b-breadcrumb">
                <ul class="b-breadcrumb__list">
                    <li class="b-breadcrumb__item">
                        <a href="<?= \yii\helpers\Url::home()?>" class="b-breadcrumb__link">Главная</a>
                    </li>
                    <li class="b-breadcrumb__item">
                        <a href="<?= \yii\helpers\Url::to(['/cabinet'])?>" class="b-breadcrumb__link">Кабинет</a>
                    </li>
                    <li class="b-breadcrumb__item">
                        <a href="<?= \yii\helpers\Url::to(['/cabinet/orders'])?>" class="b-breadcrumb__link b-breadcrumb__link--active"><?= $this->title ?></a>
                    </li>
                </ul>
            </div>
            <h2 class="b-section__title"><?= $this->title ?></h2>
            <div class="row">
                <aside class="col-md-2">
                    <?= $this->render('@frontend/views/layouts/cabinet_aside', [
                        'orders' => $orders
                    ]) ?>
                </aside>
                <div class="col-md-10" data-view-type="cabinet.orders">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="b-section__contentText">
                                <h3>Список заказов</h3> <br>
                            </div>
                           <?php if(count($orders) > 0):?>
                              <div class="row">
                                  <div class="col-md-12">
                                      <ul class="customer-orders">
                                          <li class="row customer-orders-head row">
                                              <div class="col-sm-12">
                                                  <div class="col-sm-1">№</div>
                                                  <div class="col-sm-2">Дата заказа</div>
                                                  <div class="col-sm-7">Товары</div>
                                                  <div class="col-sm-2">Статус</div>
                                              </div>
                                          </li>
                                          <?php foreach ($orders as $order): ?>
                                              <?php
                                              $count = count($order->orderItems);
                                              $orderStatus = CustomerOrderStatus::getOrderStatus($order);
                                              ?>
                                          <li class="row customer-order" data-order-id="<?= $order->id?>">
                                              <div class="col-sm-12 customer-order-container">
                                                  <div class="col-sm-1"><?= $order->id ?></div>
                                                  <div class="col-sm-2"><?= $formatter->asDate($order->updated_at) ?></div>
                                                  <div class="col-sm-7">
                                                      <?= $count . ' '
                                                      . CartController::cartCountName($count)?> на
                                                      <?= number_format((float) $order->calculateTotal(), 2, '.', '') ?> грн
                                                  </div>
                                                  <div class="col-sm-2">
                                                  <span class="label label-<?= $orderStatus['label']?>">
                                                        <?= $orderStatus['status'] ?>
                                                    </span>
                                                  </div>
                                              </div>
                                              <ul class="customer-order-detail col-sm-12">
                                                      <li class="customer-order-head row">
                                                          <div class="col-sm-8 text-right">Товар</div>
                                                          <div class="col-sm-2">Цена</div>
                                                          <div class="col-sm-2">Кол-во, шт</div>
                                                      </li>
                                                  <?php foreach ($order->orderItems as $item): ?>
                                                        <li class="row">
                                                            <div class="col-sm-8 text-right"><?= $item['name'] ?></div>
                                                            <div class="col-sm-2"><?= $item['price'] ?> грн</div>
                                                            <div class="col-sm-2"><?= $item['quantity'] ?></div>
                                                        </li>
                                                  <?php endforeach; ?>
                                              </ul>
                                          </li>
                                          <?php endforeach; ?>
                                      </ul>
                                  </div>
                              </div><br>
                               <div class="row">
                                   <div class="col-md-12">
                                       <h3>Заказов всего: <?=count($orders)?></h3>
                                   </div>
                               </div>
                            <?php else: ?>
                               <br>
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="alert alert-success">
                                        <span>Вы не сделали еще ни одного заказа</span>
                                    </span>
                                </div>
                            </div>

                            <?php endif ?>
                        </div>
                    </div>
                </div>
            </div>
    </section>
</main>
