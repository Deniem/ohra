<?php
use yii\helpers\Url;

$this->title = "Оформить заказ";
?>
<main class="b-section__main">
    <section class="b-section__text">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="b-breadcrumb">
                        <ul class="b-breadcrumb__list">
                            <li class="b-breadcrumb__item">
                                <a href="<?= \yii\helpers\Url::home()?>" class="b-breadcrumb__link">Главная</a>
                            </li>
                            <li class="b-breadcrumb__item">
                                <a href="<?= \yii\helpers\Url::to(['/cart/list'])?>" class="b-breadcrumb__link b-breadcrumb__link--active">Оформить заказ</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-12">
                    <h2 class="b-section__title">Оформить заказ</h2>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-12 b-section__contentText">
                    <form action="/cart/order" type="post" id="order-form"
                          class="form-horizontal form-groups-bordered">

                        <div data-view-type="cartView.cartItem" class="row">
                            <div class="cart-list-placeholder"></div>
                            <script type="text/template" id="cart-list-template">
                                <table class="table table-bordered table-hover text-center font-size-16 responsive">
                                    <thead>
                                    <tr>
                                        <th class="col-md-1 text-center">#</th>
                                        <th class="col-md-2 text-center">Изображение</th>
                                        <th class="col-md-4 text-center">Товар</th>
                                        <th class="col-md-2 text-center">Количество</th>
                                        <th class="col-md-2 text-center">Стоимость</th>
                                        <th class="col-md-1 text-center"></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <%_.each(items, function(prop, key){ %>
                                    <tr data-id="<%= prop.id %>">
                                        <td><%= key + 1%></td>
                                        <% if(prop.thumbnail_base_url !== null && prop.thumbnail_path !== null) { %>
                                        <td><img src="<%= prop.thumbnail_base_url + '/' +  prop.thumbnail_path %>"
                                                 alt="<%= prop.name %>" class="img-responsive"></td>
                                        <% } else { %>
                                        <td></td>
                                        <% } %>
                                        <td><a href="/product/<%= prop.slug %>"><%= prop.name %></a></td>
                                        <td class="form-group">
                                            <input type="number" step="1" min="1"
                                                   class="cart-position-quantity form-control"
                                                   value="<%= prop.quantity%>">
                                        </td>
                                        <td><%= prop.discountActive ?  parseFloat(prop.discount.price).toFixed(2)
                                            : parseFloat(prop.price).toFixed(2) %></td>
                                        <td><a href="#" class="deleteItem">Удалить</a></td>
                                    </tr>
                                    <% }) //endEach %>
                                    </tbody>
                                </table>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p id="cart-cost" class="pull-right">Общая стоимость: <%=
                                            parseFloat(total).toFixed(2)%></p>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </script>
                        </div>

                        <div data-view-type="cartView.user_type" class="row">
                            <div class="cart-confirm-user-type-placeholder col-md-12"></div>
                            <script type="text/template" id="cart-confirm-user-type-template">
                                <?php if (Yii::$app->user->isGuest): ?>

                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <h3>Контактные данные</h3>
                                        </div>
                                    </div>
                                    <br><br>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="text-center col-md-2 col-md-offset-2">
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-default active"
                                                            id="login-screen-btn">
                                                        Я новый покупатель
                                                    </button>
                                                </div>
                                                <div class="form-group">
                                                    <button type="button" class="btn btn-default"
                                                            id="already-register-btn">
                                                        Уже зарегестрирован
                                                    </button>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div class="new-cutomer-fileds collapse in" id="login-screen">
                                                    <div class="form-group">
                                                        <label for="signup-username" class="col-md-4 control-label">Логин</label>
                                                        <div class="col-md-8">
                                                            <input type="text" name="name" id="signup-username"
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="form-group">
                                                        <label for="signup-email" class="col-md-4 control-label">Email</label>
                                                        <div class="col-md-8">
                                                            <input type="email" name="email" id="signup-email"
                                                                   class="form-control"
                                                                   maxlength="255" required>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="form-group">
                                                        <label for="signup-password"
                                                               class="col-md-4 control-label">Пароль</label>
                                                        <div class="col-md-8">
                                                            <input type="password" name="password" id="signup-password" required
                                                                   class="form-control">
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="form-group">
                                                        <div class="col-md-12 text-right">
                                                            <button type="button" id="signup-button" class="btn btn-success">
                                                                Далее
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <br>
                                                </div>
                                                <div class="already-register collapse in" id="already-register"
                                                     style="display: none">
                                                    <div class="form-group">
                                                        <label for="login-name" class="col-md-4 control-label">Логин</label>
                                                        <div class="col-md-8">
                                                            <input type="email" name="name" id="login-name" class="form-control"
                                                                   maxlength="255" required>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="form-group">
                                                        <label for="login-pass" class="col-md-4 control-label">Пароль</label>
                                                        <div class="col-md-8">
                                                            <input type="password" name="password" id="login-pass"
                                                                   class="form-control" required>
                                                        </div>
                                                    </div>
                                                    <br>
                                                    <div class="form-group">
                                                        <div class="col-md-12 text-right">
                                                            <button type="button" id="login-btn" class="btn btn-success">Войти
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <br>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                <?php endif; ?>
                            </script>
                        </div>

                        <?php if (!Yii::$app->user->isGuest): ?>
                            <div data-view-type="cartView.confirm">
                                <div class="cart-confirm-placeholder"></div>
                                <script type="text/template" id="cart-confirm-template">

                                    <div class="row">
                                        <div class="col-md-12 text-center">
                                            <h3>Доставка и оплата</h3>
                                        </div>
                                    </div>

                                    <br>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="order-delivery" class="col-md-4 control-label">Доставка</label>
                                                <div class="col-md-8">
                                                    <select class="form-control" name="delivery" id="order-delivery"
                                                            required>
                                                        <option value="shipping_service">Служба доставки</option>
                                                        <option value="courier">Курьер</option>
                                                        <option value="self-pickup">Самовывоз</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="form-group">
                                                <label for="order-payment" class="col-md-4 control-label">Способ
                                                    оплаты</label>
                                                <div class="col-md-8">
                                                    <select class="form-control" name="payment_method" id="order-payment"
                                                            required>
                                                        <option value="cash_shipping_service">Оплата через службу доставки
                                                        </option>
                                                        <option value="cash_in office">На месте</option>
                                                        <option value="c.o.d.">Наложенный платеж</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="form-group">
                                                <label for="order-city" class="col-md-4 control-label">Город</label>
                                                <div class="col-md-8">
                                                    <input type="text" name="city" id="order-city" class="form-control">
                                                </div>
                                            </div>
                                            <br>
                                            <div class="form-group">
                                                <label for="order-address" class="col-md-4 control-label">Адресс</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" type="text" name="address"
                                                           id="order-address">
                                                </div>
                                            </div>
                                            <br>
                                            <div class="form-group">
                                                <label for="order-email" class="col-md-4 control-label">Email
                                                    получателя</label>
                                                <div class="col-md-8">
                                                    <input type="email" name="email" id="order-email" class="form-control"
                                                           value="<?= Yii::$app->user->identity->email ?>">
                                                </div>
                                            </div>
                                            <br>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="order-name" class="col-md-4 control-label">Имя
                                                    получателя</label>
                                                <div class="col-md-8">
                                                    <input type="text" name="name" id="order-name" class="form-control"
                                                           value="<%= this.getFullUserName() %>">
                                                </div>
                                            </div>
                                            <br>
                                            <div class="form-group">
                                                <label for="order-phone" class="col-md-4 control-label">Телефон</label>
                                                <div class="col-md-8">
                                                    <input class="form-control" type="text" name="phone" id="order-phone"
                                                           required maxlength="15">
                                                </div>
                                            </div>

                                            <br>
                                            <div class="form-group">
                                                <label for="order-notes" class="col-md-4 control-label">Заметки</label>
                                                <div class="col-md-8">
											<textarea class="form-control" name="notes" id="order-notes"
                                                      rows="7"></textarea>
                                                </div>
                                            </div>
                                            <br>

                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <button id="submit-order-btn" class="btn btn-success pull-right"
                                                    formmethod="post" type="submit">Оформить заказ
                                            </button>
                                            <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>"
                                                   value="<?= Yii::$app->request->csrfToken; ?>"/>
                                        </div>
                                    </div>
                                    <br>
                                </script>
                            </div>
                        <?php endif; ?>
                    </form>
                </div>    
            </div>
        </div>
    </section>
</main>
