<?php
use common\components\widgets\Pager;
use yii\helpers\Url;

$this->title = 'Бренды'
?>


<main class="b-section__main">
	<section class="b-section__text">
        <div class="container">
            <div class="b-breadcrumb">
                <ul class="b-breadcrumb__list">
                    <li class="b-breadcrumb__item">
                        <a href="/" class="b-breadcrumb__link">Главная</a>
                    </li>
                    <li class="b-breadcrumb__item">
                        <a href="/brands" class="b-breadcrumb__link b-breadcrumb__link--active">Бренды</a>
                    </li>
                </ul>
            </div>

            <h2 class="b-section__title"><?=$content->h1;?></h2>
			<div class="b-section__contentText">
				<?=$content->content;?>
			</div>
           <div class="row">
              <?php foreach($model as $brand): ?>
                  <div class="col-md-4 b-section__contentText">
                      <div class="text-center brand-list-item">
                          <?php if ($brand->previewSrc != ''): ?>
                              <a href="<?= Url::to(['brand/' . $brand->slug]) ?>">
                                  <img src="<?= $brand->previewSrc ?>" alt="">
                              </a>
                          <?php endif ?>
                          <br>
                          <br>
                          <a href="/brand/<?= $brand->slug?>">
                              <?= Yii::$app->helpers->formattingProperty($brand, 'name')?>
                          </a>
                      </div>
                  </div>
               <?php endforeach; ?>
           </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="text-right">
                        <?= Pager::widget([
                            'pages' => $pages,
                            'viewTemplate' => 'pager/frontend'
                        ]); ?>
                    </div>
                </div>
        </div>
    </section>
</main>
