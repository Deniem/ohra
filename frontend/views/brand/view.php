<?php

use yii\helpers\Html;

$this->title = $model->name;
?>

<main class="b-section__main">
    <section class="b-section__text">
        <div class="container">
            <ul class="b-breadcrumb__list">
                <li class="b-breadcrumb__item">
                    <a href="/" class="b-breadcrumb__link">Главная</a>
                </li>
                <li class="b-breadcrumb__item">
                    <a href="/brands" class="b-breadcrumb__link">Бренды</a>
                </li>
                <li class="b-breadcrumb__item">
                    <a href="/brand/<?=$model->slug?>" class="b-breadcrumb__link b-breadcrumb__link--active"><?=$model->name?></a>
                </li>
            </ul>

            <h2 class="b-section__title"><?= Html::encode($model->h1) ?></h2>

            <br>
			<div class="b-section__contentText">
				<div class="row">
					<div class="col-md-12">
						<?php if ($model->thumbnail_base_url !== null && $model->thumbnail_path != null): ?>
							<img src="<?php $model->preview ?>" class="img-responsive" alt="<?php $model->name ?>">
						<?php endif; ?>
						<?= $model->content ?>
					</div>
				</div>
            </div>

            <br>
            <div class="row" style="background-color: #fff1f0">
                <div class="col-md-12">
                    Top 5
                </div>
            </div>

            <br>

        </div>
    </section>
</main>

