<?php
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = "Каталог - " . $model->name;
?>


<main class="b-section__main">
    <section class="b-section__text">
        <div class="container">
            <div class="row">
                <div class="col-sm-4 col-md-3">
                    <?= app\widgets\CatalogListWidget::widget(['name' => 'aside_categories_list']) ?>
                    <?= \app\widgets\BannerWidget::widget(['name' => 'col3_divan_banner'])?>
                    <?= \app\widgets\BannerWidget::widget(['name' => 'col3_subscribe_banner'])?>
                </div>

                <div class="col-sm-8 col-md-9">
                    <div data-view-type="product.Item" class="col-md-12">
                        <div class="product-item-placeholder"></div>
                        <script type="text/template" id="product-item-template">
                            <h2 class="b-section__title"><?= Html::encode($model->name) ?></h2>
                            <a data-product-id="<?= $model->id ?>" class="add-to-cart">Купить</a>
                            <br>
                            <a data-product-id="<?= $model->id ?>" class="add-to-favorite">В любимые</a>
                            <br>
                            <a data-product-id="<?= $model->id ?>" class="add-to-compare">В сравнение</a>
                        </script>
                    </div>
                </div>
            </div>

        </div>
    </section>
</main>