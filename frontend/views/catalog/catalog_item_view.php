<?php

use common\components\widgets\Pager;

$this->title = $category->name;
?>

<main class="container b-section__main">
    <div class="row">
        <div class="col-md-12">
            <?= \app\widgets\CatalogBreadCrumbsWidget::widget(['category' => $category])?>
        </div>
    </div>
    <?= \app\widgets\BannerWidget::widget(['name' => 'col12_banner.php'])?>
    <div class="row">
            <aside id="filters" class="col-md-3">
                <?= app\widgets\CatalogListWidget::widget(['name' => 'aside_categories_list']) ?>
                <?= \app\widgets\BannerWidget::widget(['name' => 'col3_divan_banner']) ?>
                <?= \app\widgets\BannerWidget::widget(['name' => 'col3_subscribe_banner']) ?>
            </aside>

            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-12">
                        <?= app\widgets\ProductFilterAsideWidget::widget([
                            'name' => 'product_filters_nav',
                            'filters' => $filters
                        ]) ?>
                    </div>
                </div>

                <?php if ($errors !== false): ?>
                    <div class="alert alert-danger col-md-12"><?= $errors['message'] ?></div>
                <?php else: ?>

                <div class="row">
                    <div class="col-md-12">
                        <section class="container-novelty b-category" data-view-type="main.Novelty">
                            <?= $models ? \app\widgets\PartInject::widget([
                                'name' => 'goods-main-page',
                                'models' => $models->getModels(),
                                'properties' => $properties
                            ]) : '' ?>
                        </section>
                    </div>
                </div>

                <?= Pager::widget([
                    'pages' => $models->pagination,
                    'viewTemplate' => 'pager/frontend'
                ]); ?>
            </div>
            <?php endif; ?>


    </div>

    <?php if(count($products) > 0): ?>
        <div class="row">
            <div class="col-md-12">
                <?= \app\widgets\PartInject::widget([
                    'title' => 'топ покупки',
                    'name' => 'novelty-main-page',
                    'models' => $products,
                    'properties' => $productProperties
                ]) ?>
            </div>
        </div>

    <?php endif; ?>
</main>