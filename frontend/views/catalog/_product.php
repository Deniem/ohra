<?php
use yii\helpers\Html;
?>
<?php /** @var $model \common\models\Product */ ?>
<h3 class="product_list_title"></h3>
<div class="product_list_block">
	<div class="product_the_number_of"></div>
	<?= Html::a($model->name, ['catalog/view', 'id' => $model->id], ['class' => 'product_title']) ?>
</div>
