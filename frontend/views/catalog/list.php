<?php
use common\components\widgets\Pager;

$this->title = "Каталог"
?>


<main class="b-section__main">
    <section class="b-section__page-category">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?= \app\widgets\CatalogBreadCrumbsWidget::widget(['category' => null]) ?>
                </div>
            </div>
            
            <?= \app\widgets\BannerWidget::widget(['name' => 'col12_banner.php'])?>

            <div class="row">
                <div class="col-sm-4 col-md-3">
                    <?= app\widgets\CatalogListWidget::widget(['name' => 'aside_categories_list']) ?>
                    <?= \app\widgets\BannerWidget::widget(['name' => 'col3_divan_banner'])?>
                    <?= \app\widgets\BannerWidget::widget(['name' => 'col3_subscribe_banner'])?>
                </div>

                <div class="col-sm-8 col-md-9">
                    <?= \app\widgets\SubCategoriesWidget::widget(['categories' => $categories]) ?>

                    <div class="row">
                        <div class="col-md-12">
                            <?= \app\widgets\GlobalCategoriesWidget::widget()?>
                        </div>
                    </div>

                    <?= Pager::widget([
                        'pages' => $pages,
                        'viewTemplate' => 'pager/frontend'
                    ]); ?>

                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <?= \app\widgets\PartInject::widget([
                        'title' => 'топ покупки',
                        'name' => 'novelty-main-page',
                        'models' => $products,
                        'properties' => $properties
                    ]) ?>
                </div>
            </div>

            <section class="b-section__text">
                <div class="container">
                    <div class="b-section__contentText">
                        <?= $catalogContent->content?>
                    </div>
                </div>
            </section>
    </section>
</main>