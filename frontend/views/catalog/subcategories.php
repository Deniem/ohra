<?php
$this->title = $category->name;
?>

<main class="b-section__main">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?= \app\widgets\CatalogBreadCrumbsWidget::widget(['category' => $category]) ?>
            </div>
        </div>
    </div>
    <section class="b-section__page-category">
        <div class="container">

           <div class="row">
               <div class="col-md-12">
                   <?= \app\widgets\BannerWidget::widget(['name' => 'col12_banner.php'])?>
               </div>
           </div>

            <div class="row">
                <div class="col-sm-4 col-md-3">
                <?= app\widgets\CatalogListWidget::widget(['name' => 'aside_categories_list']) ?>
                <?= \app\widgets\BannerWidget::widget(['name' => 'col3_divan_banner'])?>
                <?= \app\widgets\BannerWidget::widget(['name' => 'col3_subscribe_banner'])?>
                </div>
                <div class="col-sm-8 col-md-9">

                <?= \app\widgets\SubCategoriesWidget::widget(['categories' => $subcategories])?>
                    <div class="row">
                        <div class="col-md-12">
                            <?= \app\widgets\GlobalCategoriesWidget::widget()?>
                        </div>
                    </div>
            </div>
        </div>

            <?php if(count($products) > 0): ?>
                <div class="row">
                    <div class="col-md-12">
                        <?= \app\widgets\PartInject::widget([
                            'title' => 'топ покупки',
                            'name' => 'novelty-main-page',
                            'models' => $products,
                            'properties' => $productProperties
                        ]) ?>
                    </div>
                </div>
            <?php endif; ?>
    </section>
</main>

