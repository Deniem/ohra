<?php

/* @var $this \yii\web\View */
/* @var $content string */

use common\components\Settings;
use frontend\assets\AppAsset;
use frontend\assets\ResetAsset;
use yii\helpers\Html;
use yii\helpers\Url;

ResetAsset::register($this);
AppAsset::register($this);
$settings = Settings::getInstance();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

    <?php if (isset (\Yii::$app->controller->pageData)): ?>
        <script type="application/javascript">
            var app = {
                config: {
                    baseUrl: <?= \yii\helpers\Json::encode(\Yii::$app->homeUrl, 0) ?>
                },
                data: <?= \yii\helpers\Json::encode(\Yii::$app->controller->pageData, 0)  ?>
            };
        </script>
    <?php endif; ?>

</head>
<body>
<?php $this->beginBody() ?>

<div class="b-section">

    <header class="b-header">
        <section class="b-header__top">
            <div class="container">
                <div class="row">
                    <div class="col-sm-10 col-md-9">
                        <div class="b-header__feedback">
                            <a href="mailto:<?= $settings->getByName('admin_email') ?>"
                               class="b-header__feedback-link b-header__feedback-link--type1"><?= $settings->getByName('admin_email') ?></a>
                        </div>
                        <div class="b-header__feedback">
                            <a href="tel:<?= $settings->getByName('header_phone') ?>"
                               class="b-header__feedback-link b-header__feedback-link--type2"><?= $settings->getByName('header_phone') ?>
                                — звонки бесплатные</a>
                        </div>
                        <div class="b-header__feedback">
                            <a href="#" class="b-header__feedback-link b-header__feedback-link--type3">Заказать
                                звонок</a>
                        </div>
                    </div>
                    <div class="col-sm-2 col-md-3">
                        <div class="b-header__cabinet">
                            <?php if (Yii::$app->user->isGuest): ?>
                                <div class="b-header__cabinetBlock">
                                    <a class="b-header__cabinet-link" href="<?= Url::to("/login") ?>"> Войти</a>
                                </div>
                                <a href="<?= Url::to('/signup') ?>"
                                   class="b-header__cabinet-link b-header__cabinet-link--register">
                                    Регистрация
                                </a>
                            <?php else: ?>
                                <div class="b-header__cabinetBlock">
                                    <a class="b-header__cabinet-link" href="<?= Url::to('/cabinet') ?>">
                                        <?= Yii::$app->user->identity->username ?>
                                    </a>
                                    <div class="b-header__cabinetOption">
                                        <ul class="b-header__cabinetOption-list">
                                            <li class="b-header__cabinetOption-item">
                                                <a href="<?= Url::to('/cabinet') ?>"
                                                   class="b-header__cabinetOption-link">
                                                    Кабинет
                                                </a>
                                            </li>
                                            <li class="b-header__cabinetOption-item">
                                                <a href="<?= Url::to('/cabinet/orders') ?>"
                                                   class="b-header__cabinetOption-link">
                                                    Мои заказы
                                                </a>
                                            </li>
                                            <li class="b-header__cabinetOption-item">
                                                <a href="<?= Url::to('/cabinet/favorite') ?>"
                                                   class="b-header__cabinetOption-link">
                                                    Избранное
                                                </a>
                                            </li>
                                            <li class="b-header__cabinetOption-item">
                                                <a href="<?= Url::to('/cabinet/compare') ?>"
                                                   class="b-header__cabinetOption-link">
                                                    Сравнить
                                                </a>
                                            </li>
                                            <li class="b-header__cabinetOption-item">
                                                <a href="<?= Url::to('/site/logout') ?>"
                                                   class="b-header__cabinetOption-link">
                                                    Выход
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section class="b-header__bottom">
            <div class="container">
                <div class="row">
                    <div class="col-sm-2 col-md-2">
                        <div class="b-header__logo">
                            <a href="<?= Url::home() ?>" class="b-header__logo-link">
                                <img src="/img/logo_header.png" alt="">
                            </a>
                        </div>
                    </div>
                    <div class="col-sm-10 col-md-10">
                        <div class="row b-header__topmenu">
                            <div class="col-sm-8 col-md-9 col-lg-10">
                                <ul class="b-header__topmenu-list">
                                    <li class="b-header__topmenu-item">
                                        <a href="<?= Url::to(['/catalog']) ?>"
                                           class="b-header__topmenu-link">Каталог</a>
                                    </li>
                                    <li class="b-header__topmenu-item">
                                        <a href="<?= Url::to(['/about']) ?>" class="b-header__topmenu-link">О нас</a>
                                    </li>
                                    <li class="b-header__topmenu-item">
                                        <a href="<?= Url::to(['/brands']) ?>" class="b-header__topmenu-link">Бренды</a>
                                    </li>
                                    <li class="b-header__topmenu-item">
                                        <a href="<?= Url::to(['/sales']) ?>" class="b-header__topmenu-link">Акции</a>
                                    </li>
                                    <li class="b-header__topmenu-item">
                                        <a href="<?= Url::to(['/blog']) ?>" class="b-header__topmenu-link">Блог</a>
                                    </li>
                                    <li class="b-header__topmenu-item">
                                        <a href="<?= Url::to(['/payment_delivery']) ?>" class="b-header__topmenu-link">Доставка
                                            и оплата</a>
                                    </li>
                                    <li class="b-header__topmenu-item">
                                        <a href="<?= Url::to(['/guarantees']) ?>" class="b-header__topmenu-link">Гарантии</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-sm-4 col-md-3 col-lg-2 b-header__main-option-border" id="cart">
                                <?= $this->render('cart') ?>
                            </div>
                        </div>

                        <button class="top__button top__button--anima">
                            <span></span>
                        </button>

                        <div class="b-header__bottommenu">
                            <?= \app\widgets\CatalogListWidget::widget(['name' => 'menu_categories_list']) ?>
                            <form action="" class="b-header__search">
                                <a class="b-header__search-show"></a>
                                <div class="b-header__search_block">
                                    <input type="search" class="b-header__search-input" placeholder="Поиск">
                                    <button type="submit" class="b-header__search-button"></button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </header>
    <?= $content; ?>
    <footer class="b-footer">
        <div class="b-footer__top">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="b-footer__title">Меню</div>
                        <ul class="b-footer__list">
                            <li class="b-footer__item">
                                <a href="<?= Url::to(['/how-to']) ?>" class="b-footer__link">Как оформить заказ</a>
                            </li>
                            <li class="b-footer__item">
                                <a href="<?= Url::to(['/advices']) ?>" class="b-footer__link">Советы по выбору</a>
                            </li>
                            <li class="b-footer__item">
                                <a href="<?= Url::to(['/payment_delivery']) ?>" class="b-footer__link">Доставка и
                                    оплата</a>
                            </li>
                            <li class="b-footer__item">
                                <a href="<?= Url::to(['/guarantees']) ?>" class="b-footer__link">Гарантии и возврат</a>
                            </li>
                            <li class="b-footer__item">
                                <a href="<?= Url::to(['/loyalty-program']) ?>" class="b-footer__link">Программа
                                    лояльности</a>
                            </li>
                            <li class="b-footer__item">
                                <a href="<?= Url::to(['/contacts']) ?>" class="b-footer__link">Контакты</a>
                            </li>
                            <li class="b-footer__item">
                                <a href="<?= Url::to(['/partnership']) ?>" class="b-footer__link">Сотрудничество</a>
                            </li>
                        </ul>
                    </div>

                    <div class="col-md-3">
                        <div class="b-footer__title">о нас</div>
                        <a href="<?= Url::to(['/']); ?>" class="b-footer__logo">
                            <img src="/img/logo_footer.png" alt="">
                        </a>
                        <p class="b-footer__text">
                            <?= $settings->getByName('about_text'); ?>
                        </p>
                        <ul class="b-footer__social-list">
                            <li class="b-footer__social-item">
                                <a href="<?= $settings->getByName('socials_vk'); ?>"
                                   class="b-footer__social-link b-footer__social-link--vk"></a>
                            </li>
                            <li class="b-footer__social-item">
                                <a href="<?= $settings->getByName('socials_fb'); ?>"
                                   class="b-footer__social-link b-footer__social-link--f"></a>
                            </li>
                            <li class="b-footer__social-item">
                                <a href="<?= $settings->getByName('socials_gplus'); ?>"
                                   class="b-footer__social-link b-footer__social-link--g"></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <div class="b-footer__title">новости</div>
                        <a href="<?= $settings->getByName('callback_link'); ?>" class="b-footer__partner">
                            <img src="/img/partners_1.png" alt="" class="b-footer__partner-img">
                            <span class="b-footer__partner-text">Читайти отзывы</span>
                        </a>
                        <a href="<?= Url::to(['/partners']); ?>" class="b-footer__partner">
                            <img src="/img/partners_2.png" alt="" class="b-footer__partner-img">
                            <span class="b-footer__partner-text">Наши партнёры</span>
                        </a>
                        <p class="b-footer__text">Оплачивайте покупки наличными при получении, либо выбирайте удобный
                            для вас способ оплаты.</p>
                        <ul class="b-footer__payment-list">
                            <li class="b-footer__payment-item">
                                <a href="#" class="b-footer__payment-link b-footer__payment-link--type1"></a>
                            </li>
                            <li class="b-footer__payment-item">
                                <a href="#" class="b-footer__payment-link b-footer__payment-link--type2"></a>
                            </li>
                            <li class="b-footer__payment-item">
                                <a href="#" class="b-footer__payment-link b-footer__payment-link--type3"></a>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <div class="b-footer__title">Контакты</div>
                        <ul class="b-footer__contact-list">
                            <li class="b-footer__contact-item b-footer__contact-item--type1">
                                <div class="b-footer__contact-content">
                                    <span
                                        class="b-footer__contact-text"><?= $settings->getByName('contact_address') ?></span>
                                </div>
                            </li>
                            <li class="b-footer__contact-item b-footer__contact-item--type2">
                                <div class="b-footer__contact-content">
                                    <a href="tel:<?= $settings->getByName('contact_phone_1') ?>"
                                       class="b-footer__contact-link"><?= $settings->getByName('contact_phone_1') ?></a>
                                    <a href="tel:<?= $settings->getByName('contact_phone_2') ?>"
                                       class="b-footer__contact-link"><?= $settings->getByName('contact_phone_2') ?></a>
                                </div>
                            </li>
                            <li class="b-footer__contact-item b-footer__contact-item--type3">
                                <div class="b-footer__contact-content">
                                    <a href="mailto:<?= $settings->getByName('admin_email') ?>"
                                       class="b-footer__contact-link"><?= $settings->getByName('admin_email') ?></a>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="b-footer__bottom">
            <div class="b-footer__trademark"><?= $settings->getByName('copyright_text') ?></div>
        </div>
    </footer>

    <div class="b-section__anchor" data-path="">
        <a href="#" class="b-section__anchor-link"></a>
    </div>

</div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
