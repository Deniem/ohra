<?php
use frontend\controllers\CartController;
use yii\helpers\Url;

$cart = \Yii::$app->cart;
$cart_items_count = count($cart->getPositions());
?>
<div class="b-header__main-option">
    <!--if click this elements on the page,
    add class .b-header__option--active-->
    <div data-view-type="cartNav.Main">
        <a href="/cabinet/compare" class="b-header__option b-header__option--share"
           data-number=""></a>
        <a href="/cabinet/favorite" class="b-header__option b-header__option--like"
           data-number=""></a>
        <div class="b-header__option b-header__option--basket"
             data-number="<?= $cart_items_count ?>"></div>
    </div>

    <!--if product in basket, add class .m-basket__product-yes -->
    <div class="b-basket b-basket--product-yes">

        <?php if ($cart_items_count == 0): ?>
            <div>В вашей корзине нет товаров</div>
            <div class="b-basket__not-product">В вашей корзине нет товаров</div>
        <?php else: ?>
            <div class="b-basket__product">
                <div class="b-basket__product-number">В вашей корзине
                    <span><?= $cart_items_count ?></span> <?= CartController::cartCountName($cart_items_count) ?>
                </div>
                <div class="b-basket__listWrap">
                    <ul class="b-basket__list">
                        <?php foreach ($cart->getPositions() as $product): ?>
                            <li class="b-basket__item" data-product-id="<?= $product->id ?>">
                                <?php if ($product->thumbnail_base_url !== null
                                    && $product->thumbnail_path !== null): ?>
                                    <div class="b-basket__img">
                                        <?= $product->preview ?>
                                    </div>
                                <?php else: ?>
                                    <div class="b-basket__img">
                                        <img src="/img/product/product_1.jpg"
                                             alt="<?= Yii::$app->helpers->formattingProperty($product, 'name') ?>">
                                    </div>
                                <?php endif; ?>
                                <div class="b-basket__describe">
                                    <a href="/product/<?= $product->slug?>"
                                       class="b-basket__title"><?= Yii::$app->helpers->formattingProperty($product, 'name') ?></a>
                                    <div
                                        class="b-basket__detal"><?= Yii::$app->helpers->formattingProperty($product, 'description') ?></div>
                                    <div class="b-basket__item-price">
                                    <span>
                                        <?= number_format((float)Yii::$app->helpers->formattingProperty($product, 'price'), 2, '.', '');?>
                                    </span>
                                        грн
                                    </div>
                                </div>
                                <span class="b-basket__close"></span>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
                <div class="b-basket__total-price">Итого: <span><?= number_format((float)$cart->getCost(), 2, '.', '');?></span> грн</div>
                <div class="b-basket__option clearFix">
                    <a href="<?= Url::to('/cart/view') ?>" class="b-basket__button1">В корзину</a>
                    <a href="<?= Url::to('/cart/list') ?>" class="b-basket__button2">Оформить</a>
                </div>
            </div>
        <?php endif; ?>
    </div>
</div>
