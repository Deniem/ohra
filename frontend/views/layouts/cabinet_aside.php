<?php
$session = Yii::$app->session;
?>

<ul class="list-group">
    <li class="list-group-item disabled cursor-ussual">Кабинет</li>
    <li class="list-group-item">
        <a href="/cart/view"
           class="icon-cart icon-sprite aside-orders aside-cart">
            Корзина
        </a>
        <?php if(count(Yii::$app->cart->getPositions()) > 0):?>
        <span class="badge brand-label"><?= count(Yii::$app->cart->getPositions()) ?></span>
        <?php endif; ?>

    </li>
    <li class="list-group-item">
        <a href="/cabinet/favorite" class="icon-sprite icon-love aside-fav">&nbsp;Избранное</a>
        <?php if ($session->has('favorite')): ?>
            <?php if (count($session->get('favorite')) > 0): ?>
                <span class="badge brand-label"><?= count($session->get('favorite')) ?></span>
            <?php endif; ?>
        <?php endif; ?>

    </li>
    <li class="list-group-item">
        <a href="/cabinet/compare" class="icon-sprite icon-compare aside-compare">В сравнении</a>
        <?php if ($session->has('compare')): ?>
            <?php if (count($session->get('compare')) > 0): ?>
                <span class="badge brand-label"><?= count($session->get('compare')) ?></span>
            <?php endif; ?>
        <?php endif; ?>
    </li>
    <li class="list-group-item">
        <a href="<?=(Yii::$app->user->isGuest)? '/cart/view': '/cabinet/orders'?>" class="icon-cart icon-sprite aside-orders">&nbsp;Мои заказы</a>
    </li>
    <li class="list-group-item">
        <i class="glyphicon glyphicon-cog gray-color"></i>
        <a href="<?=(Yii::$app->user->isGuest)? '/cart/view': '/cabinet/settings'?>">Настройки</a>
    </li>
</ul>