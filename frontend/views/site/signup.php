<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Регистрация нового пользователя';
$this->params['breadcrumbs'][] = $this->title;
?>

<main class="b-section__main">
    <section class="b-section__text">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <br>
                    <h2 class="b-section__title text-center"><?= Html::encode($this->title) ?></h2>
                </div>
                <div class="col-lg-3"></div>
            </div>


            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                Заполните форму регистрации
                            </h3>
                        </div>
                        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                        <div class="panel-body panel-info">
                            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                            <?= $form->field($model, 'email') ?>

                            <?= $form->field($model, 'password')->passwordInput() ?>

                        </div>
                        <div class="panel-footer panel-info">
                            <div class="form-group">
                                <?= Html::submitButton('Зарегестрироваться', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
