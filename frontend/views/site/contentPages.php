<?php
use yii\bootstrap\Html;
use yii\helpers\Url;
$this->title = $model->title;

if ($model->description != "") $this->registerMetaTag(['name' => 'description', 'content' => $model->description]);
if ($model->keywords != "") $this->registerMetaTag(['name' => 'keywords', 'content' => $model->keywords]);
?>

<main class="b-section__main">
    <section class="b-section__text">
        <div class="container">
            <div class="b-breadcrumb">
                <ul class="b-breadcrumb__list">
                    <li class="b-breadcrumb__item">
                        <a href="/" class="b-breadcrumb__link">Главная</a>
                    </li>
                    <?php for($x = count($parents)-1; $x >= 0; $x--){?>
                        <li class="b-breadcrumb__item">
                            <a href="/<?= $parents[$x]->action ?>" class="b-breadcrumb__link"><?= $parents[$x]->h1 ?></a>
                        </li>
                    <?php } ?>

                    <li class="b-breadcrumb__item">
                        <a href="/<?= $model->action ?>" class="b-breadcrumb__link b-breadcrumb__link--active">
                            <?= $model->h1 ?>
                        </a>
                    </li>
                </ul>
            </div>
            <h2 class="b-section__title"><?= Html::encode($model->h1) ?></h2>

            <div class="b-section__contentText">
				<?= $model->content ?>
				<?php if ( count($childs) > 0):?>
					<h3>Страницы</h3>
					<ul>
						<?php foreach ($childs as $child):?>
							<li>
								<a href="<?=Url::to(['/'.$child->action])?>">
									<?=$child->h1?>
								</a>
							</li>
						<?php endforeach;?>
					</ul>
				<?php endif;?>
            </div>

        </div>
    </section>
</main>
