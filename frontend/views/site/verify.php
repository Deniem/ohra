<?php

/* @var $this yii\web\View */

$this->title = 'After verification!';
?>
<div class="site-index">
    <div class="jumbotron">
        <h1>Congratulations!</h1>
        <p class="lead">You have successfully created your account.</p>
    </div>
    <div class="body-content">
        <div class="row">
            <div class="col-md-12">
                <h4>Please check your mailbox for verification email with insructions to follow</h4>
            </div>
        </div>

    </div>
</div>
