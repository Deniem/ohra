<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Авторизация';
$this->params['breadcrumbs'][] = $this->title;
?>
<main class="b-section__main">
    <section class="b-section__text">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <br>
                    <h2 class="b-section__title text-center"><?= Html::encode($this->title) ?></h2>
                </div>
                <div class="col-lg-3"></div>
            </div>
            <br/>


            <div class="row">
                <div class="col-lg-6 col-lg-offset-3">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                Заполните поля ниже, чтобы войти
                            </h3>
                        </div>
                        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                        <div class="panel-body panel-info">
                            <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
                            <?= $form->field($model, 'password')->passwordInput() ?>
                            <?= $form->field($model, 'rememberMe')->checkbox() ?>
                            <div style="color:#999;margin:1em 0">
                                Если вы забили пароль, вы
                                можете <?= Html::a('Восстановить пароль', ['site/request-password-reset']) ?>.
                            </div>
                        </div>
                        <div class="panel-footer panel-info">
                            <div class="form-group">
                                <?= Html::submitButton('Войти', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                            </div>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>


        </div>
    </section>
</main>


