<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<main class="b-section__main">
    <section class="b-section__text">
        <div class="container">

            <div class="b-section__contentText">
                <h1>
                    <?= Html::encode($this->title) ?>
                </h1>
                <div class="site-error">
                    <div class="alert alert-danger">
                        <?= nl2br(Html::encode($message)) ?>
                    </div>

                    <p>
                        Произошла ошибка во время выполнения вашего запроса.
                    </p>
                    <p>
                        Пожалуйста, <a href="/contacts">свяжитесь</a> с нами, если ошибка возникла снова.
                    </p>

                </div>

            </div>

        </div>
    </section>
</main>
