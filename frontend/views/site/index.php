<?php

/* @var $this yii\web\View */

$this->title = 'OHRA';
?>
<main class="b-section__main">

    <!--slider-->
        <?=\app\widgets\PartInject::widget(['name'=>'slider-main-page'])?>
    <!--slider-->

    <!--novelty-->
        <?= $new ? \app\widgets\PartInject::widget([
            'name'=>'novelty-main-page',
            'models' => $new,
            'properties' => $properties,
            'title' => 'новинки'
        ]) : ''?>>
    <!--novelty-->

    <!--categories-->
    <?=\app\widgets\PartInject::widget(['name'=>'categories-main-page'])?>
    <!--categories-->

    <!--goods of the month + sales-->
	<?= $sale ? \app\widgets\PartInject::widget([
        'name'=>'goods-sales-main-page',
        'models' => $sale,
        'properties' => $properties
    ]): ''?>
    <!--goods of the month + sales-->

    <!--brands-->
    <?=\app\widgets\PartInject::widget([
        'name'=>'brands-main-page',
        'models' => $topBrands
    ])?>
    <!--brands-->
    <!--share delivery support-->
    <?=\app\widgets\PartInject::widget(['name'=>'share-main-page'])?>
    <!--share delivery support-->

</main>
