<?php
use yii\helpers\Html;

$this->title = $model->title;

if ($model->description != "") $this->registerMetaTag(['name' => 'description', 'content' => $model->description]);
if ($model->keywords != "") $this->registerMetaTag(['name' => 'keywords', 'content' => $model->keywords]);
?>

<main class="b-section__main">
    <section class="b-section__text">
        <div class="container">
            <h2 class="b-section__title"><?= Html::encode($model->h1) ?></h2>
            <?php if ($model->published_at) : ?>
                <div>
                    <span class="glyphicon glyphicon-time"></span>&nbsp;
                    <?= Yii::$app->formatter->asDate($model->published_at); ?>
                </div>
            <?php endif; ?>
            <br>

            <div class="b-section__contentText">
                <div>
                    <?= $model->description ?>
                </div>
                <br>

                <div class="blog-content">
                    <img src="<?= $model->getPreviewSrc() ?>" class="img-responsive blog-content-image"
                         alt="<?= $model->title ?>">
                    <div class="blog-content-text">
                        <?= $model->content ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
