<?php
use common\components\widgets\Pager;
use yii\helpers\Html;

$this->title = $blogPage->title;

if ($blogPage->description != "") $this->registerMetaTag(['name' => 'description', 'content' => $blogPage->description]);
if ($blogPage->keywords != "") $this->registerMetaTag(['name' => 'keywords', 'content' => $blogPage->keywords]);
?>


<main class="b-section__main">
    <section class="b-section__text">
        <div class="container">
            <h2 class="b-section__title"><?= Html::encode($blogPage->h1) ?></h2>

            <div class="b-section__contentText">
                <?= $blogPage->content ?>
            </div>
            <br>
            <?= \app\widgets\BlogPageWidget::widget([
                'model' => $model
            ]) ?>


                <?= Pager::widget([
                    'pages' => $model->pagination,
                    'viewTemplate' => 'pager/frontend'
                ]); ?>
        </div>
    </section>
</main>

