<?php

namespace frontend\controllers;

use common\components\controllers\page\BaseController;
use common\models\db\Category;
use common\models\db\ContentPages;
use common\models\db\Product;
use common\models\db\ProductProperty;
use common\models\db\Property;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\Pagination;
use yii\db\Query;
use yii\web\NotFoundHttpException;

class CatalogController extends BaseController
{
    public function actionList($id = null)
    {
		$catalogContent = ContentPages::findOne(['action'=>'catalog']);
	    $categoriesQuery = Category::find()
		->where(['status' => Category::$STATUS_ACTIVE]);

	    if ($id !== null) {
		    $categoriesQuery->where(['id_parent' => $id]);
	    } else {
		    $categoriesQuery->root();
	    }


	    $countQuery = clone $categoriesQuery;
	    $pages = new Pagination([
		    'totalCount' => $countQuery->count(),
		    'pageSize' => 9
	    ]);
	    $categories = $categoriesQuery->offset($pages->offset)
		    ->limit($pages->limit)
		    ->all();

		$products = Product::find()
			->where(['status' => Product::$STATUS_ACTIVE])
			->andWhere(['top_sale' => true])
			->all();
		
		$productProperties = Property::find()
			->where(['is_filter' => true])
			->with('units')
			->asArray()
			->all();


        return $this->render('list', [
            'categories' => $categories,
	        'pages' => $pages,
			'catalogContent' => $catalogContent,
			'products' => $products,
			'properties' => $productProperties
        ]);
    }


	public function actionView($slug)
	{
		$category = Category::find()
			->where(['slug' => $slug])
			->andWhere(['status' => Category::$STATUS_ACTIVE])
			->one();
		$errors = false;

		if ($category === null) {
			throw new NotFoundHttpException('Данной категории не существует');
		} else {

			$subcategories = Category::find()
				->where(['id_parent' => $category->id])
				->andWhere(['status' => Category::$STATUS_ACTIVE])
				->all();

			$topSaleProducts = Product::find()
				->where(['status' => Product::$STATUS_ACTIVE])
				->andWhere(['top_sale' => true])
				->andWhere(['id_category' => $category->id])
				->all();

			$topSaleProductProperties = Property::find()
				->where(['is_filter' => true])
				->with('units')
				->asArray()
				->all();

			if (count($subcategories) != 0) {
				//pagination for sub categories is not good idea, will skip it
				return $this->render('subcategories', [
					'errors' => $errors,
					'category' => $category,
					'subcategories' => $subcategories,
					'products' => $topSaleProducts,
					'productProperties' => $topSaleProductProperties
				]);
			}
			//skip first param - slug:slug
			$params = array_slice(Yii::$app->request->get(), 1);

			//check pagination params 'page' and 'per-page'
			if (count($params) == 0 ||
				(count($params) == 2
					&& array_key_exists('page', $params)
					&& array_key_exists('per-page', $params)
				)
			) {
				$products = $this->viewAllCatalogProducts($category);
			} else {
				$products = $this->viewFilteredCatalogProducts($category, $params);
			}

			// sort product search result
			if(array_key_exists('sort-by', $params)){
				$this->applySortParams($products, $params['sort-by']);
			}

			$products = new ActiveDataProvider([
				'query' => $products,
				'pagination' => [
					'pageSize' => 9,
				],
			]);

			$productsCount = count($products->getModels());
			if ($productsCount === 0) {
				$errors = ['message' => 'В данной категории нет товаров'];
			}

			$filters = $this->selectAllFilterOptions($category);

			$properties = Property::find()
				->where(['is_filter' => true, 'id_category' => $category->id])
				->with('units')
				->asArray()
				->all();

			return $this->render('catalog_item_view', [
				'errors' => $errors,
				'models' => $products,
				'category' => $category,
				'filters' => $filters,
				'properties' => $properties,
				'products' => $topSaleProducts,
				'productProperties' => $topSaleProductProperties
			]);
		}
	}

	private function viewFilteredCatalogProducts($category, $params){
		$products = Product::find()
			->where(['id_category' => $category->id])
			->andWhere(['!=', 'status', Product::$STATUS_NOT_ACTIVE])
			->andWhere(['!=', 'status', Product::$STATUS_DELETED])
			->with('productValues');

		foreach ($params as $k => $v){
			if($k == 'page' || $k == 'per-page' || $k == 'sort-by') continue;

            //if have few values for same filter
			if(strpos($v, ',') !== false){
                $this->orFilterSearchParam($products, $category, $k, $v);
			}else{
                $this->addFilterSearchParam($products, $category, $k, $v);
			}
		}

		return $products;
	}

	private function applySortParams(&$query, $paramValue){
		switch ($paramValue){
			case 'popularity':
				return $query->orderBy('top_sale DESC');
			case 'new':
				return $query->orderBy('is_new DESC');
			case 'action':
				return $query->orderBy('is_action DESC');
			case 'sale':
				return $query->orderBy('is_sale DESC');
			case 'from-cheap':
				return $query->orderBy('price ASC');
			case 'from-expensive':
				return $query->orderBy('price DESC');
			default: return $query;
		}
	}

    /**
     * @param $query Query
     * @param $category Category
     * @param $property string
     * @param $value string
     * @return mixed Query
     */
    private function addFilterSearchParam(&$query, $category, $property, $value){
		$query->andWhere(['in', 'id',
				array_map(function($item) {
					return $item['id_product'];
				},	ProductProperty::find()->select('id_product')
					->leftJoin('value', 'value.id=product_property.id_value')
					->leftJoin('property', 'property.id = value.id_property')
					->where(['property.id_category' => $category->id])
					->andWhere(['property.slug' => $property])
					->andWhere(['value.slug' => $value])->asArray()->all()
				)
			]
		);
	}

    /**
     * @param $query Query
     * @param $category Category
     * @param $property string
     * @param $value string
     * @return mixed Query
     */
    private function orFilterSearchParam(&$query, $category, $property, $strValue){
        $values = explode(',', $strValue);
        $orCase = ProductProperty::find()->select('id_product')
                ->leftJoin('value', 'value.id=product_property.id_value')
                ->leftJoin('property', 'property.id = value.id_property')
                ->andWhere(['property.id_category' => $category->id]);

        $orString = ' ( property.slug = :prop AND value.slug = :val0)';
        for ($i = 1 ; $i< count($values); $i++) {
            $orString .= ' OR ( property.slug = :prop AND value.slug = :val'.$i.' )';
        }

        $orCase = $orCase->andWhere($orString)
            ->addParams([':prop' => $property, ':val0' => $values[0]]);
        for ($i = 1 ; $i< count($values); $i++) {
            $orCase->addParams([':val' .$i => $values[$i]]);
        }

        $query->andWhere(['in', 'id',
                array_map(function ($item) {
                    return $item['id_product'];
                }, $orCase->asArray()->all())
            ]
        );
    }

	private function viewAllCatalogProducts($category){
		return Product::find()
				->where([
					'id_category' => $category->id,
					'status' => Product::$STATUS_ACTIVE
				])
				->with('productValues');
	}

	private function selectAllFilterOptions($category){
		$res = Property::find()
			->select(['property.name as property_name',
				'value.name as value_name',
				'value.slug as value_slug',
				'property.slug as property_slug'])
			->where(['property.id_category' => $category->id])
			->andWhere(['is_filter'=>'1'])
			->leftJoin('value', 'value.id_property = property.id')
			->asArray()->all();

		$arr = [];
		foreach ($res as $k=>$v){
			if(isset($arr[$v['property_slug']])){
				$arr[$v['property_slug']][] = [
					'value_slug' => $v['value_slug'],
					'value_name' => $v['value_name']
				];
			}else{
				$arr[$v['property_slug']]['property_name'] = $v['property_name'];
				$arr[$v['property_slug']][] = [
					'value_slug' => $v['value_slug'],
					'value_name' => $v['value_name']
				];
			}
		}
		return $arr;
	}
}
