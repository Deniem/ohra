<?php
/**
 * Created by PhpStorm.
 * User: bbyte
 * Date: 7/4/16
 * Time: 1:15 PM
 */

namespace frontend\controllers;


use common\models\db\CustomerFavorite;
use Yii;
use yii\rest\ActiveController;

class CustomerFavoriteController extends ActiveController
{

    public $modelClass = 'common\models\db\CustomerFavorite';

    public function beforeAction($action)
    {
//        $this->isSessionOpen();
        return parent::beforeAction($action);
    }
    
    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['class'] = '\frontend\controllers\api\favorite\IndexAction';
        $actions['update']['class'] = '\frontend\controllers\api\favorite\UpdateAction';
        $actions['delete']['class'] = '\frontend\controllers\api\favorite\DeleteAction';
        
        return $actions;
    }

    public static function removeFromDb($productId){
        if(!\Yii::$app->user->isGuest){
            $customerId = \Yii::$app->user->identity->id;

            $customerFavorite = CustomerFavorite::find()
                ->where(['customer_id' => $customerId])
                ->andWhere(['product_id' => $productId])
                ->one();

            if($customerFavorite !== null){
                $customerFavorite->delete();
            }
        }
    }

    public static function saveToDataBase($productId){
        if(!\Yii::$app->user->isGuest){
            $customerId = \Yii::$app->user->identity->id;

            $customerFavorite = CustomerFavorite::find()
                ->where(['customer_id' => $customerId])
                ->andWhere(['product_id' => $productId])
                ->one();

            if($customerFavorite === null){
                $customerFavorite = new CustomerFavorite();
                $customerFavorite->product_id = $productId;
                $customerFavorite->customer_id = $customerId;
                $customerFavorite->save();
            }
        }
    }
    

    private function isSessionOpen(){
        $session = Yii::$app->session;
        if ($session->isActive){
            return $session;
        }else{
            $session->open();
            return $session;
        }
    }
}