<?php
namespace frontend\controllers\api;


use frontend\models\Customer;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class UserController extends Controller
{

    public function actionCheck(){
        \Yii::$app->response->format = Response::FORMAT_JSON;
        $username = \Yii::$app->request->get('username');

        if($username != null){
            $customer = Customer::find()->where(['username' => $username])->one();
            if($customer === null){
                return [
                    'status' => 'ok',
                    'message' => 'free username'
                ];
            }else{
                return [
                    'status' => 'exists',
                    'message' => 'Пользователь с таким именем уже существует'
                ] ;
            }
        }else {
            return [
                'status' => 'error',
                'message' => 'missing params'
            ];
        }
    }

    public function actionUpdatePassword(){
        
        if(!\Yii::$app->request->isPost){
            throw new BadRequestHttpException();
        }

        $data = \Yii::$app->request->post();

        $customer = Customer::find()
            ->where(['id' => $data['user_id']])
            ->one();

        if($customer === null){
            throw new NotFoundHttpException('People are strange');
        }

        \Yii::$app->response->format = Response::FORMAT_JSON;
        if($customer->validatePassword($data['old_pass'])){
            $customer->setPassword($data['new_pass']);
            if($customer->save()){
                return [
                    'status' => 'success',
                    'message' => 'Пароль успешно обновлен'
                ];
            }else{
                return [
                    'status' => 'error',
                    'message' => 'Ошибка сохранения. Попробуйте позже'
                ];
            }

        }else{
            return [
                'status' => 'error',
                'message' => 'Неправильно введен пароль'
            ];
        }
    }


    public function actionUpdatePersonal(){

        if(!\Yii::$app->request->isPost){
            throw new BadRequestHttpException();
        }

        $data = \Yii::$app->request->post();

        $customer = Customer::find()
            ->where(['id' => $data['user_id']])
            ->one();

        if($customer === null){
            throw new NotFoundHttpException('People are strange');
        }

        $customer->first_name = $data['first_name'];
        $customer->last_name = $data['last_name'];
        $customer->middle_name = $data['middle_name'];

        \Yii::$app->response->format = Response::FORMAT_JSON;

        if ($customer->save()) {
            return [
                'status' => 'success',
                'message' => 'Данные успешно сохраннены'
            ];
        } else {
            return [
                'status' => 'error',
                'message' => 'Невозможно сохранить пользовательские данные. Попробуйте позже'
            ];
        }
    }
}