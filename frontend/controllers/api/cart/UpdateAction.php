<?php
namespace frontend\controllers\api\cart;

use common\models\db\Product;
use frontend\controllers\CartController;
use yii\web\BadRequestHttpException;

/**
 * Created by PhpStorm.
 * User: bbyte
 * Date: 6/17/16
 * Time: 7:38 PM
 */
class UpdateAction extends \yii\rest\UpdateAction
{
    public $modelClass = 'yz\shoppingcar\ShoppingCart';

    public function run($id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $raw = json_decode(\Yii::$app->request->getRawBody());
        if($raw == null){
            throw new BadRequestHttpException();
        }

        $product = Product::findOne($id);
        $cart = \Yii::$app->cart;
        if ($product) {
            $cart->update($product, $raw->quantity);
            CartController::insertCartItemToDataBase($product, $raw->quantity);
        }

        return [
            'positions' => $cart->getPositions(),
            'count' => $cart->getCount(),
            'cost' => $cart->getCost(),
        ];
    }

}