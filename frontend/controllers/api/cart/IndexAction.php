<?php
namespace frontend\controllers\api\cart;

use frontend\controllers\CartController;
use Yii;
use yii\web\Response;

class IndexAction extends \yii\rest\IndexAction
{
    public $modelClass = 'yz\shoppingcar\ShoppingCart';
    
    public function run()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        return [
            'cart_view' => \Yii::$app->view->renderFile('@frontend/views/layouts/cart.php')
        ];
    }
    
}