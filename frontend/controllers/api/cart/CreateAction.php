<?php
namespace frontend\controllers\api\cart;

use common\models\db\Product;
use frontend\controllers\CartController;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Yii;
use yii\web\Response;

class CreateAction extends \yii\rest\CreateAction{

    public $modelClass = 'yz\shoppingcar\ShoppingCart';
    public function run(){

        Yii::$app->response->format = Response::FORMAT_JSON;
        $req = json_decode(Yii::$app->request->getRawBody());

        if($req == null){
            throw new BadRequestHttpException();
        }

        $id = $req->product->id;
        $cart = Yii::$app->cart;

        $product = Product::find()->where(['id'=> $id])->one();
        if ($product !== null) {
            $cart->put($product);
            CartController::insertCartItemToDataBase($product, 1);
        }

        return [
            'positions' => $cart->getPositions(),
            'count' => $cart->getCount(),
            'cost' => $cart->getCost(),
        ];
    }
}