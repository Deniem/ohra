<?php
namespace frontend\controllers\api\cart;


use common\models\db\Product;
use frontend\controllers\CartController;

class DeleteAction extends \yii\rest\DeleteAction
{
    public $modelClass = 'yz\shoppingcar\ShoppingCart';

    public function run($id)
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $product = Product::findOne($id);
        $cart = \Yii::$app->cart;
        if ($product) {
            $cart->remove($product);
            CartController::deleteCartItemDFromDataBase($product);
        }

        return [
            'positions' => $cart->getPositions(),
            'count' => $cart->getCount(),
            'cost' => $cart->getCost(),
        ];
    }

}