<?php
/**
 * Created by PhpStorm.
 * User: bbyte
 * Date: 7/4/16
 * Time: 7:17 PM
 */

namespace frontend\controllers\api\favorite;


use Yii;
use yii\base\Response;

class IndexAction extends \yii\rest\IndexAction
{
    public function run()
    {
        $session = Yii::$app->session;

        $favorite = 0;
        if ($session->has('favorite')) {
            $favorite = count($session->get('favorite'));
        }

        return [
            'favorite' => $favorite
        ];
    }
}