<?php
namespace frontend\controllers\api\favorite;

use common\models\db\CustomerFavorite;
use common\models\db\Product;
use frontend\controllers\CustomerFavoriteController;
use Yii;
use yii\web\BadRequestHttpException;

class UpdateAction extends \yii\rest\UpdateAction
{

    public function run($id)
    {
        $product = Product::find()->where(['id' => $id])->one();
        if (!$product) {
            throw new BadRequestHttpException();
        }
        $favorite = [];
        $session = Yii::$app->session;
            
        if ($session->has('favorite')){
            $favorite = $session->get('favorite');
            $favorite[$product->id] = [
                'name' => $product->name,
                'slug' => $product->slug,
                'price' => $product->price
            ];
            CustomerFavoriteController::saveToDataBase($id);
            $session->set('favorite', $favorite);
        }else{
            $favorite[$product->id] = [
                'name' => $product->name,
                'slug' => $product->slug,
                'price' => $product->price
            ];
            CustomerFavoriteController::saveToDataBase($id);
            $session->set('favorite', $favorite);
        }

        return $session->get('favorite');
    }

}