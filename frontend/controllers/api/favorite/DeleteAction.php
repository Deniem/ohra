<?php
namespace frontend\controllers\api\favorite;


use common\models\db\CustomerFavorite;
use common\models\db\Product;
use frontend\controllers\CustomerFavoriteController;
use Yii;
use yii\web\BadRequestHttpException;

class DeleteAction extends \yii\rest\DeleteAction
{

    public function run($id)
    {
        $product = Product::find()->where(['id' => $id])->one();
        if (!$product) {
            throw new BadRequestHttpException();
        }
        $favorite = [];
        $session = Yii::$app->session;
        
        if ($session->has('favorite')){
            $favorite = $session->get('favorite');
        }

        if(!empty($favorite)){
            if(array_key_exists($id, $favorite)){
                unset($favorite[$id]);
                CustomerFavoriteController::removeFromDb($id);
                $session->set('favorite', $favorite);
            }
        }

        return $session->get('favorite');
    }

} 