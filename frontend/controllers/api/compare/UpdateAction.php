<?php
namespace frontend\controllers\api\compare;

use common\models\db\CustomerCompare;
use common\models\db\Product;
use frontend\controllers\CustomerCompareController;
use frontend\models\Customer;
use Yii;
use yii\web\BadRequestHttpException;

class UpdateAction extends \yii\rest\UpdateAction
{

    public function run($id)
    {
        $product = Product::find()->where(['id' => $id])->one();
        if (!$product) {
            throw new BadRequestHttpException();
        }
        $favorite = [];
        $session = Yii::$app->session;

        if ($session->has('compare')){
            $favorite = $session->get('compare');
            $favorite[$product->id] = [
                'name' => $product->name,
                'slug' => $product->slug,
                'price' => $product->price
            ];

            CustomerCompareController::saveToDataBase($id);
            $session->set('compare', $favorite);
        }else{
            $favorite[$product->id] = [
                'name' => $product->name,
                'slug' => $product->slug,
                'price' => $product->price
            ];

            CustomerCompareController::saveToDataBase($id);
            $session->set('compare', $favorite);
        }

        return $session->get('compare');
    }
}