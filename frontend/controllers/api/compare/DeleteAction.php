<?php
namespace frontend\controllers\api\compare;

use common\models\db\CustomerCompare;
use common\models\db\Product;
use frontend\controllers\CustomerCompareController;
use Yii;
use yii\web\BadRequestHttpException;

class DeleteAction extends \yii\rest\DeleteAction
{

    public function run($id)
    {
        $product = Product::find()->where(['id' => $id])->one();
        if (!$product) {
            throw new BadRequestHttpException();
        }
        $favorite = [];
        $session = Yii::$app->session;

        if ($session->has('compare')) {
            $favorite = $session->get('compare');
        }

        if (!empty($favorite)) {
            if (array_key_exists($id, $favorite)) {
                unset($favorite[$id]);
                CustomerCompareController::removeFromDb($id);
                $session->set('compare', $favorite);
            }
        }

        return $session->get('compare');
    }

}
