<?php
/**
 * Created by PhpStorm.
 * User: bbyte
 * Date: 7/4/16
 * Time: 7:22 PM
 */

namespace frontend\controllers\api\compare;


use Yii;
use yii\base\Response;

class IndexAction extends \yii\rest\IndexAction
{
    public function run()
    {
        $session = Yii::$app->session;

        $compare = 0;
        if ($session->has('compare')) {
            $compare = count($session->get('compare'));
        }

        return [
            'compare' => $compare
        ];
    }
}