<?php

namespace frontend\controllers;

use common\components\controllers\page\BaseController;
use common\models\db\Blog;
use common\models\db\ContentPages;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;


class BlogController extends BaseController
{
    public function actionSlug(){

        $slug = Yii::$app->request->get('slug');

        $page = Blog::find()
            ->where(['status' => Blog::$STATUS_ACTIVE])
            ->andWhere(['action' => $slug])
            ->one();

        if($page == null){
            throw new NotFoundHttpException('Страница не найденна');
        }

        return $this->render('page', [
            'model' => $page
        ]);
    }

    public function actionIndex(){

        $blogPage = ContentPages::find()
            ->where(['action' => 'blog'])
            ->one();

        $pages = Blog::find()
            ->where(['status' => Blog::$STATUS_ACTIVE])
            ->orderBy(['published_at' => SORT_DESC]);
        
        $data = new ActiveDataProvider([
            'query' => $pages,
            'pagination' => [
                'pageSize' => 2
            ]
        ]);
        
        return $this->render('index', [
            'model' => $data,
            'blogPage' => $blogPage
        ]);
    }
}