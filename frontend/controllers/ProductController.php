<?php
/**
 * Created by PhpStorm.
 * User: bbyte
 * Date: 6/17/16
 * Time: 5:23 PM
 */

namespace frontend\controllers;


use common\components\controllers\page\BaseController;
use common\models\db\Product;
use yii\web\BadRequestHttpException;

class ProductController extends BaseController
{

    public function actionView($id)
    {
        $model = Product::find()->where(['id' => $id])->one();
        if (!$model) {
            throw new BadRequestHttpException();
        }
        $this->pageData['product'] = $model->toArray();
        return $this->render('view', ['model' => $model]);
    }

    public function actionSlug($slug){
        $model = Product::find()->where(['slug' => $slug])->one();
        if (!$model) {
            throw new BadRequestHttpException();
        }
        $this->pageData['product'] = $model->toArray();
        return $this->render('view', ['model' => $model]);
    }
}