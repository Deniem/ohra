<?php

namespace frontend\controllers;


use common\models\db\CustomerCompare;
use Yii;
use yii\rest\ActiveController;

class CustomerCompareController extends ActiveController
{

    public $modelClass = 'common\models\db\CustomerCompare';

    public function beforeAction($action)
    {
//        $this->isSessionOpen();
        return parent::beforeAction($action);
    }

    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['class'] = '\frontend\controllers\api\compare\IndexAction';
        $actions['update']['class'] = '\frontend\controllers\api\compare\UpdateAction';
        $actions['delete']['class'] = '\frontend\controllers\api\compare\DeleteAction';

        return $actions;
    }

    public static function removeFromDb($productId){
        if(!\Yii::$app->user->isGuest){
            $customerId = \Yii::$app->user->identity->id;

            $customerCompare = CustomerCompare::find()
                ->where(['customer_id' => $customerId])
                ->andWhere(['product_id' => $productId])
                ->one();

            if($customerCompare !== null){
                $customerCompare->delete();
            }
        }
    }


    public static  function saveToDataBase($productId){
        if(!\Yii::$app->user->isGuest){
            $customerId = \Yii::$app->user->identity->id;

            $customerCompare = CustomerCompare::find()
                ->where(['customer_id' => $customerId])
                ->andWhere(['product_id' => $productId])
                ->one();

            if($customerCompare === null){
                $customerCompare = new CustomerCompare();
                $customerCompare->product_id = $productId;
                $customerCompare->customer_id = $customerId;
                $customerCompare->save();
            }
        }
    }

    private function isSessionOpen(){
        $session = Yii::$app->session;
        if ($session->isActive){
            return $session;
        }else{
            $session->open();
            return $session;
        }
    }
}