<?php
namespace frontend\controllers;

use common\components\controllers\page\BaseController;
use common\models\db\Brand;
use common\models\db\ContentPages;
use common\models\db\CustomerCompare;
use common\models\db\CustomerFavorite;
use common\models\db\Preorder;
use common\models\db\PreorderItem;
use common\models\db\Property;
use frontend\models\Customer;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use frontend\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use yii\web\NotFoundHttpException;
use common\models\db\Product;

/**
 * Site controller
 */
class SiteController extends BaseController
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
	    $new = Product::find()
            ->where(['is_new' => 1, 'status' => Product::$STATUS_ACTIVE])
            ->with('productValues', 'discount')
            ->all();

        $sale = Product::find()
            ->where(['is_sale' => 1, 'status' => Product::$STATUS_ACTIVE])
            ->with('productValues', 'discount')
            ->all();

        $properties = Property::find()
            ->where(['is_filter' => true])
            ->with('units')
            ->asArray()
            ->all();
        
        $topBrands = Brand::find()
            ->where(['is_top_brand' => 1])
            ->all();

        return $this->render('index', [
	        'new' => $new,
	        'sale' => $sale,
            'properties' => $properties,
            'topBrands' => $topBrands
        ]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $model = new LoginForm();
        $model->load(Yii::$app->request->post());
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $this->loadFavoriteList();
            $this->loadCompareList();
            $this->loadCartList();
            if(Yii::$app->request->post('no_redirect')){
                return '{"status": "success"}';
            } else {
                return $this->redirect(['site/index']);
            }
        } else {
            if(Yii::$app->request->post('no_redirect')){
                return '{"status": "error"}';
            } else {
                return $this->render('login', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if(Yii::$app->request->post('no_redirect')){
                    $customer = Customer::findNewUser($model->username);
                    if($customer !== null){
                        $customer->status = 10;
                        $customer->save();
                        Yii::$app->user->login($customer, 3600 * 24 * 30);
                        $this->loadFavoriteList();
                        $this->loadCompareList();
                        $this->loadCartList();
                        return '{"status": "success"}';
                    }
                }else{
					/*
					 *TEMPORARY FOR DEMO USE ONLY!
					 *VERIFICATION NEEDED!
					 * */
					$customer = Customer::findNewUser($model->username);
					Yii::$app->user->login($customer, 3600 * 24 * 30);
                    return $this->redirect('/');
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    public function actionVerify($authKey = null)
    {
        if ($authKey !== null){
            $customer = Customer::findIdentityByAccessToken($authKey);
            if($customer != null){
                $customer->status = 10;
                $customer->save();
                return $this->redirect('/');
            }else{
                return $this->render('error', [
                    'name' => 'Verification failed',
                    'message' => 'Error during verification process'
                ]);
            }
        }else{
            throw new BadRequestHttpException();
        }
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function actionContentPages(){

        $slug = Yii::$app->request->get('slug');

        $page = ContentPages::find()
            ->where('action=:slug', [':slug' => $slug])
            ->andWhere(['status' => 1])
            ->one();

        if($page == null){
            throw new NotFoundHttpException('Page not found');
        }

        $parents = [];

		$childs = ContentPages::findAll([
			'id_parent' => $page->id,
			'status' => 1
		]);

        $id = $page->id_parent;
        if($id != 0){
            do{
                $parent = $this->getParent($id);
                if(!is_null($parent)){
                    $parents[] = $parent;
                    $id = $parent->id_parent;
                }
            }while($parent !== null);
        }

        return $this->render('contentPages', [
            'model' => $page,
            'parents' => $parents,
            'childs' => $childs,
        ]);

    }

    private function getParent($parent_id){
        return ContentPages::find()
            ->where(['id' => $parent_id])
            ->one();
    }

    private function loadFavoriteList()
    {
        $customerId = \Yii::$app->user->identity->id;
        $session = Yii::$app->session;
        $favorite = [];

        $customerFavoriteList = CustomerFavorite::find()
            ->where(['customer_id' => $customerId])
            ->asArray()
            ->all();

        if ($session->has('favorite')) {
            $favorite = $session->get('favorite');
        }

        if (count($customerFavoriteList) > 0) {

            //add items from db to session favorite list if they not found in current session favorite list
            foreach ($customerFavoriteList as $fav) {
                if (!array_key_exists($fav['product_id'], $favorite)) {
                    $product = Product::find()->where(['id' => $fav['product_id']])->one();

                    if ($product === null) continue;

                    //add items to temporary array
                    $favorite[$product->id] = [
                        'name' => $product->name,
                        'slug' => $product->slug,
                        'price' => $product->price
                    ];
                }
            }

            if ($session->has('favorite')) {
                // save items from session list to database if they was not found in db
                foreach ($session->get('favorite') as $sessionFavId => $sessionFavValue){
                    $founded = false;
                    foreach ($customerFavoriteList as $dbFav){
                        if($dbFav['product_id'] == $sessionFavId){
                            $founded = true;
                        }
                    }
                    if(!$founded){
                        $customerFavorite = new CustomerFavorite();
                        $customerFavorite->product_id = $sessionFavId;
                        $customerFavorite->customer_id = $customerId;
                        $customerFavorite->save();
                    }
                }
            }
        }

        // save new list to session
        $session->set('favorite', $favorite);
    }

    private function loadCompareList(){
        $customerId = \Yii::$app->user->identity->id;
        $session = Yii::$app->session;
        $compareList = [];

        $customerCompareDbList = CustomerCompare::find()
            ->where(['customer_id' => $customerId])
            ->asArray()
            ->all();

        if ($session->has('compare')) {
            $compareList = $session->get('compare');
        }

        if (count($customerCompareDbList) > 0) {

            //add items from db to session compareList list if they not found in current session compareList list
            foreach ($customerCompareDbList as $comparable) {
                if (!array_key_exists($comparable['product_id'], $compareList)) {
                    $product = Product::find()->where(['id' => $comparable['product_id']])->one();

                    if ($product === null) continue;

                    //add items to temporary array
                    $compareList[$product->id] = [
                        'name' => $product->name,
                        'slug' => $product->slug,
                        'price' => $product->price
                    ];
                }
            }

            if ($session->has('compare')) {
                // save items from session list to database if they was not found in db
                foreach ($session->get('compare') as $sessionComparableId => $sessionComparableValue){
                    $founded = false;
                    foreach ($customerCompareDbList as $dbFav){
                        if($dbFav['product_id'] == $sessionComparableId){
                            $founded = true;
                        }
                    }
                    if(!$founded){
                        $customerFavorite = new CustomerCompare();
                        $customerFavorite->product_id = $sessionComparableId;
                        $customerFavorite->customer_id = $customerId;
                        $customerFavorite->save();
                    }
                }
            }
        }

        // save new list to session
        $session->set('compare', $compareList);
    }

    private function loadCartList()
    {
        $customerId = \Yii::$app->user->identity->id;
        $cart = \Yii::$app->cart;
        $cartProducts = $cart->getPositions();

        $preorder = Preorder::find()
            ->where(['customer_id' => $customerId])
            ->andWhere(['is_active' => true])
            ->one();

        if ($preorder !== null) {
            $preorderItems = PreorderItem::find()
                ->where(['preorder_id' => $preorder->id])
                ->andWhere(['is_active' => true])
                ->all();

            if (count($cartProducts) > 0 && count($preorderItems) > 0) {

                //search cart products in preopred Items
                foreach ($cartProducts as $cartProduct) {
                    $foundedInDb = false;
                    foreach ($preorderItems as $preorderItem) {
                        if ($cartProduct->id == $preorderItem->product_id) {
                            $foundedInDb = true;
                            break;
                        }
                    }
                    if (!$foundedInDb) {
                        self::savePreorderItemToDb($preorder, $cartProduct);
                    }
                }

                //search preorder Items in cart products and add them to $futureCartItems
                $futureCartItems = [];
                foreach ($preorderItems as $preorderItem){
                    $founded = false;
                    foreach ($cartProducts as $cartProduct){
                        if ($cartProduct->id == $preorderItem->product_id) {
                            $founded = true;
                            break;
                        }
                    }
                    if (!$founded) {
                        $futureCartItems[] = $preorderItem;
                    }
                }
                foreach ($futureCartItems as $futureCartItem){
                    self::addItemToCart($futureCartItem);
                }

            }else if(count($cartProducts) > 0 && count($preorderItems) == 0){
                //save all cart products to db
                foreach ($cartProducts as $cartProduct){
                    self::savePreorderItemToDb($preorder, $cartProduct);
                }
            }else if(count($cartProducts) == 0 && count($preorderItems) > 0){
                //load all from db to session
                foreach ($preorderItems as $preorderItem){
                    self::addItemToCart($preorderItem);
                }
            }else{
//                count($cartProducts) == 0 && count($preorderItems) == 0
//                do nothing
            }
        } else {
            //if preorder is not saved before - save all new cart positions to db
            if (count($cartProducts) > 0) {
                $preorder = new Preorder();
                $preorder->customer_id = $customerId;
                $preorder->is_active = true;
                $preorder->email = Yii::$app->user->identity->email;
                if ($preorder->save()) {
                    foreach ($cartProducts as $cartProduct) {
                        self::savePreorderItemToDb($preorder, $cartProduct);
                    }
                }
            }
        }
    }

    private static function addItemToCart($preorderItem){
        $cart = \Yii::$app->cart;
        $product = Product::findOne($preorderItem->product_id);
        if ($product) {
            $cart->update($product, $preorderItem->quantity);
        }
    }

    private static function savePreorderItemToDb($preorder, $cartProduct){
        $preorderItem = new PreorderItem();
        $preorderItem->preorder_id = $preorder->id;
        $preorderItem->name = $cartProduct->name;
        $preorderItem->price = $cartProduct->getPrice();
        $preorderItem->product_id = $cartProduct->id;
        $preorderItem->quantity = $cartProduct->getQuantity();
        $preorderItem->is_active = true;
        $preorderItem->save();
    }
}
