<?php
/**
 * Created by PhpStorm.
 * User: bbyte
 * Date: 6/29/16
 * Time: 7:38 PM
 */

namespace frontend\controllers;


use common\components\controllers\page\BaseController;
use common\models\db\Brand;
use common\models\db\ContentPages;
use yii\data\Pagination;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

class BrandController extends BaseController
{

    public function actionIndex(){

		$brandContent = ContentPages::findOne(['action'=>'brands']);
     
        $brands = Brand::find();

        $countc = clone $brands;
        $pages = new Pagination([
            'totalCount' => $countc->count(),
            'pageSize' => 6
        ]);
        $brands = $brands->offset($pages->offset)
            ->limit($pages->limit)
            ->all();
        
        if($brands === null){
            throw new NotFoundHttpException(); 
        }
        
        return $this->render('index', [
            'model' => $brands,
			'content' => $brandContent,
            'pages' => $pages
        ]);
    }
    
    public function actionSlug($slug){
        $model = Brand::find()
            ->where(['slug' => $slug])
            ->one();
        if (!$model) {
            throw new BadRequestHttpException();
        }

        return $this->render('view', [
			'model' => $model
		]);
    }
}