<?php

namespace frontend\controllers;

use common\components\controllers\page\BaseController;
use common\models\db\Order;
use common\models\db\OrderItem;
use common\models\db\Preorder;
use common\models\db\PreorderItem;
use common\models\db\Product;
use common\models\db\Property;
use frontend\models\Customer;
use yii\db\Expression;
use yii\rest\ActiveController;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yz\shoppingcart\ShoppingCart;
use yii\web\Response;
use Yii;

class CartController extends BaseController
{
    public $modelClass = 'yz\shoppingcar\ShoppingCart';
	public $enableCsrfValidation = false;

	public function actions()
	{
		$actions = parent::actions();
        
        $actions['render']['class'] = '\frontend\controllers\api\cart\IndexAction';
		$actions['create']['class'] = '\frontend\controllers\api\cart\CreateAction';
		$actions['update']['class'] = '\frontend\controllers\api\cart\UpdateAction';
		$actions['delete']['class'] = '\frontend\controllers\api\cart\DeleteAction';
		return $actions;
	}

	public function actionList()
    {
        /* @var $cart ShoppingCart */
        $cart = \Yii::$app->cart;

        $positions = $cart->getPositions();
	    if (count($positions) > 0) {
		    $total = $cart->getCost();
			foreach($positions as $position) {
				$this->pageData['cart'][] = $position->toArray();
			}
		    return $this->render('list', [
			    'positions' => $positions,
			    'cartCost' => $total,
			    'cartCount' => \Yii::$app->cart->getCount()
		    ]);
	    } else {
		    return $this->redirect('/catalog/list');
	    }
    }

    public function actionCart(){
        $cart = \Yii::$app->cart;

        $positions = $cart->getPositions();
        if (count($positions) > 0) {
            $total = $cart->getCost();
            foreach($positions as $position) {
                $this->pageData['cart'][] = $position->toArray();
            }
            return $this->render('cart', [
                'positions' => $positions,
                'cartCost' => $total,
                'cartCount' => \Yii::$app->cart->getCount()
            ]);
        } else {
            return $this->redirect('/catalog/list');
        }
    }
    
    public function actionOrder()
    {
		if(!Yii::$app->request->post() || Yii::$app->user->isGuest){
			throw new BadRequestHttpException();
		}

        $order = new Order();
        $cart = \Yii::$app->cart;
        $products = $cart->getPositions();
        $total = $cart->getCost();

		$order->setAttributes(\Yii::$app->request->post(), false);
        $order->setAttribute('email', Yii::$app->user->identity->email);
        $order->setAttribute('customer_id', Yii::$app->user->identity->id);

        if ($order->validate()) {

            $transaction = $order->getDb()->beginTransaction();
            $order->save(false);

            foreach($products as $product) {
                $orderItem = new OrderItem();
                $orderItem->order_id = $order->id;
                $orderItem->name = $product->name;
                $orderItem->price = $product->getPrice();
                $orderItem->product_id = $product->id;
                $orderItem->quantity = $product->getQuantity();
                if (!$orderItem->save(false)) {
                    $transaction->rollBack();
                    \Yii::$app->session->addFlash('error', 'Cannot place your order. Please contact us.');
                    return $this->redirect('/catalog/list');
                }
            }

            $transaction->commit();
            \Yii::$app->cart->removeAll();
            self::clearPreorder();

            \Yii::$app->session->addFlash('order-status', 'Спасибо за ваш заказ. Мы свяжемся с вами в ближайшее время');
			\Yii::$app->mailer->compose('contact/order-success')
				->setFrom(Yii::$app->params['supportEmail'])
				->setTo($order->email)
				->setSubject("Ваш заказ успешно обработан")
				->send();

            return $this->redirect('/cart/cabinet');
        }


        return $this->render('list', [
            'order' => $order,
            'products' => $products,
            'total' => $total,
        ]);
    }

    public function actionCabinet(){
        if(Yii::$app->user->isGuest){
            return $this->redirect('/cart/view');
        }else{
            return $this->cabinetAction('cabinet');
        }
    }

    public function actionCabinetView($slug){
        if(!in_array($slug, ['favorite', 'orders', 'settings', 'compare'])){
            throw new NotFoundHttpException();
        }
        return $this->cabinetAction($slug);
    }

    private function cabinetAction($view){

		$properties = Property::find()
			->where(['is_filter' => true])
			->with('units')
			->asArray()
			->all();

        if (Yii::$app->user->isGuest) {
            return $this->render('/cart/' . $view,[
				'properties' => $properties,
			]);
        } else {
            $customer = Customer::find()
                ->where(['id' => Yii::$app->user->identity->id])
                ->one();

            $orders = Order::find()
                ->where(['customer_id' => $customer->id])
                ->with('orderItems')
                ->orderBy(['order.updated_at' => SORT_DESC])
                ->all();

            return $this->render('/cart/' . $view, [
                'customer' => $customer,
                'orders' => $orders,
                'properties' => $properties
            ]);
        }
    }

    public static function cartCountName($count){
        //dump but quickly meet the requirements 
        $number = substr(strval($count), -1);
        if($count == 11){
            return 'товаров';
        }
        if($number == 1){
            return 'товар';
        }else if($number == 2 || $number == 3 || $number == 4){
            return 'товара';
        }else {
            return 'товаров';
        }
    }

    public static function getProductListFromSession($listName){
        $session = Yii::$app->session;
        if(!$session->has($listName)){
            return [];
        }else{
            $productList = $session->get($listName);
            $idList= [];
            if(count($productList) > 0){
                foreach ($productList as $k => $v){
                    $idList[] = $k;
                }
            }

            $products = Product::find()
                ->where(['id' => $idList])
                ->with('category')
                ->with('productValues')
                ->all();

            return $products;   
        }
    }
    
    public static function isInCart($id){
        if(array_key_exists($id, Yii::$app->cart->getPositions())){
            return 'active';
        }
        return '';
    }
    
    public static function isInFavorite($id){
        $session = Yii::$app->session;
        if ($session->has('favorite')) {
            if(array_key_exists($id, $session->get('favorite'))){
                return 'active';
            }
        }
        return '';
    }
    
    public static function isInCompareList($id){
        $session = Yii::$app->session;
        if ($session->has('compare')) {
            if(array_key_exists($id, $session->get('compare'))){
                return 'active';
            }
        }
        return '';
    }

    public static function insertCartItemToDataBase($product, $quantity){
        if(!Yii::$app->user->isGuest){
            $userId = Yii::$app->user->identity->id;

            $preorder = Preorder::find()
                ->where(['customer_id' => $userId])
                ->andWhere(['is_active' => true])
                ->one();

            if($preorder !== null){
                //update updated_at timestamp
                $preorder->updated_at = new Expression('NOW()');
                if($preorder->save()){
                    $preorderItem = PreorderItem::find()
                        ->where(['preorder_id' => $preorder->id])
                        ->andWhere(['product_id' => $product->id])
                        ->andWhere(['is_active' => true])
                        ->one();
                    if($preorderItem !== null){
                        $preorderItem->name = $product->name;
                        $preorderItem->price = $product->price;
                        $preorderItem->quantity = $quantity;
                        $preorderItem->is_active = true;
                        $preorderItem->save();
                    }else{
                        $preorderItem = new PreorderItem();
                        self::savePreorderItem($preorder, $preorderItem, $product, $quantity);
                    }
                }
            }else{
                $preorder = new Preorder();
                $preorder->customer_id = $userId;
                $preorder->is_active = true;
                $preorder->email = Yii::$app->user->identity->email;
                if($preorder->save()){
                    $preorderItem = new PreorderItem();
                    self::savePreorderItem($preorder, $preorderItem, $product, $quantity);
                }
            }
        }
    }

    public static function deleteCartItemDFromDataBase($product){
        if(!Yii::$app->user->isGuest) {
            $userId = Yii::$app->user->identity->id;

            $preorder = Preorder::find()
                ->where(['customer_id' => $userId])
                ->andWhere(['is_active' => true])
                ->one();

            if($preorder !== null){
                $preorderItem = PreorderItem::find()
                    ->where(['preorder_id' => $preorder->id])
                    ->andWhere(['product_id' => $product->id])
                    ->andWhere(['is_active' => true])
                    ->one();

                if($preorderItem !== null){
                    $preorderItem->is_active = 0;
                    $preorderItem->save();
                }

                $itemsCount = PreorderItem::find()
                    ->where(['preorder_id' => $preorder->id])
                    ->andWhere(['is_active' => true])
                    ->count();

                if($itemsCount == 0){
                    $preorder->is_active = 0;
                    $preorder->save();
                }
            }else{
                //strange
            }
        }
    }

    public static function clearPreorder(){
        //when order is finished and saved to db
        if(\Yii::$app->user->isGuest) return;

        $preorder = Preorder::find()
            ->where(['customer_id' => \Yii::$app->user->identity->id])
            ->andWhere(['is_active' => true])
            ->one();

        if($preorder !== null){
            $preorder->is_active = 0;
            $preorder->is_order_confirmed = 1;
            $preorder->save();
            
            $preorderItems = PreorderItem::find()
                ->where(['preorder_id' => $preorder->id])
                ->andWhere(['is_active' => true])
                ->all();

            foreach ($preorderItems as $preorderItem){
                $preorderItem->is_active = 0;
                $preorderItem->is_order_confirmed = 1;
                $preorderItem->save();
            }
        }
    }

    private static function savePreorderItem($preorder, $preorderItem, $product, $quantity){
        $preorderItem->preorder_id = $preorder->id;
        $preorderItem->name = $product->name;
        $preorderItem->price = $product->price;
        $preorderItem->product_id = $product->id;
        $preorderItem->quantity = $quantity;
        $preorderItem->is_active = true;
        $preorderItem->save();
    }
}
