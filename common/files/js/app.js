var app = app || {};

(function($, app) {

	$.extend(app, {

		init: function(){
			var self = this;

			this.eventListener();
		},

		events: {
			/*here is common global events*/
		},

		views: {},
		models: {},

		eventListener: function(){
			$.each(this.events, function(event, action){
				var events = event.split(' ');
				$(events[1]).on(events[0], action);
				if (events[2]) {
					$(events[2]).on(events[0], events[1], action);
				}
				console.log(events);
			})
		}

	});


})(jQuery, app);

$('document').ready(function(ev){
	console.log('common');
	app.init();
});
