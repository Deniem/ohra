<?php
namespace common\components;
use common\models\db\Settings as SettingsModel;

class Settings {

	private $settings = [];
	private $settingsList = [];
	private static $_instance = null;

	protected function __construct()
	{
		$this->settingsList = SettingsModel::findAll(['type'=>[
			SettingsModel::$TYPE_GLOBAL,
			SettingsModel::$TYPE_FRONTEND,
		]]);
		$this->orderSettings();
	}

	private function __clone(){

	}

	static public function getInstance() {
		if(is_null(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

	private function orderSettings(){
		if ($this->settingsList && count($this->settingsList) > 0) {
			foreach ($this->settingsList as $value) {
				switch ($value->value_type) {
					case 'url':
						$this->settings[$value->name] = $value->value != '' ? $value->value : '#';
						break;
					default:
						$this->settings[$value->name] = $value->value;
						break;
				}

			}
		}
	}

	public function getAll() {
		return $this->settings;
	}

	public function getByName($name){
		return isset($this->settings[$name]) ? $this->settings[$name] : null;
	}

}
