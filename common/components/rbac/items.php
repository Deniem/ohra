<?php
return [
    'dashboard' => [
        'type' => 2,
        'description' => 'backend',
    ],
    'user' => [
        'type' => 1,
        'description' => 'Simple user',
        'ruleName' => 'userRole',
    ],
    'manager' => [
        'type' => 1,
        'description' => 'Backend auth allowed manager',
        'ruleName' => 'userRole',
        'children' => [
            'user',
            'dashboard',
        ],
    ],
    'admin' => [
        'type' => 1,
        'description' => 'Application administrator',
        'ruleName' => 'userRole',
        'children' => [
            'manager',
        ],
    ],
];
