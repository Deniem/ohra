<ul class="pagination">
	<li class="<?= $buttons['prev']['classes'] ?>">
		<a href="<?= $pages->createUrl($buttons['prev']['page']) ?>" >
			<i class="entypo-left-open-mini"></i>
		</a>
	</li>
	<?php foreach($buttons['internal'] as $internalPage): ?>
		<li class="<?= $internalPage['classes'] ?>">
			<a href="<?= $pages->createUrl($internalPage['page']) ?>">
				<?= $internalPage['label'] ?>
			</a>
		</li>
	<?php endforeach; ?>
	<li class="<?= $buttons['next']['classes'] ?>">
		<a href="<?= $pages->createUrl($buttons['next']['page']) ?>"><i class="entypo-right-open-mini"></i></a>
	</li>
</ul>