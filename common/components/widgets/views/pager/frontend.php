<?php
$from = (++$pages->page * $pages->pageSize) - $pages->pageSize + 1;

if($from == $pages->totalCount){
    $to = $pages->totalCount;
}else{
    $to = $from + $pages->pageSize - 1;
    if($to > $pages->totalCount){
        $to = $pages->totalCount - $from;
    }
}



?>
<nav class="b-pagination">
    <div class="b-pagination__left">
        <div class="b-pagination__show">
            Показано с <span class="b-pagination__firstShow"><?=$from?>
            </span>— <span class="b-pagination__secondShow"><?= $to?></span>
            из <span class="b-pagination__allItem"><?=$pages->totalCount?></span> позиций
        </div>
    </div>
    <div class="b-pagination__right">

        <ul class="b-pagination__list">
            <li class="<?= $buttons['prev']['classes'] ?>">
                <a href="<?= $pages->createUrl($buttons['prev']['page']) ?>"
                   class="<?= $buttons['prev']['linkClass'] ?>
                <?php if ($buttons['prev']['disabled']): ?>
                        <?= $buttons['prev']['disabledLinkClass'] ?>
                    <?php endif ?>
                    ">
                    <?= $buttons['prev']['label'] ?>
                </a>
            </li>
            <?php foreach ($buttons['internal'] as $internalPage): ?>
                <li class="<?= $internalPage['classes'] ?>">
                    <a href="<?= $pages->createUrl($internalPage['page']) ?>"
                       class="<?= $internalPage['linkClass'] ?>
                <?php if ($internalPage['active']): ?>
                    <?= $internalPage['activeLinkClass'] ?>
                 <?php endif ?>">
                        <?= $internalPage['label'] ?>
                    </a>
                </li>
            <?php endforeach; ?>
            <li class="<?= $buttons['next']['classes'] ?>">
                <a href="<?= $pages->createUrl($buttons['next']['page']) ?>"
                   class="<?= $buttons['next']['linkClass'] ?>
                    <?php if ($buttons['next']['disabled']): ?>
                        <?= $buttons['next']['disabledLinkClass'] ?>
                    <?php endif ?>">
                    <?= $buttons['next']['label'] ?>
                </a>
            </li>
        </ul>
    </div>
</nav>