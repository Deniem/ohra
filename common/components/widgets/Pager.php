<?php

namespace common\components\widgets;

use yii\base\Widget;


class Pager extends Widget{

	public $prevPageLabel = 'Назад';
	public $nextPageLabel = 'Вперед';
	public $disabledClass = 'b-pagination__link--disabled';
	public $disabledLinkClass = 'b-pagination__link--disabled';
	public $activeClass = ' active ';
	public $activeLinkClass = ' b-pagination__link--active ';
	public $itemClass = ' b-pagination__item ';
	public $linkClass = 'b-pagination__link';
	public $template;
	public $pages;
	public $viewTemplate;

	protected function getPageRange ()
	{
		$currentPage = $this->pages->getPage();
		$pageCount = $this->pages->getPageCount();

		$maxButtonCount = 10;
		$beginPage = max(0, $currentPage - (int) ($maxButtonCount / 2));
		if (($endPage = $beginPage + $maxButtonCount - 1) >= $pageCount) {
			$endPage = $pageCount - 1;
			$beginPage = max(0, $endPage - $maxButtonCount + 1);
		}
		return [$beginPage, $endPage];
	}

	public function run(){
		$pageCount = $this->pages->getPageCount();
		if ($pageCount < 2) {
			return '';
		}

		$buttons = [];
		$currentPage = $this->pages->getPage();

		// first page
		$buttons['first'] = [
			'label' => '1',
			'page'  => 0,
			'disabled' => $currentPage <= 0,
			'active' => false,
		];
		$buttons['first']['classes'] = $this->getClasses($buttons['first']);

		// prev page
		if (($page = $currentPage - 1) < 0) {
			$page = 0;
		}
		$buttons['prev'] = [
			'label' => $this->prevPageLabel,
			'page'  => $page,
			'disabled' => $currentPage <= 0,
			'linkClass' => $this->linkClass,
			'disabledLinkClass' => $this->disabledLinkClass,
			'active' => false
		];
		$buttons['prev']['classes'] = $this->getClasses($buttons['prev']);

		// internal pages
		list($beginPage, $endPage) = $this->getPageRange();
		for ($i = $beginPage; $i <= $endPage; ++$i) {
			$active = $i == $currentPage;
			$buttons['internal'][] = [
				'label' => $i + 1,
				'page'  => $i,
				'disabled' => false,
				'active' => $active,
				'activeLinkClass' => $this->activeLinkClass,
				'linkClass' => $this->linkClass,
				'classes' => $this->getClasses(['disabled' => false, 'active' => $active])
			];
		}

		// next page
		if (($page = $currentPage + 1) >= $pageCount - 1) {
			$page = $pageCount - 1;
		}
		$buttons['next'] = [
			'label' => $this->nextPageLabel,
			'page'  => $page,
			'disabled' => $currentPage >= $pageCount - 1,
			'linkClass' => $this->linkClass,
			'active' => false,
			'disabledLinkClass' => $this->disabledLinkClass
		];
		$buttons['next']['classes'] = $this->getClasses($buttons['next']);

		// last page
		$lastPageLabel = $pageCount;
		if ($lastPageLabel !== false) {
			$buttons['last'] = [$lastPageLabel, $pageCount - 1, $currentPage >= $pageCount - 1, false];
		}
		$buttons['last'] = [
			'label' => $lastPageLabel,
			'page'  => $pageCount - 1,
			'disabled' => $currentPage >= $pageCount - 1,
			'active' => false
		];
		$buttons['last']['classes'] = $this->getClasses($buttons['last']);

		return $this->render($this->viewTemplate, [
			'buttons' => $buttons,
			'pages' => $this->pages
		]);
	}

    protected function getClasses ($button) {
		$result = '';
		$result .= $this->itemClass;
		if ($button['disabled']) {
			$result .= $this->disabledClass;
		}
		if ($button['active']) {
			$result .= $this->activeClass;
		}
		return trim($result);
	}
}
