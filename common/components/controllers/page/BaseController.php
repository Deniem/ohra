<?php
namespace common\components\controllers\page;

use common\models\db\User;
use Yii;
use yii\base\Action;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * Base class for all page controllers in the project
 *
 * @package app\components
 */
class BaseController extends Controller
{
    /**
     * @var  array
     */
    public $pageData = [];
	public $notifications;

    /**
     * @var  Session
     */
    public $session;

    /**
     * Before the action is being executed, this function prepares session object.
     *
     * @param   Action  $action  Action to be executed
     *
     * @return  bool  True to proceed to the action; false to stop processing the request
     *
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {

        if (parent::beforeAction($action))
        {
	        $this->notifications = Yii::$app->notifications;
			$this->pageData['notifications'] =$this->notifications->getNotifications();

			if (!Yii::$app->user->isGuest) {

				$user = Yii::$app->user->getIdentity();

				$this->pageData['user']['id'] = $user->id;
				$this->pageData['user']['status'] = $user->status;

				if (isset($user->role)) {
					$this->pageData['user']['role'] = User::getUserRoleName($user->role);
				}

				$this->pageData['user']['email'] = $user->email;
				$this->pageData['user']['username'] = $user->username;

                if(isset($user->first_name)){
                    $this->pageData['user']['first_name'] = $user->first_name;
                    $this->pageData['user']['last_name'] = $user->last_name;
                    $this->pageData['user']['middle_name'] = $user->middle_name;
                }

			}
            return true;
        }
        return false;

    }

}
