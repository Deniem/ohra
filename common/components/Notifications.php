<?php
namespace common\components;
use Yii;


/**
 * Login form
 */
class Notifications
{
    public $success = 'success';
    public $info = 'info';
    public $warning = 'warning';
    public $error = 'error';

	private $notyName = 'notifications';
	private $notification = ['status','message'];

	public function setNotifications($status, $message){
		if (!$message || trim($message) == '') return;
		$this->notification['status'] = $status;
		$this->notification['message'] = $message;
		Yii::$app->session->setFlash($this->notyName,$this->notification);
	}

	public function getNotifications(){
		if (!Yii::$app->session->hasFlash($this->notyName)) return false;
		return Yii::$app->session->getFlash($this->notyName);
	}

}
