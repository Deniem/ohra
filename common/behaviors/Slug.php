<?php

namespace common\behaviors;

use Yii;
use yii\db\ActiveRecord;
use yii\base\Behavior;
use yii\helpers\Inflector;
use Transliterator;

class Slug extends Behavior {

	public $target = 'name';
	public $slug = 'slug';

	public function events(){

		return [
			ActiveRecord::EVENT_BEFORE_VALIDATE => 'getSlug'
		];
	}

	public function getSlug(){
		if ( empty( $this->owner->{$this->slug} ) ) {
			$this->owner->{$this->slug} = $this->generateSlug( $this->owner->{$this->target} );
		} else {
			if (isset($this->owner->can_delete) && $this->owner->can_delete == 0) {
				$this->owner->{$this->slug};
			} else {
				$this->owner->{$this->slug} = $this->generateSlug( $this->owner->{$this->target} );
			}
		}
	}

	private function generateSlug( $slug )
	{
		$slug = $this->slugify( $slug );
		if ( $this->checkUniqueSlug( $slug ) ) {
			return $slug;
		} else {
			for ( $suffix = 2; !$this->checkUniqueSlug( $new_slug = $slug . '-' . $suffix ); $suffix++ ) {}
			return $new_slug;
		}
	}

	private function slugify( $slug )
	{
		return Yii::$app->helpers->url_slug($slug, ['transliterate' => true]);
	}


	private function checkUniqueSlug( $slug )
	{
		$pk = $this->owner->primaryKey();
		$pk = $pk[0];

		$condition = $this->slug . ' = :slug';
		$params = [ ':slug' => $slug ];
		if ( !$this->owner->isNewRecord ) {
			$condition .= ' and ' . $pk . ' != :pk';
			$params[':pk'] = $this->owner->{$pk};
		}

		return !$this->owner->find()
			->where( $condition, $params )
			->one();
	}



}