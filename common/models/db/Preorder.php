<?php

namespace common\models\db;


use common\models\db\base\PreorderBase;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

class Preorder extends PreorderBase
{

    public function rules()
    {
        return [
            [['email'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'city', 'address', 'notes'], 'string'],
            [ 'delivery', 'checkDelivery'],
            [ 'payment_method', 'checkPaymentMethod'],
            [['phone', 'email'], 'string', 'max' => 255],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ]
        ];
    }

    public function checkDelivery($attribute, $params){
        if(!in_array($this->$attribute, array_keys(OrderOptions::getDeliveryOptions()))){
            $this->addError($attribute, "Неподходещее значение доставки");
        }
    }

    public function checkPaymentMethod($attribute, $params){
        if(!in_array($this->$attribute, array_keys(OrderOptions::getPaymentOptions()))){
            $this->addError($attribute, "Неподходещее значение статуса");
        }
    }

}