<?php
namespace common\models\db\query;

use yii\db\ActiveQuery;

class CategoryQuery extends ActiveQuery
{
	public function root()
	{
		$this->andWhere('id_parent IS NULL');
		return $this;
	}
}
