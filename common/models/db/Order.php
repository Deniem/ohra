<?php
/**
 * Created by PhpStorm.
 * User: bbyte
 * Date: 6/13/16
 * Time: 5:33 PM
 */

namespace common\models\db;


use common\models\db\base\OrderBase;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

class Order extends OrderBase
{

    public static $STATUS_CREATED = 'created';
    public static $STATUS_VIEWED = 'viewed';
    public static $STATUS_VERIFIED = 'verified';
    public static $STATUS_PENDING = 'pending';
    public static $STATUS_IN_PROCESS = 'in_process';
    public static $STATUS_SENT = 'sent';
    public static $STATUS_REJECTED = 'rejected';
    public static $STATUS_COMPLETE = 'complete';


    public function rules()
    {
        return [
            [['email'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'city', 'address', 'notes'], 'string'],
            [ 'status', 'checkStatus'],
            [ 'delivery', 'checkDelivery'],
            [ 'payment_method', 'checkPaymentMethod'],
            [['phone', 'email'], 'string', 'max' => 255],
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
        return $fields;
    }

    public function calculateTotal(){
        $total = 0;
        foreach ($this->orderItems as $item){
            $total += $item->price * $item->quantity;
        }

        return $total;
    }

    public function behaviors()
    {
        return [
                [
                    'class' => TimestampBehavior::className(),
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                        ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                    ],
                    'value' => new Expression('NOW()'),
                ]
            ];
    }

    public function checkStatus($attribute, $params){
        if(!in_array($this->$attribute, array_keys(OrderOptions::getStatusOptions()))){
            $this->addError($attribute, "Неподходещее значение статуса");
        }
    }

    public function checkDelivery($attribute, $params){
        if(!in_array($this->$attribute, array_keys(OrderOptions::getDeliveryOptions()))){
            $this->addError($attribute, "Неподходещее значение доставки");
        }
    }

    public function checkPaymentMethod($attribute, $params){
        if(!in_array($this->$attribute, array_keys(OrderOptions::getPaymentOptions()))){
            $this->addError($attribute, "Неподходещее значение статуса");
        }
    }
}