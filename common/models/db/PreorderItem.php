<?php

namespace common\models\db;


use common\models\db\base\PreorderBase;
use common\models\db\base\PreorderItemBase;
use common\models\db\base\ProductBase;

class PreorderItem extends PreorderItemBase
{
    public function rules()
    {
        return [
            [['preorder_id', 'product_id', 'quantity', 'is_active'], 'integer'],
            [['price'], 'required'],
            [['price'], 'number'],
            [['name'], 'string', 'max' => 255],
            [['preorder_id'], 'exist', 'skipOnError' => true,
                'targetClass' => PreorderBase::className(), 'targetAttribute' => ['preorder_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 
                'targetClass' => ProductBase::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }
}