<?php
/**
 * Created by PhpStorm.
 * User: bitybite
 * Date: 6/15/16
 * Time: 12:09 AM
 */

namespace common\models\db;


class OrderOptions
{

    public static function getAllOptions(){
        return [
            'status' => self::getStatusOptions(),
            'statusLabels' => self::getStatusLabels(),
            'delivery' => self::getDeliveryOptions(),
            'payment_method' => self::getPaymentOptions()
        ];
    }

    public static function getStatusOptions(){
        return [
            'created' =>'Новый',
            'viewed' =>'Просмотренный',
            'verified' =>'Подтвержденный',
            'pending' =>'Проверяется',
            'in_process' =>'Комплектуется',
            'sent' =>'Отправленный',
            'rejected' =>'Отклоненный',
            'complete' =>'Завершенный'
        ];
    }

	public static function getStatusLabels(){
        return [
			'created'	=>'info',
			'viewed'	=>'default',
			'verified'	=>'primary',
			'pending'	=>'secondary',
			'in_process'=>'success',
			'sent'		=>'warning',
			'rejected'	=>'danger',
			'complete'	=>'success',
        ];
    }

    public static function getDeliveryOptions(){
        return [
            'courier' => 'Курьер',
            'pickup' => 'Самовывоз',
            'shipping_service' => 'Служба доставки'
        ];
    }

    public static function getPaymentOptions(){
        return [
            'cash_shipping_service' => 'Оплата через службу доставки',
            'cash_in office' => 'На месте',
            'c.o.d.' => 'Наложенный платеж'
        ];
    }

}






