<?php

namespace common\models\db;

use Yii;
use common\models\db\base\SettingsBase;

class Settings extends SettingsBase
{
	public static $TYPE_FRONTEND = 'frontend';
	public static $TYPE_BACKEND = 'backend';
	public static $TYPE_GLOBAL = 'global';

}
