<?php

namespace common\models\db;

use Yii;
use common\models\db\base\PropertyBase;
use yii\behaviors\SluggableBehavior;

class Property extends PropertyBase
{

	public function behaviors()
	{
		return [
			[
				'class' => SluggableBehavior::className(),
				'attribute' => 'name',
			],
		];
	}
	
	public function fields()
	{
		$fields = parent::fields();
		$fields['unit'] = 'units';
		$fields['values'] = 'values';
		return $fields;
	}
}
