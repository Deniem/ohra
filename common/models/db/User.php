<?php

namespace common\models\db;

use Yii;
use common\models\db\base\UserBase;

class User extends UserBase
{
	public static function getUserRoleName($id){
		$roles = [
			'1' => 'Пользователь',
			'5' => 'Менеджер',
			'10' => 'Администратор',
		];
		return $roles[$id];
	}
}
