<?php

namespace common\models\db;

use Yii;
use common\models\db\base\ProductPropertyBase;

class ProductProperty extends ProductPropertyBase
{
	public function fields()
	{
		$fields = parent::fields();
		$fields['value'] = 'idValue';
		return $fields;
	}
}
