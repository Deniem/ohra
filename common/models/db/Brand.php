<?php
/**
 * Created by PhpStorm.
 * User: bbyte
 * Date: 6/29/16
 * Time: 7:28 PM
 */

namespace common\models\db;


use common\models\db\base\BrandBase;
use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\SluggableBehavior;
use yii\helpers\Html;
use common\behaviors\Slug;

class Brand extends BrandBase
{
    public $thumbnail;

    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'thumbnail',
                'pathAttribute' => 'thumbnail_path',
                'baseUrlAttribute' => 'thumbnail_base_url'
            ],
			[
				'class' => Slug::className(),
				'target'=>'name',
				'slug'=>'slug',
			],
        ];
    }

    public function getPreview()
    {
        if ($this->thumbnail_path) {
            $src = Yii::$app->glide->createSignedUrl([
                'glide/index',
                'path' => $this->thumbnail_path,
                'w' => 120,
                'h' => 60,
                'fit' => 'crop'
            ], true);
            return Html::img($src);
        } else {
            return '';
        }
    }

    public function getPreviewSrc()
    {
        if ($this->thumbnail_path) {
            $src = Yii::$app->glide->createSignedUrl([
                'glide/index',
                'path' => $this->thumbnail_path,
                'w' => 120,
                'h' => 60,
                'fit' => 'fit'
            ], true);
            return $src;
        } else {
            return '';
        }
    }
}