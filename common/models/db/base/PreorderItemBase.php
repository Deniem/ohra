<?php
namespace common\models\db\base;


use common\models\db\Product;

class PreorderItemBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'preorder_items';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'preorder_id' => 'Preorder ID',
            'name' => 'Name',
            'price' => 'Price',
            'product_id' => 'Product ID',
            'quantity' => 'Quantity',
            'is_active' => 'Is Active'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreorder()
    {
        return $this->hasOne(PreorderBase::className(), ['id' => 'preorder_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(ProductBase::className(), ['id' => 'product_id']);
    }
}