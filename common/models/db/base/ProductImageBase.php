<?php

namespace common\models\db\base;

use Yii;
use common\models\db\Product;

/**
 * This is the model class for table "product_image".
 *
 * @property integer $id
 * @property integer $id_product
 * @property string $base_url
 * @property string $path
 *
 * @property Product $idProduct
 */
class ProductImageBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_image';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_product'], 'required'],
            [['id_product'], 'integer'],
            [['base_url', 'path'], 'string', 'max' => 1024],
            [['id_product'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['id_product' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_product' => 'Id Product',
            'base_url' => 'Base Url',
            'path' => 'Path',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'id_product']);
    }
}
