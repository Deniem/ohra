<?php

namespace common\models\db\base;

use Yii;

/**
 * This is the model class for table "settings".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $value
 * @property string $type
 * @property string $value_type
 * @property integer $required
 */
class SettingsBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description','label'], 'required'],
            [['description', 'type', 'value_type'], 'string'],
            [['required'], 'integer'],
            [['name', 'value'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'value' => 'Value',
            'type' => 'Type',
            'value_type' => 'Value Type',
            'required' => 'Required',
            'label' => 'Label',
        ];
    }
}
