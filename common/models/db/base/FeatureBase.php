<?php

namespace common\models\db\base;

use Yii;
use common\models\db\ProductFeature;

/**
 * This is the model class for table "feature".
 *
 * @property integer $id
 * @property string $name
 * @property string $status
 *
 * @property ProductFeature[] $productFeatures
 */
class FeatureBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'feature';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductFeatures()
    {
        return $this->hasMany(ProductFeature::className(), ['id_feature' => 'id']);
    }
}
