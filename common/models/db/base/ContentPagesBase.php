<?php

namespace common\models\db\base;

use Yii;

/**
 * This is the model class for table "content_pages".
 *
 * @property integer $id
 * @property string $h1
 * @property string $title
 * @property string $description
 * @property string $keywords
 * @property string $content
 * @property string $created_at
 * @property string $updated_at
 * @property integer $status
 * @property string $action
 */
class ContentPagesBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'content_pages';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['h1', 'action'], 'required'],
            [['h1', 'title', 'description', 'keywords', 'content'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['status', 'id_parent'], 'integer'],
            [['action'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'h1' => 'H1',
            'title' => 'Title',
            'description' => 'Description',
            'keywords' => 'Keywords',
            'content' => 'Content',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
            'action' => 'Action',
            'id_parent' => 'Parent Id'
        ];
    }
}
