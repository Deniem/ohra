<?php

namespace common\models\db\base;

use common\models\db\Property;
use common\models\db\Units;
use Yii;
use common\models\db\Category;
use common\models\db\Value;

/**
 * This is the model class for table "property".
 *
 * @property integer $id
 * @property string $name
 * @property integer $id_category
 *
 * @property Category $idCategory
 * @property Value[] $values
 */
class PropertyBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'property';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'id_category','id_unit', 'is_filter'], 'required'],
            [['id_category','id_unit', 'is_filter'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['id_category'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['id_category' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'id_category' => 'Id Category',
            'id_unit' => 'Id Unit',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'id_category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getValues()
    {
        return $this->hasMany(Value::className(), ['id_property' => 'id']);
    }

    public function getUnits()
    {
        return $this->hasOne(Units::className(), ['id' => 'id_unit']);
    }
}
