<?php

namespace common\models\db\base;


use common\models\db\Product;
use yii\db\ActiveRecord;

class DiscountBase extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'discount';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['prod_id', 'price', 'value', 'type'], 'required'],
            [['prod_id'], 'integer'],
            [['price', 'value'], 'number'],
            [['type', 'status'], 'string'],
            [['start_date', 'end_date'], 'safe'],
            [['prod_id'], 'exist', 'skipOnError' => true,
                'targetClass' => Product::className(),
                'targetAttribute' => ['prod_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'prod_id' => 'Prod ID',
            'price' => 'Price',
            'value' => 'Value',
            'type' => 'Type',
            'status' => 'Status',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProd()
    {
        return $this->hasOne(Product::className(), ['id' => 'prod_id']);
    }
}