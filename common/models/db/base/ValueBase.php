<?php

namespace common\models\db\base;

use Yii;
use common\models\db\ProductProperty;
use common\models\db\Property;

/**
 * This is the model class for table "value".
 *
 * @property integer $id
 * @property string $name
 * @property integer $id_property
 *
 * @property ProductProperty[] $productProperties
 * @property Property $idProperty
 */
class ValueBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'value';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'id_property'], 'required'],
            [['id_property'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['id_property'], 'exist', 'skipOnError' => true, 'targetClass' => Property::className(), 'targetAttribute' => ['id_property' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'id_property' => 'Id Property',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductProperties()
    {
        return $this->hasMany(ProductProperty::className(), ['id_value' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProperty()
    {
        return $this->hasOne(Property::className(), ['id' => 'id_property']);
    }
}
