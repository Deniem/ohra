<?php
/**
 * Created by PhpStorm.
 * User: bbyte
 * Date: 6/13/16
 * Time: 5:13 PM
 */

namespace common\models\db\base;
use common\models\db\OrderItem;
use common\models\db\Product;
use frontend\models\Customer;


/**
 * This is the model class for table "order_items".
 *
 * @property integer $id
 * @property integer $order_id
 * @property string $name
 * @property double $price
 * @property integer $product_id
 * @property integer $quantity
 *
 * @property OrderBase $order
 * @property Product $product
 */
class OrderBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'name' => 'Name',
            'delivery' => 'Delivery',
            'payment_method' => 'Payment Method',
            'phone' => 'Phone',
            'city' => 'City',
            'address' => 'Address',
            'email' => 'Email',
            'notes' => 'Notes',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItem::className(), ['order_id' => 'id']);
    }

    public function getCustomers(){
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }
}