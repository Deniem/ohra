<?php

namespace common\models\db\base;

use Yii;
use common\models\db\Feature;
use common\models\db\Product;

/**
 * This is the model class for table "product_feature".
 *
 * @property integer $id
 * @property integer $id_product
 * @property integer $id_feature
 *
 * @property Feature $idFeature
 * @property Product $idProduct
 */
class ProductFeatureBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_feature';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_product', 'id_feature'], 'required'],
            [['id_product', 'id_feature'], 'integer'],
            [['id_feature'], 'exist', 'skipOnError' => true, 'targetClass' => Feature::className(), 'targetAttribute' => ['id_feature' => 'id']],
            [['id_product'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['id_product' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_product' => 'Id Product',
            'id_feature' => 'Id Feature',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdFeature()
    {
        return $this->hasOne(Feature::className(), ['id' => 'id_feature']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'id_product']);
    }
}
