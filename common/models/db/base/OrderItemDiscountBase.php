<?php

namespace common\models\db\base;

class OrderItemDiscountBase extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'order_items_discount';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_item_id', 'price', 'value', 'type'], 'required'],
            [['order_item_id'], 'integer'],
            [['price', 'value'], 'number'],
            [['type', 'status'], 'string'],
            [['start_date', 'end_date'], 'safe'],
            [['order_item_id'], 'exist', 'skipOnError' => true,
                'targetClass' => OrderItemBase::className(),
                'targetAttribute' => ['order_item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_item_id' => 'Order Item ID',
            'price' => 'Price',
            'value' => 'Value',
            'type' => 'Type',
            'status' => 'Status',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItem()
    {
        return $this->hasOne(OrderItemBase::className(), ['id' => 'order_item_id']);
    }
}