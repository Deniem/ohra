<?php
/**
 * Created by PhpStorm.
 * User: bbyte
 * Date: 6/29/16
 * Time: 7:26 PM
 */

namespace common\models\db\base;


use common\models\db\Product;
use Yii;

/**
 * This is the model class for table "brand".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $thumbnail_base_url
 * @property string $thumbnail_path
 * @property string $h1
 * @property string $description
 *
 * @property Product[] $products
 */
class BrandBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brand';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description',], 'string'],
            ['content', 'safe'],
            [['is_top_brand'], 'integer'],
            [['name', 'slug', 'h1'], 'string', 'max' => 255],
            [['thumbnail_base_url', 'thumbnail_path'], 'string', 'max' => 1024],
            ['thumbnail', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'slug' => 'Slug',
            'thumbnail_base_url' => 'Thumbnail Base Url',
            'thumbnail_path' => 'Thumbnail Path',
            'h1' => 'H1',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['brand_id' => 'id']);
    }
}