<?php

namespace common\models\db\base;

use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "blog".
 *
 * @property integer $id
 * @property integer $id_parent
 * @property string $h1
 * @property string $title
 * @property string $keywords
 * @property string $description
 * @property string $content
 * @property string $created_at
 * @property string $updated_at
 * @property string $published_at
 * @property string $status
 * @property string $action
 * @property string $thumbnail_base_url
 * @property string $thumbnail_path
 */

class BlogBase extends ActiveRecord
{

    public static $STATUS_ACTIVE = 'active';
    public static $STATUS_INACTIVE = 'inactive';
    public static $STATUS_DRAFT = 'draft';
    public static $STATUS_ARCHIVE = 'archive';
    
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blog';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['h1', 'title', 'keywords', 'description', 'content', 'status', 'action'], 'string'],
            [['created_at', 'updated_at', 'published_at'], 'safe'],
            [ 'status', 'checkStatus'],
            ['title', 'required'],
            [['thumbnail_base_url', 'thumbnail_path'], 'string', 'max' => 1024],
            ['thumbnail', 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_parent' => 'Id Parent',
            'h1' => 'H1',
            'title' => 'Title',
            'keywords' => 'Keywords',
            'description' => 'Description',
            'content' => 'Content',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'published_at' => 'Published At',
            'status' => 'Status',
            'action' => 'Action',
            'thumbnail_base_url' => 'Thumbnail Base Url',
            'thumbnail_path' => 'Thumbnail Path',
        ];
    }

    public function checkStatus($attribute, $params){
        if(!in_array($this->$attribute, array_keys($this->getStatusLabel()))){
            $this->addError($attribute, "Неподходещее значение статуса");
        }
    }

    public function getStatusLabel(){
        return [
            self::$STATUS_ACTIVE => 'активная',
            self::$STATUS_INACTIVE => 'неактивная',
            self::$STATUS_DRAFT => 'черновик',
            self::$STATUS_ARCHIVE => 'архивная'
        ];
    }
}