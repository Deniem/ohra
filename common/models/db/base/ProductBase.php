<?php

namespace common\models\db\base;

use common\models\db\CustomerCompare;
use common\models\db\CustomerFavorite;
use common\models\db\Discount;
use common\models\db\OrderItem;
use common\models\db\PreorderItem;
use common\models\db\Value;
use Yii;
use common\models\db\Brand;
use common\models\db\Category;
use common\models\db\ProductFeature;
use common\models\db\ProductImage;
use common\models\db\ProductProperty;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $name
 * @property string $price
 * @property integer $id_category
 * @property string $status
 *
 * @property Category $idCategory
 * @property ProductFeature[] $productFeatures
 * @property ProductImage[] $productImages
 * @property ProductProperty[] $productProperties
 */
class ProductBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'price', 'id_category', 'article'], 'required'],
            [['price', 'price_usd', 'price_eur'], 'number'],
            [['id_category', 'brand_id', 'is_action', 'is_sale', 'is_new', 'top_sale', 'count_left'], 'integer'],
            [['status', 'description'], 'string'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['thumbnail_base_url', 'thumbnail_path'], 'string', 'max' => 1024],
            [['article'], 'string', 'max' => 30],
            [['article'], 'unique'],
            [['brand_id'], 'exist', 'skipOnError' => true, 'targetClass' => Brand::className(), 'targetAttribute' => ['brand_id' => 'id']],
            [['id_category'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['id_category' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'price' => 'Price',
            'id_category' => 'Id Category',
            'brand_id' => 'Id Brand',
            'status' => 'Status',
            'is_action' => 'Акционный',
            'is_sale' => 'Распродажа',
            'is_new' => 'Новинка'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBrand()
    {
        return $this->hasOne(Brand::className(), ['id' => 'brand_id']);
    }

	/**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Category::className(), ['id' => 'id_category']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductFeatures()
    {
        return $this->hasMany(ProductFeature::className(), ['id_product' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImages()
    {
        return $this->hasMany(ProductImage::className(), ['id_product' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductProperties()
    {
        return $this->hasMany(ProductProperty::className(), ['id_product' => 'id']);
    }

    public function getProductValues(){
        return $this->hasMany(Value::className(), ['id' => 'id_value'])
            ->viaTable('product_property',['id_product' => 'id'] );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerCompares()
    {
        return $this->hasMany(CustomerCompare::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerFavorites()
    {
        return $this->hasMany(CustomerFavorite::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderItems()
    {
        return $this->hasMany(OrderItem::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreorderItems()
    {
        return $this->hasMany(PreorderItem::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscount()
    {
        return $this->hasOne(Discount::className(), ['prod_id' => 'id']);
    }

}
