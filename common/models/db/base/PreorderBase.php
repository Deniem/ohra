<?php
namespace common\models\db\base;


use frontend\models\Customer;
use Yii;

/**
 * This is the model class for table "preorder".
 *
 * @property integer $id
 * @property string $created_at
 * @property string $updated_at
 * @property string $name
 * @property string $delivery
 * @property string $payment_method
 * @property string $phone
 * @property string $city
 * @property string $address
 * @property string $email
 * @property string $notes
 * @property integer $customer_id
 * @property integer $is_active
 *
 * @property Customer $customer
 * @property PreorderItemBase[] $preorderItems
 */
class PreorderBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'preorder';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'name' => 'Name',
            'delivery' => 'Delivery',
            'payment_method' => 'Payment Method',
            'phone' => 'Phone',
            'city' => 'City',
            'address' => 'Address',
            'email' => 'Email',
            'notes' => 'Notes',
            'customer_id' => 'Customer ID',
            'is_active' => 'Is Active',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customer::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPreorderItems()
    {
        return $this->hasMany(PreorderItemBase::className(), ['preorder_id' => 'id']);
    }
}