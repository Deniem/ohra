<?php

namespace common\models\db\base;

use Yii;
use common\models\db\Value;
use common\models\db\Product;

/**
 * This is the model class for table "product_property".
 *
 * @property integer $id
 * @property integer $id_product
 * @property integer $id_value
 *
 * @property Value $idValue
 * @property Product $idProduct
 */
class ProductPropertyBase extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_property';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_product', 'id_value'], 'required'],
            [['id_product', 'id_value'], 'integer'],
            [['id_value'], 'exist', 'skipOnError' => true, 'targetClass' => Value::className(), 'targetAttribute' => ['id_value' => 'id']],
            [['id_product'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['id_product' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_product' => 'Id Product',
            'id_value' => 'Id Value',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdValue()
    {
        return $this->hasOne(Value::className(), ['id' => 'id_value']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'id_product']);
    }
}
