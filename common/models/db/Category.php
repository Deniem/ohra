<?php

namespace common\models\db;

use Yii;
use common\models\db\base\CategoryBase;
use common\models\db\query\CategoryQuery;
use trntv\filekit\behaviors\UploadBehavior;
use yii\behaviors\SluggableBehavior;
use yii\helpers\Html;
use common\behaviors\Slug;

class Category extends CategoryBase
{
	public $thumbnail;
	public static $STATUS_ACTIVE = 'active';
	public static $STATUS_DELETED = 'deleted';

	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			[
				'class' => UploadBehavior::className(),
				'attribute' => 'thumbnail',
				'pathAttribute' => 'thumbnail_path',
				'baseUrlAttribute' => 'thumbnail_base_url'
			],
			[
				'class' => Slug::className(),
				'target'=>'name',
				'slug'=>'slug',
			],
		];
	}

	/**
	 * @return CategoryQuery
	 */
	public static function find()
	{
		return new CategoryQuery(get_called_class());
	}

	/**
	 * Adds calculated properties to the list of fields
	 *
	 * @return  array  A list of fields for serialization
	 */
	public function fields()
	{
		$fields = parent::fields();
		$fields['parent'] = 'parent';
		$fields['properties'] = 'properties';
		return $fields;
	}

	public function getPreview()
	{
		if ($this->thumbnail_path) {
			$src = Yii::$app->glide->createSignedUrl([
				'glide/index',
				'path' => $this->thumbnail_path,
				'w' => 120,
				'h' => 60,
				'fit' => 'crop'
			], true);
			return Html::img($src);
		} else {
			return '';
		}
	}

	public function getPreviewCatalog()
	{
		if ($this->thumbnail_path) {
			$src = Yii::$app->glide->createSignedUrl([
				'glide/index',
				'path' => $this->thumbnail_path,
				'w' => 270,
				'h' => 278,
				'fit' => 'crop'
			], true);
			return Html::img($src);
		} else {
			return '';
		}
	}

	public function getPreviewFill()
	{
		if ($this->thumbnail_path) {
			$src = Yii::$app->glide->createSignedUrl([
				'glide/index',
				'path' => $this->thumbnail_path,
				'w' => 120,
				'h' => 60,
				'fit' => 'fill'
			], true);
			return Html::img($src);
		} else {
			return '';
		}
	}
	
	public function getPreviewSrc(){
		if ($this->thumbnail_path) {
			$src = Yii::$app->glide->createSignedUrl([
				'glide/index',
				'path' => $this->thumbnail_path,
				'w' => 540,
				'h' => 280,
				'fit' => 'stretch'
			], true);
			return $src;
		} else {
			return '';
		}
	}

	public function getParentName() {
		if ($this->parent) {
			return $this->parent->name;
		} else {
			return 'Корневая';
		}
	}

	public function hasChilds(){
		if (count($this::findAll(['id_parent'=>$this->id])) > 0) {
			return true;
		}
		return false;
	}

	public static function getStatusLabels(){
		return [
			self::$STATUS_ACTIVE => 'активная',
			self::$STATUS_DELETED => 'удаленная',
		];
	}

}
