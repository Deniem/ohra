<?php

namespace common\models\db;


use common\behaviors\Slug;
use common\models\db\base\BlogBase;
use trntv\filekit\behaviors\UploadBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

class Blog extends BlogBase
{
    public $thumbnail;

    public function behaviors()
    {
        return [
            [
                'class' => UploadBehavior::className(),
                'attribute' => 'thumbnail',
                'pathAttribute' => 'thumbnail_path',
                'baseUrlAttribute' => 'thumbnail_base_url'
            ],
            [
                'class' => Slug::className(),
                'target'=>'title',
                'slug'=>'action',
            ],
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    public function getPreviewSrc(){
        if ($this->thumbnail_path) {
            $src = Yii::$app->glide->createSignedUrl([
                'glide/index',
                'path' => $this->thumbnail_path,
                'w' => 540,
                'h' => 280,
                'fit' => 'stretch'
            ], true);
            return $src;
        } else {
            return '';
        }
    }
}