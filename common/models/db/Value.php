<?php

namespace common\models\db;

use Yii;
use common\models\db\base\ValueBase;
use yii\behaviors\SluggableBehavior;
use common\behaviors\Slug;

class Value extends ValueBase
{

    public function behaviors()
    {
        return [
			[
				'class' => Slug::className(),
				'target'=>'name',
				'slug'=>'slug',
			],
        ];
    }

}
