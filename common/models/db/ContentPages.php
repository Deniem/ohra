<?php

namespace common\models\db;

use Yii;
use common\models\db\base\ContentPagesBase;
use yii\behaviors\SluggableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use common\behaviors\Slug;

class ContentPages extends ContentPagesBase
{

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
			[
				'class' => Slug::className(),
				'target'=>'h1',
				'slug'=>'action',
			],
        ];
    }

    public function fields()
    {
        $fields = parent::fields();
        return $fields;
    }

}
