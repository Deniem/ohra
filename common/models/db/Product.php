<?php

namespace common\models\db;

use DateTime;
use Yii;
use common\models\db\base\ProductBase;
use trntv\filekit\behaviors\UploadBehavior;
use yii\behaviors\SluggableBehavior;
use yii\helpers\Html;
use yz\shoppingcart\CartPositionInterface;
use yz\shoppingcart\CartPositionTrait;
use common\behaviors\Slug;

class Product extends ProductBase implements CartPositionInterface
{
	use CartPositionTrait;

	public static $STATUS_ACTIVE = 'active';
	public static $STATUS_NOT_ACTIVE = 'not active';
	public static $STATUS_CONDITIONALY_ACTIVE = 'conditionally active';
	public static $STATUS_DELETED = 'deleted';


	public $thumbnail;
	public $photos;
	/**
	 * @inheritdoc
	 */
	public function behaviors()
	{
		return [
			[
				'class' => UploadBehavior::className(),
				'multiple' => true,
				'attribute' => 'photos',
				'uploadRelation' => 'images',
				'pathAttribute' => 'path',
				'baseUrlAttribute' => 'base_url'
			],
			[
				'class' => UploadBehavior::className(),
				'attribute' => 'thumbnail',
				'pathAttribute' => 'thumbnail_path',
				'baseUrlAttribute' => 'thumbnail_base_url'
			],
			[
				'class' => Slug::className(),
				'target'=>'name',
				'slug'=>'slug',
			],
		];
	}

	/**
	 * Adds calculated properties to the list of fields
	 *
	 * @return  array  A list of fields for serialization
	 */
	public function fields()
	{
		$fields = parent::fields();
		$fields['category'] = 'category';
		$fields['images'] = 'images';
		$fields['quantity'] = 'quantity';
		$fields['cost'] = 'cost';
		$fields['discount'] = 'discount';
		$fields['discountActive'] = 'discountActive';
		return $fields;
	}

	public function getPreview()
	{
		if ($this->thumbnail_path) {
			$src = Yii::$app->glide->createSignedUrl([
				'glide/index',
				'path' => $this->thumbnail_path,
				'w' => 200,
				'h' => 100,
				'fit' => 'crop'
			], true);
			return Html::img($src);
		} else {
			return '';
		}
	}

	public function getPrice()
	{
		return $this->price;
	}

	public function getId()
	{
		return $this->id;
	}

	public function getDiscountActive(){
		$isActive = false;
		if($this->discount != null){
			if($this->discount->status == 'enabled'){
				$format = 'Y-m-d H:i:s';
				$currentDate = new DateTime();

				if($this->discount->start_date == null &&
					$this->discount->end_date == null){
					$isActive = true;
				}else if($this->discount->start_date != null &&
					$this->discount->end_date == null){
					$starDate = DateTime::createFromFormat($format, $this->discount->start_date);
						if($currentDate > $starDate){
							$isActive = true;
						}
				}else if($this->discount->start_date == null &&
					$this->discount->end_date != null){
						$endDate = DateTime::createFromFormat($format, $this->discount->end_date);
						if($currentDate < $endDate){
							$isActive = true;
						}
				}else if($this->discount->start_date != null &&
					$this->discount->end_date != null){
					$starDate = DateTime::createFromFormat($format, $this->discount->start_date);
					$endDate = DateTime::createFromFormat($format, $this->discount->end_date);
					if($starDate < $currentDate && $currentDate < $endDate){
						$isActive = true;
					}
				}
			}
		}

		return $isActive;
	}

	public static function getStatusLabels(){
		return [
			self::$STATUS_NOT_ACTIVE => 'default',
			self::$STATUS_DELETED => 'secondary',
			self::$STATUS_ACTIVE => 'success',
			self::$STATUS_CONDITIONALY_ACTIVE => 'info',
		];
	}

	public static function getStatusNames(){
		return [
			self::$STATUS_NOT_ACTIVE => 'не активный',
			self::$STATUS_DELETED => 'удаленный',
			self::$STATUS_ACTIVE => 'активный',
			self::$STATUS_CONDITIONALY_ACTIVE => 'условно активный',
		];
	}

	public static function getStatusOptions(){
		return [
			'labels' => self::getStatusLabels(),
			'names' => self::getStatusNames(),
			'statuses' => [
				self::$STATUS_ACTIVE,
				self::$STATUS_CONDITIONALY_ACTIVE,
				self::$STATUS_NOT_ACTIVE,
				self::$STATUS_DELETED,
			]
		];
	}

	public static function getAllForSale(){
		return self::findAll(['is_sale'=>1, 'status' => self::$STATUS_ACTIVE,]);
	}

	public static function getAllForAction(){
		return self::findAll(['is_action'=>1, 'status' => self::$STATUS_ACTIVE,]);
	}

	public static function getAllForNew(){
			return self::findAll(['is_new'=>1, 'status' => self::$STATUS_ACTIVE,]);
	}

}
