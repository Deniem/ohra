<?php
return [
    'class' => 'yii\swiftmailer\Mailer',
    'useFileTransport' => 'true',
    'transport' => [
        'class' => 'Swift_SmtpTransport',
        'host' => 'localhost',
        'username' => 'username',
        'password' => 'password',
        'port' => '587',
        'encryption' => 'tls',
    ],
];