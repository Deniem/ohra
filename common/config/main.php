<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class' => 'yii\rbac\PhpManager',
            'defaultRoles' => ['user','manager','admin'],
            'itemFile' => '@common/components/rbac/items.php',
            'assignmentFile' => '@common/components/rbac/assignments.php',
            'ruleFile' => '@common/components/rbac/rules.php'
        ],
        'mailer' => require ('mailer.php'),
	    'fileStorage' => [
		    'class' => '\trntv\filekit\Storage',
		    'baseUrl' => '@storageUrl/source',
		    'filesystem' => [
			    'class' => 'common\components\filesystem\LocalFlysystemBuilder',
			    'path' => '@storage/web/source'
		    ],
		    'as log' => [
			    'class' => 'common\behaviors\FileStorageLogBehavior',
			    'component' => 'fileStorage'
		    ]
	    ],
		'helpers' => [
			'class' => '\common\components\Helpers',
		],
		'settings' => [
			'class' => '\common\components\Settings',
		],
	    'notifications' => [
			'class' => '\common\components\Notifications',
		],
	    'glide' => [
		    'class' => 'trntv\glide\components\Glide',
		    'sourcePath' => '@storage/web/source',
		    'cachePath' => '@storage/cache',
		    'urlManager' => 'urlManagerStorage',
		    'maxImageSize' => getenv('GLIDE_MAX_IMAGE_SIZE'),
		    'signKey' => getenv('GLIDE_SIGN_KEY')
	    ],
	    'urlManagerStorage' => \yii\helpers\ArrayHelper::merge(
		    [
			    'hostInfo'=>Yii::getAlias('@storageUrl')
		    ],
		    require(Yii::getAlias('@storage/config/_urlManager.php'))
	    ),
	    'cart' => [
		    'class' => 'yz\shoppingcart\ShoppingCart',
	    ],
    ],
];
